import React, { Component } from 'react';
import T from "i18n-react";

class MakeInfo extends Component {

  render() {

    return(
      <div className="make-info">
        <div className="tab">
          <input type="radio"
            id="tab-1"
            name="tab-group-1"
            checked={this.props.tabNo === 2.1}
            value={2.1}
            onChange={() => this.props.tabChanged('direct', 2.1)}/>
          <label htmlFor="tab-1" className="tab-label">{T.translate('make.divide-section')}</label>
        </div>
        <div className="tab">
           <input type="radio"
             id="tab-2"
             name="tab-group-1"
             value={2.2}
             checked={this.props.tabNo === 2.2}
             onChange={() => this.props.tabChanged('direct', 2.2)}/>
           <label htmlFor="tab-2" className="tab-label">{T.translate('make.add-knowledge')}</label>
        </div>
        <div className="tab">
          <input type="radio"
            id="tab-3"
            name="tab-group-1"
            value={2.3}
            checked={this.props.tabNo === 2.3}
            onChange={() => this.props.tabChanged('direct', 2.3)}/>
          <label htmlFor="tab-3" className="tab-label">{T.translate('make.add-event')}</label>
        </div>
      </div>
    );
  }
}

export default MakeInfo;
