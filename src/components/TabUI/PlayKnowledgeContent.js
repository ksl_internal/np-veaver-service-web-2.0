import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import Section from './Section';
import { onAddVoteExample, onVote,onVoteReload, stackMoved } from '../../modules/player';
import { axios } from '../../util/api';

class PlayKnowledgeContent extends Component {

 state  = {
   scrolling : false,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.needMoveStack) {
      this.props.stackMoved();
      this._moveKnowledgeScroll();

        this.setState(state => ({
            ...state,
            scrolling : false,
        }));
    } else {
        this._detectKnowledgeScroll();
    }
  }
  _detectKnowledgeScroll() {
      const { currentTime, timelineData } = Object.assign({}, this.props);
      let currentSectionIdx;
      let currentStackIdx = 0;
      let currentStackBegin
      let scrollHeight = 0;

      for(var stackCnt = 0 ; stackCnt < timelineData.stacks.length ; stackCnt++ ) {
          const stack = timelineData.stacks[stackCnt];

          if(stack.begin <= currentTime && currentTime < stack.end) {
              currentStackIdx = stack.stackIdx;
              currentStackBegin = stack.begin;

              for(var sectionCnt = 0 ; sectionCnt < timelineData.sections.length ; sectionCnt++ ) {
                  const section = timelineData.sections[sectionCnt];

                  if(section.begin <= currentStackBegin && currentStackBegin < section.end) {
                      currentSectionIdx = section.sectionIdx;
                      break;
                  }
              }
              break
          }
      }

      if(currentStackIdx && currentSectionIdx && !this.state.scrolling) {
          const $sections = document.querySelectorAll('[data-sectionidx]');
          for (var sectionCnt = 0; sectionCnt < $sections.length; sectionCnt++) {
              const section = $sections[sectionCnt];
              if (currentSectionIdx === Number(section.getAttribute('data-sectionidx'))) {
                  const sectionStyle = getComputedStyle(section);
                  const sectionInfo = section.getElementsByClassName('section-info')[0];
                  const secTionStyle = getComputedStyle(sectionInfo);
                  scrollHeight += (sectionInfo.scrollHeight+ parseInt(sectionStyle.paddingTop) + parseInt(secTionStyle.marginBottom));

                  const $stacks = section.querySelectorAll('[data-stackidx]');

                  for(var k = 0 ; k < $stacks.length ; k++) {
                      const stack = $stacks[k];

                      if(currentStackIdx === Number(stack.getAttribute('data-stackidx'))) {
                          this.konwledgeScroll.scrollTop(scrollHeight);
                          this.setState(state => ({
                              ...state,
                              scrolling : true,
                          }));
                          break;
                      }
                      scrollHeight += stack.scrollHeight;
                  }
              }

              scrollHeight += section.scrollHeight;
          }
      } else {
          if(!currentSectionIdx ) {
              this.setState(state => ({
                  ...state,
                  scrolling : false,
          }));
          }
      }
  }
  _moveKnowledgeScroll() {
    const { currentTime, timelineData } = Object.assign({}, this.props);
    let currentSectionIdx;
    let scrollHeight = 0;
    for (var i = 0; i < timelineData.sections.length; i++) {
      const section = timelineData.sections[i];
      if (section.begin <= currentTime && currentTime < section.end) {
          currentSectionIdx = section.sectionIdx;
          break;
        } else if (section.begin > currentTime) {
          currentSectionIdx = section[i - 1].sectionIdx;
          break;
        }
    }

    if (currentSectionIdx) {
      const $sections = document.querySelectorAll('[data-sectionidx]');
      for (var j = 0; j < $sections.length; j++) {
        const section = $sections[j];
        if (currentSectionIdx === Number(section.getAttribute('data-sectionidx'))) {
          this.konwledgeScroll.scrollTop(scrollHeight);
          break;
        }
        scrollHeight += section.scrollHeight;
      }
    }
  }

  get _calculateDummyHeight() {
     let dummyHeight = 0+'px'

      const $contents = document.querySelectorAll('.content');
     if($contents.length) {
         const content = $contents[0]
         let contentHeight = content.scrollHeight
         const $sections = content.querySelectorAll('[data-sectionidx]');

         if($sections.length) {
             dummyHeight = (contentHeight - $sections[$sections.length -1].scrollHeight -1) + 'px'
         }
     }
    return dummyHeight
  }

  // 퀴즈 답변하기
    _onAnswer(e) {
      //  console.log("<<<<<<<<<<<<<<<<<<_onAnswer");
    let $target = e.currentTarget;
    let cardIdx, cardType, stackIdx, sectionIdx, answerId, answer;
    let $btn, $answer, $input;

    let answerFlag = 'N';
    $btn = $target;

    do {
      if ($target.className.indexOf('card type-quiz') !== -1) {
        cardIdx = Number($target.getAttribute('data-cardidx'));
        cardType = $target.getAttribute('data-type');
        switch(cardType) {
          case 'MULTIPLE':
            if ($target.querySelectorAll(`input[name=quiz-${cardIdx}]:checked`).length === 0) return;
            $input = $target.querySelectorAll('.answers');
            answerId = $target.querySelectorAll(`input[name=quiz-${cardIdx}]:checked`)[0].value;
          break;
          case 'SHORT':
            $input = $target.querySelectorAll(`input[name=quiz-${cardIdx}]`);
            answer = $input[0].value;
            if (answer === '') return;
          break;
          default: return null;
        }
        $answer = $target.querySelector('a');
      } else if ($target.className.indexOf('card-wrapper') !== -1) {
        stackIdx = Number($target.getAttribute('data-stackidx'));
      } else if ($target.className.indexOf('section-wrapper') !== -1) {
        sectionIdx = Number($target.getAttribute('data-sectionidx'));
        break;
      }
      $target = $target.parentElement;
    } while ($target !== null);

    // 정답 찾기
    const { sections } = Object.assign({}, this.props.timelineData);
    const section = sections.filter(section => {
      return section.sectionIdx === sectionIdx;
    });
    const stack = section[0].stacks.filter(stack => {
      return stack.stackIdx === stackIdx;
    });
    const card = stack[0].cards.filter(card => {
      return card.cardIdx === cardIdx;
    });

    let correctAnswer;
    let data;
    switch (cardType) {
      case 'MULTIPLE':
        const correctAnswerContent = card[0].contents.answers.filter(answer => {
          return answer.answerFlag === 'Y';
        });
        correctAnswer = correctAnswerContent[0].answerId;
        if (correctAnswer === answerId) answerFlag = 'Y';
        data = { answerId, answerFlag };
      break;
      case 'SHORT':
        correctAnswer = card[0].contents.answer.example;
        if (correctAnswer.trim() === answer.trim()) answerFlag = 'Y';
        data = { answer, answerFlag };
      break;
      default: return null;
    }

    axios.post(`/timelines/quiz-answers/${cardIdx}`, data)
    .then(result => {
        $answer.classList.remove('dimmed');
        $input.forEach(input => input.classList.add('dimmed'));
        $btn.classList.add('dimmed');
    });
  }

  // 투표하기
  _onVote(e) {
    // 가장 가까운 부모 찾기 (IE)
    let $target = e.currentTarget;
    const $btn = $target;
    do {
      if ($target.className.indexOf('card type-vote') !== -1) break;
      $target = $target.parentElement;
    } while ($target !== null);

    const cardIdx = $target.getAttribute('data-cardidx');
    const multipleFlag = $target.getAttribute('data-multiple');

    let answerId;
    const $checked = $target.querySelectorAll(`input[name=vote-${cardIdx}]:checked`);

    if ($checked.length === 0) return;
    switch (multipleFlag) {
      case 'Y':
        let answerArray = [];
        $checked.forEach(check => {
          answerArray.push(check.id);
        });
        answerId = answerArray.join();
      break;
      case 'N':
        answerId = $checked[0].id;
      break;
      default: return;
    }

    axios.post(`/timelines/vote-answers/${cardIdx}`, {
      answerId
    })
    .then(result => {
        $btn.classList.add('dimmed');
        $target.querySelector('a').classList.remove('dimmed');
        $target.querySelectorAll('.examples').forEach(example => example.classList.add('dimmed'));
    });
  }

  // 투표 새 항목 추가
  _addVoteExample(e) {
    const example = e.currentTarget.previousSibling.value;
    if (example.trim() === '' || !example) return;

    // 가장 가까운 부모 찾기 (IE)
    let $target = e.currentTarget;
    do {
      if ($target.className.indexOf('card type-vote') !== -1) break;
      $target = $target.parentElement;
    } while ($target !== null);

    const cardIdx = $target.getAttribute('data-cardidx');
    const orderValue = $target.querySelectorAll(`input[name=vote-${cardIdx}]`).length + 1;

    axios.post(`/timelines/vote-answers/${cardIdx}/example`, {
      example,
      orderValue,
    })
    .then(result => {
        this.props.onAddVoteExample(true);
        $target.querySelector('input[type=text]').value = '';
        this.props.onVoteReload();
    });
  }

    renderSections() {
       // console.log("renderSection");
    const { sections, timelineIdx } = Object.assign({}, this.props.timelineData);
    const isOwner = this.props.userProfile.userKey === this.props.timelineData.user.userKey;
    return (<div>
    {
      sections.map((section, i) => (
        <Section key={`section-${section.sectionIdx}`}
          timelineIdx = {timelineIdx}
          section={section}
          index={i}
          isOwner={isOwner}
          onAnswer={this._onAnswer.bind(this)}
          onVote={this.props.onVote}
          onAddExample={this._addVoteExample.bind(this)}
        />
      ))
    }
    <div className="dummy" style={{height: this._calculateDummyHeight}}>

      </div>
    </div>);
  }

  render() {
    return (
      <div className="content">
        <Scrollbars
          style={{ width: '100%', height: '100%', position: 'absolute' }}
          renderThumbVertical={() => <div className='scroll-bar'/>}
          ref={ref => { this.konwledgeScroll = ref; }}
        >
            {/* 섹션 Wrapper */}
            {this.props.timelineData && this.renderSections()}
        </Scrollbars>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  needMoveStack: state.player.needMoveStack,
  currentTime: state.player.currentTime,
  userProfile: state.user.userProfile,
  addVoteEvent: state.player.addVoteEvent,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onAddVoteExample,
  onVote,
  onVoteReload,
  stackMoved,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PlayKnowledgeContent);
