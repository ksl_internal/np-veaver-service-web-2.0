import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import cx from 'classnames';
import ReactionComment from '../CardForm/ReactionComment';
import {
  likeReaction,
  commentReaction,
  refreshLikeCount,
  refreshCommentCount,
  activeCommentIcon,
  stackMoved,
  findTabScrollPositionFlag,
} from '../../modules/player';
import * as ReactionCreators from '../../modules/reaction';
import { openPopup } from '../../modules/popup';
import { convDuration } from '../../util/time';
import T from "i18n-react";
/**
 * 
 * 작성해야 하는 단위 테스트
 * 1. 본인 반응을 삭제 할 수 있다.
 * 2. 본인 반응을 추가 할 수 있다.
 * 3. 댓글을 달수 있다.
 * 4. 답글을 달수 있다.
 * 5. 본인 댓글을 수정 할 수 있다.
 * 6. 본인 답글을 수정 할 수 있다.
 * 7. 신고를 할 수 있다.
 * 8. 반응순 정렬을 할 수 있다.
 * 9. 시간순 정렬을 할 수 있다.
 * 10. 댓글을 필터링 할 수 있다.
 * 11. 반응을 필터링 할 수 있다.
 * 12. 답글 또는 수정을 취소 할 수 있다.
 * 13. 답글 더보기를 할 수 있다.
 * 14. 상황에 따라 입력창을 제어 할 수 있다.(포커싱, 기본값)
 * @class PlayReactionContent
 * @extends {Component}
 */
const commentInitState = {
  type: 'CREATE', // CREATE: 일반, UPDATE: 수정
  reactionId: undefined,
  nested: false,
  placeholder: '댓글',
};

export class PlayReactionContent extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      comment: commentInitState,
      scrolling: false,
      clickedReactionId: '',
    };
    
    this.commentArea = null; // comment refs
  }

  componentDidMount() {
    const {
       playTime,
       videoIdx,
       setVirtualTimeline,
       getReactions,
    } = this.props;
    setVirtualTimeline(videoIdx, playTime);
    getReactions(videoIdx);
  }

  // 코멘트 상태 업데이트
  _commentAreaUpdate(commentState, comment = '') {
      this.setState(state => ({...state,
         comment: {
        ...state.comment,
        ...commentState,
        },
        clickedReactionId: '',
      }));
    this.commentArea.value = comment;
    this.commentArea.focus();
  }
  

  initCommentState = (comment) => 
   ((commentState = {...commentInitState}) => this._commentAreaUpdate(commentState, comment))()

  createNestedCommentState = (reactionId, comment) =>
   ((commentState = { type: 'CREATE', nested: true, reactionId, placeholder: '답글' }) => this._commentAreaUpdate(commentState, comment))()

  updateCommentState = (reactionId, comment) =>
   ((commentState = { type: 'UPDATE', nested: false, reactionId, placeholder: '댓글' }) => this._commentAreaUpdate(commentState, comment))()

  updateNestedCommentState = (reactionId, comment) =>
   ((commentState = { type: 'UPDATE', nested: true, reactionId, placeholder: '답글' }) => this._commentAreaUpdate(commentState, comment))()

  // 댓글 달기
  _applyComment = (e) => {
    const comment = this.commentArea.value;
    const { changeComment, getReactions, videoIdx } = this.props;
    const { type, nested, reactionId } = this.state.comment;
    changeComment(type, nested, comment, reactionId)
    .then(res => res === 'success' && getReactions(videoIdx))
    .then(() => this.initCommentState())
    .then(() => { 
      this.commentArea.value = '';
      this.commentArea.autofocus = false;
      this.reactionScroll.scrollToBottom(42);
     })
    .catch(err => Error(err));
  }

  _changeReactionOrdering = ({ target }) => {
    const ordering = target.value;
    const { setListCondition, getReactions, videoIdx } = this.props;
    setListCondition(ordering);
    getReactions(videoIdx);
  }

  _changeReactionType = ({ target }) => {
    const showType = target.value;
    const { setShowType, getReactions, videoIdx } = this.props;
    setShowType(showType);
    getReactions(videoIdx);
  }

  _onDeleteReaction(nested, reactionId) {
      const { videoIdx, getReactions } = this.props;
      this.props.deleteReaction(videoIdx, nested, reactionId)
      .then(res => res === 'success' && getReactions(videoIdx))
      .catch(err => Error(err));
  }

  _openSettingHover(e, reactionId) {
    this.setState(state => ({
      ...state, 
      clickedReactionId: state.clickedReactionId === reactionId ? '' : reactionId
    }));
  }

  _openReportComment(reactionId) {
    this.props.openPopup('REPORT', {
      reactionId,
      targetType: 'C',
    });
  }

  _getComment = (commentKey, children, isNested, reactionId) => {
    const { userProfile } = this.props;
    const { clickedReactionId } = this.state;
    return (<ReactionComment
      nested={isNested}
      key={commentKey} 
      reaction={children} 
      userProfile={userProfile}
      clickedReactionId={clickedReactionId}
      onClickNestedComment={e => this.createNestedCommentState(reactionId)}
      onClickModifyComment={e => isNested ? 
        this.updateNestedCommentState(reactionId, children.comment) : 
        this.updateCommentState(reactionId, children.comment)}
      onClickSettingOptions={e => this._openSettingHover(e, reactionId)}
      onClickDeleteReaction={() => this._onDeleteReaction(isNested, reactionId)}
      onClickReportComment={() => this._openReportComment(reactionId)}
    />);
  }

  _getMoreComments = (e, reactionIdx, childrenIdx, reactionId, nestedReactionId) => {
    this.props.getMoreComment(reactionIdx, childrenIdx, reactionId, nestedReactionId);
  }

  get reactions() {
    const { reactions } = this.props;
    
    return reactions.map((reaction, reactionIdx) => {
      return (<div className="comment-list" key={`comment-list-${reactionIdx}`} >
        {/*<div className="section-info">*/}
          {/*<p className="section-playtime">*/}
            {/*{convDuration(reaction.begin)} - {convDuration(reaction.end)}*/}
          {/*</p>*/}
        {/*</div>*/}
        <ul>
        {
          reaction.reactionChildrens.map((children, dataIdx) => {
            return (<li className="comment-wrapper" key={`comment-list-${dataIdx}`}>
            { this._getComment(`comment-${dataIdx}`, children, false, children.reactionId) }
                <div className="re-comment">
                {
                  children.moreNestedComments === 'Y' && 
                  <button
                    className="more"
                    onClick={e => this._getMoreComments(e, reactionIdx, dataIdx, children.reactionId, children.nestedComment.reactionId )}
                  >{T.translate('common.reply')} &nbsp; {T.translate('common.view-more')}</button>
                }
                  {/*첫번째 리스트 */
                    children.nestedComment !== null && this._getComment(`nested-comment-${dataIdx}`, children.nestedComment, true, children.nestedComment.reactionId)
                  }
                  {/*더보기 리스트 */
                    children.nestedComments !== null && children.nestedComments.map((nestedData, nestedIndex) =>
                    this._getComment(`nested-comments-${nestedIndex}`, nestedData, true, nestedData.reactionId))
                  }
                </div>
          </li>)
            })
          }
          <li>
            <div className={cx('list-loading', { hide: true })}>
                <img src="/images/card-loading.png" alt="images loading" />
            </div>
          </li>
        </ul>
      </div>);
    }).sort((a,b) => a -b)
  }

  render() {
    return (
      <div className="content">
        <Scrollbars
          style={{ width: '100%', height: '86%', position: 'absolute' }}
          renderThumbVertical={() => <div className='scroll-bar'/>}
          ref={ref => { this.reactionScroll = ref; }}
        >
          <div className="section-wrapper">
            {/*<div className="sorting">*/}
              {/*<select className="time-newest" onChange={this._changeReactionOrdering}>*/}
                {/*<option value="PLAYTIME">영상 시간 순</option>*/}
                {/*<option value="RECENTTIME">등록 순</option>*/}
              {/*</select>*/}
              {/*<select className="reaction-content" onChange={this._changeReactionType}>*/}
                {/*<option value="ALL">전체</option>*/}
                {/*<option value="TEXT">댓글</option>*/}
                {/*<option value="LIKE">좋아요</option>*/}
              {/*</select>*/}
            {/*</div>*/}
            {this.reactions}
          </div>
        </Scrollbars>
        <div className="comment-textarea">
          <textarea
            id="comment-textrea" 
            rows="4"
            placeholder={`${this.state.comment.placeholder}을 입력하세요.`}
            ref={(refs => { this.commentArea = refs; })}
            required={true}
          />
          <div className="comment-button" style={{ float: 'right' }}>
            <button
            type="button"
            className={cx({ hide: this.state.comment.type === 'CREATE' && !this.state.comment.nested })}
            onClick={() => this.initCommentState()} 
            style={{ 
              color: 'red',
              marginRight: '10px',
            }}>{T.translate('common.cancel')}</button>
            <button
              type="button" 
              onClick={this._applyComment}>{T.translate('common.register')}</button>
          </div>
        </div>
      </div>
    ); 
  }
}
  
const mapStateToProps = state => ({
  needMoveStack: state.player.needMoveStack,
  currentTime: state.player.currentTime,
  commentReaction: state.player.commentReaction,
  likeReaction: state.player.likeReaction,
  scrolling: state.player.scrolling,
  userProfile: state.user.userProfile,
  reactions: state.reaction.reactions,
  showType: state.reaction.showType,
  listCondition: state.reaction.listCondition,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onLikeReaction: likeReaction,
  onCommentReaction: commentReaction,
  onActiveCommentIcon: activeCommentIcon,
  setLikeCount: refreshLikeCount,
  setCommentCount: refreshCommentCount,
  stackMoved,
  openPopup,
  findTabScrollPositionFlag,
  ...ReactionCreators,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PlayReactionContent);
