import React, { Component } from "react";
import MakeCard from "../CardForm/MakeCard";
import MakeEventCard from "../CardForm/MakeEventCard";
import MakeSelector from "../CardForm/MakeSelector";
import MediaElement from "../Player/MediaElement";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { convDuration } from "../../util/time";
import * as MakeActionCreators from "../../modules/make";
import * as PlayerActionCreators from "../../modules/player";
import { Scrollbars } from "react-custom-scrollbars";
import { toastr } from "react-redux-toastr";
import T from "i18n-react";
import ReactHtmlParser from 'react-html-parser';


const scrollbarsInlineStyle = {
  position: "absolute",
  overflow: "hidden",
  width: "480px",
  height: "772px",
  top: 0,
  right: 0
};

const mapStateToProps = state => {
  return {
    step: state.make.step,
    videoSource: state.make.videoSource,
    stacks: state.make.stacks,
    events: state.make.events,
    sections: state.make.sections,
    currentTime: state.player.currentTime,
    duration: state.player.duration,
    focused: state.make.focused,
    deployFlag: state.make.deployFlag,
      needMoveStack:state.player.needMoveStack,
      addKnowledgeFlag:state.addKnowledgeFlag,
      preventScroll:state.player.preventScroll,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...MakeActionCreators,
    ...PlayerActionCreators,
  }, dispatch);


class MakeInfoContent extends Component {
  static defaultProps = {
    isModification: false
  }

  state = {
    currentTimeOfEventButtonClicked: -1,
    addKnowledgeFlag: 'false'
  }

  shouldComponentUpdate(nextProps) {
    return !nextProps.focused
  }

  get sectionButton() {
    const currentTime = Math.floor(this.props.currentTime) 
    const duration = Math.floor(this.props.duration);
    const sections = this.props.sections;
    const rangeTick = duration < 60 ? 2 : Math.floor(duration * 0.03);

    if (duration > 10 && currentTime >= 5 && currentTime <= duration - 5) {
      return sections.some(
        (item, index) =>
          (index !== 0 && item.begin === currentTime) ||
          (index !== 0 && item.begin - rangeTick <= currentTime &&
            item.begin + rangeTick >= currentTime)
      )
        ? <div className="combine">
            <button onClick={this.props.onCombineSection} />
            <p>
              {ReactHtmlParser(T.translate('make.press-something',{it: T.translate('make.section'),something:T.translate('make.combine')})) }
            </p>
          </div>
        : <div className="split">
            <button onClick={this.props.splitSection} />
            <p>
            {ReactHtmlParser(T.translate('make.press-something',{it: T.translate('make.section'),something:T.translate('make.split')})) }
            </p>
          </div>;
    } else {
      return (
        <div className="split">
          <button onClick={() => toastr.light('', T.translate('make.section-least-error'))} />
          <p>
          {ReactHtmlParser(T.translate('make.press-something',{it: T.translate('make.section'),something:T.translate('make.split')})) }
          </p>
        </div>
      );
    }
  }
    addKnowledgefunction = () => {
        this._movesKnowledgeAreaScroll();
  }

  get knowledgeButton() {
    const currentTime = Math.floor(this.props.currentTime)
    const stacks = this.props.stacks;
    return stacks.some(
      item => item.begin <= currentTime && currentTime <= item.end
    )
      ? <MakeSelector
          exportContentsType={(type, paths, currentTime) =>
            this.props.attachCard(type, paths, currentTime)}
        />
      : <div className="add-board">
          <button onClick={() =>{
              this.props.splitStack();
              this.setState(state => ({
                  ...state,
                  addKnowledgeFlag: 'true',
            }),this.addKnowledgefunction);

          }} />
          <p>
          {ReactHtmlParser(T.translate('make.press-something',{it: T.translate('make.board'),something:T.translate('make.add')})) }

            {/* 버튼을 눌러 지식을
            <mark> 추가</mark>
            해주세요. */}
          </p>
        </div>;
  }

  get eventButton() {
    const currentTime = Math.floor(this.props.currentTime)
    const { events } = this.props
    const { currentTimeOfEventButtonClicked } = this.state
   
    if (events.some(({ begin }) => begin === currentTime)) {
      return ''
    } else if (currentTimeOfEventButtonClicked === currentTime) {
      return (<MakeSelector
        mode="events"
        currentTime={currentTime}
        exportContentsType={(type, paths, curTime) => {
          this.props.attachEvent(type, paths, curTime);
          // window.player.play();
        }}
      />)
    } 

    return (<div className="event-add">
              <button
                onClick={() => {
                  window.player.pause();
                  this.setState(state => ({
                    ...state,
                    currentTimeOfEventButtonClicked: currentTime
                  }));
                }}
              />
              <p>
              {ReactHtmlParser(T.translate('make.press-something',{it: T.translate('make.event'),something:T.translate('make.add')})) }
                {/* 버튼을 눌러 이벤트를
                <mark> 추가</mark>
                해주세요. */}
              </p>
            </div>)
  }

  get sectionArea() {
    return this.props.sections.map((item, idx, array) => {
      const isLastItem = idx === array.length - 1;
      return (
        <div className="add-section" section-begin ={item.begin} section-end={isLastItem ? this.props.duration : array[idx + 1].begin} key={`sections-key-${item.begin}`}>
          <p className="section-range">
            {`${convDuration(item.begin)} - ${convDuration(
              isLastItem ? this.props.duration : array[idx + 1].begin
            )}`}
          </p>
          <input
            type="text"
            maxLength={20}
            defaultValue={item.name.toLowerCase().includes('section title') ? '' : item.name}
            placeholder={item.name}
            onChange={event => this.props.updateSectionName(event, idx)}
            title="섹션입력"
          />
        </div>
      );
    });
  }

  get knowledgeArea() {
    const bindEvent = {
      detachStack: this.props.detachStack,
      detachStackInCard: this.props.detachStackInCard,
      onSortEndStackInCard: this.props.onSortEndStackInCard,
      onChangeStackName: this.props.onChangeStackName,
      onChangeCardTypeText: this.props.onChangeCardTypeText,
    };

    
    return this.props.sections.map((section, index, array) => {
      const isLastItem = index === array.length - 1;
      const { duration } = this.props
      const nextBegin = isLastItem ? duration : array[index + 1].begin
      const filterCondition = ((item, idx) => {
          return array[index].begin <= item.begin && nextBegin > item.begin
      })
      const filterdStack = this.props.stacks.map((item, idx) => ({...item, stackIdx: idx ,placeholder : ''})).filter(filterCondition)
      return (
        <div className="add-knowledge" section-begin={section.begin} section-end={isLastItem ? this.props.duration : array[index + 1].begin} key={`knowledge-${index}`}>
          <p className="section-range">
            {`${convDuration(section.begin)} - ${convDuration(
              isLastItem ? this.props.duration : array[index + 1].begin
            )}`}
          </p>
          <p className="section-title">
            {section.name}
          </p>
          <div className="make-card">
            {
              filterdStack.map((item, idx) => {
                return <MakeCard
                  key={`make-card-${idx}`}
                  defaultPlaceholder={T.translate('make.knowledge-name') + ' ' + (idx+1)}
                  items={item}
                  cards={item.cards.sort((a, b) => {
                    if (a.orderValue > b.orderValue) return 1;
                    else return -1;
                  })}
                  cardsIndex={idx}
                  currentTime={this.props.currentTime}
                  deleteQuizCardAtStack={this.props.deleteQuizCardAtStack}
                  updateQuizCardAtStack={this.props.updateQuizCardAtStack}
                  updateVoteCardAtStack={this.props.updateVoteCardAtStack}
                  stackIndex={item.stackIdx}
                  onFocus={this.props.onFocus}
                  onFocusOut={this.props.onFocusOut}
                  isModification={this.props.isModification}
                  isDeployed={this.props.deployFlag === 'Y'}
                  {...bindEvent}
                />
              })
            }
          </div>
        </div>
      );
    });
  }

  get eventArea() {
    const isEmptyEvents = this.props.events.length === 0;
    return isEmptyEvents
      ? <div className="no-event">
          <p>{T.translate('make.press-add-event-bottom')}</p>
        </div>
      : <div className="add-knowledge">
          <div className="make-card">
            <MakeEventCard
              events={this.props.events}
              detachEvent={this.props.detachEvent}
              onFocus={this.props.onFocus}
              currentTime={this.props.currentTime}
              onFocusOut={this.props.onFocusOut}
              onChangeEventTypeText={this.props.onChangeEventTypeText}
              isModification={this.props.isModification}
            />
          </div>
        </div>;
  }

  componentWillReceiveProps(nextProps) {
      if (this.props.needMoveStack || this.state.addKnowledgeFlag === 'true') {
          if (this.props.step === 2.1) {
              this._moveSectionAreaScroll();

          } else if (this.props.step === 2.2) {
              if(!this.props.preventScroll)
                this._movesKnowledgeAreaScroll();
          }
          this.props.stackMoved();
          this.props.preventScrollFalse();

          this.setState(state => ({
              ...state,
              addKnowledgeFlag : 'false',
            } ));
      }


      const { videoSource } = this.props;
    if ([null, undefined].some(condition => condition === videoSource))
      return false;
    
    let source = null;
    if (videoSource.type === "NP") {
      source = { src: videoSource.file, type: "video/mp4" };
    } else if (videoSource.type === "LINK"){
      source = { src: videoSource.url, type: "video/mp4" };
    } else {
      source = { src: videoSource.url, type: "video/youtube" };
    }

    const sources = [source];

    this.setState(state => {
      return {
        ...state,
        sources
      };
    });
  }

  _moveSectionAreaScroll() {
      let scrollHeight = 0;
      const $sections = document.querySelectorAll('.add-section');

      for ( var j = 0; j < $sections.length; j++) {
          const section = $sections[j];

          if(section.hasAttribute('section-begin')) {
              const begin  = section.getAttribute('section-begin');
              const end  = section.getAttribute('section-end');

              if(this.props.currentTime > Number(begin) && Number(begin) !== 0) {
                  scrollHeight += section.scrollHeight;
              }
              if(this.props.currentTime < Number(end))
                  break;

          }
      }
      this.cardSectionScroll.scrollTop(scrollHeight);
  }

  _movesKnowledgeAreaScroll() {

      let scrollHeight = 0;
      const $knowledges = document.querySelectorAll('.add-knowledge');

      for ( var j = 0; j < $knowledges.length; j++) {
          const knowledge = $knowledges[j];

          const sectionBegin = knowledge.getAttribute('section-begin');
          const sectionEnd  = knowledge.getAttribute('section-end');

          if(this.props.currentTime >= Number(sectionEnd)) {
              scrollHeight += knowledge.scrollHeight;
          }


          if(this.props.currentTime < Number(sectionEnd)) {

              const sectionRange = knowledge.getElementsByClassName('section-range')[0];
              const sectionTitle = knowledge.getElementsByClassName('section-title')[0];
              const secTionStyle = getComputedStyle(sectionRange);

              scrollHeight += (sectionRange.offsetHeight + sectionTitle.offsetHeight +parseInt(secTionStyle.marginTop));
              //make-card height 계산

              const $makeCards = knowledge.querySelectorAll('.card-type');
              for(var i = 0 ; i < $makeCards.length ; i++) {
                const cardType = $makeCards[i];

                  const cardTypeStyle = getComputedStyle(cardType);

                  scrollHeight += parseInt(cardTypeStyle.marginTop);

                  const cardBegin = cardType.getAttribute('card-begin');
                  const cardEnd  = cardType.getAttribute('card-end');

                  if(this.props.currentTime >= Number(cardEnd)) {
                      scrollHeight += cardType.scrollHeight;
                  }

                  if(this.props.currentTime < Number(cardEnd)) {
                    break;
                  }
               }
              break;
          }
      }
      this.cardSectionScroll.scrollTop(scrollHeight);
  }


  render() {
    const { videoSource } = this.props;
    if ([null, undefined].some(condition => condition === videoSource))
      return false;

    let type;
    if (this.props.step === 2.1) {
      type = "section";
    } else if (this.props.step === 2.2) {
      type = "knowledge";
    } else {
      type = "event";
    }


    const videoSourceType = {
      YOUTUBE: 'video/youtube',
      NP: videoSource.hlsFlag === 'Y' ? 'application/x-mpegURL' : 'video/mp4', 
      LINK: 'video/mp4',
    };

    const hasOwnPropertyFIle = Object.prototype.hasOwnProperty.call(videoSource, 'file');

    const sources = [
       { src: hasOwnPropertyFIle ? videoSource.file : videoSource.url , type: videoSourceType[videoSource.type] }
    ];

    return (
      // tab active에 따라 type-section, type-knowledge, type-event 클래스로 구분
      <div className={`making-wrapper type-${type}`}>
        <div className="media-section">
          <div className="video-container">
            <MediaElement
              id="player"
              mediaType="video"
              preload
              width="780"
              height="438"
              poster=""
              sources={sources}
              makeMode={true}
              makeStep={this.props.step}
            />
          </div>

          <div className="make-function section">
            {this.sectionButton}
          </div>
          <div className="make-function knowledge">
            {this.knowledgeButton}
          </div>
          <div className="make-function event">
            {this.eventButton}
          </div>
        </div>
        <div className="card-section"> 
        <Scrollbars
          style={scrollbarsInlineStyle}
          renderThumbVertical={() => <div className="scroll-bar" />}
            ref={ref => { this.cardSectionScroll = ref; }}
        >
            {type === "section" &&
              <div className="section">
                {this.sectionArea}
              </div>}
            {type === "knowledge" &&
              <div className="knowledge">
              <form id="knowledgeform">
                {this.knowledgeArea}
              </form>
              </div>}
            {type === "event" &&
              <div className="event">
                {this.eventArea}
              </div>}
          </Scrollbars>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MakeInfoContent);

