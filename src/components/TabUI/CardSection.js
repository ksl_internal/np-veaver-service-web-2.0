import React from ' react';


export default class CardSection extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (<Scrollbars
style={scrollbarsInlineStyle}
renderThumbVertical={() => <div className="scroll-bar" />}
>
            {type === "section" &&
              <div className="section">
                {this.sectionArea}
              </div>}
            {type === "knowledge" &&
              <div className="knowledge">
              <form onSubmit={this.testSubmit} id="knowledgeform">
                {this.knowledgeArea}
              </form>
              </div>}
            {type === "event" &&
              <div className="event">
                {this.eventArea}
              </div>}
          </Scrollbars>
        )
    }
}