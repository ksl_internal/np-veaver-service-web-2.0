import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment';

import ReactionComment from '../CardForm/ReactionComment';
import {
  likeReaction,
  commentReaction,
  refreshLikeCount,
  refreshCommentCount,
  activeCommentIcon,
  stackMoved,
  findTabScrollPositionFlag,
} from '../../modules/player';
import { openPopup } from '../../modules/popup';
import { axios } from '../../util/api';
import T from "i18n-react";
class PlayReactionContent extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      divideReactions: [],
      recentReactions: [],
      reactionDivideTime: 5,
      ordering: 'PLAYTIME',
      showType: 'ALL',
      comment: {
        type: 'BASIC', // BASIC: 일반, NESTED: 대댓글, MODIFY: 수정
        reactionId: undefined,
        quizFlag: false,
        nested: false,
      },
      scrolling: false,
    };
  }

  componentDidMount() {
    this._callTimelineReactions(this.props.timelineData.videoIdx, this.state.ordering);
  }

  componentWillReceiveProps(nextProps) {
    // 코멘트달기 버튼 눌렀을 경우 comment stat 초기화
    // console.log('nextProps :: ', nextProps);
    if (nextProps.commentReaction) {
      this.setState(state => {
        return {
          ...state,
          comment: {
            type: 'BASIC',
            reactionId: undefined,
            quizFlag: false,
            nested: false,
          }
        }
      });
      document.getElementsByClassName('comment-textarea')[0].childNodes[0].value='';
      this.props.onCommentReaction(false);
    } else if (nextProps.likeReaction) {
      this._callTimelineReactions(this.props.timelineData.videoIdx, this.state.ordering);
      this.props.onLikeReaction(false);
    }

    if (this.props.needMoveStack && this.state.ordering === 'PLAYTIME') {
        this.props.stackMoved();
        this._moveReactionScroll();
      }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.scrolling) {
      this._moveReactionScroll();
      this.props.findTabScrollPositionFlag(false);
    }
  }

  _callTimelineReactions(videoIdx, ordering) {




    switch(ordering) {
      case 'PLAYTIME':
        axios.get(`/videos/${videoIdx}/reactions`, {
        })
        .then(result => {
          // reaction 시간별 재분배
          this.props.setLikeCount(result.data.likeCount);
          this.props.setCommentCount(result.data.commentCount);
          let tempReactions = [];
          let divideReactions = [];
          let divideTime = {start:0, end:5};

          result.data.reactionTimes.forEach((reactions, i) => {
            if (reactions.reactionTime >= divideTime.start && reactions.reactionTime < divideTime.end) {
              tempReactions.push(reactions);
              if (result.data.reactionTimes.length === i + 1) divideReactions.push(tempReactions);
            } else if (result.data.reactionTimes.length === i + 1) {
              if (tempReactions.length > 0) divideReactions.push(tempReactions);
              divideReactions.push([reactions]);
            } else {
              if (tempReactions.length > 0) {
                divideReactions.push(tempReactions);
                tempReactions = [];
              }
              divideTime = {
                start: reactions.reactionTime - reactions.reactionTime % 5,
                end: reactions.reactionTime +  (5 - reactions.reactionTime % 5)
              }
              tempReactions.push(reactions);
            }
          });

          this.setState((state) => {
            return {
              ...state,
              divideReactions,
              ordering: 'PLAYTIME',
            }
          });
          this.props.findTabScrollPositionFlag(this.state.scrolling);
        });
      break;
      case 'RECENTTIME':
        axios.get(`/videos/${videoIdx}/latest-reactions`)
        .then(result => {
          this.props.setLikeCount(result.data.likeCount);
          this.props.setCommentCount(result.data.commentCount);
          this.setState((state) => {
            return {
              ...state,
              recentReactions : result.data.reactions,
              ordering: 'RECENTTIME',
            }
          });
        });
      break;
      default: return null;
    }
  }

  // 반응 State 초기화
  initComment(scrolling) {
    this.setState(state => {
      return {
        ...state,
        comment: {
          type: 'BASIC',
          reactionId: undefined,
          quizFlag: false,
          nested: false,
          scrolling,
        }
      }
    })
    this._callTimelineReactions(this.props.timelineData.videoIdx, this.state.ordering);
    document.querySelectorAll('ul.settings.active').forEach(ul => ul.classList.remove('active'));
    document.getElementById('comment-textrea').value = '';
  }

  _changeReactionOrdering(e) {
    this._callTimelineReactions(this.props.timelineData.videoIdx, e.currentTarget.value);
  }

  _changeReactionType(e) {
    let { showType } = Object.assign({}, this.state);
    showType = e.currentTarget.value;
    this.setState(state => {
      return {
        ...state,
        showType,
      }
    });
  }

  // 댓글 달기
  _applyComment(e) {
    const { comment } = Object.assign({}, this.state);
    const { videoIdx } = Object.assign({}, this.props.timelineData);
    const commentText = document.getElementById('comment-textrea').value;
    if (commentText.trim() === '') return;
    if (!comment.reactionId && comment.quizFlag && comment.type === 'BASIC') {
      // 질문 댓글
      axios.post(`/videos/${videoIdx}/reactions/question`, {
          reactionTime: this.props.currentTime,
          comment: commentText
      })
      .then(result => {
          this.initComment(true);
      });

    } else if (!comment.reactionId && !comment.quizFlag && comment.type === 'BASIC') {
      // 일반 댓글 등록
      axios.post(`/videos/${videoIdx}/reactions/comment`, {
          reactionTime: this.props.currentTime,
          comment: commentText
      })
      .then(result => {
          this.initComment(true);
      });

    } else if (comment.reactionId && comment.type === 'NESTED') {
      // 대댓글 등록
      axios.post(`/videos/${videoIdx}/reactions/${comment.reactionId}`, {
          reactionType: 'TEXT',
          comment: commentText
      })
      .then(result => {
          this.initComment(false);
      });
    } else if (comment.reactionId && comment.type === 'MODIFY' && !comment.nested) {
      // 일반 댓글 수정
      axios.put(`/videos/${videoIdx}/reactions/${comment.reactionId}`, {
          reactionType: 'TEXT',
          comment: commentText
      })
      .then(result => {
          this.initComment(false);
      });

    } else if (comment.reactionId && comment.type === 'MODIFY' && comment.nested) {
      // 대댓글 수정
      axios.put(`/videos/${videoIdx}/nested-reactions/${comment.reactionId}`, {
          reactionType:'TEXT',
          comment: commentText
      })
      .then(result => {
          this.initComment(false);
      });
    }
  }

  // 대댓글 Click
  _setNestedComment(e) {
    // 가장 가까운 부모 찾기 (IE)
    let $target = e.currentTarget;
    do {
      if ($target.className.indexOf('comment-card') !== -1) break;
      $target = $target.parentElement;
    } while ($target !== null);

    if ($target !== null) {
      const { comment } = Object.assign({}, this.state);
      comment.reactionId = $target.getAttribute('data-reactionId');
      comment.type = 'NESTED';
      comment.nested = false;
      this.setState((state) => {
        return {
          ...state,
          comment,
        }
      });

      this.props.onCommentReaction(false);
      this.props.onActiveCommentIcon(false);
      document.getElementsByClassName('comment-textarea')[0].childNodes[0].value='';
      document.getElementsByClassName('comment-textarea')[0].childNodes[0].focus();
    }
  }

  _setModifyComment(e) {
    // 가장 가까운 부모 찾기 (IE)
    let $target = e.currentTarget;
    do {
      if ($target.className.indexOf('comment-card') !== -1) break;
      $target = $target.parentElement;
    } while ($target !== null);

    if ($target !== null) {
      const { comment } = Object.assign({}, this.state);
      comment.reactionId = $target.getAttribute('data-reactionId');
      comment.nested = $target.getAttribute('data-nested') === 'true' ? true : false;
      comment.type = 'MODIFY';
      this.setState((state) => {
        return {
          ...state,
          comment,
        }
      });

      this.props.onCommentReaction(false);
      this.props.onActiveCommentIcon(false);
      document.getElementsByClassName('comment-textarea')[0].childNodes[0].focus();
      document.getElementsByClassName('comment-textarea')[0].childNodes[0].value = $target.getElementsByClassName('comment')[0].innerText;
    }
  }

  _setDeleteComment(e) {
    // 가장 가까운 부모 찾기 (IE)
    let $target = e.currentTarget;
    do {
      if ($target.className.indexOf('comment-card') !== -1) break;
      $target = $target.parentElement;
    } while ($target !== null);

    if ($target !== null) {
      const reactionId = $target.getAttribute('data-reactionId');
      const nested = $target.getAttribute('data-nested') === 'true' ? true : false;

      if (nested) {
        // 대댓글 삭제
        axios.delete(`/videos/${this.props.timelineData.videoIdx}/nested-reactions/${reactionId}`, {
        })
        .then(result => {
            this.initComment(false);
        });
      } else {
        // 일반 댓글 삭제
        axios.delete(`/videos/${this.props.timelineData.videoIdx}/reactions/${reactionId}`, {
        })
        .then(result => {
            this.initComment(false);
        });
      }
    }
  }

  _setReportComment(e) {
    // 가장 가까운 부모 찾기 (IE)
    let $target = e.currentTarget;
    const reactionId = $target.getAttribute('data-reactionId');
    do {
      if ($target.className.indexOf('comment-card') !== -1) break;
      $target = $target.parentElement;
    } while ($target !== null);

    this.props.openPopup('REPORT', {
      reactionId,
      targetType: 'C',
    });
  }

  // 댓글 입력 TextArea 포커스 시, 일반 댓글 일 경우와 대댓글, 수정일 경우 구분
  _onFocusTextArea(e) {
    if (!this.state.comment.reactionId && this.state.comment.type === 'BASIC') {
      this.props.onCommentReaction(true);
      this.props.onActiveCommentIcon(true);
    } else {
      this.props.onCommentReaction(false);
      this.props.onActiveCommentIcon(false);
    }
  }

  // 질문 답변 모드 Click Event
  _onQuizMode(e) {
    const $target = e.currentTarget;
    const { comment } = Object.assign({}, this.state);
    if ($target.className.indexOf('on') !== -1) comment.quizFlag = false;
    else comment.quizFlag = true;

    this.setState((state) => {
      return {
        ...state,
        comment,
      }
    });
  }

  _moveReactionScroll() {
    let scrollHeight = 0;
    const { currentTime } = Object.assign({}, this.props);
    const $reactions = document.getElementsByClassName('comment-list');
    const sortingHeight = document.getElementsByClassName('sorting')[0].scrollHeight;
    let beginTime = 0;

    for (let i = 0; i < this.state.divideReactions.length; i++) {
      const divideReaction = this.state.divideReactions[i];
      beginTime = divideReaction[0].reactionTime;
      if (beginTime <= Math.ceil(currentTime) && Math.ceil(currentTime) < beginTime + this.state.reactionDivideTime) break;
      else if (beginTime >= Math.ceil(currentTime)) break;
      scrollHeight += $reactions[i].scrollHeight;
    }
    this.reactionScroll.scrollTop(scrollHeight + sortingHeight + 20);
  }

  convertTimeFormat(time) {
    return moment("1900-01-01").startOf('day').seconds(time).format('HH:mm:ss');
  }

  render() {
    return (
      <div className="content">
        <Scrollbars
          style={{ width: '100%', height: '86%', position: 'absolute' }}
          renderThumbVertical={() => <div className='scroll-bar'/>}
          ref={ref => { this.reactionScroll = ref; }}
        >
        <div className="section-wrapper">
          <div className="sorting">
            <select className="time-newest" onChange={e => this._changeReactionOrdering(e)}>
              <option value="PLAYTIME">{T.translate('play.order-videotime')}</option>
              <option value="RECENTTIME">최신반응 순</option>
            </select>
            <select className="reaction-content" onChange={e=> this._changeReactionType(e)}>
              <option value="ALL">{T.translate('play.view-all')}</option>
              <option value="TEXT">{T.translate('play.view-comments')}</option>
              <option value="LIKE">{T.translate('play.like')}</option>
            </select>
          </div>

          {this.state.divideReactions.length > 0 ? this.renderReactions() : () => {}}
          {/* 리액션 렌더링
          {this.props.reactionData ? this.renderReactions() : () => {}}
          */}
        </div>
        </Scrollbars >
        <form className="comment-textarea">
          <textarea id="comment-textrea" rows="4" placeholder={T.translate('play.enter-comment')} onFocus={(e) => this._onFocusTextArea(e)}></textarea>
          <div className="comment-button">
            <button type="button" className={`quiz-mode ${this.state.comment.quizFlag ? 'on' : ''}`} onClick={e => this._onQuizMode(e)}></button>
            <button type="button" onClick={e => this._applyComment(e)}>{T.translate('common.enter')}</button>
          </div>
        </form>
      </div>
    );
  }

  renderComments(reaction) {
    return (
      <li className="comment-wrapper" key={reaction.id}>
        <ReactionComment
          nested={false}
          key={`comment-${reaction.id}`} reaction={reaction} showType={this.state.showType}
          onClickNestedComment={this._setNestedComment.bind(this)}
          onClickModifyComment={this._setModifyComment.bind(this)}
          onClickDeleteComment={this._setDeleteComment.bind(this)}
          onClickReportComment={this._setReportComment.bind(this)}
        />
        <div className={`re-comment ${reaction.nestedComment != null ? '' : 'hide'}`}>
          <button
            className={`more ${this.state.showType === 'LIKE' ? 'hide' : reaction.moreNestedComments === 'Y' ? '' : 'hide'}`}
            onClick={e => this._moreNestedComments(e).bind(this)}
          >
            {T.translate('common.reply')} &nbsp; {T.translate('common.view-more')}
          </button>
          {
            reaction.nestedComment != null ?
            <ReactionComment
              nested={true}
              reaction={reaction.nestedComment} showType={this.state.showType}
              onClickNestedComment={this._setNestedComment.bind(this)}
              onClickModifyComment={this._setModifyComment.bind(this)}
              onClickDeleteComment={this._setDeleteComment.bind(this)}
              onClickReportComment={this._setReportComment.bind(this)}
            />
            :
            null
          }
          {
            reaction.nestedComments != null && reaction.nestedComments.map(nestedComment => {
              return (
                <ReactionComment
                  nested={true}
                  key={nestedComment.reactionId} reaction={nestedComment} showType={this.state.showType}
                  onClickNestedComment={this._setNestedComment.bind(this)}
                  onClickModifyComment={this._setModifyComment.bind(this)}
                  onClickDeleteComment={this._setDeleteComment.bind(this)}
                  onClickReportComment={this._setReportComment.bind(this)}
                />
              );
            })
          }
        </div>
      </li>
    );
  }

  renderReactions() {
    const { ordering, recentReactions, divideReactions } = Object.assign({}, this.state);
    if (ordering === 'PLAYTIME') {
      return divideReactions.map((bunchOfReactions, i) => {
        const begin = bunchOfReactions[0].reactionTime % 5 === 0 ? bunchOfReactions[0].reactionTime : bunchOfReactions[0].reactionTime - bunchOfReactions[0].reactionTime % 5
        const end = begin + 5;
        return (
          <div className="comment-list" key={`comment-list-${i}`} >
            <div className="section-info">
              <p className="section-playtime">
                {this.convertTimeFormat(begin)} - {this.convertTimeFormat(end)}
              </p>
            </div>
            <ul>
              {
                bunchOfReactions.map(reactions => {
                  return reactions.reactions.map(reaction => this.renderComments(reaction));
                })
              }
            </ul>
          </div>
        )
      });
    } else {
      return recentReactions.map(reaction => {
        return (
          <div className="comment-list" key={`recent-comment-list-${reaction.reactionId}`}>
            <ul>
              { this.renderComments(reaction) }
            </ul>
          </div>
        );
      });
    }
  }

}

const mapStateToProps = state => ({
  needMoveStack: state.player.needMoveStack,
  currentTime: state.player.currentTime,
  commentReaction: state.player.commentReaction,
  likeReaction: state.player.likeReaction,
  scrolling: state.player.scrolling,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onLikeReaction: likeReaction,
  onCommentReaction: commentReaction,
  onActiveCommentIcon: activeCommentIcon,
  setLikeCount: refreshLikeCount,
  setCommentCount: refreshCommentCount,
  stackMoved,
  openPopup,
  findTabScrollPositionFlag,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PlayReactionContent);
