import React from 'react';
import PlayKnowledgeContent from './PlayKnowledgeContent';
import PlayReactionContent from './PlayReactionContent';
import T from "i18n-react";

export default class PlayInfoTab extends React.Component {
  render() {
    const { activeTab, changeActiveInfoTab, timelineData } = this.props;
    return (
      <div className="tabs">
        <div className="tab knowledge" style={{ float: `${timelineData.type === 'CP' && 'none'}`, width:'50%'}}>
          <input type="radio" id="tab-knowledge" name="tab-ui" value="KNOWLEDGE" onChange={changeActiveInfoTab} checked={activeTab === 'KNOWLEDGE'} />
          <label htmlFor="tab-knowledge" className="tab-label" style={{ width: `${timelineData.type === 'CP' && '100%'}`}}>{T.translate('play.additional-knowledge')}</label>
          <PlayKnowledgeContent timelineData={timelineData} />
        </div>
        <div className={`tab reaction ${timelineData.type === 'CP' && 'hide'}`} style={{width:'50%'}}>
          <input type="radio" id="tab-reaction" name="tab-ui" value="REACTION" onChange={changeActiveInfoTab} checked={activeTab === 'REACTION' } />
          <label htmlFor="tab-reaction" className="tab-label">{T.translate('play.view-comments')}</label>
          <PlayReactionContent playTime={timelineData.playTime} videoIdx={timelineData.videoIdx} />
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   commentReaction: state.player.commentReaction,
//   likeReaction: state.player.likeReaction,
//   activeCommentIcon: state.player.activeCommentIcon,
// });

// const mapDispatchToProps = dispatch => bindActionCreators({
//   onActiveCommentIcon: activeCommentIcon,
//   onCommentReaction: commentReaction,
// }, dispatch);

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(PlayInfoTab);
