import React, { Component } from 'react';
import { File, Image, Link, CPLink, Map, Quiz, Text, Vote } from '../CardForm/Knowledge';
import { toastr } from "react-redux-toastr";
import { convDuration } from '../../util/time';
import {changeTimeline,refreshCurrentTime} from "../../modules/player";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class Section extends Component {

  renderStacks = this.renderStacks.bind(this)


  render() { 
    const { section, index } = Object.assign({}, this.props);
    return (
      <div className="section-wrapper" data-sectionidx={section.sectionIdx}>
        <div className="section-info" onClick={() =>{this.props.changeTimeline(section.begin); this.props.refreshCurrentTime(Number(section.begin));}}>

          <p className="section-playtime">
            {`${convDuration(section.begin)} - ${convDuration(section.end)}`}
          </p>
          <p className="section-title" style={{ color: 'rgb(53, 102, 214)', fontSize: '17px', fontWeight: 600 }}>
            {`${section.name}`}
          </p>
        </div>
        {section.stacks.length > 0 && this.renderStacks(section.stacks)}
      </div>
    );
  }

  renderStacks(stacks) {
    // console.log(stacks);
    return stacks.map(stack => (
      <div className="card-wrapper" key={`stack-${stack.stackIdx}`} data-stackidx={stack.stackIdx}>
        <div className="knowledge-card">
          <p className="time-range"  onClick={() => {this.props.changeTimeline(stack.begin); this.props.refreshCurrentTime(stack.begin); }}>
            {`${convDuration(stack.begin)} - ${convDuration(stack.end)}`}
          </p>
          <p className="board-title"  onClick={() => {this.props.changeTimeline(stack.begin); this.props.refreshCurrentTime(Number(stack.begin)); }}>
            {stack.name}
          </p>
          {stack.cards.length > 0 ? this.renderCards(stack.cards) : null}
        </div>
      </div>
    ));
  }

  renderCards(cards) {
    const { onAnswer, onVote, onAddExample, isOwner } = Object.assign({}, this.props);
    cards.sort((a, b) => a.orderValue - b.orderValue);
    return cards.map(card => {
      switch (card.type) {
        case 'FILE': return <File key={`file-key-${card.cardIdx}`} data={card.contents.files ? card.contents.files[0] : undefined} make={false} />;
        case 'IMAGE': return <Image key={`image-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'LINK': return <Link key={`link-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'CP_LINK': return <CPLink key={`link-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'MAP': return  <Map key={`map-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'QUIZ': return <Quiz key={`quiz-key-${card.cardIdx}`} timelineIdx={this.props.timelineIdx} cardIdx={card.cardIdx} data={card.contents} make={false} onAnswer={onAnswer} isOwner={isOwner}/>
        case 'TEXT': return <Text key={`text-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'VOTE': return <Vote 
         key={`vote-key-${card.cardIdx}`}
         cardIdx={card.cardIdx}
         data={card.contents} 
         make={false} 
         onVote={onVote}
         isOwner={isOwner}
         onAddExample={card.contents.answers.length < 5 ? onAddExample : () => { toastr.light('', '최대 5개 항목까지 등록 가능합니다.'); }}
        />
        default: return <div>No Card</div>;
      }
    });
  }

}


const mapDispatchToProps = dispatch => bindActionCreators({
    changeTimeline,
    refreshCurrentTime,
}, dispatch);


export default connect(
    null,
    mapDispatchToProps,
)(Section);


