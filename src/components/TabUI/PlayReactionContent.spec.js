import React from 'react';
import { mount, shallow } from 'enzyme';

import { PlayReactionContent } from './PlayReactionContent';

function setup() {
    const props = {
        needMoveStack: 0,
        currentTime: 0,
        commentReaction: 0,
        likeReaction: 0,
        scrolling: false,
        userProfile: {},
        reactions:[],
        showType: 'ALL',
        listCondition: 'PLAYTIME',
        setShowType: jest.fn(),
        setListCondition: jest.fn(),
        setVirtualTimeline: jest.fn(),
        getReactions: jest.fn(),
        getMoreComment: jest.fn(),
        changeComment: jest.fn(),
        deleteReaction: jest.fn(),
        increaseLikeCount: jest.fn(),
        onLikeReaction:jest.fn() ,
        onCommentReaction:jest.fn(),
        onActiveCommentIcon:jest.fn(),
        setLikeCount: jest.fn(),
        setCommentCount:jest.fn(),
        stackMoved: jest.fn(),
        openPopup: jest.fn(),
        findTabScrollPositionFlag: false,
    }

    const enzymeWrapper = mount(<PlayReactionContent {...props} />)

    return {
        props,
        enzymeWrapper
    }
}


describe('리액션의 삭제 기능을 테스트 한다.', () => {
    // given videoIdx, reactionId 가 주어진다
    // when 리액션을 삭제하면
    // then 1.리스트를 갱신된다  2.카운트가 갱신된다.
    it('답글을 삭제 한다.', () => {})
    test('댓글을 삭제 한다.', () => {})
    test('반응을 삭제 한다.', () => {})
    test('자기 자신의 리액션만 삭제가 된다.', () => {})
});