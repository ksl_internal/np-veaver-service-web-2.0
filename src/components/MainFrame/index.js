import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { logout } from "../../modules/auth";
import { initVideoInfo } from "../../modules/make";
import { initPlayer } from "../../modules/player";
import { Link } from "react-router-dom";
import cx from "classnames";
import RadialProgress from "../../components/RadialProgress";
import { toastr } from "react-redux-toastr";
import T from 'i18n-react';
import {makebackPreventStateMakePush} from "../../modules/app";
import { openPopup } from "../../modules/popup";
import * as searchActions from "../../modules/searchM";
import * as notiActions from "../../modules/noti";
import * as language from "../../modules/language";
import Navbar from "../../containers/Navbar";
import queryString from 'stringquery'
const containerCss = {
  margin: "70px 0 0 0",
  padding: "0",
  width:"calc(100vw - 405px)",
  boxSizing:'border-box',
  minHeight: "calc(100vh - 70px - 60px)"
};
class MainFrame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProfileLayerOn: false,
      isScrollOn: false,
      orderType: "R",
      keyword:'',
      languageToggle:false,
      navbarOpen:true,
      data:null
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
    this.getNotificationCount();
    const values = queryString(this.props.location.search)
    if(values.keyword != null){
      this.setState({keyword:values.keyword})
    }
  }
  /**
   * 읽지 않은 메시지 갯수를 가져옵니다.
   */
  getNotificationCount = async () => {
    await this.props.NotiActions.getNotificationCount({});
  };
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }
  handleScroll = () => {
    const scrollTop = document.scrollingElement
      ? document.scrollingElement.scrollTop
      : document.documentElement.scrollTop;
    this.setState(state => ({
      ...state,
      isScrollOn: scrollTop >= 80
    }));
  };

  onProfileClick() {
    this.setState(state => {
      return {
        ...state,
        isProfileLayerOn: !state.isProfileLayerOn
      };
    });
  }

  moveMake = (e, loc) => {
    // const toastrConfirmOptions = {
    //   onOk: () => console.log('OK: clicked'),
    //   onCancel: () => console.log('CANCEL: clicked')
    // };
    // toastr.confirm('', toastrConfirmOptions);

    e.stopPropagation();
    this.props.initPlayer(false, 0);

    if(loc === 'make') {
        if (this.props.makebackPreventState === 'makestep2') {
            this.props.makebackPreventStateMakePush()
        } else {
            this.props.initVideoInfo();
            this.props.history.push(`/${loc}`);
        }

    } else {
        if (this.props.makebackPreventState === 'makestep2') {
        } else {
            this.props.initVideoInfo();
        }
        this.props.history.push(`/${loc}`);
    }

  };

  _openServicePolicy = e => {
    e.preventDefault();
    this.props.openPopup("SERVICEPOLICY", null);
  };

  _openPrivacyPolicy = e => {
    e.preventDefault();
    this.props.openPopup("PRIVACYPOLICY", null);
  };
  /**
   * 언어 설정 토글 함수
   */
  languageToggle() {
    this.setState(state => {
      return {
        ...state,
        languageToggle: !state.languageToggle
      };
    });
  };
  /**
   * 언어 설정
   */
  selectLanguage = value => {
    this.props.changeLanguage(value);
    this.languageToggle();
  };
  /**
   * 검색 페이지로 이동 합니다.
   */
  searchKeyword = async () => {
    try {
      this.props.history.push(`/search/timeline?keyword=${this.state.keyword}`);
    } catch (e) {}
  };
  render() {

    const { userProfile, logout, notificationCountResult } = this.props;
    if (!userProfile) {
      return null;
    }
    let footerMargin='0px';
    if(this.props.navbarStatus === 3){ //LNB 가 아예 없으면,
      footerMargin = '70px'
    }else if (!this.props.navbarIsOpen){ //LNB 가 닫혀 있으면,
      footerMargin = '35px'
    }
    return (
      <div className="layout">
        <Navbar {...this.props}/>
        <div className="contents-wrapper">
          <header className={cx({ "scroll-on": this.state.isScrollOn })}>
            <Link
              to={{ pathname: "/make" }}
              className={`file-upload ${!this.props.percentage ? "hide" : ""}`}
            >
              <p className="percentage">{this.props.percentage}%</p>
              <RadialProgress percentage={this.props.percentage} />
            </Link>
            <div className="search-div">
              <input
                placeholder={T.translate('play_menu.toast')}
                name="keyword"
                value={this.state.keyword}
                onChange={e => {
                  this.setState({
                    [e.target.name]: e.target.value
                  });
                }}
                onKeyPress={e => {
                  if (e.key === "Enter") {
                    this.searchKeyword();
                  }
                }}
              />
              <img
                src="/images/v1/icon/ic-search-popup.svg"
                style={{ width: "19px", height: "19px" ,cursor:'pointer'}}
                onClick={this.searchKeyword}
              />
              <button
                type="submit"
                name="submit-search"
                className="submit-search"
              />
            </div>
            <div className="user-noti">
              <Link to="/noti/i">
                <img src="/images/v1/icon/ic-noti.svg" />
                {this.props.notificationCountResult.sum > 0 && (
                  <div>{this.props.notificationCountResult.sum}</div>
                )}
              </Link>
            </div>
            <div className="user-profile" style={{width:'36px',height:'36px'}}>
              <Link to="/profile">
                <img src={userProfile.thumbnail}  style={{borderRadius:'36px',width:'36px',height:'36px'}}/>
              </Link>
            </div>
          </header>
          <main>
            <div className="flex"><div className="main_container">{this.props.children}</div></div>
          </main>
          <footer style={{marginLeft:footerMargin}}>
            <div className="company-info">
              <a
                href="https://veaver.com"
                target="_blank"
                rel="noopener noreferrer"
                title="Veaver Inc. Company Link"
              >
                Veaver Inc. Company Link
              </a>
              <p>&copy; 2018 Veaver, Inc All rights reserved</p>
            </div>
            <div className="notice-info">
              <ul>
                <li>
                  <Link to={"/contact"}>{T.translate('service.service-inquiry')}</Link>
                </li>
                <li>
                  <a href="#" onClick={e => this._openServicePolicy(e)}>
                    {T.translate('terms.terms')}
                  </a>
                </li>
                <li>
                  <a href="#" onClick={e => this._openPrivacyPolicy(e)}>
                    {T.translate('terms.private')}
                  </a>
                </li>
                <li>
                  <div className="language-selector-container">
                    <a onClick={e => this.languageToggle(e)}>
                      {T.translate('terms.language')}
                    </a>
                    <ul className={`language-selector-box ${this.state.languageToggle ? 'active': ''}`}>
                      {
                        <Fragment>
                          <li>
                            <span className="btn-link" onClick={() => this.selectLanguage('ko')}>한국어</span>
                          </li>
                          <li>
                            <span className="btn-link" onClick={() => this.selectLanguage('en')}>English</span>
                          </li>
                        </Fragment>
                      }
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   userProfile: state.user.userProfile,
//   percentage: state.make.uploadVideoInfo.uploadProgress,
//   items: state.searchM.get('items'),
// });

// const mapDispatchToProps = dispatch => bindActionCreators({
//   logout,
//   initVideoInfo,
//   initPlayer,
//   openPopup,
//   actions
// }, dispatch);
// export default withRouter(connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(MainFrame));
export default withRouter(
  connect(
    state => ({
      userProfile: state.user.userProfile,
      navbarStatus: state.navbar.status,
      navbarIsOpen: state.navbar.isOpen,
      percentage: state.make.uploadVideoInfo.uploadProgress,
      makebackPreventState:state.app.makebackPreventState,
      notificationCountResult: state.noti.get("notificationCountResult"),
      noticesData:state.noti.get('noticesData')
    }),
    dispatch => ({
      logout: bindActionCreators(logout, dispatch),
      initVideoInfo: bindActionCreators(initVideoInfo, dispatch),
      initPlayer: bindActionCreators(initPlayer, dispatch),
      makebackPreventStateMakePush: bindActionCreators(makebackPreventStateMakePush, dispatch),
      openPopup: bindActionCreators(openPopup, dispatch),
      SearchActions: bindActionCreators(searchActions, dispatch),
      NotiActions: bindActionCreators(notiActions, dispatch),
      changeLanguage: ( $language ) => {
        dispatch( language.setLanguage( $language ) );
      }
    })
  )(MainFrame)
);
