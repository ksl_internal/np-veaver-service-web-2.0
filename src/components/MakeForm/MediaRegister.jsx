import React from "react";
import { bindActionCreators } from "redux";
import { toastr } from "react-redux-toastr";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import pt from "prop-types";
import cx from "classnames";
import { updateVideoSource, updateVideoThumbNail, initVideoInfo, H263NotSupported, extractYoutubeDuration } from "../../modules/make";
import { initPlayer } from "../../modules/player";
import { openPopup } from "../../modules/popup";
import T from "i18n-react";

const mapStateToProps = state => {
    return {
      videoSource: state.make.videoSource,
    };
  };

const mapDispatchToProps = dispatch =>
    bindActionCreators({
      updateVideoSource,
      updateVideoThumbNail,
      initVideoInfo,
      H263NotSupported,
      initPlayer,
      openPopup,
      extractYoutubeDuration,
    }, dispatch);

const styles = {
    cursor: { cursor: "pointer" },
    position: { position: "relative" },
    file: {
      opacity: 0,
      cursor: "pointer",
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%"
    }
};

class MediaRegister extends React.Component {
    state = {
      fileName: '',
    }

    componentWillMount() {
      this.props.initPlayer(false, 0);
      this.props.initVideoInfo();
      const azScript = document.createElement('script');
      azScript.defer = true;
      azScript.async = true;
      azScript.src = "https://veaver-endpoint-common.azureedge.net/js/azure-storage.blob.min.js";
      document.body.appendChild(azScript);
    }

    // _onVideoUpload = evt => {
    //   let myVideos = [];
    //   const target = evt.target;
    //   const video = target.files[0];
    //   myVideos.push(target.files[0]);
    //   if (video !== undefined && video instanceof File) {
    //     const URL = window.URL || window.webkitURL;
    //     let tempUrl = URL.createObjectURL(video);
    //     let ve = document.createElement('video');
    //     ve.src = tempUrl;
    //     ve.muted = true;
    //     ve.preload = 'auto';
    //     ve.play();
    //     ve.onplaying = () => {
    //       console.dir()
    //       window.URL.revokeObjectURL(ve.src);
    //       let duration = ve.duration;
    //       myVideos[myVideos.length - 1].duration = duration;
    //       let time = Math.round(myVideos[0].duration)
    //       if(time <= 5){
    //         toastr.light('최초 5초 이상의 영상을 선택해주세요.');
    //         return;
    //       }else{
    //         this.setState({ fileName:  `파일명: ${video.name}` });
    //         this.props.updateVideoSource({ type: "NP", file: tempUrl, resource: video });
    //         this._autoUploadThumbnail(tempUrl);
    //         target.value = null;
    //       }
    //     }
    //   }
    // }

    _onVideoUpload = evt => {
      const target = evt.target;
      const video = target.files[0];
      if (video !== undefined && video instanceof File) {
        const URL = window.URL || window.webkitURL;
        let tempUrl = URL.createObjectURL(video);
        let ve = document.createElement('video');
        ve.src = tempUrl;
        ve.muted = true;
        ve.preload = 'auto';
        ve.play();
        ve.onplaying = () => {
          console.dir()
            this.setState({ fileName:  `파일명: ${video.name}` });
            this.props.updateVideoSource({ type: "NP", file: tempUrl, resource: video });
            this._autoUploadThumbnail(tempUrl);
          target.value = null;
        }

        ve.onerror = () => {
            const data = {
                title: '에러',
                description: '지원하지 않는 포멧입니다.',
                showcancel : 'showcancel',
            };

            this._initVideoSource()
            this.props.openPopup('BASIC', data);
        }
      }
    }

    _onURLPopupOpen = () => {
      this.props.openPopup("URL", null, this._onVideoUrlChanged);
    }

    _onVideoUrlChanged = typedUrl => {
      if (typeof typedUrl === "string") {
        if (/^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/.test(typedUrl)) {
          this.setState({ fileName:  `URL: ${typedUrl}` });
          this.props.extractYoutubeDuration(typedUrl).then(res => {
            if (!(res instanceof Error)) {
              this.props.updateVideoSource({ type: "YOUTUBE", url: typedUrl });
            }
          });
        } else {
          const ve = document.createElement('video');
          ve.src = typedUrl;
          ve.muted = true;
          ve.preload = 'auto';
          ve.muted = true;
          ve.onloadeddata = () => {
            this.setState({ fileName:  `URL: ${typedUrl}` });
            this.props.updateVideoSource({ type: "LINK",  url: typedUrl });
            this.props.initPlayer(false, ve.duration);
            //this._autoUploadThumbnail(typedUrl);
          }

          ve.onerror = () => {
            this.props.H263NotSupported();
          }
        }
      }
    }

    _autoUploadThumbnail = (tempUrl) => {
      const videoElement = document.createElement('video');
      videoElement.src = tempUrl;
      videoElement.preload = 'auto';
      let videoElementWidth = 160;
      let videoElementHeight = 90;
      let duration = 0;
      const generateThumbnail = () => {
        const canvasElement = document.createElement("canvas");
        const canvasContext = canvasElement.getContext("2d");
        canvasElement.width = videoElementWidth;
        canvasElement.height = videoElementHeight;
        canvasContext.drawImage(videoElement, 0, 0, videoElementWidth, videoElementHeight);
        const dataUrl = canvasElement.toDataURL('image/jpeg');
        const file = this._dataURItoBlob(dataUrl);
        this.props.updateVideoThumbNail(file);
        this.props.initPlayer(false, duration);
      };
      const ve = this
      videoElement.addEventListener('loadedmetadata', function() {

          if(videoElement.videoWidth && videoElement.videoHeight) {
              videoElement.currentTime = 1;
              videoElementWidth = videoElement.videoWidth;
              videoElementHeight = videoElement.videoHeight;
              duration = this.duration;
          } else {
              const data = {
                  title: '에러',
                  description: '지원하지 않는 포멧입니다.',
                  showcancel : 'showcancel',
              };

              ve._initVideoSource()
              ve.props.openPopup('BASIC', data);
          }
      }, false);

      videoElement.addEventListener('seeked', function() {
        generateThumbnail();
      }, false);
    }

    _dataURItoBlob = (dataURI) => {
      let byteString;
      if (dataURI.split(',')[0].indexOf('base64') >= 0)
          byteString = atob(dataURI.split(',')[1]);
      else
          byteString = unescape(dataURI.split(',')[1]);

      let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
      let ia = new Uint8Array(byteString.length);
      for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
      }
      return new Blob([ia], {type: mimeString });
    }

    _initVideoSource = () => {
      this.props.initPlayer(false, 0);
      this.props.updateVideoSource(null);
    }

    _moveToTempLocker = () => {
      this.props.history.push('/locker');
    }

    get inactiveVideoArea() {
        return (
        <div className="nodata-type">
          <p>{T.translate('make.description')}</p>
        </div>);
    }

    get activeVideoArea() {
      const { fileName } = this.state;
        return (
        <div className="upload-type">
          <p>{fileName} {T.translate('make.finish-upload')}</p>
          <button className="cancel" onClick={this._initVideoSource} />
        </div>
        )
    }

    render() {
        const urlUpload = { onClick: this._onURLPopupOpen };
        const moveToTempLocker = { onClick: this._moveToTempLocker}
        const { videoSource } = this.props;
        return (
            <div className="upload-box">
                <ul className="upload-select">
                    <li className="pc-upload" style={styles.position}>
                      <input
                        type="file"
                        // safari issue attached video/mp4,video/x-m4v
                        accept="video/mp4,video/x-m4v1 ,video/webm"
                        style={styles.file}
                        onChange={this._onVideoUpload}
                      />
                      <button />
                      <p>{T.translate('make.pc-upload')}</p>
                    </li>
                    <li className="url-upload" {...urlUpload}>
                      <button />
                      <p>{T.translate('make.url-upload')}</p>
                    </li>
                      {/* <li className="draft" {...moveToTempLocker}>
                        <button></button>
                        <p>
                            {T.translate('make.temp-storage')}
                        </p>
                      </li> */}
                </ul>
             <div className="progress-area">
               { videoSource ? this.activeVideoArea : this.inactiveVideoArea  }
            </div>
        </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MediaRegister));
