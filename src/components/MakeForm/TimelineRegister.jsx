import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import pt from "prop-types";
import cx from "classnames";
import { cancelVideoUpload } from "../../modules/make";
import CircularProgressBar from "../../components/CircularProgress";
import { isLocker } from '../../util/api';
import T from "i18n-react";

const mapStateToProps = state => {
  return {
    percentage: state.make.uploadVideoInfo.uploadProgress,
    videoName: state.make.uploadVideoInfo.name,
    uploadVideoStatus: state.make.uploadVideoStatus,
    S3ManagedUpload: state.make.S3ManagedUpload,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    cancelVideoUpload,
  }, dispatch);


class TimelineRegister extends React.Component {
  static propTypes = {
    percentage:  pt.oneOfType([
      pt.string,
      pt.number,
    ]),
    videoName: pt.string,
    uploadVideoStatus: pt.string,
  }
  
  static defaultProps = {
    percentage: 0,
    videoName: '영상',
    uploadVideoStatus: 'READY',
  }
  
  shouldComponentUpdate(props) {
    const conditions = [
      this.props.percentage !== props.percentage,
      this.props.uploadVideoStatus !== props.uploadVideoStatus,
    ].some(condition => condition)
    return conditions;
  }

  _cancelVideoUpload(e) {
    this.props.cancelVideoUpload();
  }

  render() {
    const locker = isLocker();
    const { percentage, videoName, uploadVideoStatus } = this.props;
    return (
      <div className="posting-box">
        <div className="post-progress">
            <CircularProgressBar
                strokeWidth="3"
                sqSize="100"
                percentage={typeof percentage === 'number' ? percentage : Number(percentage).toFixed(0)}/>
          <p>
            <mark>
              {videoName}
            </mark>
            <br />
            {uploadVideoStatus === "READY" && '을(를) 업로드를 준비중 입니다.'}
            {uploadVideoStatus === "COMPLETE" && T.translate('make.finish-to-open')}
            {uploadVideoStatus === "CANCEL" && '을(를) 업로드 취소하였습니다.'}
            {uploadVideoStatus === "PROGRESS" && '을(를) 업로드 중입니다.'}
            {(uploadVideoStatus === "MODIFY" && locker) && '을(를) 수정했습니다. 바로 게시 하시겠습니까?'}
            {(uploadVideoStatus === "MODIFY" && !locker) && T.translate('make.finish-to-detail')}
          </p>
          <button
            className={cx("cancel", { hide: uploadVideoStatus !== "PROGRESS" })}
            onClick={e => this._cancelVideoUpload(e)}
          />
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TimelineRegister));



