import React from "react";
import { bindActionCreators } from "redux";
import { toastr } from "react-redux-toastr";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import pt from "prop-types";
import cx from "classnames";
import {
  overrideMetaData,
  uploadVideoInfoDataSet,
  videoPublish,
  uploadVideo,
  updateTimeline,
  stepUpdate,
  initVideoInfo,
} from "../../modules/make";
import { initPlayer } from '../../modules/player';
import { isLocker } from '../../util/api';
import T from "i18n-react";
import {makebackPreventStateInit,makebackPreventStateMakestep2Container} from "../../modules/app";

const mapStateToProps = (state, props) => {
  return {
    uploading: state.make.uploading,
    timelineIdx: state.make.timelineIdx,
    videoSource: state.make.videoSource,
    uploadVideoStatus: state.make.uploadVideoStatus,
    uploadVideoInfo: state.make.uploadVideoInfo,
    step: state.make.step,
    title: state.make.formdata.title,
    thumbnail: state.make.formdata.thumbnail,
    tag: state.make.formdata.tag,
    recommendTags: state.make.formdata.recommendTags,
    categoryIdx: state.make.formdata.categoryIdx,
    modify: props.modify,
    selectVideoStepDone: state.make.selectVideoStepDone,
    stacks: state.make.stacks,
    sections: state.make.sections,
    events: state.make.events,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    overrideMetaData,
    uploadVideoInfoDataSet,
    videoPublish,
    uploadVideo,
    stepUpdate,
    updateTimeline,
    initVideoInfo,
    initPlayer,
      makebackPreventStateMakestep2Container,
      makebackPreventStateInit,
  }, dispatch);

class StepController extends React.Component {
  static propTypes = {
    modify: pt.bool,
  }

  static defaultProps = {
    modify: false,
  }

  _moveToMake = () => {
    this.props.initVideoInfo();
    this.props.initPlayer(false, 0);
    this.props.history.push('/make');
  }

  _moveToDetail = () => {
    this.props.initVideoInfo();
    this.props.initPlayer(false, 0);
    this.props.history.push(`${isLocker() ? '/locker' : ''}/play/${this.props.timelineIdx}`)
  }

  _moveToLocker = () => {
    this.props.initVideoInfo();
    this.props.initPlayer(false, 0);
    //this.props.history.push('/locker');
    this.props.history.push('/make');
  }

  _tempVideoPublish = () => {
    this.props.uploadVideoInfoDataSet(this.props.timelineIdx);
  }

  _printMessage = () => {
    toastr.light('업로드 중입니다. 다시 시도해 주세요');
  }

  _validation = () => {
    const { title, tag, thumbnail, categoryIdx } = this.props;
    const titleCondition = title !== ''
    const tagCondition = tag.length !== 0
    const thumbnailCondition = !thumbnail.includes('test.png')
    const categoryCondition = categoryIdx !== 0
    return [titleCondition, tagCondition, thumbnailCondition, categoryCondition].every(elm => elm)
  }
  get publishButton() {
    this.props.makebackPreventStateInit()
    const locker = isLocker();
    const { modify, uploadVideoStatus } = this.props;
    const isDimmed = ["READY", "CANCEL" , "PROGRESS"].some(status => status === uploadVideoStatus);
    return (modify && !locker) ?  (<button
      className='btn-solid btn-blue space-left_10'  onClick={() => this._moveToDetail()}>
        <a>
        {T.translate('make.view-details')}
        </a>
      </button>) : (<button
      onClick={() => locker ? this._tempVideoPublish() : this.props.videoPublish()}
      className={cx('btn-solid btn-blue space-left_10', {
        dimmed: isDimmed,
      })}>{T.translate('common.publish')}</button>);
  }

  get completeButton() {
    const { modify, uploading } = this.props;
    return modify ? (<button
      onClick={() => {uploading ? this._printMessage() : this.props.updateTimeline()}}
      className={cx('btn-solid btn-blue space-left_10', {
        dimmed: !this._validation(),
      })}>{uploading ? '...' : T.translate('common.edit2')}</button>) : (<button
      onClick={() => { uploading ? this._printMessage() : this.props.uploadVideo()}}
      className={cx('btn-solid btn-blue space-left_10', {
        dimmed: !this._validation(),
      })}>{uploading ? '...' : T.translate('common.complete')}</button>);
  }

  get makeButton() {
    const isDimmed =["PROGRESS", "READY"].some(
      str => str === this.props.uploadVideoStatus);
    return (<button
      className={cx("btn-solid btn-gray", {
        dimmed: isDimmed,
        hide: this.props.step !== 3 || this.props.modify
      })}
      onClick={() => this._moveToMake()}
      >{T.translate('common.make-new')}
      </button>);
  }

  get prevButton() {
    const { modify, step } = this.props;
    const locker = isLocker();
    const isDimmed =["PROGRESS", "READY"].some(
      str => str === this.props.uploadVideoStatus);

    if (modify && step === 2) {
      return ;
    } else if (step === 2) {
       // USE STEP 2
      return (<button
      className={cx("btn-solid btn-gray", {
        dimmed: (step === 3 && isDimmed),
        hide: step === 1 || (modify && !locker),
      })}
      onClick={e => e.persist() ||  this._moveToMake()}
      >{T.translate('make.video-reselection')}</button>);
    } else if (step === 3) {
      // USE STEP 3
      return (<button
      className={cx("btn-solid btn-gray", {
        dimmed: (step === 3 && isDimmed),
        hide: step === 1 || (modify && !locker),
      })}
      onClick={e => e.persist() ||  this._moveToLocker()}
      >{T.translate('common.later')}</button>);
    } else {
      // USE STEP 2.1, 2.2, 2.3
      return (<button
      className={cx("btn-solid btn-gray", {
        dimmed: (step === 3 && isDimmed),
        hide: step === 1,
      })}
      onClick={e => e.persist() ||  this.props.overrideMetaData()}
      >{T.translate('common.cancel')}</button>);
    }
  }

  _secondStepSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const { sections, stacks, events } = this.props;
    const isNotEmptySectionsNameSpace = sections.every((section) => section.name !== '');
    let isNotEmptyStacksNameSpace = stacks.every((stack) => stack.name !== '');
    
    for (let i = 0; i < stacks.length; i++) {
       if(stacks[i].name === ""){
        stacks[i].name = T.translate('make.knowledge-name') + ' ' + (i+1);
        isNotEmptyStacksNameSpace = true;
       } 
     }
    const isNotEmptyStackValue = stacks.every((stack) => 
      stack.cards.every((card) => {
        const { type, contents } = card;
        if (type === 'TEXT' && contents.text === '') {
          return false;
        } else if (type === 'QUIZ' && contents.type === 'SHORT') {
          const { title, answer } = contents;
          return title !== '' && answer.example !== '';
        } else if ((type === 'QUIZ' && contents.type === 'MULTIPLE') || type === 'VOTE') {
          const { title, answers } = contents;
          return title !== '' && answers.every(answer => answer.example !== '');
        }
        return true;
      })
    );
    const isNotEmptyEventValue = events.every((event) => {
      const { type, contents } = event;

      if (type === 'TEXT' && contents.text === '') {
        return false;
      } else if (type === 'QUIZ' && contents.type === 'SHORT') {
        const { title, answer } = contents;
        return title !== '' && answer.example !== '';
      } else if (type === 'QUIZ' && contents.type === 'MULTIPLE') {
        const { title, answers } = contents;
        return title !== '' && answers.every(answer => answer.example !== '');
      }
      return true;
    });

    if (isNotEmptySectionsNameSpace
      && isNotEmptyStacksNameSpace
      && isNotEmptyEventValue
      && isNotEmptyStackValue) {
      this.props.stepUpdate('direct', 2);
    } else if (!isNotEmptySectionsNameSpace) {
      //|| !isNotEmptyStacksNameSpace 덧지식 명 빈칸일때도 저장할수 있게끔 주석
      toastr.light('섹션 타이틀, 또는 덧지식 명을 입력해 주세요.');
    } else if (!isNotEmptyEventValue || !isNotEmptyStackValue) {
      toastr.light('항목을 입력해주세요.');
    }
  }

  get updateButton() {
    // USE STEP 2.1, 2.2, 2.3
      return (<button
      onClick={this._secondStepSubmit}
      className={`
        btn-solid btn-blue space-left_10
        ${this.props.videoSource === null ? "dimmed" : ""}
      `}
    >{T.translate('common.save')}</button>);
  }

  get nextButton() {
    // ONLY USE STEP1
    const handler = (e) => {
        window.history.pushState(null, null, window.location.href);
        this.props.makebackPreventStateMakestep2Container();
        e.preventDefault();
      e.stopPropagation();
      this.props.stepUpdate('next');
    }
      return (<button
      onClick={e => handler(e)}
      className={`
        btn-solid btn-blue space-left_10
        ${!this.props.selectVideoStepDone ? "dimmed" : ""}
      `}
    >{T.translate('make.next')}</button>);
  }

  render() {
    return (
      <div className="button-group">
        <div className="add-section">
          {this.makeButton}
        </div>
        <div className="submit-section">
          {this.prevButton}
          {this.props.step === 1 && this.nextButton}
          {this.props.step === 2 && this.completeButton}
          {[2.1, 2.2, 2.3].some(step => step === this.props.step) && this.updateButton}
          {this.props.step === 3 && this.publishButton}
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(StepController));
