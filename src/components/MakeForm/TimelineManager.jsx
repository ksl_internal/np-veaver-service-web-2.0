import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import pt from "prop-types";
import cx from "classnames";
import MakeInfoTab from "../../components/TabUI/MakeInfoTab";
import MakeInfoContent from "../../components/TabUI/MakeInfoContent";
import * as MakeActionCreators from "../../modules/make";
import { initPlayer } from "../../modules/player";
import { openPopup } from "../../modules/popup";


const mapStateToProps = state => {
  return {
    step: state.make.step,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...MakeActionCreators,
    initPlayer,
    openPopup,
  }, dispatch);

class TimelineManager extends React.Component {

  render() {
    return (
      <div className="make-info_wrapper">
      <MakeInfoTab
        tabNo={this.props.step}
        tabChanged={this.props.stepUpdate}
      />
      <MakeInfoContent />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineManager);