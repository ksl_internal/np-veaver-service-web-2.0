import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import pt from "prop-types";
import cx from "classnames";
import { stepUpdate, updateFormData, metadataToTemp } from "../../modules/make";
import { initPlayer } from "../../modules/player";
import { openPopup } from "../../modules/popup";
import Tags from "../../components/Others/Tags";
import CategorySelectBox from '../Others/CategorySelectBox';
import { toastr } from "react-redux-toastr";
import T from "i18n-react";

const initArray = [];

const mapStateToProps = (state, props) => {
  const { viewVideoCategoryNames } = state.make;
  const { recommendTags } = state.make.formdata;
    return {
      thumbnailPreview: state.make.formdata.thumbnail,
      title: state.make.formdata.title,
      description: state.make.formdata.description,
      categoryIdx: state.make.formdata.categoryIdx,
      tags: state.make.formdata.tag,
      accountStatus: 'user',
      publicRange: state.make.formdata.publicRange,
      publicUsers: state.make.formdata.publicUsers,
      adminFlag: state.user.userProfile.adminFlag,
      categories: state.make.categories,
      recommendTags: recommendTags ? recommendTags : initArray,
      viewVideoCategoryNames: viewVideoCategoryNames ? viewVideoCategoryNames : initArray,
      sectionsCount: state.make.sections.length,
      stacksCount: state.make.stacks.length,
      eventsCount: state.make.events.length,
      mainCategoryIdx: state.make.mainCategoryIdx,
      smallCategoryIdx: state.make.smallCategoryIdx,
      modifyMode: props.modifyMode,
      videoSource: state.make.videoSource,
    };
  };

  const mapDispatchToProps = dispatch =>
    bindActionCreators({
      metadataToTemp,
      updateFormData,
      stepUpdate,
      initPlayer,
      openPopup,
    }, dispatch);

const styles = {
    cursor: { cursor: "pointer" },
    position: { position: "relative" },
    file: {
      opacity: 0,
      cursor: "pointer",
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%"
    }
};

class MetadataRegister extends React.Component {

  static defaultProps = {
    recommendTags: []
  }

  _onChangeThumbnail = (e) => {
    const image = e.target.files[0];
    this.props.updateFormData('image', image);
  }

  _onFocusOutTitle = (e) => {
    const title = e.target.value.trim();
    this.props.updateFormData('title', title);
  }

  _onFocusOutDescription = (e) => {
    const description = e.target.value.trim();
    this.props.updateFormData('description', description);
  }

  _onChangeCategory = (result, condition, recommendTags, prevRecommendTags) => {
    this.props.updateFormData('categoryIdx', {result ,recommendTags});
    this.props.updateFormData('tag', this._tagsFilter(prevRecommendTags));
  }

  _tagsFilter = (prevTags) => {
    const { tags, recommendTags } = this.props
    const tagsTotalLimit = 50;
    const prevRecommendTags = prevTags.split(',');
    const filteringPrevTags = tags.filter(str => prevRecommendTags.indexOf(str) < 0);
    let recommendTagsFilter = recommendTags.filter(str => filteringPrevTags.indexOf(str) < 0);
    const endOfSliceCount = tagsTotalLimit - filteringPrevTags.length;
    if (recommendTagsFilter.length) {
      recommendTagsFilter = recommendTagsFilter.slice(0, endOfSliceCount);
    }
    return [...recommendTagsFilter, ...filteringPrevTags]
  }

  // _onChangeAccountStatus = (e) => {
  //   const accountStatus = e.target.value;
  //   this.props.updateFormData('accountStatus', accountStatus);
  // }
  _getPublicUsers=(users)=>{
    this.props.updateFormData('publicUsers', users);
  }
  _onChangePublicStatus = (e) => {
    const publicRange = e.target.value;
    this.props.updateFormData('publicRange', publicRange);
    if(publicRange==='U'){
      console.log(this.props.publicUsers)
      this.props.openPopup('CHOOSEUSERS',{
        returnType:'returnUsers',
        userKeys: (this.props.publicUsers !=undefined)? this.props.publicUsers:[],
        returnCallBack:this._getPublicUsers
      })
    }
  }

  _onTagAttach = (value) => {
    const tag = value;
    this.props.updateFormData('tag', this.props.tags.concat(tag));
  }

  _onTagDetach = (e) => {
    const tags = this.props.tags;
    tags.pop();
    this.props.updateFormData('tag', tags);
  }

  _onRecommendTagAttach = (e) => {
    e.persist();
    const tag = e.target.innerText.replace(/#/g, "");
    const isExist = this.props.tags.indexOf(tag);
    if (isExist < 0) {
     this.props.updateFormData('tag', this.props.tags.concat(tag));
    }
  }

  _onStepUpdate = (value) => {
    this.props.metadataToTemp();
    this.props.stepUpdate('direct', value);
  }

  _inputValidator = ({ target }) => {
      switch (target.name) {
        case 'title': {
          if (target.value.length > 40) {
            target.value = this.props.title
            toastr.light('', '40자 이내로 입력해주세요.')
          } else {
            this.props.updateFormData('title', target.value);
          }
          break;
        }
        case 'description' : {
          if (target.value.length > 1000) {
            target.value = this.props.description
            toastr.light('', '1,000자 이내로 입력해주세요.')
          } else {
            this.props.updateFormData('description', target.value);
          }
          break;
        }
        default: throw new Error('unexpected name');
      }
  }

  get CategoryName() {
    return this.props.categories.filter(item => item.idx === this.props.categoryIdx)[0].name;
  }

  render() {
    return (
      <div className="enter-information">
      <div className="information-form">
        <div className="edit-thumbnail">
          <img
            src={this.props.thumbnailPreview}
            alt="thumbnail"
            role="presentation"
          />
          <div >
            <input
              ref={input => this.inputThumbnail = input}
              type="file"
              accept="image/*"
              style={styles.file}
              title="이미지 선택"
              onChange={this._onChangeThumbnail}
            />
            {this.props.thumbnailPreview.includes('test.png')
              ? T.translate('knowledge.select-file')
              : T.translate('knowledge.thumbnail-change')}
          </div>
          </div>
          <span className="thumnail-upload-info" style={{
            display: 'block',
            fontSize: '11px',
            fontWeight: 600,
          }}>{(this.props.videoSource.type === 'YOUTUBE' || this.props.videoSource.type === 'LINK') && T.translate('knowledge.thumbnail-description')}</span>
        <form>
        <ul className="form-field">
          <li>
            <label>{T.translate('knowledge.title')}</label>
            <div className="enter-area">
              <input autoFocus
                type="text"
                name="title"
                placeholder={T.translate('knowledge.palceholder40')}
                maxLength="41"
                title="지식 제목"
                onChange={this._inputValidator}
                onBlur={this._onFocusOutTitle}
                defaultValue={this.props.title}
              />
            </div>
          </li>
          <li>
            <label>{T.translate('knowledge.description')}</label>
            <div className="enter-area">
              <textarea
                placeholder={T.translate('knowledge.palceholder1000')}
                maxLength="1001"
                rows="3"
                name="description"
                title="설명"
                onChange={this._inputValidator}
                onBlur={this._onFocusOutDescription}
                defaultValue={this.props.description}
              />
            </div>
          </li>
          <li>
            <label>{T.translate('knowledge.category')}</label>
            <div className="enter-area">
              <CategorySelectBox
              mainCategoryIdx={this.props.mainCategoryIdx}
              smallCategoryIdx={this.props.smallCategoryIdx}
              exportsCategory={this._onChangeCategory}
              />
              <div className="selected-category">
                {this.props.viewVideoCategoryNames.map((name, idx) =>
                  <p key={`category-names-${idx}`} className="depth-group">
                    <span>
                      {this.getCategoryName}
                    </span>
                    <span>
                      {name}
                    </span>
                  </p>
                )}
              </div>
            </div>
          </li>
          <li>
            <label>{T.translate('knowledge.tag')}</label>
            <div className="enter-area">
              <Tags
                data={this.props.tags}
                curData={this.props.recommendTags}
                bindBlurEvent={true}
                onTagAttach={this._onTagAttach}
                onTagDetach={this._onTagDetach}
              />
            </div>
          </li>
          <li>
            {/*
                this.props.adminFlag === 'Y' &&
                <div className="col-2">
                  <label>계정 선택</label>
                  <div className="enter-area">
                    <div className="type-radio">
                      <input
                        type="radio"
                        id="admin"
                        name="account"
                        value="admin"
                        onChange={this._onChangeAccountStatus}
                        checked={this.props.accountStatus === 'admin'}
                      />
                      <label htmlFor="admin">
                        <span />
                        관리자
                      </label>
                    </div>
                    <div className="type-radio">
                      <input
                        type="radio"
                        id="user"
                        name="account"
                        value="user"
                        checked={this.props.accountStatus === 'user'}
                        onChange={this._onChangeAccountStatus}
                      />
                      <label htmlFor="user">
                        <span />
                        개인
                      </label>
                    </div>
                  </div>
                </div>
            */}
            <div className="col-2">
              <label>{T.translate('knowledge.visibility')}</label>
              <div className="enter-area">
                <div className="type-radio">
                  <input
                    type="radio"
                    id="unlock"
                    name="visibility"
                    value="A"
                    onChange={this._onChangePublicStatus}
                    checked={this.props.publicRange === 'A'}
                  />
                  <label htmlFor="unlock">
                    <span />
                    {T.translate('knowledge.visibility-option1')}
                  </label>
                </div>
                <div className="type-radio">
                  <input
                    type="radio"
                    id="lock"
                    name="visibility"
                    value="P"
                    onChange={this._onChangePublicStatus}
                    checked={this.props.publicRange === 'P'}
                  />
                  <label htmlFor="lock">
                    <span />
                    {T.translate('knowledge.visibility-option2')}
                  </label>
                </div>
                <div className="type-radio">
                  <input
                    type="radio"
                    id="lock"
                    name="visibility"
                    value="U"
                    onChange={this._onChangePublicStatus}
                    onClick={this._onChangePublicStatus}
                    checked={this.props.publicRange === 'U'}
                  />
                  <label htmlFor="lock">
                    <span />
                    {T.translate('knowledge.visibility-option3')}
                  </label>
                </div>
              </div>
            </div>
          </li>
        </ul>
        </form>
        <div className="add-knowledge">
          <div className="add-mode">
            <div className="add-button section-divide">
              <button onClick={() => this._onStepUpdate(2.1)} />
              <p>
                {T.translate('knowledge.divide-section')} <mark>{this.props.sectionsCount}</mark>
              </p>
            </div>
            <div className="add-button knowledge">
              <button onClick={() => this._onStepUpdate(2.2)} />
              <p>
                {T.translate('knowledge.add-knowledge')} <mark>{this.props.stacksCount}</mark>
              </p>
            </div>
            <div className="add-button add-event">
              <button onClick={() => this._onStepUpdate(2.3)} />
              <p>
                {T.translate('knowledge.add-event')} <mark>{this.props.eventsCount}</mark>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MetadataRegister));
