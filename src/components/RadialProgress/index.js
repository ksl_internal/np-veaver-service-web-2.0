import React from 'react';
import cx from 'classnames';

const RadialProgress = ({ percentage }) =>
      <div 
        className={cx('radial-progress', { complete: percentage === 100 })} 
        data-progress={percentage}>
          <div className="circle">
            <div className="mask full">
              <div className="fill"></div>
            </div>
            <div className="mask half">
              <div className="fill"></div>
              <div className="fill fix"></div>
            </div>
          </div>
          <div className="inset"></div>
          <div className="success">
            <span></span>
          </div>
      </div>;

export default RadialProgress;
