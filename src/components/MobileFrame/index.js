import React from 'react';

const styles = {
  root: {
    minHeight: '100%',
    background: '#325aed',
  },
  img1: {
    display: 'block',
    margin: '13.333vw auto 6vw auto',
    width: '31.466vw'
  },
  p: {
    fontSize: '4.266vw',
    lineHeight: '5.6vw',
    color: '#fff',
    textAlign: 'center',
    fontWeight: 300,
  },
  img2: {
    display: 'block',
    margin: '19.333vw auto',
    width: '46.666vw'
  },
  a: {
    display: 'block',
    margin: '0 auto',
    fontSize: '4vw',
    lineHeight: '12.266vw',
    height: '12.266vw',
    width: '90vw',
    borderRadius: '6.133vw',
    background: '#fff',
    color: '#325aed',
    textAlign: 'center',
    fontWeight: 300,
  }
}

const MobileFrame = ({ marketPath }) => 
<div style={styles.root}>
<img style={styles.img1} src="/images/veaver_logo.png" alt="" />
<p style={styles.p}>해당 페이지는 모바일에서 이용할 수 없습니다.<br/>모바일 앱을 통해 이용해주세요.</p>
<img style={styles.img2} src="/images/veaver_img.png" alt=""/>
	<a style={styles.a} href={marketPath} target="_blank">앱 다운로드</a>
</div>


export default MobileFrame;