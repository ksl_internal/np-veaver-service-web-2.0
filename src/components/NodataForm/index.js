import React, { Component } from 'react';
import T from "i18n-react";

class NodataForm extends Component {

  render() {
    return(
      <div className="nodata-wrapper">
        <div className="nodata">
          <img src="/images/nodata.png" alt="Has no data" />
          <p>
            {T.translate('common.no-result')}
          </p>
        </div>
      </div>
    );
  }
}

export default NodataForm;
