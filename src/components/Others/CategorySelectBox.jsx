import React from 'react'
import PropTypes from 'prop-types'
import { axios } from '../../util/api'

const exceptionHandler = (err) => console.error(err)

const initList = []

export default class CategorySelectBox extends React.Component {
  static propTypes = {
    mainCategoryIdx: PropTypes.number,
    smallCategoryIdx: PropTypes.number,
    addClassName: PropTypes.oneOfType([ PropTypes.string, PropTypes.array ]),
    searchMode: PropTypes.bool,
    exportsCategory: PropTypes.func.isRequired,
  }

  static defaultProps = {
    mainCategoryIdx: 0,
    smallCategoryIdx: 0,
    addClassName: '',
    searchMode: false,
    exportsCategory: () => {},
  }

  state = {
    mainCategories: initList,
    smallCategories: initList,
    mainCategoryIdx: 0,
    smallCategoryIdx: 0,
    exportCategoryCondition: false,
    isReceiveToCategoryIdxProps: false,
    prevRecommandTags: '',
    recommandTags: '',
  }

  shouldComponentUpdate(nextProps) {
    const { mainCategoryIdx, smallCategoryIdx, searchMode } = nextProps
    const { isReceiveToCategoryIdxProps } = this.state
    if (mainCategoryIdx && !isReceiveToCategoryIdxProps) {
      this.setState(state => ({
        ...state,
        mainCategoryIdx, 
        smallCategoryIdx,
        exportCategoryCondition: searchMode,
        isReceiveToCategoryIdxProps: true,
      }))
      this.onFetchCategories(this.mainCategoriesCallback, mainCategoryIdx)
    }
    return true
  }

  componentDidMount() {
    this.onFetchCategories(this.initFetchCallback)
  }
  
  onFetchCategories = (callback, categoryIdx = 0) => {
    const url = categoryIdx ? `/videos/view-categories/${categoryIdx}` : '/videos/view-categories'
    return axios.get(url,{ params: { makeFlag: 'Y' }})
    .then(res => callback(res, categoryIdx))
    .catch(exceptionHandler)
  }

  initFetchCallback = ({ data }) => {
    const { mainViewCategories } = data
    if (!this.props.mainCategoryIdx) {
      const dyObj = {};
      dyObj.mainCategoryIdx = mainViewCategories[0].idx;
      dyObj.prevRecommandTags = mainViewCategories[0].tag;
      dyObj.recommandTags =  mainViewCategories[0].tag;

      this.setState(state => ({
        ...state,
        mainCategories: mainViewCategories,
        ...dyObj,
      }), () => this.onFetchCategories(this.mainCategoriesCallback, dyObj.mainCategoryIdx).then(() => {
        this.exportsMetadata();
      }));

    } else {
      this.setState(state => ({
        ...state,
        mainCategories: mainViewCategories,
      }));
    }

  }

  mainCategoriesCallback = ({ data }, mainCategoryIdx) => {
    const { subViewCategories } = data
    const { mainCategories } = this.state
    let recommandTags = ''
    let smallCategoryIdx = 0
    if (mainCategoryIdx && !subViewCategories.length) {
      recommandTags = mainCategories.filter(item => item.idx === mainCategoryIdx)[0].tag
    } else if (subViewCategories.length) {
      smallCategoryIdx = subViewCategories[0].idx
      recommandTags = subViewCategories[0].tag
    }
    const prevRecommandTags = `${this.state.recommandTags}`
    this.setState(state => ({
        ...state,
       smallCategories: mainCategoryIdx ? subViewCategories : initList,
       mainCategoryIdx,
       recommandTags,
       prevRecommandTags,
       smallCategoryIdx,
    }))
  }

  smallCategoriesCallback = ({ data }, smallCategoryIdx) => {
    const { smallCategories } = this.state
    let recommandTags = ''
    const prevRecommandTags = `${this.state.recommandTags}`
    if (smallCategories.length) {
      recommandTags = (smallCategoryIdx && smallCategories.length)? smallCategories.filter(item => item.idx === smallCategoryIdx)[0].tag : ''
    }
    this.setState(state => ({
        ...state,
        smallCategoryIdx,
        recommandTags,
        prevRecommandTags,
    }))
  }

  onSelectMainCategory = ({ target }) => {
    const mainCategoryIdx = this.getIdx(target.value)
    this.setState(state => ({...state, mainCategoryIdx }))
    this.onFetchCategories(this.mainCategoriesCallback, mainCategoryIdx)
    .then(this.validateCategory)
    .then(this.exportsMetadata)
    .catch(exceptionHandler)
  }

  onSelectSmallCategory = ({ target }) => {
    const smallCategoryIdx = this.getIdx(target.value)
    this.setState(state => ({...state, smallCategoryIdx }))
    this.onFetchCategories(this.smallCategoriesCallback, smallCategoryIdx)
    .then(this.validateCategory)
    .then(this.exportsMetadata)
    .catch(exceptionHandler)
  }

  validateCategory = () => {
    const { smallCategories, smallCategoryIdx, mainCategoryIdx } = this.state
    const { searchMode } = this.props
    const hasSmallCategories = smallCategories.length
    let exportCategoryCondition = true
    if ((!searchMode && !mainCategoryIdx) || (hasSmallCategories && !smallCategoryIdx)) {
      exportCategoryCondition = false
    }
    this.setState(state => ({...state, exportCategoryCondition }))
  }

  exportsMetadata = () => {
    const { mainCategoryIdx, smallCategoryIdx, exportCategoryCondition, recommandTags, prevRecommandTags } = this.state
    const categoryIdx = smallCategoryIdx ? smallCategoryIdx: mainCategoryIdx
    this.props.exportsCategory({categoryIdx, mainCategoryIdx, smallCategoryIdx }, exportCategoryCondition, recommandTags, prevRecommandTags)
  }
  
  get renderMainCategories() {
    const { mainCategories } = this.state
    return this.iteratingCategories(mainCategories)
  }
  
  get renderSmallCategories() {
    const { smallCategories } = this.state
    return this.iteratingCategories(smallCategories)
  }
  
  iteratingCategories = (categories) => {
    const isEmptyCategories = categories.length
    return  !isEmptyCategories ? '' : categories.map(this.makeOption)
  }
  
  makeOption (category) {
    return (<option
      key={`category-option-${category.idx}`}
      value={category.idx}
      >{category.name}</option>)
  }

  getIdx = (str) => ['string', 'number'].some(type => typeof str === type) ? +str : new Error('not expected categoryIdx type. please check this categoryIdx.')

  render() {
    const smallCategoryDisabledCondition = this.state.smallCategories.length
    const { mainCategoryIdx, smallCategoryIdx } = this.state
    const { addClassName } = this.props
    const defaultClassName = 'form-control input-sm'.concat(` ${addClassName}`)

    return (
        [<select
          name="mainCategory"
          key="mainCategory"
          style={{ width: '49%', marginRight: '1%' }}
          className={defaultClassName}
          title="대분류 카테고리 선택"
          value={mainCategoryIdx}
          onChange={this.onSelectMainCategory}>
          <option
            value={0} 
            key="main-category-option-first"
            disabled={true}
          >카테고리 선택</option>
            {this.renderMainCategories}
        </select>,
        <select
          name="smallCategory"
          key="smallCategory"
          style={{width: '50%'}}
          className={defaultClassName}
          title="소분류 카테고리 선택"
          disabled={!smallCategoryDisabledCondition}
          value={smallCategoryIdx}
          onChange={this.onSelectSmallCategory}>
          <option
            value={0} 
            key="small-category-option-first"
            disabled={true}
          >-</option>
            {this.renderSmallCategories}
        </select>]
    )
  }
}
