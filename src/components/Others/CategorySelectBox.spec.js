import React from 'react';
import { shallow, mount, render } from 'enzyme';
import CategorySelectBox from './CategorySelectBox';

/**
 * 해당 컴포넌트는 3가지 경우에, 재사용을 합니다.
 * 1. 영상을 업로드 할때 필수 값으로 카테고리를 선택해야 합니다.
 * 2. 영상을 수정 할때 영상의 기본 카테고리 값을 셋하고, 선택해야 합니다.
 * 3. 전체 영상을 검색 할때 카테고리를 선택해야 합니다.
 * @description
 * 1. 대분류 카테고리 목록을 호출 할 수 있다.
 * 2. 소분류 카테고리 목록을 호출 할 수 있다.
 * 3. 대분류 카테고리를 선택 할 수 있다.
 * 4. 소분류 카테고리를 선택 할 수 있다.
 * 5. 대분류 에서 makeFlag 가 true 일때, 컨텐츠 프로바이더의 카테고리를 목록에서 제외 시킨다.
 * 6. 대분류 에서 categoryIdx 는 소분류 카테고리 목록을 조회 하는 키의 역할을 한다.
 * 7. 대분류의 선택결과 호출된 소분류 카테고리 목록이 존재 하면, 선택된 카테고리의 값은 반드시 소분류 카테고리 키 값이 되어야 한다.
 * 8. 영상을 검색할때 searchMode 는 true 가 되며, 대분류 카테고리의 전체 검색을 사용 할 수 있다.
 * 9. 영상을 수정할때 props 로 받은 대분류와 소분류 카테고리 키값이 존재하면, 해당 키값으로 선택값을 설정 할수있다.
 * 10. addClassName 을 통해 select 태그의 클래스 명을 추가 할 수 있다. (배열 형식도 가능 하지만, 추가 처리 안해놓음)
 * 11. exportsCategory 는 필수 값이며 함수이다.
 * 12. exportsCategory 는 최종 선택된 카테고리의 키값과, 조건에 부합한지여부를 반환 한다.
 * 13. exportsCategory 는 카테고리 선택에 변화가 있을때 최종적으로 트리거 된다.
 * 14. api 에 대한 mock 객체의 형태는 다음과 같다.
 {
   data: {
     list: [
{idx: 1, name: "전사공지1", tag: "전사,공지"},
{idx: 2, name: "전사공지2", tag: "전사,공지"},
{idx: 3, name: "전사공지3", tag: "전사,공지"},
{idx: 4, name: "전사공지4", tag: "전사,공지"},
{idx: 5, name: "전사공지5", tag: "전사,공지"}]
   }
 }
 15. 
 */

describe('<CategorySelectBox /> 컴포넌트 초기값을 확인 한다.', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(<CategorySelectBox />);
  });

   test('init defaultProps', () => {
    const defaultProps = {
      mainCategoryIdx: 0,
      smallCategoryIdx: 0,
      addClassName: '',
      searchMode: false,
      exportsCategory: () => {},
    };

    expect(JSON.stringify(wrapper.props())).toEqual(JSON.stringify(defaultProps));
   });

   test('init state', () => {
     const initState = {
      mainCategories: [],
      smallCategories: [],
      mainCategoryIdx: 0,
      smallCategoryIdx: 0,
      exportCategoryCondition: false,
      isReceiveToCategoryIdxProps: false,
     }

     expect(wrapper.state()).toEqual(initState);
   });

   test.skip('maybe wrong propTypes', () => {
    // init propTypes 체크 해봐야함.
    console.error = jest.fn();
    const defaultProps = {
      mainCategoryIdx: '0',
      smallCategoryIdx: '0',
      addClassName: false,
      searchMode: 'wrong',
      exportsCategory: false,
    };     
     const eComponent = shallow(<CategorySelectBox {...defaultProps} />);
     console.error.mockClear();
   });
});

describe('<CategorySelectBox /> 함수를 테스트 한다.', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<CategorySelectBox />);
  });

  test('iteratingCategories 함수를 테스트 한다.', () => {
    const wrapperInstance = wrapper.instance();
    const categoryObject = [{ idx: 0, name: '카테고리1' }, { idx: 0, name: '카테고리2' }];
    expect.assertions(2)
    const options = wrapperInstance.iteratingCategories(categoryObject);
    const emptyOptions = wrapperInstance.iteratingCategories([]);
    expect(options.length).toBe(2)
    expect(emptyOptions).toBe('');
  })

  test('makeOption 함수를 테스트 한다.', () => {
    const wrapperInstance = wrapper.instance();
    const categoryObject = { idx: 0, name: '카테고리' };
    const option = wrapperInstance.makeOption(categoryObject);
    expect(option.type).toBe('option');
    expect(option.key).toBe('category-option-0');
    expect(option.props.children).toEqual(categoryObject.name);
  });
  
  test('getIdx 함수를 테스트 한다.', () => {
    const wrapperInstance = wrapper.instance();
    const error = new Error('not expected categoryIdx type. please check this categoryIdx.');
    expect(wrapperInstance.getIdx(1)).toEqual(1);
    expect(wrapperInstance.getIdx('1')).toEqual(1);
    expect(wrapperInstance.getIdx({ number: 1 })).toEqual(error);
    expect(wrapperInstance.getIdx([1])).toEqual(error);
  });
})
 
