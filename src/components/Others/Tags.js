import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { toastr } from "react-redux-toastr";
import T from "i18n-react";
// import moment from 'moment';
/**
 * @export
 * @class Tags
 * @extends {Component}
 * @prop @method {onTagDetach} 태그를 삭제한다.
 * @prop @method {onTagAttach} 태그를 추가한다.
 * @method {toString} 태그 배열을 문자열로 반환한다.
 * @method {onTagValidation} 태그 입력을 평가 한다.
 * @prop tagLimitEach={number} 태그를 최대 몇개까지 추가 할수 있는지 제한 한다.
 * @prop tagTextLimit={number} 태그 문자열 길이를 제한 한다.
 * @prop data={String} 문자열 or 배열을 받는다. willMount 에서 split 한다.
 * @prop curData={String} 커리큘럼 태그 문자열을 받는다.
 * @prop eventKeys={["Enter", " ", ",", 13, 32, 188]} 어떤 키보드 이벤트에 반응할지 키값을 지정한다.
 * @prop bindBlurEvent={true} 포커스 아웃될때 반응하는 이벤트를 추가 한다.
 */
export default class Tags extends Component {
  static defaultProps = {
    data: '',
    curData: [],
    tagLimitEach: 50,
    tagTextLimit: 10,
    eventKeys: ['Enter', ' ', ',', 13, 32, 188],
    bindBlurEvent: false,
    onTagAttach: () => {},
    onTagDetach: () => {},
  }

  static propTypes = {
    data: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    curData: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    tagLimitEach: PropTypes.number,
    tagTextLimit: PropTypes.number,
    eventKeys: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
    bindBlurEvent: PropTypes.bool,
    onTagDetach: PropTypes.func,
    onTagAttach: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      tags: [],
      backspaceCount: 0,
    };
    this.onTypingTags = this.onTypingTags.bind(this);
  }

  componentWillMount() {
    this.setState({
      tags: Array.isArray(this.props.data) ? this.props.data : this.props.data.split(','),
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ tags: Array.isArray(nextProps.data) ? nextProps.data : nextProps.data.split(',') });
  }

  onTypingTags(e) {
    const {value} = e.target
    const key = e.key || e.keyCode;
    const state = Object.assign({}, this.state);

    if (value.length > this.props.tagTextLimit) {
      this.inputText.value = value.substring(0, 10)
      toastr.light('', '10자 이내로 입력해주세요.')
    }

    if (this.onTypingEnter(key) && value.length !== 0) {
      if (this.onTagValidation(e.target, key)) {
        this.props.onTagAttach(value.replace(/#|\s|,/g, ''));
        this.inputText.value = '';
        this.setState(state);
      }
    } else if (this.onTypingBackspace(key)) {
      if (value === '') state.backspaceCount += 1;
      if (state.backspaceCount > 1) {
        this.props.onTagDetach();
        state.backspaceCount = 0;
      }
      this.setState(state);
    }
    e.preventDefault();
    e.stopPropagation();
  }

  onTypingEnter(key) {
    return this.props.eventKeys.some(elm => (elm === key));
  }

  onTypingBackspace = (key) => {
    return key === ('Backspace' || 8);
  }

  onTagValidation(target, key) {
    
    if (this.state.tags.length >= this.props.tagLimitEach) {
      alert(`태그는 최대 ${this.props.tagLimitEach}개까지 등록할 수 있습니다.`);
      //swal(Object.assign(swalStandard, { text: `태그는 최대 ${this.props.tagLimitEach}개까지 등록할 수 있습니다.` }));
      return false;
    } else if (target.value.length > this.props.tagTextLimit) {
      alert(`태그는 한글 기준 ${this.props.tagTextLimit}자 이내로 입력해주세요.`);
      
      //swal(Object.assign(swalStandard, { text: `태그는 한글 기준 ${this.props.tagTextLimit}자 이내로 입력해주세요.` }));
      return false;
    } else if (target.value.replace(/#|\s|,/g, '') === '') {
      alert('태그에 빈값이 들어갈수 없습니다.');
      //swal(Object.assign(swalStandard, { text: '태그에 빈값이 들어갈수 없습니다.' }));
      return false;
    } else if (this.state.tags.some(elm => elm === target.value.replace(/#|\s|,/g, ''))) {
      alert('중복된 태그 입니다.');
      //swal(Object.assign(swalStandard, { text: '중복된 태그 입니다.' }));
      return false;
    } else if (this.props.curData.length > 0 && this.props.curData.some(elm => elm === target.value.replace(/#|\s|,/g, ''))) {
      alert('관리자만 사용할수있는 태그입니다');
      //swal(Object.assign(swalStandard, { text: '관리자만 사용할수있는 태그입니다' }));
      return false;
    }
    return true;
  }

  onFocusOut(e) {
    const targetValue = e.target.value.trim().replace(/#/g, '');
    const condition = [(targetValue !== '' && targetValue !== ','),
      this.state.tags.length < this.props.tagLimitEach,
      targetValue.length <= this.props.tagTextLimit,
      !this.state.tags.some(elm => elm === targetValue),
      !this.props.curData.some(elm => elm === targetValue)];
    const isPassed = condition.every(elm => elm);

    e.target.value = '';
    if (isPassed) {
      this.props.onTagAttach(targetValue);
    }
  }

  render() {
    const bindEvent = {
      onKeyUp: e => this.onTypingTags(e),
    };
    if (this.props.bindBlurEvent) bindEvent.onBlur = e => this.onFocusOut(e);

    return (
      <div className="tag-input">
      <label className="tags" htmlFor="tags">
      {
        this.state.tags.map((tag, idx) =>
         (<span className="tag-value" key={`tag-value-${idx}`}>#{tag}
         {(this.state.tags.length - 1) === idx ? '' : ','}</span>))
      }
        <input
          style={{
            minWidth: '150px',
          }}
          ref={(ref) => { this.inputText = ref }}
          id="tags"
          type="text"
          placeholder={T.translate('make.hash-tag')}
          size="10"
          pattern={"[A-Za-z0-9]{10}"}
          {...bindEvent}
        />
      </label>
      </div>
    );
  }
}
