import React, { Component , Fragment} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import T from 'i18n-react';


/**
 * 
 * 모든 페이지 우측 상단 버튼 뷰입니다.
 */
class TimelineButtonForm extends Component{
    //해당 선택된 아이템을 수정하여 부모 뷰로 전달 합니다.
    //selectedItems.isChanging true / false  여부로 모든 카드 뷰에 체크 박스 표시를 변경합니다.
    //selectedItems.idx 현재 선택된 id 값만 들어있으며 originObj 에 해당 객체 가 들어갑니다.
    handleToggle = () => {
        const { selectedItems ,folder=false} = this.props;
        if(selectedItems.isChanging){
            selectedItems.idx=[]
            selectedItems.originObj=[]
            selectedItems.isChanging = false
            this.props.updateCallback(selectedItems)
        }else{
            selectedItems.isChanging = true
            this.props.updateCallback(selectedItems)
        }
    }
    render (){
        const {buttonData={default:{openButton:T.translate('common.select'),closeButton:T.translate('common.cancel')},pageButton:[]}, selectedItems ,disabled=false} = this.props;
        //전달 받은 버튼 객체별로 뷰와 클릭 이벤트를 설정합니다.
        const buttonView = buttonData.pageButton.map( (item,index)=>{
            return (
                <button key={index} className={`${item.className} ${item.lengthCheck !== undefined || selectedItems.idx.length >0 ? "active" : ''}`} onClick={(e)=>{
                    if(item.lengthCheck == undefined && this.props.selectedItems.idx.length==0){
                        return false
                    }
                    item.fucOnClick(e,selectedItems)
                } }>{item.name}</button>
            )
        })
        return (
            <div className="control-btn">
                {
                    (this.props.selectedItems.isChanging) ?
                        <Fragment>
                            {buttonView}
                            <button className="btn active" onClick={this.handleToggle}>{buttonData.default.closeButton}</button>
                        </Fragment>
                    :  <button className={`btn ${!disabled?'active':''}`} onClick={this.handleToggle} disabled={disabled} >{buttonData.default.openButton}</button>
                }
            </div>
        );
    }

}
export default connect(
    (state) => ({
        userProfile: state.user.userProfile,
        uploadVideoStatus: state.make.uploadVideoStatus,
    }),
    (dispatch) => ({
        //MyPageActionCreators: bindActionCreators(MyPageActionCreators,dispatch),
        //OpenPopup: bindActionCreators(openPopup,dispatch),
        //FolderActions : bindActionCreators(folderActions,dispatch),
    })
)(withRouter(TimelineButtonForm));
