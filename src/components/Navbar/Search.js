import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter , NavLink} from "react-router-dom";
import { openPopup }  from "../../modules/popup";
import * as searchActions  from "../../modules/searchM";
import T from 'i18n-react';
import queryString from 'stringquery'
import MenuTree from "./MenuTree";
/**
 * 검색 페이지 LNB 항목입니다
 */
class Search extends Component{
    state = {
        categories:[  // 현재 페이지에 나타낼 카테고리들 입니다. 최상단 idx:0 , depth:0 으로 구분 하고 하위 카테고리는 각각 서버에 전부 요청해서 가져옵니다. 
            {
                idx:0,
                name : '지식명',
                languagePack:'search_result.knowledge-name',
                depth:0,
                isOpen:false,
                selected:false,
                timelineCount:0,
                type:'timeline'
              },
              {
                idx:0,
                name : '섹션',
                languagePack:'search_result.section',
                depth:0,
                isOpen:false,
                selected:false,
                timelineCount:0,
                type:'section'
              },
              {
                idx:0,
                name : '덧지식',
                languagePack:'search_result.additional-knowledge',
                depth:0,
                isOpen:false,
                selected:false,
                timelineCount:0,
                type:'stack'
              },
              {
                idx:0,
                name : '태그',
                languagePack:'search_result.tag',
                depth:0,
                isOpen:false,
                selected:false,
                timelineCount:0,
                type:'tag'
              },
              {
                idx:0,
                name : '사용자',
                languagePack:'common.user',
                depth:0,
                isOpen:false,
                selected:false,
                timelineCount:0,
                type:'users'
              }
        ]
    }
    componentDidMount(){
        this.fetchData()
    }
    // 페이지에 나타낼 모든 카테고리를 서버에 요청하여 가져옵니다.
    // 이유는 하위 카데고리가 있는지 여부를 서버에서 알려주지 않기 때문에 (요구 사항. 하위 카테고리가 있으면 화살표 표시 없으면 표시하지 않음)
    // 전부 요청후 하위 카테고리를 가지고 있는지 검사하여 뷰를 나타냅니다.
    fetchData = async () =>{
        const { SearchActions, location} = this.props
        const values = queryString(location.search)
        const keyword = values.keyword
        try {
            // 1뎁스 카테고리를 가져옵니다.
            await this.props.SearchActions.getSearchCategoriesUsers({keyword:keyword,orderType: 'L',});
            await this.props.SearchActions.getSearchCategoriesTimeline({
                id: "timeline",
                keyword: keyword,
            });
            await this.props.SearchActions.getSearchCategoriesSection({
                id: "section",
                keyword: keyword,
            });
            await this.props.SearchActions.getSearchCategoriesStack({
                id: "stack",
                keyword: keyword,
            });
            await this.props.SearchActions.getSearchTag({
                id: "tag",
                keyword: keyword,
            });
            // 2뎁스 카테고리를 가져옵니다
            this.initCategory(this.props.timelineCategory , keyword)
            this.initCategory(this.props.sectionCategory , keyword)
            this.initCategory(this.props.stackCategory , keyword)
            this.initCategory(this.props.tagCategory , keyword)
            this.initCategory(this.props.usersCategory , keyword)
        } catch (error) {
            console.log(error)
        }
    }
    // 1차 뎁스 카테고리를 찾아 2차 뎁스를 자식요소로 세팅합니다.
    initCategory = async (category , _keyword) => {
        try {
            const {SearchActions} = this.props
            let timelineCount = 0;
            let list=[];
            if(category.type ==='tag'){
                list = category.children
                category.children.map(o=>{
                    timelineCount= timelineCount+o.count
                })
            }else if (category.type==='users'){
                timelineCount = category.userCount
            }else{
                if(category.children !== undefined){
                    const promises = category.children.map(async obj => {
                        timelineCount= timelineCount+obj.count
                        let query ={
                            id:category.type,
                            keyword:_keyword
                        }
                        if(obj.idx > 0){
                            query.categoryIdx = obj.idx
                        }
                        await SearchActions.getSearchCategoriesChildren(query);
                        const { child } = this.props
                        obj.children = child
                        return obj
                    }); 
                    list = await Promise.all(promises); 
                }
            }
            let newList= this.state.categories.map(root=>{
                if(root.type=== category.type){
                    root.children = list
                    root.timelineCount = timelineCount
                }
                return root
            })
            this.setState({ categories: Object.assign([],newList) })
        } catch (error) {
            console.log(error)
        }
        
    }
    //모든 카테고리를 닫힘 isOpne false, 선택여부 selected false  시킵니다
    clearCategory(){
        function updateList(list){
            return  list.map(o=>{
                o.selected = false
                o.isOpen = false
                if(o.children !== undefined && o.children.length > 0){
                    o.children = updateList(o.children)
                }
                return o
            })
        }
        const modifiedArray = updateList(this.state.categories)
        this.setState({ categories: modifiedArray })
    }
    //해당 카테고리를 선택했을 경우 열림과 타임라인을 가져옵니다.
    selectedCategory =(item)=>{
        if(item.depth == 0){ // 최상단 루트일 경우.
            this.clearCategory();
            const modifiedArray =this.state.categories.map(o=>{
                if(o.name == item.name){
                    o.selected = !o.selected
                    o.isOpen = !o.isOpen
                }else{
                    o.isOpen = false
                    o.selected  =false;
                }
                return o
            })
            this.setState({ categories: modifiedArray })
        }else{
            function updateList(list , obj){
                return  list.map(o=>{
                    if(o.idx > 0){ 
                        if(o.isOpen){ // 최상단 루트이면서 현재 열려있는 페이지인지 검사하는 이유는 최상단에 포함된 카테고리들이 중복됨니다. 예를들면 지식명 하위와 섹션 하위가 같은 카테고리들이 중복됩니다.

                            if(o.name == obj.name){ //idx 값으로 비교하지 않은 이유는 태그 항목은 클릭했을 경우 idx 값으로 비교 불가 합니다.
                                o = obj
                            }else {
                                o.selected = false;
                               //하위를 가지고 있는 카테고리 
                                if(o.children !== undefined && o.children.length > 0){
                                    //현재 선택한 카테고리의 하위가 있을경우 모두 닫는다.
                                    if(obj.children !== undefined && obj.children.length > 0){
                                        o.isOpen = false
                                    }else{ //현재 선택한 카테고리가 하위가 없는 마지막 노드일 경우.
                                        if(!o.children.some(idx=>idx.idx===obj.idx)){
                                            o.isOpen = false
                                        }
                                    }
                                }else{ //하위가 는 카테고리기 때문에 전부 닫는다.
                                    o.isOpen = false
                                }
                            }
                            if(o.children !== undefined && o.children.length > 0){
                                o.children = updateList(o.children, obj)
                            }
                        }
                    }else{
                        if(o.children !== undefined && o.children.length > 0){
                            o.children = updateList(o.children, obj)
                        }
                        o.selected = false
                    }
                    return o
                })
            }
            item.isOpen = !item.isOpen
            item.selected =  true
            const modifiedArray = updateList(this.state.categories , item)
            this.setState({ categories: modifiedArray })
        }
        const { SearchActions ,location , keyword,orderType , match} = this.props;
        let query = {
            keyword: keyword,
            orderType: 'R',
            categoryIdx:item.idx
        }
        if(item.idx === 0){ //최상단 루트일경우 서브카테고리 조회 하지 않습니다.
            delete query['categoryIdx']
        }
        if (item.type === 'timeline' ){
            SearchActions.getSearchTimelines(query);
        }else if (item.type === 'section'){
            SearchActions.getSearchTimelinesSection(query);
        }else if (item.type === 'stack'){
            SearchActions.getSearchTimelinesStack(query);
        }else if (item.type === 'tag'){
            //태그 일경우 idx 값이 없습니다. 현재 태그 이름으로 타임라인을 검색합니다.
            let key = item.name
            if(item.idx===0){
                key = keyword
            }
            SearchActions.getSearchTimelinesTag({
                keyword: key,
                orderType: 'R'
              });
        }else if (item.type === 'users'){
            SearchActions.getSearchUsers({keyword:keyword,orderType: 'L',});
        }
    }
    
    render(){
        const {handleToggle,root} = this.props;
        if(root == null) return null
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="main-content-left clearfix">
                <div className="right">
                    <div className="page-title">
                        <h1 className="page-heading">Search</h1>
                        <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" className="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            }
                        </div>
                    </div>
                    <div className="heading-category">{T.translate('search_result.category')}</div>
                        <div className="wrap-list-menu nav_sub_scroll scrollbarStyle1" style={{height:'calc(100vh - 125px)'}}>
                            <ul className="list-menu">
                            <MenuTree data={this.state.categories} handleOnClickTimelines={this.selectedCategory}></MenuTree>         
                            </ul>
                        </div>
                    </div>
            </div>
        );
    }
}
export default withRouter(connect(
    state => ({
        root: state.searchM.get("root"),
        keyword: state.searchM.get("keyword"),
        orderType: state.searchM.get("orderType"),

        timelineCategory: state.searchM.get("timelineCategory"),
        sectionCategory :  state.searchM.get("sectionCategory"),
        stackCategory :  state.searchM.get("stackCategory"),
        tagCategory :  state.searchM.get("tagCategory"),
        usersCategory :  state.searchM.get("usersCategory"),
        child : state.searchM.get("child"),
    }),
    dispatch => ({
        SearchActions: bindActionCreators(searchActions, dispatch),
    })
  )(Search));
