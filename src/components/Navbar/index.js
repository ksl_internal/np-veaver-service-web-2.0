import Play from './Play';
import Make from './Make';
import Edu from './Edu';
import Folder from './Folder';
import Search from './Search';
import Profile from './Profile';
import Noti from './Noti';



export {
    Play,
    Make,
    Edu,
    Folder,
    Search,
    Profile,
    Noti
}
