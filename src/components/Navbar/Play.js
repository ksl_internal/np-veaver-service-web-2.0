import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter , NavLink} from "react-router-dom";
import * as videoActions from '../../modules/video'
import { getTimelines } from "../../modules/mypage";
import T from 'i18n-react';
import { isAssociative } from "immutable";

import MenuTree from "./MenuTree";
class Play extends Component{
    state = {
        categories:[],
        all:true
    }
    componentDidMount(){
        this.fetchData()
    }
    fetchData = async () =>{
        try {
            const { VideoActions} = this.props
            //1차 카테고리를 가져옵니다.
            await VideoActions.getVideoViewCategories({withTimelineCountFlag:'Y'});
            //2차 카테고리를 가져옵니다.
            this.getChildren()
        } catch (error) {
            console.log(error)
        }
    }
    //2차 카테고리를 가져온후 1차 카테고리 자식요소로 세팅합니다.
    // 페이지에 나타낼 모든 카테고리를 서버에 요청하여 가져옵니다.
    // 이유는 하위 카데고리가 있는지 여부를 서버에서 알려주지 않기 때문에 (요구 사항. 하위 카테고리가 있으면 화살표 표시 없으면 표시하지 않음)
    // 전부 요청후 하위 카테고리를 가지고 있는지 검사하여 뷰를 나타냅니다.
    getChildren = async () =>{
        const { root, VideoActions} = this.props
        const promises = root.map(async obj => {
            await VideoActions.getVideoViewCategoriesId({id:obj.idx,withTimelineCountFlag:'Y'});
            const { child } = this.props
            obj.children = child
            return obj
        }); 
        const list = await Promise.all(promises); 
        this.setState({ categories: Object.assign([],list) })
    }
    //해당 카테고리를 선택했을 경우 열림과 타임라인을 가져옵니다.
    selectedCategory =(item)=>{
        if(item.idx===0){ // 선택한 카테고리가 전체 일경우.
            function updateList(list , obj){
                return  list.map(o=>{
                    if(o.children !== undefined && o.children.length > 0){
                        o.children = updateList(o.children, item)
                    }
                    o.selected = false
                    return o
                })
            }
            const modifiedArray = updateList(this.state.categories , item)
            this.setState({ categories: modifiedArray ,all:true})
            this.props.getTimelines(false,0)
        }else{
            function updateList(list , obj){
                return  list.map(o=>{
                    if(o.idx == obj.idx){
                        o = obj
                    }else {
                        o.selected = false;
                        //하위를 가지고 있는 카테고리 
                        if(o.children !== undefined && o.children.length > 0){
                            //현재 선택한 카테고리의 하위가 있을경우 모두 닫는다.
                            if(obj.children !== undefined && obj.children.length > 0){
                                o.isOpen = false
                            }else{ //현재 선택한 카테고리가 하위가 없는 마지막 노드일 경우.
                                if(!o.children.some(idx=>idx.idx===obj.idx)){
                                    o.isOpen = false
                                }
                            }
                        }else{ //하위가 는 카테고리기 때문에 전부 닫는다.
                            o.isOpen = false
                        }
                    }
                    if(o.children !== undefined && o.children.length > 0){
                        o.children = updateList(o.children, obj)
                    }
                    return o
                })
            }
            item.isOpen = !item.isOpen
            item.selected =  true
            const modifiedArray = updateList(this.state.categories , item)
            this.setState({ categories: modifiedArray ,all:false})
            this.props.getTimelines(false,item.idx)
        }
    }
    isMainUrl = () =>{
        const {location} =this.props;
        if(location.pathname ==='/')
            return true;
        else
            return false;
    }
    childrenData = async (id)=>{
        const { VideoActions} = this.props
        await VideoActions.getVideoViewCategoriesId({id:id,withTimelineCountFlag:'Y'});
    }
    isActive(path){
        const {location} = this.props;
        if(location.pathname === path){
            return 'active'
        }
    }
    render(){
        const {handleToggle } = this.props;
        let totalCount = 0;
       this.state.categories.map( (parent,index) =>{
            totalCount+=parent.timelineCount
        })
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
          <div className="main-content-left clearfix">
              <div className="right">
                {this.props.isOpen!==true && null}
                  <div className="page-title">
                      <h1 className="page-heading">Play</h1>
                      <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" className="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            }
                        </div>
                  </div>
                  <ul className="tab-title clearfix">
                    <NavLink  to="/" ><li className={this.isActive('/')}><div className="icon"><img src="/images/v1/icon/ic-all.svg" alt="image"/></div>{T.translate('play_menu.all')}</li></NavLink>
                    <NavLink  to="/assigned" ><li className={this.isActive('/assigned')}><div className="icon"><img src="/images/v1/icon/ic-essential.svg" alt="image"/></div>{T.translate('play_menu.need')}</li></NavLink>
                    <NavLink  to="/received" ><li className={this.isActive('/received')}><div className="icon"><img src="/images/v1/icon/ic-assigned.svg" alt="image"/></div>{T.translate('play_menu.receive')}</li></NavLink>
                    <NavLink  to="/sent" ><li className={this.isActive('/sent')}><div className="icon"><img src="/images/v1/icon/ic-sent.svg" alt="image"/></div>{T.translate('play_menu.send')}</li></NavLink>
                  </ul>
                  <div className={`heading-category ${!this.isMainUrl() && 'opacity05'}`}>{T.translate('play_menu.category')}</div>
                  <div className="tab-content-one nav_sub_scroll scrollbarStyle1">
                      <div className="tab-content-inner">
                          <div className={`wrap-list-menu  ${!this.isMainUrl() && 'opacity05'}`}>
                              <ul className="list-menu">
                                  <li>
                                      <a className={this.isMainUrl()&&this.state.all==true?'active':''} onClick={()=>this.selectedCategory({idx:0})}>
                                          <span className="left">{T.translate('play_menu.tap-all')}</span>
                                          <span className="right">{totalCount}</span>
                                      </a>
                                  </li>
                                  <MenuTree data={this.state.categories} isMain={this.isMainUrl()} handleOnClickTimelines={this.selectedCategory}></MenuTree>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );

    }
}
export default withRouter(connect(
(state)=>({
    root: state.video.get('root'),
    child: state.video.get('child'),
}),
(dispatch)=>({
    VideoActions:bindActionCreators(videoActions, dispatch),
    getTimelines:bindActionCreators(getTimelines, dispatch),

})
)(Play))
