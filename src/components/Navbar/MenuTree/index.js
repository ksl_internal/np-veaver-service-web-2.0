import React, { Component , Fragment } from "react";
import T from 'i18n-react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter , NavLink} from "react-router-dom";

const MenuItem =({item ,isMain=true, handleOnClickTimelines })=>{
    return(
        <li className={`has-sub ${item.isOpen!==undefined && item.isOpen ? 'active' :''}` }>
            <a onClick={(e)=>{e.preventDefault();if(isMain)handleOnClickTimelines(item,e)} } className={item.selected ? 'active' : '' } >
                {item.languagePack ? <span className="left">{T.translate(item.languagePack)}</span> : <span className="left">{item.name}</span>}
                <span className="right">{item.timelineCount}</span>
            </a>
            {
                item.children!==undefined && item.children.length > 0 &&
                <span className={`btn-submenu ${item.isOpen!==undefined && item.isOpen ? 'active' :''}`}  onClick={(e)=>{e.preventDefault();if(isMain)handleOnClickTimelines(item,e)}}></span>
            }
            <ul className={`dropdown ${item.isOpen!==undefined && item.isOpen ? 'active' :'hide'}`}>
            {
                item.children!==undefined && item.children.length > 0 && item.children.map((child,index)=>{
                    return (
                        <MenuItem key={`child-${index}-${child.idx}`} isMain={isMain} item={child} handleOnClickTimelines={handleOnClickTimelines}></MenuItem>
                    )
                })
            }
            </ul>
        </li>
    )
}
/**
 * Play 및 Search LNB에 사용된 메뉴 뷰 입니다.
 * click 이벤트는 하나로 통일로 변경되었으며 이벤트에 메뉴 오픈과 선택 둘다 작용 되도록 되어있습니다.
 * isMain 의 경우 Play LNB에서 / (루트) 패스를 검사하기 위함입니다.
 */
class MenuTree extends Component {

    render (){
        const { data ,isMain, handleOnClickTimelines} = this.props;
        const items = data.map( (item , index)=>{
            return (
                <MenuItem key={`parent-${index}-${item.idx}`} item={item} isMain={isMain} handleOnClickTimelines={handleOnClickTimelines}></MenuItem>
            )
        })
        return (
            <Fragment>
                {items}
            </Fragment>
        )
    }
}
export default MenuTree
