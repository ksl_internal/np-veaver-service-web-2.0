import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter , Link , NavLink} from "react-router-dom";
import * as timelinesActions from "../../modules/timelines";
import T from "i18n-react";
/**
 * 에듀 LNB 항목입니다
 */
class Edu extends Component {
    componentDidMount(){
        this.fetchData()
    }
    fetchData = async () =>{
        const { TimelinesActions } = this.props;
        await TimelinesActions.getCpTimelineGroups();
    }
    render(){
        const { handleToggle ,cpTimelineGroupsResult ,location} = this.props;
        if(cpTimelineGroupsResult == null) return null
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="main-content-left clearfix">
                <div className="right">
                    <div className="page-title">
                        <h1 className="page-heading">Edu</h1>
                        <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" className="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            }
                        </div>
                    </div>
                    <div className="heading-category">{T.translate('edu.category')}</div>
                    <ul className="menu-edu">
                        <li><Link  to={`/cp`} className={(location.pathname==='/cp'?'active':'')}>{T.translate('common.all')}</Link></li>
                        {cpTimelineGroupsResult.groups.map(item=>{
                            return (
                            <li key={item.groupIdx}>
                                <NavLink  to={`/cp/${item.groupIdx}`} activeClassName={"active"} >
                                    {item.groupName}
                                </NavLink>
                            </li>)
                        })}

                    </ul>
                </div>
            </div>
        );
    }

}
export default withRouter(connect(
    state => ({
        cpTimelineGroupsResult: state.timelines.get("cpTimelineGroupsResult")
    }),
    dispatch => ({
      TimelinesActions: bindActionCreators(timelinesActions, dispatch)
    })
  )(Edu));
