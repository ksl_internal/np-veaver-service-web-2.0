import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter , Link , NavLink} from "react-router-dom";
import * as notiActions from '../../modules/noti';
import T from 'i18n-react';
/**
 * 알림 LNB 항목입니다
 */
class Noti extends Component {
    componentDidMount(){
        this.fetchData()
    }
    /**
     * 알림 페이지가 새로 만들어질 경우 최대 100개의 로우를 서버에서 가져옵니다.
     * 중요 알림과 일반 알림은 notiType 으로 분리 되어있습니다.
     */
    fetchData = async () =>{
        const { NotiActions ,location , match, } = this.props;
        try {
            await NotiActions.getNotifications({notiType:'I',limit:100});
            await NotiActions.getNotifications({notiType:'G',limit:100});
        } catch (e) {
            console.log(e);
        }
    }
    render(){
        const { handleToggle , result , match} = this.props;
        if(result===undefined || result === null) return null;
        const iCount = result['I'].unreadCount
        const gCount = result['G'].unreadCount
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="main-content-left clearfix">
                <div className="right">
                    <div className="page-title">
                        <h1 className="page-heading">Notification</h1>
                        <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" className="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            }
                        </div>
                    </div>
                    <div className="heading-category">{T.translate('noti.category')}</div>
                    <ul className="menu-edu">
                        <li><NavLink activeClassName={'active'} to={`/noti/i`} >{T.translate('noti.important')} <span style={{float:'right'}}>{iCount}</span></NavLink></li>
                        <li><NavLink activeClassName={'active'} to={`/noti/g`} >{T.translate('noti.normal')} <span style={{float:'right'}}>{gCount}</span></NavLink></li>
                    </ul>
                </div>
            </div>
        );
    }

}
export default withRouter(connect(
    state => ({
        result: state.noti.get('notificationsResult'),
    }),
    dispatch => ({
        NotiActions: bindActionCreators(notiActions, dispatch)
    })
)(Noti));
