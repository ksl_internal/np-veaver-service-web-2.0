import React, { Component , Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import * as folderActions from "../../modules/folderAndPin";
import * as popupActions from "../../modules/popup";
import FolderAdd from "../../components/PopupForm/FolderAdd";
import T from 'i18n-react';
import  {NotFound}  from '../NewCardView'
import { putFolderRespond } from "../../modules/noti";
/**
 * 폴더 LNB 항목입니다
 */
class Folder extends Component {
    state={
        open:false,
    }
    onOpenModal = () => {
        this.setState({ open: true });
      };

      onCloseModal = () => {
        this.setState({ open: false });
      };
      /**
       * 특정 폴더를 클릭했을때 폴더에있는 타임라인을 새로 불러옵니다.
       * api 문서상 
       */
      onPageChange = async (item,index) =>{
        const{FolderActions} = this.props;
        try {
            await FolderActions.currentInfo(item)
            const folerRes = await FolderActions.getFoldersId({id:item.idx});
            const folerUserRes = await FolderActions.getFoldersIdUsers({id:item.idx});
            //console.log('folerRes',folerRes)
            //console.log('folerUserRes',folerUserRes)
            if(folerUserRes.data === null){
                window.location.reload(true)
            }
        } catch (error) {
            console.log('error',error)
        }
      }

    render(){
        const {currentInfo, folders , handleToggle} = this.props;
        if(folders == null) return null;
        let total=0;

        const folers = folders.folders.map((item,i)=>{
            total += item.pinCount;
            return (
                <li key={item.idx} style={ {
                    backgroundImage:`url(${item.recentThumbnail})` ,
                    backgroundSize: 'contain',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center center',
                } } onClick={()=>this.onPageChange(item,i)} className={ item.idx===currentInfo.idx ? 'active' :'' }>
                    <div className="wrap-title">
                        <span className="image"><img src="/images/v1/icon/img-folder.svg" alt="image"/></span>
                        <span className="title">{item.folderName}</span>
                    </div>
                    <div className="bottom">
                        <div className="number">{item.pinCount}</div>
                        <div className="group-card">
                            <span className="group1"><img src="/images/v1/icon/img-group-card_2.svg" alt="image"/></span>
                            <span className="group2"><img src="/images/v1/icon/img-group-card.svg" alt="image"/></span>
                        </div>
                    </div>
                </li>
            )
        })
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
          <div className="main-content-left clearfix">
              <div className="right">
                  <div className="page-title">
                      <h1 className="page-heading">Folder</h1>
                      <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" className="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            }
                        </div>
                  </div>
                  <div className="content-list-folder">
                      <div className="heading-category clearfix">{T.translate('folder.my-folder')} ({total}) <span onClick={(e)=>{
                          this.props.openPopup('FOLDER',{
                            timelineIdxList:[],
                            folderIdx : null,
                            message : null,
                            type:'edit'
                        } )
                      }} className="setting"><img src="/images/v1/icon/pc-profile-setting.svg" alt="image"/></span></div>
                      <div className="wrap-list-folder scrollbarStyle1">
                          <ul className="list-folder">
                          {folders.folders.length==0 ?
                                    <div style={{height:'80vh',position:'relative'}}>
                                        <NotFound></NotFound>
                                    </div>
                                    :
                                    folers
                                }
                          </ul>
                      </div>
                  </div>
              </div>
              {this.state.open ?
                <Fragment>
                    <div className="popup-dimmed-layer"></div>
                    <FolderAdd
                        FolderActions={this.props.FolderActions}
                        onCloseModal = {this.onCloseModal}
                    />
                </Fragment>
                : null
              }
          </div>
        );
    }
}
export default withRouter(connect(
    state => ({
        currentInfo: state.folderAndPin.get("currentInfo"),
        folders: state.folderAndPin.get("folders")
    }),
    dispatch => ({
        FolderActions: bindActionCreators(folderActions, dispatch),
        PopupActions : bindActionCreators(popupActions, dispatch),
    })
)(Folder));
