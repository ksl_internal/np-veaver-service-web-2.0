import React, { Component , Fragment} from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import  {NotFound , TimelinesWrapper , TempCardView, ListMoreView}  from '../NewCardView'
import * as timelinesActions from '../../modules/timelines'
import T from "i18n-react";
/**
 * Make LNB 항목입니다
 */
class Make extends Component{
    componentDidMount(){
        this.fetchData({pageNum:1},false);
    }
    fetchData=async(query,attachMode)=>{
        const {mypage,handleToggle , TimelinesActions} = this.props;
        await TimelinesActions.getLockerTimelines(query,attachMode)
    }
    render(){
        const {tempItems,handleToggle } = this.props;
        if(tempItems == null) return null;
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="main-content-left clearfix">
                <div className="right">
                    <div className="page-title">
                        <h1 className="page-heading">Make</h1>
                        <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" className="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            }
                        </div>
                    </div>
                    <div className="content-list-make">
                        <div className="heading-category clearfix">{T.translate("make.temp-storage")} ({tempItems.timelinesCount})</div>
                        <div className="wrap-list-make scrollbar" id="scrollbar">
                            <ul className="list-make">
                            {tempItems.timelinesCount===0 ?
                                    <div style={{height:'80vh',position:'relative'}}>
                                        <NotFound></NotFound>
                                    </div>
                                    :
                                    <Fragment>
                                        <div className="wrap-one-three">
                                            <TimelinesWrapper data={tempItems.timelines} CardView={TempCardView}></TimelinesWrapper>
    
                                        </div>
                                        <ListMoreView req={this.props.req} isHideMoreLayer={this.props.isHideMoreLayer} callBack={this.fetchData}></ListMoreView>
                                    </Fragment>
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          );
    }
}
export default withRouter(connect(
    state => ({
        tempItems: state.timelines.get("tempItems"),
        req: state.timelines.get("req"),
        attachMode: state.timelines.get("attachMode"),
        isHideMoreLayer : state.timelines.get("isHideMoreLayer")
    }),
    dispatch => ({
        TimelinesActions: bindActionCreators(timelinesActions, dispatch),
    })
)(Make));
