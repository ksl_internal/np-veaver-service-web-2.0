import React, { Component , Fragment} from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter , Link} from "react-router-dom";
import { openPopup }  from "../../modules/popup";
import { logout } from '../../modules/auth';
import { appInitialize } from '../../modules/app';
import * as profileActions from '../../modules/profile';
import T from 'i18n-react';

class ProfileImage extends Component{
    constructor(props) {
      super(props);
      this.state = {
        message : ''
      };
    }
    handleOnChange = (e)=>{
      this.setState({
        message : e.target.value
      })
    }
    render() {
      const { image, handleToggleProfileImage } = this.props;
      return (
        <Fragment>
            <div className="popup-dimmed-layer" onClick={handleToggleProfileImage}></div>
            <div className="popup-wrapper">
                <div className="popupAddUserSelection open">
                    <img src={image}></img>
                </div>
            </div>
        </Fragment>
      );
    }
  }

/**
 * 마이 페이지 LNB 항목입니다
 */
class Profile extends Component{
    state = {
        profileOpen:false,
    }
    componentDidMount(){
        
    }
    handlePopup = async(e) =>{
        e.preventDefault();
        this.props.openPopup('LEVELSPOINTS',{
            level:this.props.userResult.user.veaverIndiceInfo.level,
            point:this.props.userResult.user.veaverIndiceInfo.point,
            usersIndicesResult : this.props.usersIndicesResult,
            veaverStackRankingsResult : this.props.veaverStackRankingsResult,
            veaverRankingsResult :this.props.veaverRankingsResult
        })
    }
    handleToggleProfileImage =()=>{
        this.setState({profileOpen:(this.state.profileOpen)?false:true})
    }
    _singleImageUpload =async event => {
        const file = event.target.files[0];
        const { ProfileActions,match, userProfile } = this.props;
        try {
            const resResult = await ProfileActions.uploadThumbnail({thumbnail:file})
            if(resResult.data.header.resultCode===0){
                this.props.appInitialize()
                await ProfileActions.getUsers({id: (match.params.id && match.params.id !== userProfile.userKey) ? match.params.id : userProfile.userKey});
                global.alert('프로필이 변경 되었습니다');
            }
        } catch (error) {
            console.log('error',error)
        }
    };
    render(){
        //console.log(this)
        const { userResult ,handleToggle ,loginUser , userTimelinesResult,usersIndicesResult} = this.props
        if(userResult == null || userTimelinesResult==null) return null
        const { user } = userResult
        if(!this.props.isOpen){
            return (
                <div className="main-content-left clearfix">
                    <div className="right">
                        <div className="page-title">
                            <div style={{top:'21px'}} className="close" id="close-content-left" onClick={handleToggle}>
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" className="imgShow" />
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="main-content-left clearfix">
                <div className="right">
                    <div className="page-title">
                        <h1 className="page-heading">Profile</h1>
                        <div className="close" id="close-content-left" onClick={handleToggle}>
                            {this.props.isOpen===true ?
                                <img src="/images/v1/icon/ic-cancel.svg" alt="image" id="imgClose" />
                                :
                                <img src="/images/v1/icon/arrow-lnb.svg" alt="image" id="imgShow" />
                            }
                        </div>
                    </div>
                    <div className="profile-sidebar" style={{backgroundColor:'#ffffff'}}>
                        <div className="profile-top">
                            <div className="image">
                                <img onClick={this.handleToggleProfileImage} src={user.thumbnailMedium} alt=""/>
                                {
                                    loginUser.userKey ==user.userKey &&
                                    <div className="setting-profile">
                                        <a >
                                            <label htmlFor="input-file">
                                                <img style={{cursor:'pointer'}} src="/images/v1/icon/icon-setting-profile.svg" alt=""/>
                                            </label>
                                            <input id="input-file" style={{display:'none'}} type="file" accept="image/*" onChange={this._singleImageUpload} />
                                        </a>
                                    </div>
                                }
                            </div>
                            <div className="info-user">
                                <div className="name">
                                    {user.nickname}
                                </div>
                                <p className="position">{user.position}· {user.department}</p>
                                <div className="more">
                                    <img style={{height:'36px'}} src={`/images/v1/user-ic-level-${user.veaverIndiceInfo.level}-l-3-x@2x.png`} alt="" onClick={this.handlePopup}/>
                                    <a href="#" onClick={this.handlePopup}>{T.translate('profile.view-more')}</a>
                                </div>
                            </div>
                        </div>
                        <div className="profile-contact">
                            <ul>
                                <li>M. {user.phone}</li>
                                <li>T. {user.officeTel}</li>
                                <li>E. <a href={'mailto:'+user.email}>{user.email}</a></li>
                            </ul>
                        </div>
                        {loginUser.userKey ==user.userKey &&
                            <div className="profile-bottom">
                                <div className="user-item">
                                    <Link  to="/profile">
                                        <span className="left">{T.translate('profile.my-knowledge')}</span>
                                        <span className="right">{userTimelinesResult.timelinesCount}</span>
                                    </Link>
                                </div>
                                <div className="user-item">
                                    <a onClick={()=> this.props.openPopup('USERBOOKMARK')}>
                                        <span className="left">{T.translate('profile.user-bookmark')}</span>
                                    </a>
                                </div>
                                <div className="user-item">
                                    <Link  to="/profile/password">
                                        <span className="left">{T.translate('profile.modify-pw')}</span>
                                    </Link>
                                </div>
                                <div className="user-item">
                                    <a onClick={()=>this.props.openPopup('BASIC', {title:T.translate('profile.logout'),description:'로그아웃 하시겠습니까?'}, (value) => {if(value){this.props.logout()}}) }>
                                        <span className="left">{T.translate('profile.logout')}</span>
                                    </a>
                                </div>
                            </div>
                        }
                    </div>
                </div>
                {this.state.profileOpen ?
                    <Fragment>

                        <ProfileImage
                        image={user.thumbnail}
                        handleToggleProfileImage = {this.handleToggleProfileImage}
                        />
                    </Fragment>
                    : null
                }
            </div>
          );
    }
}
export default withRouter(connect(
    state => ({
        loginUser : state.user.userProfile,
        userResult: state.profile.get("userResult"),
        usersIndicesResult: state.profile.get("usersIndicesResult"),
        veaverStackRankingsResult: state.profile.get("veaverStackRankingsResult"),
        veaverRankingsResult: state.profile.get("veaverRankingsResult"),
        userTimelinesResult: state.profile.get("userTimelinesResult")
    }),
    dispatch => ({
        openPopup: bindActionCreators(openPopup, dispatch),
        logout:bindActionCreators(logout, dispatch),
        ProfileActions:bindActionCreators(profileActions, dispatch),
        appInitialize : bindActionCreators(appInitialize, dispatch),
    })
  )(Profile));
