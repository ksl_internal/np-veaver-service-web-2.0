import React, { Component } from 'react';
import KnowledgeInfo from '../../components/Player/KnowledgeInfo';
import MediaElement from './MediaElement';

class Player extends Component {

  state = {
    knowledgeInfo: null,
    categories: null,
  }

  renderKnowledgeInfo() {
    if (!this.props.timelineData) {
      return null;
    }

    return (
      <KnowledgeInfo knowledgeInfo={this.props.timelineData}
                      categories={this.props.categories}
                      isSettingLayerCloseFlag = {this.props.isSettingLayerCloseFlag}
                      setPossibleClick = {this.props.setPossibleClick}
                      closeSet = {this.props.closeSet}
                      setPossibleClickFunc = {this.props.setPossibleClickFunc}/>
    );
  }

  render() {
    const { videoData, timelineData, categories, beginFrom } = this.props;
    
    const videoSourceType = {
      Y: 'video/youtube',
      N: videoData.hlsFlag === 'Y' ? 'application/x-mpegURL' : 'video/mp4', 
      L: 'video/mp4',
    };

    const sources = [
      { src: videoData.fileUrl, type: videoSourceType[videoData.fileType] }
      // { src:'http://veaver.streaming.mediaservices.windows.net/f948acf9-b223-4f71-a0e3-eceb773a10c4/no-updater2.ism/manifest(format=m3u8-aapl)' , type: 'application/x-mpegURL' }
      // { src: " https://mediatestforveaver.streaming.mediaservices.windows.net/371f62a3-31bc-4b27-91b4-ff6dda2b981e/sample.ism/manifest(format=m3u8-aapl)", type: "application/x-mpegURL" }
    ];


    return(
      <div className="player">
      {
        (videoData && timelineData && categories) ?
        <MediaElement
          id="player"
          mediaType="video"
          preload
          width="890"
          height="500"
          poster=""
          hlsFlag={videoData.hlsFlag}
          sources={sources}
          timelineSource={timelineData}
          beginFrom={beginFrom}
        /> : ''
      }
        {this.renderKnowledgeInfo()}
      </div>
    );
  }
}

export default Player;
