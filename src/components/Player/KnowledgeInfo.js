import React, { Component , Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import moment from 'moment';
import {
  getBaseDomain,
  axios,
  isLocker,
} from '../../util/api';
import {
  likeReaction,
  commentReaction,
  activeCommentIcon,
  refreshLikeCount,
  refreshCommentCount,
  changeActiveInfoTab,
} from '../../modules/player';
import {
  increaseLikeCount,
  getReactions,
    getLike,
} from '../../modules/reaction';
import {
  deleteTimeline
} from '../../modules/mypage';
import {
  openPopup,
} from '../../modules/popup';
import { uploadVideoInfoDataSet } from "../../modules/make"
import T from "i18n-react";

class KnowledgeInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isSettingLayerOn: false,
      isProfileLayerOn: false,
      isMoreLayerOn: false,
      likeReaction: false,
      commentReaction: false,
    }
  }

  componentDidMount() {


    if (this.spnDescription && this.spnDescription.offsetHeight < 84) {
      this.setState(state => ({
        ...state,
        isMoreLayerOn: true
      }));
      this.btnMoreDescription.classList.add('hide');
    }
    // console.log(this.btnMoreDescription, this.spnDescription.offsetHeight);
  }

  onSettingButtonClick() {
    this.setState((state) => {
      return {
        ...state,
        isSettingLayerOn: !state.isSettingLayerOn,
      }
    })

  }

  onProfileClick() {
    this.setState((state) => {
      return {
        ...state,
        isProfileLayerOn: !state.isProfileLayerOn,
      }
    })
  }

  onMoreButtonClick(e) {
    const state = Object.assign({}, this.state);
    if (state.isMoreLayerOn) {
      e.target.previousSibling.children[0].scrollTop = 0;
    }

    this.setState(state => ({
      ...state,
      isMoreLayerOn: !state.isMoreLayerOn,
    }))

  }
  _videoPublish = (e, timelineIdx) => {
    e.preventDefault()
    this.props.uploadVideoInfoDataSet(timelineIdx)
  }
  _increaseLikeCount = () => {
      const { videoIdx } = this.props.knowledgeInfo;
      const{ increaseLikeCount, getReactions, changeActiveInfoTab } = this.props;
      increaseLikeCount(videoIdx)
      .then(res => res === 'success' && getReactions(videoIdx))
      .then(() => changeActiveInfoTab('REACTION'))
      .catch(err => Error(err))


  }

    _viewLike = () => {
        const { videoIdx } = this.props.knowledgeInfo;

        console.log('timeline ', videoIdx)

        const{ getLike } = this.props;
        getLike(videoIdx)
            .then(result => {
                console.log('result ',result);
                this.props.openPopup("LIKE", result);
            })

    }

  _onFocusCommentArea = () => {
    const{ changeActiveInfoTab } = this.props;
    const aa = changeActiveInfoTab('REACTION');
    console.log(aa)
    changeActiveInfoTab('REACTION').then(() => {
      document.getElementById('comment-textrea').focus();
    }).catch(err => Error(err))
  }

  commentReaction() {
    this.props.onCommentReaction(true);
    this.props.onActiveCommentIcon(true);
  }

  renderCategories() {
    return this.props.categories.map((category, index) => <span key={`categoty-${index}`}>{category.videoCategoryName}</span>);
  }

  copyTimelineShareLinkToClipboard(e) {
    e.preventDefault();
    const videoIdx = this.props.knowledgeInfo.videoIdx;
    const timelineIdx = this.props.knowledgeInfo.timelineIdx;
    axios.post('/short-url', {
        url: `${getBaseDomain()}/timeline-deeplink?action=play&videoIdx=${videoIdx}&timelineIdx=${timelineIdx}`
    })
    .then(result => {
      const linkText = result.data.url;
      this.props.openPopup('LINK_COPY', {
        linkText,
      });
    })
    .catch((err) => {
      console.log('copy timeline share link error', err);
    });
  }

  deleteTimeline(e) {
      e.preventDefault();
      this.props.deleteTimeline(this.props.knowledgeInfo.timelineIdx, this.props.history);
  }

  checkClose() {
      if(!this.props.setPossibleClick) {
        this.props.setPossibleClickFunc(true)

      } else {
          if(this.props.isSettingLayerCloseFlag) {
              this.setState((state) => {
                  return {
                      ...state,
                      isSettingLayerOn: false,
                  }
              })
              this.props.closeSet(false)
              this.props.setPossibleClickFunc(false)
          }
      }

  }


  render() {

    const { knowledgeInfo, uploadVideoStatus, activeCommentIcon, userProfile, commentCount, likeCount } = Object.assign({}, this.props);
    const state = Object.assign({}, this.state);
    const isCurrentLocker = isLocker();
    const regDate = isCurrentLocker ? T.translate('play.register-date') + ' ' + moment(knowledgeInfo.regDate).format('YYYY. MM. DD HH:mm')
                  : T.translate('play.publish-date') + ' ' + moment(knowledgeInfo.regDate).format('YYYY. MM. DD HH:mm');

      if(state.isSettingLayerOn) {
          this.checkClose()
      } else {
          if(this.props.isSettingLayerCloseFlag) this.props.closeSet(false)
          if(this.props.setPossibleClick) this.props.setPossibleClickFunc(false)
      }

    return (
      <div className="video-info">
        <div className={`information }`}>
          <div className="content-title">
            <p className="title">
              {knowledgeInfo.name}
            </p>
            {(Object.prototype.hasOwnProperty.call(this.props.knowledgeInfo, 'deployStatus') && this.props.knowledgeInfo.deployStatus !== 'N') ? '' :
            <button className="setting-button" onClick={this.onSettingButtonClick.bind(this)}></button>
            }
            <div className="content-setting">
              <ul className={`content-setting_layer ${state.isSettingLayerOn && 'active'}`}>
              {
                isCurrentLocker ?
                <li>
                    <a className="btn-link" onClick={(e) => this._videoPublish(e, knowledgeInfo.timelineIdx)}>{T.translate('common.post')}</a>
                </li> :
                <Fragment>
                  <li>
                    <a href="/" className="btn-link" onClick={(e)=>{e.preventDefault(); this.props.openPopup('CHOOSEUSERS',{
                        timelineIdxs:[knowledgeInfo.timelineIdx],
                        returnType:'share'
                    } )} }>{T.translate('common.share')}</a>
                  </li>
                  <li>
                    <a href="/" className="btn-link" onClick={(e)=>{e.preventDefault(); this.props.openPopup('FOLDER',{
                        timelineIdxList:[knowledgeInfo.timelineIdx],
                        folderIdx : null,
                        message : null,
                        type:'add'
                    } )}  }>{T.translate('common.add-to-folder')}</a>
                  </li>
                  <li>
                    <a href="/" className="btn-link" onClick={this.copyTimelineShareLinkToClipboard.bind(this)}>{T.translate('common.linkcopy')}</a>
                  </li>
                </Fragment>
              }
              {knowledgeInfo.user.userKey !== userProfile.userKey ?
                  <li>
                    <a href="/" className="btn-link" onClick={(e)=>{e.preventDefault();this.props.openPopup('REPORT', {timelineIdx:knowledgeInfo.timelineIdx,targetType:'T'} )}}>{T.translate('report.report')}</a>
                  </li> :
                  <Fragment>
                    <li>
                    <Link to={`${isCurrentLocker ? '/locker' : ''}/modify/${knowledgeInfo.timelineIdx}`} className={`btn-link ${uploadVideoStatus !== 'READY' ? 'dimmed' : ''}`}>{T.translate('common.edit')}</Link>
                    </li>
                    <li>
                      <a href='#' onClick={this.deleteTimeline.bind(this)} className="btn-link">{T.translate('common.delete')}</a>
                    </li>
                  </Fragment>
              }
              </ul>
            </div>

          </div>
          <p className="upload-info">
            <span>{knowledgeInfo.publicFlag === 'A' && T.translate('play.public')}</span>
            <span>{knowledgeInfo.publicFlag === 'H' && T.translate('play.job-position')}</span>
            <span>{knowledgeInfo.publicFlag === 'D' && T.translate('play.department')}</span>
            <span>{knowledgeInfo.publicFlag === 'F' && T.translate('play.following')}</span>
            <span>{knowledgeInfo.publicFlag === 'P' && T.translate('play.private')}</span>
            <span>{knowledgeInfo.publicFlag === 'U' && T.translate('play.selected-opening')}</span>
             <span>{regDate}</span>
          </p>
          <div className="depth-category">
          <p className="depth-group">
          {this.renderCategories()}
          </p>
          </div>
      {
          this.props.videoDetail.fileType === 'Y' ? (
              <div className="content-link">
                  <p className="depth-link">
          출처 : <a href={this.props.videoDetail.fileUrl} target="_blank"> {this.props.videoDetail.fileUrl}</a>
      </p>
      </div>
      ) : ''
      }
          <div className="hashtag">

          <div className="wrap-btn">
          {knowledgeInfo.tag.split(',').map( (tag,index)=>{
              return (
                  <a key={`tag-${index}`} className="btn" onClick={(e) => { this.props.history.push(`/search/timeline?keyword=${tag}`)} }>
          <span key={`tag-${index}`}>{`#${tag}`}</span>
              </a>
          )
          })}
  </div>

      </div>
          <div className="content-context">
            <p id="description-area" style={{ overflowY : state.isMoreLayerOn ? 'scroll' : 'hidden' }} ref={(refs) => { this.descriptionArea = refs;}}>
              {knowledgeInfo.description !== null && knowledgeInfo.description.replace(/(\r\n|\n|\r)/gm, ',').split(',').map((line, index) =>
                <span ref={el => this.spnDescription = el} key={`description-${index}`}>{`${line}`}<br /></span>
              )}
            </p>
          </div>
          <button ref={el => this.btnMoreDescription = el} className="more" onClick={this.onMoreButtonClick.bind(this)}></button>
        </div>
        <div className="social">
          <div className="social-count">
            <div className="view">
              <p className="title">View</p>
              <p>{knowledgeInfo.viewCount}</p>
            </div>
            <div className="like">
              <p className="title" >Like</p>
              <p onClick={this._viewLike}>{likeCount}</p>
              <button className={`reaction-like ${this.props.likeReaction ? 'on' : ''} ${knowledgeInfo.type === 'CP' && 'hide'}`} onClick={this._increaseLikeCount}></button>
            </div>
            <div className="comment">
              <p className="title">Comment</p>
              <p>{commentCount}</p>
              <button className={`reaction-comment ${activeCommentIcon ? 'on' : ''} ${knowledgeInfo.type === 'CP' && 'hide'}`} onClick={this._onFocusCommentArea}></button>
            </div>
          </div>
          <div className="relationship">
            <div className="follow-area">
              <button className="profile-thumbnail">
                <Link to={`/profile/${knowledgeInfo.user.userKey}`}>
                    <img src={knowledgeInfo.user.thumbnail} alt="test" />
                </Link>
              </button>
              <p className="follow-button">
                <Link to={`/profile/${knowledgeInfo.user.userKey}`}>
                  {knowledgeInfo.user.nickname}
                </Link>
              </p>
              <div className={`profile-layer ${state.isProfileLayerOn && 'active'}`}>
                <div className="profile-link dimmed">
                  <a className="btn-link nickname ">
                    {knowledgeInfo.user.nickname}
                  </a>
                  <br />
                  <a className="btn-link profile-info">
                    <span className="position">{knowledgeInfo.user.position}</span>
                    <span>.</span>
                    <span className="department">{knowledgeInfo.user.department}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  timelineCurrent: state.player.currentTime,
  likeReaction: state.player.likeReaction,
  commentReaction: state.player.commentReaction,
  likeCount: state.reaction.likeCount,
  commentCount: state.reaction.commentCount,
  activeCommentIcon: state.player.activeCommentIcon,
  openPopup: state.openPopup,
  uploadVideoStatus: state.make.uploadVideoStatus,
  userProfile: state.user.userProfile,
    videoDetail: state.player.videoDetail,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  onLikeReaction: likeReaction,
  onCommentReaction: commentReaction,
  onActiveCommentIcon: activeCommentIcon,
  setLikeCount: refreshLikeCount,
  setCommentCount: refreshCommentCount,
  increaseLikeCount,
  getReactions,
  deleteTimeline,
  openPopup,
  changeActiveInfoTab,
    getLike,
    uploadVideoInfoDataSet
}, dispatch);

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(KnowledgeInfo));
