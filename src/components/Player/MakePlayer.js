import React, { Component } from 'react';
// import PropTypes from 'prop-types';
// import { changeForm } from '../../actions/AppActions';
import MakeTimeline from '../../components/Timeline/MakeTimeline';

class MakePlayer extends Component {

  render() {
    return(
      <div className="video-container">
        <div className="control-area">
          <div className="controls">
            <div className="play-layer">
              <button className="pause"></button>
            </div>
            <div className="control-elements">
              <div className="left-side">
                <button className="play"></button>
                <div className="run-time">
                  <span className="running">
                    00:08:24
                  </span>
                  <span className="ending">
                    00:30:45
                  </span>
                </div>
              </div>
              <div className="right-side">
              </div>
            </div>
          </div>
        </div>
        <MakeTimeline />
      </div>
    );
  }
}

export default MakePlayer;
