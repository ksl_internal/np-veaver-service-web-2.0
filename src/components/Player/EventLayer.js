import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  axios,
} from '../../util/api';
import {
  openPopup,
} from '../../modules/popup';
import T from "i18n-react";


class EventLayer extends Component {

    componentWillMount() {
        const { userProfile } = this.props;
       // console.log("user profile: " + userProfile);
    this.props.onEventFire();
  }

    answer() {
    const { event } = this.props.source;

    let answerFlag = null;
    let answer = null;
    let answerId = null;

    //console.log("<<<<<<<<<<<<answer()");

    if (event.contents.type === 'MULTIPLE') {
      const radios = document.getElementsByName('radio-answer');
      let checkedValue = null;
      for (let i = 0; i < radios.length; ++i) {
        if(radios[i].checked) {
          checkedValue = radios[i].value;
        }
      }

      if (checkedValue) {
        for (let i = 0; i < event.contents.answers.length; i++) {
          if (checkedValue === event.contents.answers[i].answerId) {
            answerFlag = event.contents.answers[i].answerFlag;
          }
        }
        answerId = checkedValue;
      }
    } else if (event.contents.type === 'SHORT' && this.shortAnswer.value.length) {
      answerFlag = this.shortAnswer.value === event.contents.answer.example ? 'Y' : 'N';
      answer = this.shortAnswer.value;
      }


    if (!answerFlag || !(answer || answerId)) {
      return console.log('answer is must not null');
    }

    this.btnConfirm.disabled = true;
    axios.post(`/timelines/event-quiz-answers/${event.eventIdx}`, {
        answerFlag,
        answer,
        answerId,
    })
    .then((result) => {
      this.btnConfirm.disabled = false;
      this.btnConfirm.dataset.needAnswer = false;
      this.btnConfirm.textContent = '영상 보기';
      this.props.onAnswered(answer ? answer : answerId);
    });
  }

  _onShortAnswerChanged(evt) {
      evt.persist();
     // console.log('onShortAnswerChanged!');
      //console.log("user profile: " + this.props.userProfile);
      if (evt.target.value.length) {
      this.btnConfirm.classList.remove('dimmed');
    } else {
      this.btnConfirm.classList.add('dimmed');
    }
  }

    _onAnswerCheck(evt) {
      //  console.log('_onAnswerCheck');
    this.btnConfirm.classList.remove('dimmed');
  }

  _onConfirm(evt) {
    evt.persist();
    if (evt.target.dataset.needAnswer === 'true') {
      this.answer();
    } else {
      this.props.onEventConfirmed();
    }
  }

  renderConfirmButton() {
      const { source } = this.props;
      
      let btnText = '';
      let needAnswer = false;

     // console.log('renderConfirmButton, SNSLink: ' + this.props.snsFlag);


    switch (source.event.type) {
      case 'IMAGE':
      case 'TEXT':
        btnText = '영상 보기';
        break;
        case 'QUIZ':
            if (this.props.snsFlag && this.props.userProfile==null) {
                btnText = '영상 보기';
            } 
            else if (source.event.contents.myAnswer) {
                  btnText = '영상 보기';
            } else {
                  btnText = '답변 하기';
                  needAnswer = true;
            }
            break;
      default:
        btnText = '영상 보기';
        break;
    }

    return (
      <button className={`btn-solid btn-blue submit ${needAnswer && 'dimmed'}`}
        ref={btn => this.btnConfirm = btn}
        onClick={this._onConfirm.bind(this)}
        data-need-answer={needAnswer}
      >
        {btnText}
      </button>
    );
  }

  _showAnswer(e) {
    e.preventDefault();
    this.props.openPopup('QUIZ', {...this.props, isEvent: true});

    // this.props.onShowAnswerList();
    // const { event } = this.props.source;
    // const { contents } = event;
    // const { type } = contents;
    // const isMultiple = type === 'MULTIPLE';
    // axios.get(`/timelines/event-quiz-answers/${event.eventIdx}/${isMultiple ? 'multiple' : 'short'}`, {
    // })
    // .then(res => res.data)
    // .then(result => {
    //   this.props.openPopup('QUIZ', {
    //     event: event,
    //     answers: result.quizResult,
    //   });
    // })
    // .catch((err) => {
    //   console.log('show answer error', err);
    // });
    }
    _snsLinkOrNotAnswerView(event) {
        if (this.props.snsFlag == true && this.props.userProfile == null)
            return (false);
        else
            return (event.contents.myAnswer === null);
    }

    _snsTextArea(){
      if (this.props.snsFlag == true && this.props.userProfile == null)
        return true;
      else
        return false;  
    }

  render() {
   
    const { source } = this.props;
    const { event } = source;
    return (
      <div className="event-layer">
        <div className={`type-normal ${event.type !== 'TEXT' && 'hide'}`}>
          <p>
            {event.contents.text}
          </p>
        </div>
        <div className={`type-quiz ${event.type !== 'QUIZ' && 'hide'}`}>
          <div className="quiz-header">
            <span></span>
                    <button className={`answer-view ${this._snsLinkOrNotAnswerView(event) && 'dimmed'}`}
              onClick={this._showAnswer.bind(this)}>답변 보기 ></button>
          </div>
          <p className="quiz-context">
            {event.contents.title}
          </p>
          <div className={`answer-area single-select ${event.contents.type !== 'MULTIPLE' && 'hide'} ${event.contents.myAnswer ? 'dimmed' : ''}`}>
            {event.contents.type === 'MULTIPLE' && event.contents.answers.map((answer, idx) => {
              return (
                <div key={`event-${idx}`} className="type-radio">
                  <input type="radio"
                    id={answer.answerId}
                    name="radio-answer"
                    disabled={event.contents.myAnswer !== null}
                    defaultChecked={event.contents.myAnswer === answer.answerId}
                    onClick={this._onAnswerCheck.bind(this)}
                    value={answer.answerId} 
                    disabled={this._snsTextArea()}
                    />
                  <label htmlFor={answer.answerId}>
                    <span></span>
                    {answer.example}
                  </label>
                </div>
              )
            })}
          </div>
          <div className={`answer-area short-answer ${event.contents.myAnswer ? 'dimmed' : ''} ${event.contents.type !== 'SHORT' && 'hide'}`}>
            <textarea rows="3"
              ref={txt => this.shortAnswer = txt}
              placeholder={T.translate('make.enter-answer')}
              defaultValue={event.contents.myAnswer}
              readOnly={event.contents.myAnswer !== null}
              onChange={this._onShortAnswerChanged.bind(this)}
              disabled={this._snsTextArea()}
            ></textarea>
          </div>
        </div>
        <div className={`type-image ${event.type !== 'IMAGE' && 'hide'}`}>
          <img src={event.contents.thumbnail} alt="test" />
        </div>
        {this.renderConfirmButton()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
    openPopup: state.openPopup,
    userProfile: state.user.userProfile,
  // timelineDetail: state.player.timelineData,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  openPopup
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventLayer);
