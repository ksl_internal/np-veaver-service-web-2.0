import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { convDuration } from "../../util/time";
import cx from 'classnames';
import hlsjs from 'hls.js';
import 'moment-duration-format';
import 'mediaelement';

// Import stylesheet and shims
import 'mediaelement/build/mediaelementplayer.min.css';
import 'mediaelement/build/mediaelement-flash-video.swf';
import './custom.css';

import EventLayer from './EventLayer';
import PlayTimeline from '../Timeline/PlayTimeline';
import MakeTimeline from '../Timeline/MakeTimeline';
import device from "current-device";

import {
    initPlayer,
    seekTimeline,
    refreshCurrentTime,
    setPlayer,
    setMyPlaytime
} from '../../modules/player'; 
let _realPlaytimes=[] //이번에 재생된 시간 목록

let _watchingTime={}
let _playtimes=[]
let _playtime = {}

class MediaElement extends Component {

    state = {
        paused: true,
        ended: false,
        muted: false,
        speed: 1,
        isSpeedOpen: false,
        currentTime: 0,
        duration: 0,
        repeatAB: false,
        volumeRange: 0,
        mediaLoaed: false,
        makeMode: this.props.makeMode,
        showEvent: false,
        eventItems: [],
        isFullScreen: false,
        dynamicWidth:this._calculateDynamicHeightWidth()[0],
        dynamicHeight:this._calculateDynamicHeightWidth()[1]
    }

    constructor(props) {
        super(props);
        this.maxVolumeHeight = 56;
        this.speedOptions = [0.5, 1.0, 1.5, 2.0]; // 4단계로 수정.

        this._onFullscreenControlHandler = this._onFullscreenControlHandler.bind(this);
    }

    _calculateDynamicHeightWidth(){ //this calculation was previously inside the render
        const mediaw= document.getElementById("root");
        const makeMode = this.props.makeMode;
        const snsFlag  = this.props.snsFlag;	
        let smallDevice=false;

        let ret = new Array();

        const defaultWidth = makeMode ? 780 : 890
        const defaulHeight = makeMode ? 438 : 500
    
        if(device.mobile() || device.ipad() || device.ipod() || device.tablet())
    		smallDevice=true;

       //console.log(snsFlag, smallDevice);
     
		if(smallDevice && snsFlag) {
            ret[0] = mediaw.offsetWidth; // width
        	ret[1] = defaulHeight*mediaw.offsetWidth/defaultWidth; // height
            
            // ret[0] = mediaw.offsetWidth > defaultWidth ? defaultWidth : mediaw.offsetWidth; // width
            // ret[1] = defaulHeight*mediaw.offsetWidth/defaultWidth > defaulHeight ? defaulHeight : defaulHeight*mediaw.offsetWidth/defaultWidth; // heigh
                        
        }
        else{
            ret[0] = defaultWidth
            ret[1] = defaulHeight
        }
        return ret;
    }

    initFullScreenAPI(element) {
        document.exitFullscreen = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;
        element.requestFullscreen = element.requestFullscreen || element.webkitRequestFullscreen || element.mozRequestFullScreen || element.msRequestFullscreen;

        document.onfullscreenchange =
            document.onwebkitfullscreenchange =
                document.onmozfullscreenchange =
                    document.onmsfullscreenchange = () => {
                        document.fullscreen = document.mozFullScreen || document.webkitIsFullScreen || document.msFullscreenElement;
                        if (!document.fullscreen) {
                            this.player.exitFullScreen();
                            this.setState((state) => ({...state, isFullScreen: false }));
                        } else {
                            this.setState((state) => ({...state, isFullScreen: true }));
                        }
                    };
    }

    success = (media, node, instance) => {
        this.media = media;

        media.addEventListener('loadedmetadata', this._onLoadedmetadata.bind(this));
        media.addEventListener('play', this._onPlay.bind(this));
        media.addEventListener('pause', this._onPause.bind(this));
        media.addEventListener('ended', this._onEnded.bind(this));
        media.addEventListener('seeked', this._onSeeked.bind(this));

        media.addEventListener(hlsjs.Events.KEY_LOADING, function (data) {
            // All the code when this event is reached...
            // 키값을 쿠키로 처리하기위한 설정.
            const isLoader = Object.prototype.hasOwnProperty.call(data.data[1].frag, 'loader');
            if (isLoader) {
                data.data[1].frag.loader.xhrSetup = function(xhr, location) {
                    if (this.props.timelineSource.type === 'CP') {
                        let serverId = window.location.host.split('.ent')[0];
                        if (serverId.includes('localhost')) {
                            serverId = process.env.REACT_APP_SERVICE_ID;
                        }
                    }
                    xhr.withCredentials = false;
                    xhr.setRequestHeader('Date', new Date().getTime());
                    xhr.setRequestHeader('Vary','Origin');
                    xhr.setRequestHeader('Vary','Access-Control-Request-Headers');
                    xhr.setRequestHeader('Vary','Access-Control-Request-Method');
                    xhr.setRequestHeader('Access-Control-Allow-Origin','*');
                }
            }
        });



    }

    error(media) {
        //console.error('error', media);
    }

    _onLoadedmetadata(evt) {
        // 재생속도 유튜브 지원 안함
        if (!this.player) return;
        this.media.playbackRate = this.state.speed;
        this.props.initPlayer(true, this.player.duration || Math.floor(this.player.domNode.duration));
        this.setState((state) => {
            return {
                ...state,
                mediaLoaed: true,
                volumeRange: this.maxVolumeHeight * this.media.getVolume(),
                currentTime: this.props.beginFrom != null? this.props.beginFrom : 0,
            }
        })
        this.player.currentTime = this.props.beginFrom != null? this.props.beginFrom : 0;
    }

    _onPlay() {
        this.setState((state) => {
            return {
                ...state,
                paused: false,
                ended: false,
            }
        });
        if (this.isPlayerExist()) {
            this.btnPlayAndPause.style.opacity = 0;
            this.btnPlayAndPause.style.visibility = 'hidden';
        }
        // 영상이 재생이 됬을 경우 현재 시간을 시작 시간으로 저장
        this._setMyRealPlaytime(this.player.currentTime,true)
    }
    cloneObject=(obj)=> {
        return JSON.parse(JSON.stringify(obj));
    }
    /**
     * 현재 재생 시간을 저장 합니다.
     * 필수 지식과 일반 지식의 구분을 문의 하였으나 구분을 하지 못함.
     * 상세페이지 진입시 나의 재생시간을 체크하여 서버 전송.
     * currentTime 현재 시간.
     * begin : true/ false  true일 경우 _playtime.begin(임시객체) 에 저장, false 일 경우  _realPlaytimes(리스트) 에 push
     * _watchingTime 사용 되지 않음.
     * real 사용 되지않음.
     */
    _setMyRealPlaytime=(currentTime,begin,real=true)=>{
        //처음 재생하여 보기 시작 할 경우 시작 시간으로 저장
        if(begin){
            if(real){ //임시 객체에 begin 프로퍼티로 시간 저장.
                _watchingTime.begin=Math.floor(currentTime)
                _playtime.begin=Math.floor(currentTime)
            }else{
            }
        }else{ // 재생을 멈추었거나 영상을 건너 뛰었을 경우 end 타임 지정 하여 리스트에 저장.
            if(real){ // 임시 객체에 end 타임으로 저장 후 최종 리스트에 추가.
                if(_watchingTime.begin !=undefined){
                    _watchingTime.end=Math.floor(currentTime)
                    _realPlaytimes.push(this.cloneObject(_watchingTime))
                    _watchingTime = {}
                }
                if(_playtime.begin !=undefined){
                    _playtime.end=Math.floor(currentTime)
                    _playtimes.push(this.cloneObject(_playtime))
                    _playtime = {}
                }
            }else{
            }
        }
    }
    _onPause() {
        this.setState((state) => {
            return {
                ...state,
                paused: true,
            }
        });
        // 사용자가 플레이중 정지 시켰을 경우 현재 시간을 저장
        this._setMyRealPlaytime(this.player.currentTime,false)
        if (this.isPlayerExist()) {
            this.btnPlayAndPause.style.opacity = 1;
            this.btnPlayAndPause.style.visibility = 'visible';
        }
    }

    _onSeeked() {

        this.setState((state) => {
            const eventItems = state.eventItems.map(item => {
                item.fire = false;
                return item;
            })

            return {
                ...state,
                eventItems,
                currentTime: this.player.currentTime,
            }
        });
        this.props.seekTimeline(this.player.currentTime);
        this.props.refreshCurrentTime(this.player.currentTime);
    }

    _onEnded(e) {
        this.setState((state) => {
            const eventItems = state.eventItems.map(item => {
                item.fire = false;
                return item;
            })

            return {
                ...state,
                eventItems,
                currentTime: this.player.duration,
                ended: true,
            }
        });
        // 영상이 완료됬을 경우 현재 시간을 저장
        this._setMyRealPlaytime(this.player.duration,false)
        this.props.refreshCurrentTime(this.player.duration);
    }

    _onFileChange(evt) {
        const reader  = new FileReader();
        reader.addEventListener("load", () => {
            this.player.src = reader.result;
        }, false);

        reader.readAsDataURL(this.file.files[0]);
    }

    _onSpeedChanged(evt) {
        const { textContent } = evt.target;
        this.setState((state) => {
            return {
                ...state,
                speed: parseFloat(textContent),
                isSpeedOpen: !state.isSpeedOpen,
            }
        });
        this.media.playbackRate = parseFloat(textContent);
    }

    _onSpeedControlClick() {
        this.setState((state) => {
            return {
                ...state,
                isSpeedOpen: !state.isSpeedOpen,
            }
        });
    }

    _onVolumeHandle(evt) {
        let volumeRange = this.volumeController.getClientRects()[0].bottom - evt.clientY - 8;
        if (this.maxVolumeHeight < volumeRange) {
            volumeRange = this.maxVolumeHeight;
        }

        this.setState((state) => {
            return {
                ...state,
                volumeRange
            }
        });
    }

    _onVolumeMouseLeave() {
        this.setState((state) => {
            return {
                ...state,
                volumeRange: this.maxVolumeHeight * this.media.getVolume(),
            }
        })
    }

    _onEventLayerOpen = () => {
        //  console.log('On Event Fire');

        this.setState((state) => {
            return {
                ...state,
                showEvent: true,
            }
        });
        if (this.isPlayerExist()) {
            this.player && this.pause();
        }

    }

    _onEventAnswered(eventIdx, answer) {
        // console.log("<<<<<<<<on event answered ");
        this.setState((state) => {
            state.eventItems[eventIdx].event.contents.myAnswer = answer;

            return {
                ...state,
                eventItems: state.eventItems,
            }
        });
    }

    _onEventLayerConfirmed(eventIdx) {
        //console.log("<<<<<<<<<<<<<<<onEventConfirmed");
        this.setState((state) => {
            state.eventItems[eventIdx].fire = true;

            return {
                ...state,
                eventItems: state.eventItems,
                showEvent: false,
            }
        });
        this.media.play();
        this.mediaContainer.classList.remove('event-on');
    }

    _onFullscreenControlHandler() {
        this.mediaContainer.classList.remove('inactive');
        if (this.fullScreenControlViewInterval) {
            clearTimeout(this.fullScreenControlViewInterval);
            this.fullScreenControlViewInterval = undefined;
        }

        this.fullScreenControlViewInterval = setTimeout(() => {
            this.mediaContainer.classList.add('inactive');
        }, 3000);
    }

    setVolume(evt) {
        let volumeRange = this.volumeController.getClientRects()[0].bottom - evt.clientY - 8;
        if (this.maxVolumeHeight < volumeRange) {
            volumeRange = this.maxVolumeHeight;
        }

        this.media.setVolume(volumeRange / this.maxVolumeHeight);

        this.setState((state) => {
            return {
                ...state,
                volumeRange
            }
        });
    }

    setRepeat() {
        const timeline = this.timeline.getWrappedInstance();
        timeline.setRepeat(!this.state.repeatAB);
        this.setState((state) => {
            return {
                ...state,
                repeatAB: !state.repeatAB,
            }
        });
    }

    toggleFullscreen() {
        const timeline = this.timeline.getWrappedInstance();
        timeline.setRepeat(false);
        this.setState((state) => {
            return {
                ...state,
                repeatAB: false,
            }
        });

        if (this.player.isFullScreen) {
            this.exitFullscreen();

        } else {
            this.enterFullScreen();
        }
    }

    enterFullScreen() {
        document.fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.documentElement.webkitRequestFullScreen;
        this.player.enterFullScreen();
    
        if (this.mediaContainer.mozRequestFullScreen) { /* Firefox */
            this.mediaContainer.mozRequestFullScreen();
        } else if (this.mediaContainer.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
            this.mediaContainer.webkitRequestFullscreen();
        } else if (this.mediaContainer.msRequestFullscreen) { /* IE/Edge */
            this.mediaContainer.msRequestFullscreen();
        } else if (this.mediaContainer.requestFullscreen) {
              this.mediaContainer.requestFullscreen();
        }
        
        this.mediaContainer.addEventListener('mousemove', this._onFullscreenControlHandler);
        this.setState((state) => {
            return {
                ...state,
               isFullScreen:true
            }
        });
    }

    exitFullscreen() {
        console.log("answer list");
        if (this.player.isFullScreen) {
            if (document.mozCancelFullScreen) { /* Firefox */
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { /* IE/Edge */
                document.msExitFullscreen();
            }else  if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            this.mediaContainer.removeEventListener('mousemove', this._onFullscreenControlHandler);
            this.setState((state) => {
                return {
                    ...state,
                   isFullScreen:false
                }
            });
        }
    }

    muteChange() {
        this.player.setMuted(!this.state.muted);
        this.setState((state) => {
            return {
                ...state,
                muted: !state.muted
            }
        });
    }

    playAndPause() {
        this.player.paused ?
            this.player.play()
            : this.player.pause()
    }

    pause() {
        this.player.pause();
    }

    renderCurrentTime() {
        return convDuration(this.props.currentTime);
    }

    renderDuration() {
        return convDuration(this.props.duration);
    }

    renderTimeLine() {
        const { mediaLoaed, makeMode } = this.state;

        // if (!mediaLoaed) {
        // 	return null;
        // }

        if (makeMode) {
            return (
                <MakeTimeline step={this.props.makeStep}
                              player={this.player}
                />
            )
        } else {
            return (
                <PlayTimeline
                    ref={el => this.timeline = el}
                    player={this.player}
                    paused={this.state.paused}
                    source={this.props.timelineSource}
                    operable={!this.state.showEvent}
                    _setMyRealPlaytime={this._setMyRealPlaytime}

                />
            )
        }
    }

    get renderEvents() {
        const { snsFlag } = this.props;

        if (this.props.makeMode) return null;

        const eventIdx = this.state.eventItems.findIndex(
            eventItem => Math.floor(this.props.currentTime) === Math.floor(eventItem.event.begin)
        );

        const eventItem = 0 <= eventIdx ? this.state.eventItems[eventIdx] : null;

        if (!this.state.ended && eventItem && !eventItem.fire) {
            this.mediaContainer.classList.add('event-on');

            if (window.player) {
                window.player.pause();
            }

            return (
                <EventLayer
                    timelineDetail={this.props.timelineSource}
                    source={eventItem}
                    onEventFire={this._onEventLayerOpen}
                    onEventConfirmed={this._onEventLayerConfirmed.bind(this, eventIdx)}
                    onAnswered={answer => this._onEventAnswered(eventIdx, answer)}
                    onShowAnswerList={() => this.exitFullscreen()}
                    snsFlag={snsFlag}
                />
            )

        }
    }

    disableFullScreen(){
        if(device.ios() && !device.desktop()){
            return true;
        }
        else
            return false;    

    }

    render() {
        const props = this.props;
        const sources = props.sources;
        const tracks = props.tracks;
        const sourceTags = [];
        const tracksTags = [];
        // const makeMode = this.props.makeMode
        let isFull = this.state.isFullScreen
        for (let i = 0, total = sources.length; i < total; i++) {
            const source = sources[i];
            sourceTags.push(`<source src="${source.src}" type="${source.type}">`);
        }

        for (let i = 0, total = tracks.length; i < total; i++) {
            const track = tracks[i];
            tracksTags.push(`<track src="${track.src}" kind="${track.kind}" srclang="${track.lang}"${(track.label ? ` label=${track.label}` : '')}>`);
        }

        const mediaBody = `${sourceTags.join("\n")} ${tracksTags.join("\n")}`;

        let dynamicHeight= this.state.dynamicHeight;
        let dynamicWidth=this.state.dynamicWidth;


        let FullScreenStyle = {width : dynamicWidth, height : dynamicHeight}

        let mediaHtml;

        if(device.mobile() || device.ipad() || device.ipod() || device.tablet()){

            mediaHtml = props.mediaType === 'video' ?
                `<video autoplay webkit-playsinline="true" playsinline="true" id="${props.id}"  width="100%" height="100%" position:'absolute' top:0 left:0 enablejsapi border:0 box-sizing: border-box frameborder="0" allowfullscreen  ${(props.poster ? ` poster=${props.poster}` : '')}
                    ${(props.controls ? ' controls' : '')}${(props.preload ? ` preload="${props.preload}"` : '')}>
                    ${mediaBody}
                </video>` :
                `<audio id="${props.id}" width="${dynamicWidth}" controls>
                    ${mediaBody}
                </audio>`;   
        }
        else{

            mediaHtml = props.mediaType === 'video' ?
            `<video autoplay webkit-playsinline="true" playsinline="true" id="${props.id}"  width="${dynamicWidth}" height="${dynamicHeight}" position:'absolute' top:0 left:0 enablejsapi border:0 box-sizing: border-box frameborder="0" allowfullscreen  ${(props.poster ? ` poster=${props.poster}` : '')}
                 ${(props.controls ? ' controls' : '')}${(props.preload ? ` preload="${props.preload}"` : '')}>
                 ${mediaBody}
            </video>` :
            `<audio id="${props.id}" width="${dynamicWidth}" controls>
                  ${mediaBody}
            </audio>`;

        }

        // const mediaHtml = props.mediaType === 'video' ?
        // 		`<video autoplay id="${props.id}" width="${props.width}" height="${props.height}"${(props.poster ? ` poster=${props.poster}` : '')}
        // 			${(props.controls ? ' controls' : '')}${(props.preload ? ` preload="${props.preload}"` : '')}>
        // 			${mediaBody}
        // 		</video>` :
        // 		`<audio id="${props.id}" width="${props.width}" controls>
        // 			${mediaBody}
        // 		</audio>`
        // ;

        return (
            <div className="media-wrapper" ref={el => this.mediaContainer = el}>
                {/*<input ref={input => this.file = input}
					type="file"
					accept="video/*"
					style={{ position: 'absolute', top: '-25px' }}
					onChange={this._onFileChange.bind(this)}
				/>*/}
                <div className="media-container" style={{width : dynamicWidth, height : dynamicHeight}}>
                    <div
                        style={{ height: '100%', width:'100%', position:'relative',overflow: 'hidden', display:'block'}}
                        dangerouslySetInnerHTML={{__html: mediaHtml}}>
                    </div>
                    {this.renderEvents}
                </div>
                <div className="control-area player" style={ isFull ? {} : FullScreenStyle} onKeyUp={e => {
                     // console.log(e);
                }}>
                    <div className="reaction-graph" style={{ visibility: 'hidden' }}>
                        <svg width={dynamicWidth} height="80" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 80 C 40 10, 65 50, 95 80 S 130 100, 200 80, M200 80 C 290 0, 350 70, 480 50 S 730 100, 890 80" stroke="transparent" fill="rgba(53, 102, 214, .7)"/>
                        </svg>
                    </div>
                    <div className="controls">
                        <div className="play-layer">
                            <button  style={{border:0, outline:'none'}}
                                ref={btn => this.btnPlayAndPause = btn}
                                onClick={this.playAndPause.bind(this)}
                                className={`${!this.state.paused ? 'pause' : 'play'}`}
                            />
                        </div>
                        <div className="control-elements">
                            <div className="left-side">
                                <button
                                    onClick={this.playAndPause.bind(this)}
                                    className={`play ${!this.state.paused ? 'pause' : ''}`}
                                />
                                <div className={`volume ${this.state.muted ? 'mute': ''}`}>
                                    <button onClick={this.muteChange.bind(this)}></button>
                                    <div className="volume-controller"
                                         ref={el => this.volumeController = el}
                                         onMouseMove={this._onVolumeHandle.bind(this)}
                                         onMouseLeave={this._onVolumeMouseLeave.bind(this)}
                                         onClick={this.setVolume.bind(this)}>
                                        <span className="volume-range"></span>
                                        <span className="volume-slider"
                                              style={{ height: `${this.state.volumeRange}px` }}
                                        ></span>
                                    </div>
                                </div>
                                <div className="run-time">
									<span className="running">
										{this.renderCurrentTime()}
									</span>
                                    <span className="ending">
										{this.renderDuration()}
									</span>
                                </div>
                            </div>
                            <div className={`right-side ${this.state.makeMode && 'hide'}`}>
                                <div className={`speed ${!this.state.makeMode && this.props.timelineSource.type === 'YOUTUBE' && 'hide'}`}>
                                    <button className="selected"
                                            onClick={this._onSpeedControlClick.bind(this)}
                                    >
                                        {`${this.state.speed} x`}
                                    </button>
                                    <ul style={{ visibility: `${this.state.isSpeedOpen ? 'visible' : 'hidden'}` }}>
                                        {this.speedOptions.map((option, i) =>
                                            <li key={`speend-options-key-${i}`} className={this.state.speed === option ? 'selected' : ''}>
                                                <button onClick={this._onSpeedChanged.bind(this)}>{option}</button>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                                <button className={cx('section-repeat', { 'repeat-on': this.state.repeatAB })}
                                        onClick={this.setRepeat.bind(this)}
                                />
                                <button className={this.disableFullScreen()?'':cx('full-screen', { reduce: this.state.isFullScreen })} disabled={this.disableFullScreen()} onClick={this.toggleFullscreen.bind(this)}></button>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.renderTimeLine()
                }
            </div>
        );
    }

    _onOrientationChange(){

        let width = this._calculateDynamicHeightWidth()[0];

        if(device.mobile() || device.ipad() || device.ipod() || device.tablet()){
           
            if(width!=this.state.dynamicWidth && this.player.isFullScreen)
                this.enterFullScreen();

            this.setState((state) => {
                return {
                    ...state,
                    dynamicWidth: width,
                    dynamicHeight: this._calculateDynamicHeightWidth()[1]
                }
            });
        }

    }

    componentDidMount() {

        let _this=this;
        window.addEventListener("resize", function(e) {
            _this._onOrientationChange();
        }, false);

        const { MediaElementPlayer } = global;
        if (!MediaElementPlayer) return;

        this.initFullScreenAPI(this.mediaContainer);

        if (this.props.timelineSource) {
            const { events } = this.props.timelineSource;
            const eventItems = events.map(event => {
                return { event, fire: false }
            });

            this.setState((state) => {
                return {
                    ...state,
                    eventItems,
                }
            });
        }
        const options = Object.assign({ }, this.props.options, {
            // Read the Notes below for more explanation about how to set up the path for shims
            // pluginPath: './static/media/',
            hls: {
                xhrSetup: function(xhr, location) {
                    xhr.setRequestHeader('Cache-Control', 'no-cache');
                    xhr.setRequestHeader('Cache-Control', 'no-store');
                    xhr.setRequestHeader('cache-control', 'max-age=0');
                    xhr.setRequestHeader('X-Date', new Date().getTime());
                    xhr.setRequestHeader('Vary','Origin');
                    xhr.setRequestHeader('Vary','Access-Control-Request-Headers');
                    xhr.setRequestHeader('Vary','Access-Control-Request-Method');
                    xhr.setRequestHeader('Access-Control-Allow-Origin','*');
                },
                manifestLoadingTimeOut:30000,
                manifestLoadingMaxRetry:10,
                manifestLoadingMaxRetryTimeout:100000,
                fragLoadingTimeOut:30000,
                fragLoadingMaxRetry:10,
                fragLoadingMaxRetryTimeout:100000
            },
            autoRewind: false,
            success: (media, node, instance) => this.success(media, node, instance),
            error: (media, node) => this.error(media, node)
        });

        this.player = new MediaElementPlayer(this.props.id, options);
        this.props.setPlayer(this.player);


        this.player.timeFormat = 'mm:ss';
        this.timelineTick = setInterval(() => {

            !this.state.paused && this.timelineTick && this.setState((state) => {
                return {
                    ...state,
                    currentTime: this.player.currentTime,
                }
            });
            if (!this.props.needMoveStack) {
                this.props.refreshCurrentTime(this.state.currentTime);
            }
        }, 500);

    }

    // 영상이 변경되었을때 메타정보를 다시 설정 한다.
    componentDidUpdate(nextProps) {
        if (this.props.makeStep > 0 && this.player.node.readyState !== 4) {
        }

        if (this.props.sources[0].src !== nextProps.sources[0].src) {
            const { MediaElementPlayer } = global;
            const options = Object.assign({ }, this.props.options, {
                autoRewind: false,
                success: (media, node, instance) => this.success(media, node, instance),
                error: (media, node) => this.error(media, node)
            });
            this.player = new MediaElementPlayer(this.props.id, options);
            this.player.timeFormat = 'mm:ss';
            this.setState((state) => ({ ...state, currentTime: 0 }));
        }
    }

    componentWillMount() {
        this.props.refreshCurrentTime(0);
    }

    componentWillUnmount() {

        let _this=this;
        window.removeEventListener("resize", function(e) {
            _this._onOrientationChange();
        }, false);

        //마지막 재생 시간 this.player.currentTime
        //사용자가 해당 뷰에서 벗어 날 경우 저장되어있던 재생 시간 목록을 서버로 요청 하여 저장.
        try {
            if(_realPlaytimes.length===0 && !this.state.ended){
                this._setMyRealPlaytime(this.player.currentTime,false)
            }
            //let temp = this.props.timelineSource.playtimes == undefined ? [] :this.props.timelineSource.playtimes
            //temp.concat(_playtimes)
            let req = {
                playtimes:_playtimes,
                realPlaytimes:_realPlaytimes,
                realPlaytime:0,
                lastPlaytime:Math.floor(this.player.currentTime)
            }
            if (this.player !== undefined) {
                this.props.setMyPlaytime(this.props.timelineSource.timelineIdx,req)
            }
        } catch (error) {

        }
        if (this.player !== undefined && this.player.node.readyState === 4) {
            this.player.muted = true;
            this.player.remove();
            this.player = undefined;
        }
        clearInterval(this.timelineTick);
        this.timelineTick = false;
        clearTimeout(this.fullScreenControlViewInterval);
        this.fullScreenControlViewInterval = false;
    }

    isPlayerExist = () => this.player !== undefined && this.player.node.readyState === 4
}

MediaElement.propTypes = {
    makeMode: PropTypes.bool,
    options: PropTypes.object,
    tracks: PropTypes.object,
};

MediaElement.defaultProps = {
    makeMode: false,
    options: {
        features: [],
    },
    tracks: {},
};

const mapStateToProps = state => ({
    currentTime: state.player.currentTime,
    duration: state.player.duration,
    needMoveStack: state.player.needMoveStack,
    userKey: state.auth.userAuthToken,
    reaction : state.reaction,
    isFullScreen : state.isFullScreen
});

const mapDispatchToProps = dispatch => bindActionCreators({
    initPlayer,
    refreshCurrentTime,
    seekTimeline,
    setPlayer,
    setMyPlaytime
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MediaElement);
