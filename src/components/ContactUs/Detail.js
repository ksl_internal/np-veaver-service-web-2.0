import React, { Component } from "react";
import moment from 'moment';
import T from "i18n-react";
/**
 * 문의하기 상세페이지 뷰
 * @param {*} param0 
 */
const Detail = ({item,page}) =>{
  function getEemarks(item,page){
    if(item.statusFlag=='C'){
      return (
        <div className="detail-answer">
          <div><span>{T.translate('service.reception-status')}</span> : 처리 완료 </div>
          <div><span>{T.translate('service.answer')}</span> : {item.remarks}</div>
        </div>
      )
    }
  }
  return (
    <div className="detail-wapper">
      <div className="detail-title">
        {item.subject}
      </div>
      <div className="detail-info">
        <div>
          <div>
            <label>{T.translate('service.date')}</label>
            <span>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</span>
          </div>
          <div>
            <label>{T.translate('service.classify')}</label>
            <span>{item.contactUsTypeInfo}</span>
          </div>
        </div>
        <div>
          <div>
            <label>{T.translate('service.number')}</label>
            <span>{item.regNum}</span>
          </div>
          <div>
            <label>{T.translate('service.degree-importance')}</label>
            <span>{item.importantFlag=='Y' ? T.translate('service.urgent') : T.translate('service.normal')}</span>
          </div>
        </div>
      </div>
      {getEemarks(item,page)}

      <div className="detail-content">
        {item.content}
      </div>
    </div>
  );
}

// class Detail extends Component {
//   getEemarks(item){
//     if(item.statusFlag=='C'){
//       return (
//         <div className="detail-answer">
//           <div><span>접수상태</span> : 처리 완료 </div>
//           <div><span>처리답변</span> : {item.remarks}</div>
//         </div>
//       )
//     }
//   }
//   render() {
//     const {item} = this.props;
//     return (
//         <div className="detail-wapper">
//           <div className="detail-title">
//             {item.subject}
//           </div>
//           <div className="detail-info">
//             <div>
//               <div>
//                 <label>접수일시</label>
//                 <span>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</span>
//               </div>
//               <div>
//                 <label>분류</label>
//                 <span>{item.contactUsTypeInfo}</span>
//               </div>
//             </div>
//             <div>
//               <div>
//                 <label>접수일시</label>
//                 <span>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</span>
//               </div>
//               <div>
//                 <label>중요도</label>
//                 <span>{item.importantFlag=='Y' ? '중요' : '보통'}</span>
//               </div>
//             </div>
//           </div>
//           {this.getEemarks(item)}

//           <div className="detail-content">
//             {item.content}
//           </div>
//         </div>
//     );
//   }
// }

export default Detail;
