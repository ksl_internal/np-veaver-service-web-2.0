import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../modules/contact';
import moment from 'moment';
import { Link } from 'react-router-dom';
import T from "i18n-react";
/**
 * 문의하기 리스트 뷰
 * @param {*} param0 
 */
const List = ({items,page ,history,language}) =>{
  if(items== undefined){
    return null;
  };
  function getPage(e,item){
    e.stopPropagation();
    history.push(`/contact/${item.idx}`);
  }
  function convertText(idx){
    let afterText=''
    switch(idx){
      case 1:
        afterText="장애(앱종료/로그인/업로드)"   
         break;
      case 2:
        afterText="버그(서비스 이용 중 오류)"
         break;
      case 3:
        afterText="이용 방법 문의(공유/폴더에 담기 등)"
         break;
      case 4:
        afterText="서비스 개선 의견"
         break; 
      default :
        afterText="기타"   
    }
    return afterText
  }
  function getItem(items,page){
    //console.log('items',items)
     const list =items.list.map(item=>{
      if(language === 'ko'){
        item.contactUsTypeInfo = convertText(item.contactUsTypeIdx)
      }
      return (
        // <Link key={item.idx} to={{pathname:`/contact/${item.idx}`,state:{page:page}}}>
        // </Link>
        <tr key={item.idx} onClick={(e)=>getPage(e,item)}>
          <td>{item.idx}</td>
          <td>{item.contactUsTypeInfo}</td>
          <td style={{textAlign:'left',paddingLeft:'43px',lineHeight:'26px',paddingTop:'9px',paddingBottom:'10px'}}>{item.subject}<div className="reception-number">접수번호:{item.regNum}</div></td>
          <td>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</td>
        </tr>
      )
    })
    return list;
  }
  return (
    <div className="list-wapper">
      {items.length ==0
        ? ''
        :(
          <table>
              <colgroup>
                <col width="80px"/>
                <col width="150px"/>
                <col width="*"/>
                <col width="185px"/>
              </colgroup>
            <thead>
              <tr>
                <th>No.</th>
                <th>{T.translate('service.classify')}</th>
                <th style={{textAlign:'left',paddingLeft:'123px'}}>{T.translate('service.title')}</th>
                <th>{T.translate('service.date')}</th>
              </tr>
            </thead>
            <tbody>
              {getItem(items,page,history)
                }
            </tbody>
          </table>
        )
      }
      
    </div>
  );
}

// class List extends Component {

//   render() {
//     const { items } = this.props;
//     let itemList =null
//     if(items != undefined){
//       console.log('items : ',items)
//       itemList = items.list.map(item=>{
//         return (
//           <tr key={item.idx}>
//             <td>{item.idx}</td>
//             <td>{item.contactUsTypeInfo}</td>
//             <td><Link to={`/contact/${item.idx}`}>{item.subject}<div className="reception-number">{item.regNum}</div></Link></td>
//             <td>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</td>
//           </tr>
//         )
//       })
//     }
//     return (
//         <div className="list-wapper">
//           <table>
//               <colgroup>
//                 <col width="80px"/>
//                 <col width="150px"/>
//                 <col width="*"/>
//                 <col width="200px"/>
//               </colgroup>
//             <thead>
//               <tr>
//                 <th>No.</th>
//                 <th>분류</th>
//                 <th>제목</th>
//                 <th>접수 일시</th>
//               </tr>
//             </thead>
//             <tbody>
//               {itemList}
//             </tbody>
//           </table>
//          </div>
//     );
//   }
// }

export default List;
