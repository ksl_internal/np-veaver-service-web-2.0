import React, { Component } from "react";
import T from "i18n-react";
/**
 * 문의하기 글쓰기 뷰
 * @param {*} param0 
 */
class Write extends Component {
  componentDidUpdate(prevProps, prevState) {

    document.documentElement.scrollTop = 0;
  }
  shouldComponentUpdate(nextProps, nextState){
    if(nextProps.categories !== this.props.categories) {
      document.documentElement.scrollTop = 0;
      return true;
    }else{

      return true;
    }
  }
  render() {
    const { onChange ,postItem ,categories ,language} = this.props;
    if(categories == null) return null;
    function getOptions(categories){
      return categories.map(category=>{
        function convertText(category){
          if(language!=='ko'){
            return category.name;
          }else{
            let afterText=''
            switch(category.idx){
              case 1:
                afterText="장애(앱종료/로그인/업로드)"   
                 break;
              case 2:
                afterText="버그(서비스 이용 중 오류)"
                 break;
              case 3:
                afterText="이용 방법 문의(공유/폴더에 담기 등)"
                 break;
              case 4:
                afterText="서비스 개선 의견"
                 break; 
              default :
                afterText="기타"   
            }
            return afterText
          }
        }
        return (
          <option value={category.idx} key={category.idx} defaultChecked>{convertText(category)}</option>
        )
      })
    }
    return (
        <div className="write-wapper">
          <div className="write-row">
            <div className="lable">{T.translate('service.classify')}</div>
              <select className="w270" name="categoryIdx" onChange={onChange}>
               {getOptions(categories)}
             </select>
            <div className="write-word-count"></div>
          </div>
          <div className="write-row">
            <div className="lable">{T.translate('service.title')}</div>
            <div className="write-input-field">
              <input className="w100p" name="subject" onChange={onChange} maxLength={20}></input>
            </div>
            <div className="write-word-count">({postItem.subject.length}/20)</div>
          </div>
          <div className="write-row">
            <div className="lable">{T.translate('service.degree-importance')}</div>
            <div className="write-input-field">
              <input type="radio" id="vote-01" name="importantFlag" value="N" defaultChecked onChange={onChange}/>
              <label htmlFor="vote-01">
                {T.translate('service.normal')}
              </label>

              <input type="radio" id="vote-02" name="importantFlag" value="Y" onChange={onChange} />
              <label htmlFor="vote-02">
                {T.translate('service.urgent')}
              </label>
            </div>
            <div className="write-word-count"></div>
          </div>
          <div className="write-row h298">
            <div className="lable">{T.translate('service.content')}</div>
            <div className="write-input-field">
              <textarea name="content" onChange={onChange} maxLength={500}></textarea>
            </div>
            <div className="write-word-count">({postItem.content.length}/500)</div>
          </div>
         </div>
    );
  }
}

export default Write;
