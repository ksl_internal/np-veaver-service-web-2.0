import React, { Component } from 'react';
import pt from 'prop-types';
import cx from 'classnames';

class LoadingForm extends Component {

  static propTypes = {
    isLoadingStoped: pt.bool.isRequired,
  }

  static get defaultProps() {
    return {
      isLoadingStoped: true,
    }
  }

  render() {
    return(
      <div className={cx('page-loading', { hide: this.props.isLoadingStoped })}>
        <div className="loading-wrapper">
          <span className="loading-icon left-top"></span>
          <span className="loading-icon right"></span>
          <span className="loading-icon left-bottom"></span>
        </div>
      </div>
    );
  }
}

export default LoadingForm;
