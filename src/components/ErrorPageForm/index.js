import React, { Component } from 'react';
// import PropTypes from 'prop-types';
// import { changeForm } from '../../actions/AppActions';

class ErrorPageForm extends Component {

  render() {
    return(
      <div className="error-page-wrapper hide">
        <div className="error-page not-found">
          <img className="not-found_img" src="/images/error_404.png" alt="Error Page" />
          <img className="else" src="/images/error.png" alt="Error Page" />
          <div className="error-context">
            <p className="main-message">
              죄송합니다. 현재 찾을 수 없는 페이지를 요청하셨습니다.
            </p>
            <p className="sub-message">
              존재하지 않는 주소를 입력하셨거나 올바르지 않은 페이지입니다.
            </p>
            <p className="sub-message">
              기타 궁금하신 사항이 있으시면 고객센터를 통해 문의주시기 바랍니다.
            </p>
            <a href="/" className="btn-solid btn-blue shortcut">비버 홈 바로가기</a>
          </div>
        </div>
      </div>
    );
  }
}

export default ErrorPageForm;
