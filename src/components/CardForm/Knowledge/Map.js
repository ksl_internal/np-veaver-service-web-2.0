import { PropTypes } from 'prop-types';
import React, { Component } from 'react';

class Map extends Component {

  renderMakeMapCard(lat, lng) {
    return(
      <div className="card-item map-card">
        <div className="map-info-wrapper">
          <span className="map-icon"></span>
          <div className="map-info">
            <p className="location">
              {this.props.data.name}
            </p>
            <p className="address">
              {this.props.data.description}
            </p>
          </div>
        </div>
        <div className="card-button">
          <button className="delete" onClick={(e) => this.props.onDelete(e, true)}></button>
        </div>
      </div>
    );
  }

  renderMapCard(lat, lng) {
    return (
      <div className="card type-map">
        <a className="map-area" href={`https://maps.google.com/maps?q=${lat},${lng}`} target="_blank" rel="noopener noreferrer">
          <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=16&size=330x154&markers=color:blue%7Clabel:S%7C${lat},${lng}&key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}`} alt="Map Thumbnail" />
        </a>
        <div className="map-info">
          <p className="location">
            {this.props.data.name}
          </p>
          <p className="address">
            {this.props.data.description}
          </p>
        </div>
      </div>
    );
  }

  render() {
    const lat = this.props.data.latitude;
    const lng = this.props.data.longitude;
    return this.props.make ? this.renderMakeMapCard(lat, lng) : this.renderMapCard(lat, lng);
  }
}

Map.propTypes = {
  make: PropTypes.bool,
  data: PropTypes.shape({
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }),
  onDelete: PropTypes.func,
}

Map.defaultProps = {
  make: false,
  data: {
    latitude: 0,
    longitude: 0,
    name: '무제',
    description: '',
  },
  onDelete: () => {},
}

export default Map;
