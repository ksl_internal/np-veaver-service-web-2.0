import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toastr } from "react-redux-toastr";
import Flatpickr from 'react-flatpickr'
import moment from 'moment';
import { openPopup } from '../../../modules/popup';
import cx from 'classnames';
import {changeTimeline} from "../../../modules/player";
import T from "i18n-react";

class Vote extends Component {
  static defaultPorps = {
    onFocusOut: () => {},
    onFocus: () => {},
  }
  
  constructor(props, context) {
    super(props, context);
    moment.updateLocale('ko', {
      weekdays: ["일요일","월요일","화요일","수요일","목요일","금요일","토요일"],
      weekdaysShort: ["일","월","화","수","목","금","토"],
    });
    this.state = {
      availableFlag: props.data.limitDate && props.data.limitDate <= moment().valueOf() ? false : true,
    }
  }
 
  _showAnswer(e) {
    e.preventDefault();
    this.props.openPopup('VOTE', this.props);
  }

  componentDidMount() {
    this.moment = moment();
    const minORDays = 30;
    this.time = this.moment.add(minORDays - (this.moment.minute() % minORDays), 'minutes')
    
  }
  /**
   * 투표 제목 수정
   */
  _onChangeVoteTitle = ({ target }) => {
    this.limitStringHandler(target);
    this.props.exportVoteModel(Object.assign(this.props.data, { title: target.value }))
  }
  /**
   * 투표 항목 추가
   */
  _onAddExample = (e) => {
      this.props.changeTimeline(this.props.begin);
    e.preventDefault()
    const data = Object.assign({}, this.props.data);
    if (data.answers.length < 5) {
      data.answers.push({
        example: '',
        orderValue:  Math.max(...data.answers.map(item => item.orderValue)) + 1,
      });
      this.props.exportVoteModel(data);
    } else {
      toastr.light('', '최대 5개 항목까지 등록 가능합니다.')
    }
    this.props.onFocusOut()
  }
  /**
   * 투표 항목 제거
   */
  _onDeleteExample = (e, i) => {
      this.props.changeTimeline(this.props.begin);
    e.preventDefault()
    const data = Object.assign({}, this.props.data);
    if (data.answers.length > 2) {
      data.answers.splice(i ,1);
      this.props.exportVoteModel(data);
      this.props.onFocusOut();
    } else {
      toastr.light('', '최소 2개 항목은 등록해야 합니다.')
    }
  }
  /**
   * 투표 항목 수정
   */
  _onChangeExample = ({ target }, i) => {
    this.limitStringHandler(target);
    const data = Object.assign({}, this.props.data);
    data.answers[i].example = target.value;
    this.props.exportVoteModel(data);
  }
  /**
   * 투표 항목에 대한 중복평가.
   */
  _onAnswerValidator = ({target}, i) => {
    this.limitStringHandler(target);
    const data = Object.assign({}, this.props.data);
    const answers = data.answers.filter(a => a.example !== '').map(a => a.example);
    const alreadyExists = answers.filter(a => a === target.value).length > 1;
    if (target.value !== '' && alreadyExists) {
      target.value = '';
      target.focus();
      data.answers[i].example = '';
      this.props.exportVoteModel(data);
      toastr.light('', '정답문항이 동일한 항목이 있습니다.')
    }
    this.props.onFocusOut();
  }
  /**
   * 투표 제한 시간 여부 체크
   */
  _onChangeVoteCheckOfLimitDate = ({ target }, time) => {
    this.props.changeTimeline(this.props.begin);
    const data = Object.assign({}, this.props.data);
    if (target.checked) {
      data.limitDate = time;
    } else {
      delete data.limitDate;
    }
    this.props.exportVoteModel(data);
    this.props.onFocusOut();
  }
  /**
   * 투표 제한 시간 여부 체크
   */
  _onChangeVoteDate = (time) => {
    if (!time.length) return;
    const data = Object.assign({}, this.props.data);
    data.limitDate = time[0].valueOf();
    this.props.exportVoteModel(data);
    this.props.onFocusOut();
  }
  /**
   * 다중 투표 가능 여부 체크
   */
  _onMultipleCheck = ({ target }) => {
      this.props.changeTimeline(this.props.begin);
    const data = Object.assign({}, this.props.data);
    data.multipleFlag = target.checked ? 'Y' : 'N';
    this.props.exportVoteModel(data);
    this.props.onFocusOut();
  }
  /**
   * 익명 투표 가능 여부 체크
   */
  _onAnonymousCheck = ({ target }) => {
      this.props.changeTimeline(this.props.begin);
    const data = Object.assign({}, this.props.data);
    data.anonymousFlag = target.checked ? 'Y' : 'N';
    this.props.exportVoteModel(data);
    this.props.onFocusOut();
  }
  /**
   * 항목 추가 가능 여부 체크
   */
  _onExampleExtends = ({ target }) => {
      this.props.changeTimeline(this.props.begin);
    const data = Object.assign({}, this.props.data);
    data.addAnswerFlag = target.checked ? 'Y' : 'N';
    this.props.exportVoteModel(data);
    this.props.onFocusOut();
  }
  /**
   * 투표하기
   * 
   * @memberof Vote
   */
  _onVote = (e) => {
    e.preventDefault();
    const { cardIdx } = this.props;
    const resultElements = e.target[`vote-${cardIdx}`];
    const elementFilter = () => Array.prototype.filter.call(resultElements, e => e.checked);
    const answerIds = elementFilter().map(a => a.value).join(',');

    if(answerIds) {
      this.props.onVote(cardIdx, answerIds);
    } else {
      toastr.light('', '답변을 선택해 주세요.');
    }
    // this.props.onFocusOut();
  }

  limitStringHandler = (target) => {
    const { maxLength, value } = target;
    if (value.length >= maxLength) {
      toastr.light('', `${maxLength}자 이내로 입력해주세요.`);
      value.substring(0, maxLength);
    }
  }

  renderMakeVoteCard() {
    return (
      <div className="card-item vote-card">
        <div className="vote-wrapper">
          <div className="vote-title">
            <span></span>
            <input
              type="text"
              maxLength={100}
              placeholder={T.translate('make.enter-vote-title')}
              onFocus={e =>{this.props.onFocus; this.props.changeTimeline(this.props.begin)}}
              onBlur={this.props.onFocusOut}
              onChange={this._onChangeVoteTitle}
              defaultValue={this.props.data.title}
            />
          </div>

          <div className="vote-list">
            {
              this.props.data.answers && this.props.data.answers.map((answer, i) => {
                return (
                  <div className="vote-item" key={`vote-item${i}`}>
                    <button className="except" onClick={e => this._onDeleteExample(e, i)}></button>
                    <input
                    type="text"
                    maxLength={50}
                    placeholder={`${T.translate('make.items')}${i + 1}`}
                    defaultValue={answer.example}
                    onFocus={e =>{this.props.onFocus; this.props.changeTimeline(this.props.begin)}}
                    onBlur={e => this._onAnswerValidator(e, i)}
                    onChange={e => this._onChangeExample(e, i)} />
                  </div>
                );
              })
            }
            <div className="vote-item">
              <button className="add" onClick={this._onAddExample}></button>
              <input
              type="text"
              maxLength={50}
              onFocus={e =>{this.props.onFocus; this.props.changeTimeline(this.props.begin)}}
              onBlur={this.props.onFocusOut}
              placeholder={`${T.translate('make.items')}${this.props.data.answers ? this.props.data.answers.length + 1 : 1}`}
              className="dimmed" />
            </div>
          </div>

          <div className="vote-settings">
            <div className="type-check">
              <input
                type="checkbox"
                id="deadline"
                checked={Object.prototype.hasOwnProperty.call(this.props.data, 'limitDate') && this.props.data.limitDate !== null}
                onChange={e => {
                  this._onChangeVoteCheckOfLimitDate(e, this.time.valueOf());
                }}
               />
              <label htmlFor="deadline">
                <span></span>
                {T.translate('make.set-deadline')}
              </label>
            </div>

              <div className={cx('deadline-selection',{ hide: !(Object.prototype.hasOwnProperty.call(this.props.data, 'limitDate') && this.props.data.limitDate !== null) })}>
                <div className="calendar">
                   <Flatpickr
                   value={this.props.data.limitDate}
                   onChange={date => this._onChangeVoteDate(date)}
                   onOpen={this.props.onFocus}
                   onClose={this.props.onFocusOut}
                   options={{ 
                     mode: 'single',
                     minDate: new Date()
                   }}
                   data-enable-time 
                   />
                </div>
              </div>

            <div className="type-check">
              <input
              type="checkbox"
              id="multiselect"
              checked={this.props.data.multipleFlag === 'Y'}
              onChange={this._onMultipleCheck} />
              <label htmlFor="multiselect">
                <span></span>
                {T.translate('make.multiple-section')}
              </label>
            </div>
            <div className="type-check">
              <input
              type="checkbox"
              id="anonymous"
              checked={this.props.data.anonymousFlag === 'Y'}
              onChange={this._onAnonymousCheck}/>
              <label htmlFor="anonymous">
                <span></span>
                {T.translate('make.anonymous-vote')}
              </label>
            </div>
            <div className="type-check">
              <input
              type="checkbox"
              id="allow-add"
              checked={this.props.data.addAnswerFlag === 'Y'}
              onChange={this._onExampleExtends} />
              <label htmlFor="allow-add">
                <span></span>
                {T.translate('make.allow-more-choices')}
              </label>
            </div>
          </div>
        </div>
        {
           this.props.isDeployed ? (this.props.cardIdx !== -1 ?  <div className="modify-dimmed"></div> : '') : ''
        }
        <div className="card-button">
          <button className="delete" onClick={e => { 
            this.props.onDelete(e, true);
            this.props.onFocusOut();
          }}></button>
        </div>
      </div>
    );
  }



  renderLimitDate = (limitDate) => {
    return limitDate ? <p className="limit">{`${moment(limitDate).format('YYYY. MM. DD (ddd) HH:mm')} 까지`}</p> : <p className="limit"></p>;
  }

  renderExample = (answers, cardIdx, multipleFlag, condition, myAnswer) => {
    let results;
    switch (multipleFlag) {
      case 'Y':
      results = answers.map((answer, i) => {
            return (
              <div className={cx('type-check examples', { dimmed: condition })} key={`check-${answer.answerId}`}>
                <input
                  type="checkbox"
                  id={answer.answerId}
                  value={answer.answerId}
                  name={`vote-${cardIdx}`}
                  defaultChecked={myAnswer === answer.answerId}
                />
                <label htmlFor={answer.answerId}>
                  <span></span>
                  {answer.example}
                </label>
              </div>
            );
          })
      break;
      case 'N':
        results = answers.map((answer, i) => {
            return (
              <div className={cx('type-radio examples', { dimmed: condition })} key={`radio-${answer.answerId}`}>
                <input 
                  type="radio"
                  id={answer.answerId}
                  value={answer.answerId}
                  name={`vote-${cardIdx}`}
                  defaultChecked={myAnswer === answer.answerId}
                />
                <label htmlFor={answer.answerId}>
                  <span></span>
                  {answer.example}
                </label>
              </div>
            );
          })
      break;
      default: throw new Error('unexpected type');
    }
    return results;
  }

  voteCardValidator = (myAnswer, limitDate, addAnswerFlag) => {
    const isNotAddAnswer = addAnswerFlag === 'N'
    const isAnswered =  myAnswer !== '';
    let isLimitDateEnded = false;
        
    if (limitDate) {
      const currentDate = Date.now();
      isLimitDateEnded = limitDate <= currentDate;
    }
    return Object.assign({} ,{
      standard: (isAnswered || isLimitDateEnded),
      isNotAddAnswer: (isAnswered || isLimitDateEnded || isNotAddAnswer),
     })
  }

  renderVoteCard() {
    const { data, cardIdx } = this.props;
    const { answers, myAnswer, multipleFlag, limitDate, addAnswerFlag } = data;
    const condition = this.voteCardValidator(myAnswer, limitDate, addAnswerFlag);

    return (
      <div className="card type-vote" data-cardidx={this.props.cardIdx} data-multiple={data.multipleFlag}>
      <form onSubmit={this._onVote}>
        <div className="vote-header">
          <span className="vote-icon"></span>
          <a className={cx('view-result',{ dimmed: !condition.standard })} href="#" onClick={e => this._showAnswer(e)}>
            결과 보기
          </a>
        </div>
        <div className={cx('vote-form', multipleFlag ?'multiple-choice' : 'single-choice')}>
          <div className="context">
            <p>{data.title}</p>
            {this.renderLimitDate(limitDate)}
          </div>
          {this.renderExample(answers, cardIdx, multipleFlag, condition.standard, myAnswer)}
          <div className={cx('vote-input', { hide: condition.isNotAddAnswer })}>
            <input type="text" placeholder="투표항목 추가" maxLength={50} />
            <button type="button" onClick={e => this.props.onAddExample(e)}>입력</button>
          </div>
        </div>
        <div className={cx('vote-submit', { dimmed: condition.standard })}>
          <button type="submit">투표 하기</button>
        </div>
        </form>
      </div>
    );
  }

  render() {
    return this.props.make ? this.renderMakeVoteCard() : this.renderVoteCard();
  }
}

Vote.propTypes = {
  make: PropTypes.bool,
  cardIdx: PropTypes.number,
  data: PropTypes.shape({
    title: PropTypes.string,
    limitDate: PropTypes.number,
    multipleFlag: PropTypes.string,
    anonymousFlag: PropTypes.string,
    addAnswerFlag: PropTypes.string,
    myAnswer: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    answers: PropTypes.arrayOf(PropTypes.shape({
      answerId: PropTypes.string,
      orderValue: PropTypes.number,
      example: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }))
  }),
  onInput: PropTypes.func,
  onDelete: PropTypes.func,
  onAddExample: PropTypes.func,
  onDeleteExample: PropTypes.func,
  onVote: PropTypes.func.isRequired,
}

Vote.defaultProps = {
  make: false,
  cardIdx: 0,
  data: {
    title:'',
    answers: [],
    limitDate: null,
    multipleFlag: 'N',
    anonymousFlag: 'N',
    addAnswerFlag: 'N',
    myAnswer: '',
      begin : 0,
  },
  onInput: () => {},
  onDelete: () => {},
  onAddExample: () => {},
  onDeleteExample: () => {},
  onVote: () => {},
};

const mapStateToProps = state => ({
	openPopup: state.openPopup,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  openPopup,
  changeTimeline,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Vote);
