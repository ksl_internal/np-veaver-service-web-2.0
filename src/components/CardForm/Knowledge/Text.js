import { PropTypes } from 'prop-types';
import React, { Component } from 'react';
import { toastr } from "react-redux-toastr";
import { connect } from 'react-redux';
import {changeTimeline} from "../../../modules/player";
import {bindActionCreators} from "redux";
import T from "i18n-react";

class Text extends Component {

  renderEventTextCard() {
    // console.log('event :: ', this.props);
    return (
      <div className={`card-item text-card ${this.props.isModification && this.props.item.eventIdx > 0 ? 'dimmed' : ''}`}>
      <textarea
        rows="3"
        maxLength={10000}
        placeholder={T.translate('make.pls-enter')}
        //defaultValue={this.props.item.contents.text}
        value={this.props.val}
        onFocus={e =>{this.props.onFocus; this.props.changeTimeline(this.props.begin)}}
        onChange={this._onInputChange}
        onBlur={e => this.props.onInput(e.target.value)}
      />
      </div>
    )
  }

  _onInputChange = ({ target }) => {
    const { value } = target;
    if (value.length > 10000) {
      toastr.light('', '10,000자 이내로 입력해주세요.');
    }
    this.props.onInput(value);
  }

  renderMakeTextCard() {
    return (
        <div className={`card-item text-card ${this.props.isModification && this.props.item.cardIdx > 0 ? 'dimmed' : ''}`}>
          <textarea rows="3"
            maxLength={10000}
            name={'textcard-' + this.props.item.cardIdx}
            //placeholder={this.props.placeholder}
            placeholder={T.translate('make.pls-enter')}
            onFocus={e =>{this.props.onFocus; this.props.changeTimeline(this.props.begin)}}
            onBlur={this.props.onFocusOut}
            onChange={this._onInputChange}
            value={this.props.val}
          />
          <div className="card-button">
            <button className="delete" onClick={e => this.props.onDelete(e, true)}></button>
          </div>
        </div>
    );
  }

  renderTextCard() {
    return (
      <div className="card type-text">
          {
            this.props.data.text.split('\n').map((item, i) => {
              return (
                <span key={`text-${i}`}>{item}<br/></span>
              )
            })
          }
      </div>
    );
  }

  render() {
    if (this.props.make && this.props.mode !== 'events') return this.renderMakeTextCard();
    if (this.props.make && this.props.mode === 'events') return this.renderEventTextCard();
    return this.renderTextCard();
  }
}

Text.propTypes = {
  make: PropTypes.bool,
  placeholder: PropTypes.string,
  data: PropTypes.shape({
    text: PropTypes.string,
  }),
  onDelete: PropTypes.func,
  onInput: PropTypes.func
};

Text.defaultProps = {
  make: false,
  placeholder: T.translate('make.pls-enter'),
  data: {
    text: ''
  },
  onDelete: () => {},
  onInput: () => {},
  begin: 0,
  mode: 'all',
  val: '',
}

const mapDispatchToProps = dispatch => bindActionCreators({
    changeTimeline,
}, dispatch);

export default connect(
    null,
    mapDispatchToProps,
)(Text);