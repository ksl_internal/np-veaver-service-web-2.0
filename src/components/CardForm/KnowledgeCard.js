import React, { Component } from 'react';

class KnowledgeCard extends Component {

  render() {
    return(
      <div className="knowledge-card">
        <p className="time-range">
          00:01:30 - 00:01:45
        </p>
        <p className="board-title">
          파워포인트 사용법
        </p>
        <div className="card type-text">
          <p className="context">
            회사 생활하면서 가장 많이 사용하는
            프로그램 중 하나인 MS사의 파워포인트.
            과연 여러분은 얼마나 많이 알고 계세요?
          </p>
        </div>
        <div className="card type-img">
          <div className="image-wrapper">
            <img src="" alt="test" />
            <img src="" alt="test" />
          </div>
          <a href="/" className="more">+10</a>
        </div>
        <div className="card type-file hide">
          <div className="download-area">
            <span className="file-icon"></span>
            <div className="file-info">
              <p className="file-name">
                Veaver User Guide v1.0 prompt
              </p>
              <p className="file-size">
                2.4mb
              </p>
            </div>
          </div>
        </div>
        <div className="card type-link hide">
          <div className="link-thumbnail">
            <img src="" alt="Website Preview" />
          </div>
          <div className="link-info">
            <p className="link-name">
              [2017] Microsoft PowerPoint Viewer
            </p>
            <p className="link-context">
              PowerPointViewer.exe
            </p>
            <a className="link-address" href="/">
              https://www.microsoft.com/ko-kr/download/test
            </a>
          </div>
        </div>
        <div className="card type-map">
          <a className="map-area" href="/">
            <img src="" alt="Map Thumbnail" />
          </a>
          <div className="map-info">
            <p className="location">
              Gangnam Station
            </p>
            <p className="address">
              170-18 Gangnamgu Seoul, South Korea
            </p>
          </div>
        </div>
        <div className="card type-vote">
          <div className="vote-header">
            <span className="vote-icon"></span>
            <a className="view-result" href="/">
              결과 보기
            </a>
          </div>
          <div className="vote-form single-choice">
            <div className="context">
              <p>
                This word test will reveal what you need
                proud most final edit version. these words.
                Quiz - ( )are
              </p>
              <p className="limit">
                2017. 07.19 (수) 00:00까지
              </p>
            </div>
            <div className="type-radio">
              <input type="radio" id="vote-01" name="radio" />
              <label htmlFor="vote-01">
                <span></span>
                test01
              </label>
            </div>
            <div className="type-radio">
              <input type="radio" id="vote-02" name="radio" />
              <label htmlFor="vote-02">
                <span></span>
                test02
              </label>
            </div>
            <div className="vote-input">
              <input type="text" placeholder="투표항목 추가" />
              <button type="submit">입력</button>
            </div>
          </div>
          <div className="vote-form multiple-choice hide">
            <div className="context">
              <p>
                This word test will reveal what you need
                proud most final edit version. these words.
                Quiz - ( )are
              </p>
              <p className="limit">
                2017. 07.19 (수) 00:00까지
              </p>
            </div>
            <div className="type-check">
              <input type="checkbox" id="vote-03" name="check" />
              <label htmlFor="vote-03">
                <span></span>
                test01
              </label>
            </div>
            <div className="type-check">
              <input type="checkbox" id="vote-04" name="check" />
              <label htmlFor="vote-04">
                <span></span>
                test02
              </label>
            </div>
            <div className="vote-input">
              <input type="text" placeholder="투표항목 추가" />
              <button type="submit">입력</button>
            </div>
          </div>
          <div className="vote-submit">
            <button>투표 하기</button>
          </div>
        </div>
        <div className="card type-quiz">
          <div className="quiz-header">
            <span className="quiz-icon"></span>
            <a className="view-result" href="/">
              답변 보기
            </a>
          </div>
          <div className="quiz-form single-choice hide">
            <div className="context">
              <p>
                This word test will reveal what you need
                proud most final edit version. these words.
                Quiz - ( )are
              </p>
              <p className="limit">
                2017. 07.19 (수) 00:00까지
              </p>
            </div>
            <div className="type-radio">
              <input type="radio" id="quiz-05" name="radio" />
              <label htmlFor="quiz-05">
                <span></span>
                test01
              </label>
            </div>
            <div className="type-radio">
              <input type="radio" id="quiz-06" name="radio" />
              <label htmlFor="quiz-06">
                <span></span>
                test02
              </label>
            </div>
            <div className="vote-input">
              <input type="text" placeholder="답변 입력" />
            </div>
          </div>
          <div className="quiz-form multiple-choice">
            <div className="context">
              <p>
                This word test will reveal what you need
                proud most final edit version. these words.
                Quiz - ( )are
              </p>
              <p className="limit">
                2017. 07.19 (수) 00:00까지
              </p>
            </div>
            <div className="type-check">
              <input type="checkbox" id="quiz-07" name="check" />
              <label htmlFor="quiz-07">
                <span></span>
                test01
              </label>
            </div>
            <div className="type-check">
              <input type="checkbox" id="quiz-08" name="check" />
              <label htmlFor="quiz-08">
                <span></span>
                test02
              </label>
            </div>
            <div className="quiz-input">
              <input type="text" placeholder="답변 입력" />
            </div>
          </div>
          <div className="quiz-submit">
            <button>답변 하기</button>
          </div>
        </div>
      </div>
    );
  }
}

export default KnowledgeCard;
