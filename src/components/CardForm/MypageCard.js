import React, { PureComponent , Fragment } from 'react';
import { Link } from 'react-router-dom';
import T from 'i18n-react';
import cx from 'classnames';

export default class MypageCard extends PureComponent {
  state = {
    showMsg:false
  }
  render() {
    const { userProfile, uploadVideoStatus, data, key, tabStatus, isChecked } = Object.assign({}, this.props);
    return (
      <div
      key={`contents-key-${key}`}
      className={cx('mypage-card', {
        'encoding-process': ['E', 'U'].some(item => data.videoStatusFlag === item)
      })}>
        <div className="contents-info_header">
          <div className="thumbnail">
            <img src={data.thumbnail} alt="Video Thumbnail" onLoad={e => { this.props.onLoaded(e.target.complete); }} srcSet={`${data.thumbnail} 180w`} />
          </div>
          <a
          {...{ onClick: this.props.checkAllow}}
          style={{ cursor: 'pointer' }}
          className="hover-layer">
            <div className="view">
              <p className="title">View</p>
              <p>{data.viewCount}</p>
            </div>
            <div className="like">
              <p className="title">Like</p>
              <p>{data.likeCount}</p>
            </div>
            <div className="comment">
              <p className="title">Comment</p>
              <p>{data.commentCount}</p>
            </div>
            <div className="video-length">
              <p>{data.playTime}</p>
            </div>
          </a>
        </div>
        <div className="contents-info_detail">
          <p className="contents-title">
            {data.title}
          </p>
          <div className="hashtag">
            {data.tags.map((tag, i) => {
              return (<p key={`hash-tag-${i}-${data.timelineIdx}`}>{tag}</p>);
            })}
          </div>
          <div className="card-footer">
            <div className="update">
              <div className="user-thumbnail">
                <Link to={`/profile/${data.user.userKey}`}>
                  <img src={data.user.thumbnail} alt="User thumbnail" />
                </Link>
              </div>
              <div className="user-info">
                <p className="user-profile">
                    {data.user.nickname}
                    {data.user.position ? '·' : ''}
                    {data.user.position ? data.user.position : ''}
                    {data.user.department ? '·' : ''}
                    {data.user.department ? data.user.department : ''}
                </p>
                <p className="uploaded-time">{data.regDate}</p>
              </div>
            </div>
            <div className="card-setting">
            <button className="setting-button" onClick={this.props.isActiveSettingLayer ?
               this.props.toggleInactiveLayer : this.props.toggleActiveLayer}></button>
            <ul className={`card-setting_layer ${this.props.isActiveSettingLayer && 'active'}`}>
              {
              this.props.isLocker ?
                <li>
                <a href="/" className="btn-link" onClick={this.props.videoPublish}>{T.translate('common.post')}</a>
                </li> :
                <Fragment>
                  <li>
                    <a href="/" className="btn-link" onClick={this.props.openChooseUsers }>{T.translate('common.share')}</a>
                  </li>
                  <li>
                    <a href="/" className="btn-link" onClick={this.props.openFolder }>{T.translate('common.add-to-folder')}</a>
                  </li>
                  <li>
                  <a href="/" className="btn-link" onClick={this.props.openClipBoard}>{T.translate('common.linkcopy')}</a>
                  </li>
                </Fragment>

              }
              {data.user.userKey !== userProfile.userKey ?
                  <li>
                    <a href="/" className="btn-link" onClick={this.props.openReport}>{T.translate('report.report')}</a>
                  </li> :
                  <Fragment>
                    <li>
                      <Link to={`${this.props.isLocker ? '/locker' : ''}/modify/${data.timelineIdx}`} className={`btn-link ${uploadVideoStatus !== 'READY' ? 'dimmed' : ''}`}>{T.translate('common.edit')}</Link>
                    </li>
                    <li>
                      <a href="/" className="btn-link" onClick={this.props.deleteTimeline}>{T.translate('common.delete')}</a>
                    </li>
                  </Fragment>
              }
            </ul>
          </div>
          </div>
        </div>
        <div className={cx('encoding-layer', { checked: isChecked })} onClick={this.props.selectKnowledge}>
          <span></span>
        </div>
        <div className={cx('sendStatus-Layer')} style={{ display: data.sendStatus === 'W' ? 'block' : 'none'}}>
          <span>{T.translate('common.transferring')}</span>
        </div>
      </div>
    );
  }
};
