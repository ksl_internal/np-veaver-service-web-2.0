import React, { Component } from "react";
import { Text, Image, Quiz } from "../../components/CardForm/Knowledge";
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { bindActionCreators } from 'redux';
import cx from "classnames";
import { convDuration } from "../../util/time";
import { updateQuizCardAtEvents } from '../../modules/make';
import {changeTimeline,refreshCurrentTime} from "../../modules/player";

/**
 *
 * @class MakeEventCard
 * @extends {Component}
 */


const mapStateToProps = (state, props) => ({
  ...state,
  ...props,
}); 

const mapDispatchToProps = dispatch => bindActionCreators({
  updateQuizCardAtEvents,
    changeTimeline,
    refreshCurrentTime,
}, dispatch);


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(class MakeEventCard extends Component {
  componentDidMount() {}

  _createEventCard(item, idx) {
    const isMake = true;
    switch (item.type) {
      case "TEXT":
        return (
          <Text
            make={isMake}
            mode="events"
            item={item}
            val={item.contents.text}
            onInput={value => this.props.onChangeEventTypeText(value, idx)}
            isModification={this.props.isModification}
            begin = {item.begin}
          />
        );

      case "IMAGE": {
        return <Image data={item.contents} make={isMake} mode={"events"} />;
      }

      case "QUIZ":
      

        return (
          <Quiz
              item={item}
              data={item.contents}
              make={isMake}
              unique={idx}
              onFocusOut={this.props.onFocusOut}
              isModification={this.props.isModification}
              mode={"events"}
              exportQuizModel={(state) => {
                this.props.updateQuizCardAtEvents(state, idx);
              }}
              begin = {item.begin}
          />
        );
      default:
        throw new Error("unexpacted type.");
    }
  }

  render() {
    const events = [...this.props.events].sort((a, b) => { if (a.begin > b.begin) return 1; else return -1;});
    return (
      <div className="event-mode">
        {events.map((item, idx) =>
          <div
            className={cx("card-type", {
              active: item.begin === Math.floor(this.props.currentTime)
            })}
            key={`evt-card-${idx}`}
          >
            <p className="card-time">
              {convDuration(item.begin)}
              <button
                className="cancel"
                onClick={() => this.props.detachEvent(idx)}
              />
            </p>
            {this._createEventCard(item, idx)}
          </div>
        )}
      </div>
    );
  }
}));
