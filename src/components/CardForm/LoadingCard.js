import React, { Component } from 'react';

class LoadingCard extends Component {

  render() {
    return (
      <div className="mypage-card loading-mode" key={this.props.unique}>
        <div className="contents-info_header">
          <div className="thumbnail"></div>
        </div>
        <div className="contents-info_detail">
          <p className="contents-title"></p>
          <div className="hashtag">
            <p></p>
          </div>
          <div className="card-footer">
            <div className="update">
              <p></p>
            </div>
            <div className="card-setting">
              <button className="setting-button"></button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoadingCard;
