import React from 'react';
import { toDateStr } from '../../util/time';
import cx from 'classnames';
import T from 'i18n-react';

export default class ReactionComment extends React.PureComponent {

  render() {
    const {
      onClickSettingOptions,
      onClickNestedComment,
      onClickModifyComment,
      onClickDeleteReaction,
      onClickReportComment,
    } = this.props;
    const { reaction, userProfile, nested, clickedReactionId } = this.props
    const { userKey } = userProfile;
    const { nickname, position, department, regDate, thumbnailSmall, reactionType, questionFlag } = reaction;
    const conditions = {
      like: reactionType === 'LIKE',
      quiz: questionFlag === 'Y' ,
      reply: reactionType !== 'LIKE' && !nested,
      layer: userKey !== reaction.userKey && reactionType === 'LIKE',
      active: clickedReactionId === reaction.reactionId,
      modify: userKey === reaction.userKey && reactionType === 'TEXT',
      delete: userKey === reaction.userKey,
      declare: userKey !== reaction.userKey && reactionType === 'TEXT',
    };

    return (
      <div className="comment-card">
        <div className={cx('user-info', { like: conditions.like , quiz: conditions.quiz })}>
           <span className="thumbnail">
            <img src={thumbnailSmall} alt="User Profile" />
           </span>
          <div className="user-profile">
            <p>{nickname}·{position}·{department}</p>
            <p>{toDateStr(regDate)}</p>
          </div>
          <div className={cx('setting-layer', { 'visually-hidden': conditions.layer })}>
            <button className="setting-button" onClick={onClickSettingOptions}></button>
            <ul className={cx('settings', { active: conditions.active })}>
              <li className={cx({ hide: !conditions.reply })}>
                <button className="btn-link" onClick={onClickNestedComment}>
                  {T.translate('common.comment')}
                </button>
              </li>
              <li className={cx({ hide: !conditions.modify })}>
                <button className="btn-link" onClick={onClickModifyComment}>
                  {T.translate('common.edit')}
                </button>
              </li>
              <li className={cx({ hide: !conditions.delete })}>
                <button className="btn-link reaction-delete" onClick={onClickDeleteReaction}>
                  {T.translate('common.delete')}
                </button>
              </li>
              <li className={cx({ hide: !conditions.declare })}>
                <button className="btn-link" onClick={onClickReportComment}>
                  {T.translate('report.report')}
                </button>
              </li>
            </ul>
          </div>
        </div>
        <p className="comment">
          {reaction.comment}
        </p>
      </div>
    );
  }
}

