import {
  SortableContainer,
  SortableElement,
  SortableHandle
} from "react-sortable-hoc";
import React, { PureComponent } from "react";
import { File, Image, Link, Map, Quiz, Text, Vote } from "./Knowledge";
import { convDuration } from "../../util/time";
import cx from "classnames";
import { toastr } from "react-redux-toastr";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import T from "i18n-react";
// import * as PlayerActionCreators from "../../modules/player";

import {
    changeTimeline,
    refreshCurrentTime,
} from '../../modules/player';

const DragHandle = SortableHandle(() => <span className="mode" />);

// const mapStateToProps = (state, props) => ({
//   ...props,
// });

// const mapDispatchToProps = dispatch => bindActionCreators({
//   updateQuizCardAtStack,
// }, dispatch);

const mapStateToProps = state => {
    return {
        currentTime: state.player.currentTime,
    };
};
/**
 * 
 * redux 를통해 connect 된 클래스는 shallow compare 를 수행하기떄문에 고려를 잘해야함.  
 */
class MakeCard extends PureComponent {
  static defaultProps = {
    cards: [],
    isModification: false,
    isDeployed: false,
  };


  _createKnowledgeCard(item, idx) {
    const isMake = true;
    const cardsIdx = this.props.stackIndex;//여기서 다시
    const bindCommonEvent = {
      onDelete: (event, isClick) =>
        isClick && this.props.detachStackInCard(cardsIdx, idx, event)
    };

    switch (item.type) {
      case "TEXT":
        return (
          <div className="item-wrapper">
            <Text
              make={isMake}
              isModification={this.props.isModification}
              item={item}
              val={item.contents.text}
              onInput= {value => this.props.onChangeCardTypeText(value, cardsIdx, idx)}
              onDelete={(event, isClick) =>
                isClick && this.props.detachStackInCard(cardsIdx, idx, event)}
                begin={this.props.items.begin}
                    //{...bindCommonEvent}
            />
            <DragHandle />
          </div>
        );

      case "LINK":
        return (
          <div className="item-wrapper">
            <Link data={item.contents} make={isMake} {...bindCommonEvent} />
            <DragHandle />
          </div>
        );

      case "IMAGE":
        return (
          <div className="item-wrapper">
            <Image data={item.contents} make={isMake} {...bindCommonEvent} />
            <DragHandle />
          </div>
        );

      case "FILE":
        return (
          <div className="item-wrapper">
            <File
              data={item.contents.files[0]}
              make={isMake}
              {...bindCommonEvent}
            />
            <DragHandle />
          </div>
        ); 

      case "QUIZ":
        return (
          <div className="item-wrapper">
            <Quiz
              item={item}
              data={item.contents}
              make={isMake}
              unique={idx}
              cardIdx={item.cardIdx}
              onFocusOut={this.props.onFocusOut}
              deleteQuizModel={(event , state) => {
                this.props.deleteQuizCardAtStack(state, idx, cardsIdx , event);
              }}
              exportQuizModel={(state) => {
                this.props.updateQuizCardAtStack(state, idx, cardsIdx );
              }}
              isModification={this.props.isModification}
              isDeployed={this.props.isDeployed}
              begin={this.props.items.begin}
              {...bindCommonEvent}
            />
            <DragHandle />
          </div>
        );

      case "VOTE":
        return (
          <div className="item-wrapper">
            <Vote
              data={item.contents}
              make={isMake}
              cardIdx={item.cardIdx}
              onFocusOut={this.props.onFocusOut}
              exportVoteModel={(state) => {
                this.props.updateVoteCardAtStack(state, idx, cardsIdx);
              }}
              isDeployed={this.props.isDeployed}
              begin={this.props.items.begin}
              {...bindCommonEvent}
            />
            <DragHandle />
          </div>
        );

      case "MAP":
        return (
          <div className="item-wrapper">
            <Map
              make={isMake}
              data={item.contents}
              onDelete={(event, isClick) =>
                isClick && this.props.detachStackInCard(cardsIdx, idx, event)}
            />
            <DragHandle />
          </div>
        );
      default:
        throw new Error(
          "error! create switch case in unexpacted card type.. you should check it."
        );
    }
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    const cardsIdx = this.props.stackIndex;
    this.props.onSortEndStackInCard(cardsIdx, oldIndex, newIndex);
    this.props.onFocusOut()
  };

  SortableItem = SortableElement(({ value, children }) =>
    
    this._createKnowledgeCard(value, children)
  );

  SortableList = SortableContainer(({ items }) => {
    const SortableItem = this.SortableItem;
    return (
      <div
        key={`card-type-time-${this.props.items.begin}`}
        className={cx("card-type",  {
          active:
            this.props.items.begin <= this.props.currentTime &&
            this.props.currentTime <= this.props.items.end
        } ) } card-begin={this.props.items.begin} card-end={this.props.items.end}
      >
        <p className="card-time">
          {`${convDuration(this.props.items.begin)} - ${convDuration(
            this.props.items.end
          )}`}
          <button
            className="cancel"
            onClick={() => this.props.detachStack(this.props.stackIndex)}
          />
        </p>
        <input
          type="text"
          className="stack-title"
          placeholder={this.props.defaultPlaceholder}
          maxLength={20}
          required
          defaultValue={this.props.items.name !== '' ? this.props.items.name : ''}
          onKeyPress={e => e.key === 'Enter' && e.preventDefault()}
          onFocus={e => {this.props.onFocus(e);
            this.props.changeTimeline(this.props.items.begin);
             }}
          onBlur={e => {
            this.props.onFocusOut(e);
            this.props.onChangeStackName(
              e.target.value,
              this.props.stackIndex
            )
          }}
          onChange={(e) => {
            if (e.target.value.length >= 20) {
              toastr.light('', '20자 이내로 입력해주세요.');
            }
          }} 

        />
        {items.length === 0
          ? <div className="card-item nodata-card">
              <p>{T.translate('make.press-add-knowledge')}</p>
            </div>
          : items.map((card, index) => {
              return (
                <SortableItem
                  key={`item-${index}`}
                  index={index}
                  value={card}
                  collection={this.props.cardsIndex}
                  lockToContainerEdges={false}
                >
                  {index}
                </SortableItem>
              );
            })}
      </div>
    );
  });

  get sortListing() {
    
    const {cards} = this.props;
    const SortableList = this.SortableList;
    
    return (
      
      <SortableList 
        items={cards}
        useDragHandle={true}
        onSortEnd={this.onSortEnd}
        helperClass="moving"
      />
    );
  }



  render() {
   return this.sortListing
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    changeTimeline,
    refreshCurrentTime,
}, dispatch);


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MakeCard);

