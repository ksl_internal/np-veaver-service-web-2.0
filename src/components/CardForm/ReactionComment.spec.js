import React from 'react';
import { mount, shallow } from 'enzyme';

import ReactionComment from './ReactionComment';

function setup() {
    const props = {
        onClickSettingOptions: jest.fn(), 
        onClickNestedComment: jest.fn(), 
        onClickModifyComment: jest.fn(), 
        onClickDeleteReaction: jest.fn(), 
        onClickReportComment: jest.fn(),
        reaction: {
            userKey: 'AA',
            comment: 'Hello',
            nickname: 'user',
            position: 'dev',
            department: 'dev', 
            regDate: 1111111111,
            thumbnailSmall: './test.jpg',
            reactionType: 'LIKE',
            questionFlag: 'Y',
        },
        userProfile: {
            userKey: 'BB',
        },
        nested: false,
        clickedReactionId: '',
    }

    const enzymeWrapper = shallow(<ReactionComment {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('리액션 컴포넌트 기능을 테스트 한다.', () => {
    // given videoIdx, reactionId 가 주어진다
    // when 리액션을 삭제하면
    // then 1.리스트를 갱신된다  2.카운트가 갱신된다.
    test('더보기 버튼을 누를수 있다.', () => {
    })
    test('수정 버튼을 누를수 있다.', () => {})
    test('답글 버튼을 누를수 있다.', () => {})
    test('삭제 버튼을 누를수 있다.', () => {
        const { enzymeWrapper, props } = setup()
        enzymeWrapper.find('.reaction-delete').simulate('click')
        expect(props.onClickDeleteReaction).toEqual(true);
        expect(props.onClickDeleteReaction).toHaveBeenCalled()
    })
    test('신고하기 버튼을 누를수 있다.', () => {})
})