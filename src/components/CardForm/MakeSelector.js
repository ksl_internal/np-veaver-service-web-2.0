import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { openPopup } from "../../modules/popup";
import T from "i18n-react";
/**
 * @export
 * @class MakeSelector
 * @extends {Component}
 * @prop (mode) : string 기본값 / 전체 / 이벤트 모드 일경우 3개.
 * @prop (val) : string 설장값
 * @prop (currentTime) : number 현재 플레이어 타임의 시간.
 * @prop (exportContentsType) : func 컨텐츠 타입을 추출 한다 ENUM
 */

const styles = {
  button: {
     position: "relative",
  },
  cursor: { cursor: "pointer" },
  file: {
    opacity: 0,
    cursor: "pointer",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
  }
};

class MakeSelector extends Component {
  static defaultProps = {
    mode: "all",
    exportContentsType: () => {}
  };

  _singleImageUpload = event => {
    const image = event.target.files[0];
    this.props.exportContentsType("IMAGE", image, this.props.currentTime);
  };

  _multipleImageUpload = event => {
    const images = event.target.files;
    this.props.exportContentsType("IMAGE", images, this.props.currentTime);
  };

  _attachFileUpload = event => {
    const file = event.target.files[0];
    this.props.exportContentsType("FILE", file, this.props.currentTime);
  };
 
  _onLinkClicked(url) {
    this.props.openPopup("LINK", this.props);
  }

  get events() {
    return (
      <div className="add-board-card event-type">
        <div
          className="text-mode"
          onClick={() =>
            this.props.exportContentsType(
              "TEXT",
              undefined,
              this.props.currentTime
            )}
        >
          <span>{T.translate('make.text')}</span>
        </div>
        <div
        style={styles.button}
        className="image-mode">
          <span>{T.translate('make.picture')}</span>
          <input
            type="file"
            accept="image/*"
            style={styles.file}
            onChange={this._singleImageUpload}
          />
        </div>
        <div
          className="quiz-mode"
          onClick={() =>
            this.props.exportContentsType(
              "QUIZ",
              undefined,
              this.props.currentTime
            )}
        >
          <span>{T.translate('make.quiz')}</span>
        </div>
      </div>
    );
  }

  get knowledge() {
    return (
      <div className="add-board-card">
        <div
          className="text-mode"
          onClick={() => this.props.exportContentsType("TEXT")}
        >
          <span>{T.translate('make.text')}</span>
        </div>
        <div style={{ position: "relative" }} className="image-mode">
          <span>{T.translate('make.picture')}</span>
          <input
            type="file"
            accept="image/*"
            multiple
            style={styles.file}
            onChange={this._multipleImageUpload}
          />
        </div>
        <div className="link-mode" onClick={() => this._onLinkClicked()}>
          <span>{T.translate('make.link')}</span>
        </div>
        <div
          className="map-mode"
          onClick={() => this.props.openPopup("MAP", null)}
        ><span>{T.translate('make.map')}</span>
        </div>
        <div className="doc-mode" style={{ position: "relative" }}>
          <span>{T.translate('make.file')}</span>
          <input
            type="file"
            style={styles.file}
            onChange={this._attachFileUpload}
          />
        </div>
        <div
          className="quiz-mode"
          onClick={() => this.props.exportContentsType("QUIZ")}
        ><span>{T.translate('make.quiz')}</span>
        </div>
        <div
          className="vote-mode"
          onClick={() => this.props.exportContentsType("VOTE")}
        ><span>{T.translate('make.vote')}</span>
        </div>
      </div>
    );
  }

  render() {
    const renderMode = {
      events: this.events,
      all: this.knowledge
    };
    return renderMode[this.props.mode];
  }
}

const mapStateToProps = (state, props) => {
  return {
    openPopup: state.openPopup,
    mode: props.mode,
    val: props.val,
    currentTime: props.currentTime,
    exportContentsType: props.exportContentsType
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openPopup
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MakeSelector);
