import React, { Component } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { default as storage } from 'store';
import { rememberIdCheck } from '../../modules/auth';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: null,
      autoLoginChecked: false,
    }

    this._rememberMeChanged = this._rememberMeChanged.bind(this);
  }

  componentDidMount() {
    const email = storage.get('rememberId');
    if (this.props.checkRemember && email) {
      this.email.value = email;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.code !== nextProps.code) {
      switch (nextProps.code) {
        case 2001: return this._setErrorMessage('등록된 사용자가 존재하지 않습니다.');
        case 2004: return this._setErrorMessage('입력한 정보가 올바르지 않습니다.');
        case 2005: return this._setErrorMessage('인증되지 않은 이메일 입니다.');
        case 2007: return this._setErrorMessage('로그인 5회 실패, 등록된 이메일을 확인해 주세요.');
        default: return this._setErrorMessage(null);
      }
    }
  }

  _rememberMeChanged(evt) {
    const { checked } = evt.target;
    this.props.rememberIdCheck(checked);
  }

  _autoLoginCheck = ({ target }) => {
    this.setState({ autoLoginChecked: target.checked })
  }

  _onSubmit = (event) => {
    event.preventDefault();
    if (this._formValidate()) {
      const { autoLoginChecked } = this.state;
      this.props.onSubmit(this.email.value, this.password.value, autoLoginChecked);
    }
  }

  _renderErrorMessage() {
    const { errorMessage } = this.state;

    if (!errorMessage) {
      return null;
    }

    return (
      <div className="error-message">
        <p className="message">
          {errorMessage}
        </p>
      </div>
    );
  }

  _setErrorMessage(errorMessage) {
    this.setState((state) => {
      return {
        ...state,
        errorMessage,
      }
    });
  }

  _formValidate() {
    if (!validator.isEmail(this.email.value)) {
      this._setErrorMessage('잘못된 email 형식입니다.');
      return false;
    }

    const passwordRegex = /^(?=.*[a-zA-Z])(?=.*[$@$!%*?&\d])[A-Za-z\d$@$!%*?&]{6,20}/;
    if (!passwordRegex.test(this.password.value)) {
      this._setErrorMessage('잘못된 password 형식입니다.');
      return true; // false
    }

    return true;
  }


  get getServerID () {
    return process.env.NODE_ENV === 'production' ? window.location.hostname.split('.ent')[0] : process.env.REACT_APP_SERVICE_ID;
  }

  render() {
    return(
      <div className="login-wrapper">
        <div className="login">
          <div className="brand-area">
            <img src="/images/login_logo.png" alt="Veaver" />
          </div>
          <div className="login-form">
            <form className="" onSubmit={this._onSubmit}>
              <div className="input-field">
                <input type="email"
                  name="email"
                  ref={ input => this.email = input }
                  placeholder="Email"
                />
                <input ref={ input => this.password = input } type="password" name="password" placeholder="Password" />
              </div>
              {this._renderErrorMessage()}
              <div className="login-button">
                <button className="btn-solid btn-blue" type="submit">로그인</button>
              </div>
              <div className="login-state">
                <div className="type-check">
                  <input type="checkbox"
                    id="save-id"
                    name="check"
                    checked={this.props.checkRemember}
                    onChange={this._rememberMeChanged}
                  />
                  <label htmlFor="save-id">
                    <span></span>
                    아이디 저장
                  </label>
                </div>
                <div className="type-check">
                  <input type="checkbox"
                    id="save-auto-login"
                    name="auto-login-check"
                    onChange={this._autoLoginCheck}
                  />
                  <label htmlFor="save-auto-login">
                    <span></span>
                    자동 로그인
                  </label>
                </div>
                <a target="_blank"
                  rel="noopener noreferrer"
                  href={`http://${this.getServerID}.ent.veaver.com/users/reset-password`}
                >
                  비밀번호를 잊으셨습니까?
                </a>
                <input type="hidden" name="response_type" value="token" />
                <input type="hidden" name="redirect_uri" value="/"/>
                <input type="hidden" name="client_id" value="veaver-enterprise" />
                <input type="hidden" name="scope" value="read" />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  checkRemember: state.auth.checkRemember,
  code: state.error.code,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  rememberIdCheck,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginForm);
