import React, { Component ,Fragment} from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../modules/contact';
import moment from 'moment';
import T from 'i18n-react';
import { Link } from 'react-router-dom';
import { withRouter } from "react-router-dom";
const dot ={
  display:'inline-block',
  width:'6px',
  height: '6px',
  borderRadius:'6px',
  backgroundColor:'#335fc3',
  marginRight: '10px',
  marginBottom: '2px'
}
/**
 * 알림 리스트 뷰입니다.
 * @param {*} param0 
 */
const List = ({items,title,callBack,movePage}) =>{
  if(items=== undefined){
    return null;
  };
  function getItem(items,callBack){
    const list =items.map(item=>{
      const style={
        display:(item.status==='I'?'none':'table-row'),
        opacity:(item.confirmFlag==='Y') ? '0.4' : '1'
      }
      return (
        <tr key={item.idx} onClick={()=>{movePage(item)}} style={style} >
          <td colSpan='2' style={ {textAlign:'left',paddingLeft:'20px'} }><span style={dot}></span>{item.message}</td>
          <td>
            {
              item.status === "W" ?
              <Fragment>
                <button onClick={(e)=>{e.stopPropagation();callBack(item,'Y')}}  className="acceptBtn" >{T.translate('common.accept')}</button><button onClick={(e)=>{e.stopPropagation(); callBack(item,'N')} } className="cancelBtn">{T.translate('common.deny')}</button>
              </Fragment>
              :
              ''
            }  
          </td>
          <td>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</td>
        </tr>
      )
    })
    return list;
  }
  return (
    <div className="list-wapper">
      <table>
          <colgroup>
            <col width="*"/>
            <col width="*"/>
            <col width="200px"/>
            <col width="200px"/>
          </colgroup>
        <thead>
          <tr>
            <th colSpan='3'>{T.translate('make.notification')}</th>
            <th>{T.translate('common.date')}</th>
          </tr>
        </thead>
        <tbody>
          {getItem(items,callBack)
            }
        </tbody>
      </table>
    </div>
  );
}

// class List extends Component {

//   render() {
//     const { items } = this.props;
//     let itemList =null
//     if(items != undefined){
//       console.log('items : ',items)
//       itemList = items.list.map(item=>{
//         return (
//           <tr key={item.idx}>
//             <td>{item.idx}</td>
//             <td>{item.contactUsTypeInfo}</td>
//             <td><Link to={`/contact/${item.idx}`}>{item.subject}<div className="reception-number">{item.regNum}</div></Link></td>
//             <td>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</td>
//           </tr>
//         )
//       })
//     }
//     return (
//         <div className="list-wapper">
//           <table>
//               <colgroup>
//                 <col width="80px"/>
//                 <col width="150px"/>
//                 <col width="*"/>
//                 <col width="200px"/>
//               </colgroup>
//             <thead>
//               <tr>
//                 <th>No.</th>
//                 <th>분류</th>
//                 <th>제목</th>
//                 <th>접수 일시</th>
//               </tr>
//             </thead>
//             <tbody>
//               {itemList}
//             </tbody>
//           </table>
//          </div>
//     );
//   }
// }

export default withRouter(List);
