import React, { Component , Fragment } from "react";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import T from "i18n-react";

/**
 * 각 페이지 메인 하단 더보기 뷰입니다.
 */
class ListMoreView extends Component {
  render() {
    const {req, isHideMoreLayer , callBack} = this.props;
    return (
        <Fragment>
            <br></br>
            <br></br>
            <br></br>
            <div className={'more'}>
                {
                isHideMoreLayer ?
                <button
                onClick={() => {
                    try {
                        document.documentElement.scrollTop=0
                        document.getElementById('scrollbar').scrollTop=0
                    } catch (error) {
                        
                    }
                    }
                }>
                    {T.translate('common.view-up')}<span
                    className="more-arrow"
                    style={{ transform: 'rotate(180deg)'}}></span>
                </button> :
                <button
                onClick={() => callBack(req,true)}>
                    {T.translate('common.view-more')}<span
                    className="more-arrow"></span>
                </button>
                }
            </div>
            <br></br>
            <br></br>
            <br></br>
        </Fragment>
    );
  }
}
export default connect(
(state) => ({
}),
(dispatch) => ({
})
)(ListMoreView);

//export default CardMore;
