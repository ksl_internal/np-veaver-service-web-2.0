import React, { Component , Fragment } from "react";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import T from "i18n-react";
/**
 * 모든 카드뷰 하단 더보기 아이콘 클릭 메뉴 입니다.
 */
class CardMore extends Component {
  state = {
    isOpenMore:''
  }
  toggleMore = () =>{

    this.setState({ isOpenMore: (this.state.isOpenMore==='') ? 'active' : '' });
  }
  render() {
    const { item ,userProfile ,uploadVideoStatus ,isActiveSettingLayer, rotate=false} = this.props;
    const { isOpenMore } = this.state;
    return (
        <Fragment>
            <div className="card_more_layar more" onClick={
                isActiveSettingLayer ?
               this.props.toggleInactiveLayer : this.props.toggleActiveLayer}>
                <img src="/images/v1/ic-more-bigcard@2x.png" style={{transform:(rotate===true ? 'rotate(90deg)' : 'none')}} />
                <ul className={`card-setting_layer ${isActiveSettingLayer && 'active'}`}>
                    {
                        this.props.isLocker ?
                        <Fragment>
                            <li>
                                <a className="btn-link" onClick={this.props.videoPublish}>{T.translate('common.post')}</a>
                            </li> 
                            <li>
                                <a className="btn-link" onClick={this.props.openTempTimelineToUser}>{T.translate('common.transfer')}</a>
                            </li>
                        </Fragment>
                        :
                        <Fragment>
                            <li>
                                <a className="btn-link" onClick={this.props.openChooseUsers }>{T.translate('common.share')}</a>
                            </li>
                            <li>
                                <a className="btn-link" onClick={this.props.openFolder }>{T.translate('common.add-to-folder')}</a>
                            </li>
                            <li>
                            <a className="btn-link" onClick={this.props.openClipBoard}>{T.translate('common.linkcopy')}</a>
                            </li>
                        </Fragment>

                    }
                    {item.user.userKey !== userProfile.userKey ?
                    <li>
                        <a href="/" className="btn-link" onClick={this.props.openReport}>{T.translate('report.report')}</a>
                    </li> :
                    <Fragment>
                        <li>
                        <Link to={`${this.props.isLocker ? '/locker' : ''}/modify/${item.timelineIdx}`} className={`btn-link ${uploadVideoStatus !== 'READY' ? 'dimmed' : ''}`}>{T.translate('common.edit')}</Link>
                        </li>
                        <li>
                        <a href="/" className="btn-link" onClick={this.props.deleteTimeline}>{T.translate('common.delete')}</a>
                        </li>
                    </Fragment>
                    }
                </ul>
            </div>
        </Fragment>
    );
  }
}
export default connect(
(state) => ({
    userProfile: state.user.userProfile,
}),
(dispatch) => ({
})
)(CardMore);

//export default CardMore;
