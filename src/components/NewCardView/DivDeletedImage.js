import React from "react";
import T from "i18n-react";
const deleteOverlay = {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    display: 'flex',
    justifyItems: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: '25px',
    color: '#fff',
    zIndex: '1',
    backgroundColor: '#000000aa'
}
const encodingOverlay = {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    background:'rgba(255, 255, 255, 0.5)',
    zIndex: '1',
}
const encodingIcon = {
    width: '24px',
    height: '24px',
    position: 'absolute',
    top: '15px',
    right:'15px',
    background:`url('/images/encoding.png') no-repeat`,
    display: 'inline-block',
}
 /**
   * 모든 카드뷰 삭제 또는 업로드 중인 카드 레이어 표시합니다.
   */
const DivDeletedImage = ({item}) =>{
    if(item.deleteFlag==='Y'){
        return(
            <div style={deleteOverlay}><div style={{width:'100%'}}>{T.translate('play_menu.deleted-content')}</div></div>
            )
    }else if(['E', 'U'].some(status => item.videoStatusFlag === status)){
        return (
            <div style={encodingOverlay}><span style={encodingIcon}></span></div>
        )
    }else {
        return null
    }
}
export default DivDeletedImage;
