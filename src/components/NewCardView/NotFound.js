import React from "react";
import T from "i18n-react";
const NotFound = ({}) =>{
    return(
        <div className="nodata-wrapper">
        <div className="nodata">
          <img src="/images/nodata.png" alt="Has no data" />
          <p>
              {T.translate('common.no-result')}
          </p>
        </div>
      </div>
    )
}
export default NotFound;
