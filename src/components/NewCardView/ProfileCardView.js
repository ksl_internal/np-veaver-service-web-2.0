import React, { Component } from "react";
import moment from 'moment'
import CardMore from './CardMore'
import DivChooseCardView from './DivChooseCardView'
import DivImageMouseOver from './DivImageMouseOver'
/**
 * 프로필 카드 뷰입니다.
 */
class ProfileCardView extends Component {
  state = {
    showMsg:false,
    selected:0,
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }
  /**
   * 최상단 페이지 취소 눌렀을 경우 선택 체크박스 초기화
   * @param {*} nextProps 
   * @param {*} prevState 
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selectedItems.isChanging === false) {
      return { selected: 0 };
    }
    return null;
  }
  /**
   * 해당 카드를 선택 했을 경우 이벤트 입니다.
   */
  handleChecked = (item) =>{
    const { selectedItems , selectedCallback} = this.props
      if(this.state.selected === 0){
        const arr =selectedItems.idx.concat(item.timelineIdx)
        const originObj =selectedItems.originObj.concat(item)
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 1 });
      }else{
        const arr =selectedItems.idx.filter(_id=>{
            return _id !== item.timelineIdx
        })
        const originObj =selectedItems.originObj.filter(item=>{
            return item.timelineIdx !== item.timelineIdx
        })
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 0 });
      }
  } 
  render() {
    const { item , selectedItems, onClick ,userProfile , uploadVideoStatus ,isLocker,
      isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
      openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
    } = this.props;
    return (
    <div className="one-three">
        <div className="imageBox cardSize style6 flex clearfix">
            <div className="image inner-flex">
              <div className="card_thumnail_wapper"  onClick={ (item.videoStatusFlag==='C'? onClick:null)} >
                <img src={item.thumbnail} alt="image"  />
              </div>
                <div className="text"><p><a className="timeline_title">{item.name}</a></p></div>
                <span className="time">{item.playTime}</span>
                <div className="bottom">
                  <CardMore 
                      item={item} 
                      userProfile={userProfile}
                      isLocker={isLocker} 
                      uploadVideoStatus={uploadVideoStatus}
                      isActiveSettingLayer={isActiveSettingLayer} 
                      toggleActiveLayer={toggleActiveLayer}
                      toggleInactiveLayer={toggleInactiveLayer}

                      openClipBoard={openClipBoard}
                      deleteTimeline={deleteTimeline}
                      videoPublish={videoPublish}
                      openReport={openReport}
                      openChooseUsers={openChooseUsers}
                      openFolder={openFolder}
                      rotate={true}
                  ></CardMore>
                </div>
            </div>
            <div className="content inner-flex img-hover-parent">
                <div className="inner-content">
                    <div className="sub-title">{item.user.nickname}·{item.user.position}</div>
                    <div className="time">{item.regDate}</div>
                </div>
                {/* <DivImageMouseOver item={item}  onClick={onClick}></DivImageMouseOver> */}
                <DivChooseCardView selectedItems={selectedItems} selected={this.state.selected} item={item} callBack={this.handleChecked}/>
                {/* {selectedItems.isChanging ===true 
                    ?
                        (this.state.selected ===0 ?
                            <a className="check" onClick={()=>{ this.handleChecked(item.timelineIdx)}}><img src="/images/v1/icon/btn-check-card-dim.svg"/></a>
                        :
                            <a className="check" onClick={()=>{ this.handleChecked(item.timelineIdx)}}><img src="/images/v1/icon/btn-check-card-predd.svg"/></a>)
                    :
                    ''    
                } */}
            </div>
        </div>
    </div>
      // <div className="one-three">
      //   <div className="imageBox style4 clearfix">
      //     <div className="image" onClick={onClick}>
            // <div
            //   style={{
            //     height: "240px",
            //     backgroundColor: "#1f1f1f",
            //     textAlign: "center"
            //   }}
            // >
            //   <img
            //     src={item.thumbnail}
            //     alt="image"
            //     style={{ height: "100%" }}
            //   />
            // </div>

      //       <a href="#" className="message">
      //         <img src="/v1//images/v1/icon/ic-message-card.svg" />
      //       </a>
      //       <div className="text">
      //         <div className="name">
      //           <a href="#">
      //             {item.user.nickname}·{item.user.position}·{item.user.duty}
      //           </a>
      //         </div>
      //         <p>
      //           <a href="#">{item.name}</a>
      //         </p>
      //       </div>
      //       <span className="time">03:22</span>
      //       <a href="#" className="more">
      //         <img src="/v1//images/v1/icon/ic-more-card.svg" />
      //       </a>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

export default ProfileCardView;
