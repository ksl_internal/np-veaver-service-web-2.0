import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import CardMore from './CardMore'
import DivImageMouseOver from './DivImageMouseOver'
import DivDeletedImage from './DivDeletedImage'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import { textToHtml ,mergeName} from '../../util/card';
/**
 * 검색 카드 뷰입니다.
 * 검색 했을 경우 섹션 및 타임라인 서버에서 tag 값을 주지 않습니다. 
 * 그렇기 때문에 디자인 상에 tag 표현을 숨김 처리 했습니다
 */
class SearchTimelinesCardView extends Component {
  state = {
    showMsg:false,
    isOpenMore:false
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  } 
  toggleMore = () =>{
    this.setState({ isOpenMore: (this.state.isOpenMore) ? false : true });
  } 
  render() {
    const { item , onClick ,userProfile , uploadVideoStatus ,isLocker,
      isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
      openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
    } = this.props;                            
    return (
      <div className="one-three">
        <div className="imageBox image-top">
          <DivDeletedImage item={item}/>
          <div className="image img-hover-parent">
              <div className="card_thumnail_wapper">
                <img src={item.thumbnail} alt="image" />
              </div>
              <DivImageMouseOver item={item} onClick={onClick} ></DivImageMouseOver> 
          </div>
              <div className="content">
                  <div className="top">                             
                      <div className="title"> <a>{ReactHtmlParser(textToHtml(item.name,this.props.keyword))}</a></div>
                      {item.tags &&
                        <div className="wrap-btn">
                            {item.tags.map(tag=>{
                              return (
                                <a key={tag} className="btn">{tag}</a>
                              )
                            })}
                        </div>
                      }
                  </div>
                  <div className="bottom clearfix">
                      <div className="wrap-avatar">
                          {/* <div className="image-avatar">
                            <Link to={`/profile/${item.user.userKey}`}>
                              <img src={item.user.thumbnail} alt="User thumbnail" />
                            </Link>
                          </div> */}
                          <div className="wrap-info">
                              <div className="name">
                                <a >
                                {ReactHtmlParser(textToHtml(mergeName(item.user.nickname,item.user.position,item.user.department),this.props.keyword))}
                                </a>
                              </div>
                              <div className="time">{item.regDate}</div>
                          </div>
                      </div>
                      <CardMore 
                        item={item} 
                        userProfile={userProfile}
                        isLocker={isLocker} 
                        uploadVideoStatus={uploadVideoStatus}
                        isActiveSettingLayer={isActiveSettingLayer} 
                        toggleActiveLayer={toggleActiveLayer}
                        toggleInactiveLayer={toggleInactiveLayer}

                        openClipBoard={openClipBoard}
                        deleteTimeline={deleteTimeline}
                        videoPublish={videoPublish}
                        openReport={openReport}
                        openChooseUsers={openChooseUsers}
                        openFolder={openFolder}
                      ></CardMore>
                  </div>
              </div>                                            
          </div>
      </div>
    );
  }
}
export default connect(
  (state) => ({
    keyword: state.searchM.get('keyword'),
  }),
  (dispatch) => ({
  })
  )(SearchTimelinesCardView);
