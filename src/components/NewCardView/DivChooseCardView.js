import React from "react";
 /**
   * 모든 카드뷰를 체크박스 뷰입니다.
   */
const DivChooseCardView = ({item,selectedItems , selected,callBack}) =>{
    if(selectedItems.isChanging ===true ){
        if(selected ===0){
            return (<a className="check" onClick={()=>callBack(item)}><img src="/images/v1/icon/btn-check-card-dim.svg"/></a>)
        }else{
            return (<a className="check" onClick={()=>callBack(item)}><img src="/images/v1/icon/btn-check-card-predd.svg"/></a>)
        }
    }else{
        return ''
    }
}
export default DivChooseCardView;