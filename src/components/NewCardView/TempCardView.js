import React, { Component } from "react";
import moment from 'moment'
import CardMore from './CardMore'
import DivImageMouseOver from './DivImageMouseOver'
import DivDeletedImage from './DivDeletedImage'
import T from "i18n-react";
const sendOverlay = {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    display: 'flex',
    justifyItems: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: '35px',
    color: '#fff',
    zIndex: '1',
    backgroundColor: '#000000aa'
}
/**
 * 임시 보관함 카드뷰 입니다.
 */
class TempCardView extends Component {
  state = {
    showMsg:false
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }  
  render() {
    const { item , onClick ,userProfile , uploadVideoStatus ,isLocker,
        isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
        openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder,
        openTempTimelineToUser
      } = this.props;

    return (
        <li key={item.timelineIdx}>
            <div className="imagebox">
                <DivDeletedImage item={item}/>
                <div className="image card_thumnail_wapper img-hover-parent"  style={{width:"285px"}}>
                    <img src={item.thumbnail} alt="image" />
                    {item.sendStatus ==='W'
                        ?
                        <div style={sendOverlay}><div style={{width:'100%'}}>{T.translate('common.transferring')}</div></div>
                        : ''
                    }
                    <DivImageMouseOver item={item} onClick={onClick} ></DivImageMouseOver> 
                </div>
                <div className="content">
                    <p>{item.name} </p>
                    <div className="bottom clearfix">
                        <p><a>Edit {item.uptDate}</a></p>
                        {item.sendStatus ==='W'
                            ?
                            <div className="card_more_layar more">
                                <img src="/images/v1/ic-more-bigcard@2x.png" />
                            </div>
                            : <CardMore 
                                    item={item} 
                                    userProfile={userProfile} 
                                    isLocker={isLocker} 
                                    uploadVideoStatus={uploadVideoStatus}
                                    isActiveSettingLayer={isActiveSettingLayer} 
                                    toggleActiveLayer={toggleActiveLayer}
                                    toggleInactiveLayer={toggleInactiveLayer}

                                    openClipBoard={openClipBoard}
                                    deleteTimeline={deleteTimeline}
                                    videoPublish={videoPublish}
                                    openReport={openReport}
                                    openChooseUsers={openChooseUsers}
                                    openFolder={openFolder}
                                    openTempTimelineToUser={openTempTimelineToUser}
                            ></CardMore>
                        }
                        
                        
                    </div>
                </div>                                            
            </div>
        </li>
    );
  }
}

export default TempCardView;
