import React, { Component , Fragment} from "react";
import moment from 'moment'
import T from 'i18n-react';
import CardMore from './CardMore'
import DivDeletedImage from './DivDeletedImage'
import DivChooseCardView from './DivChooseCardView'
import DivImageMouseOver from './DivImageMouseOver'
const deleteOverlay = {
  width: '100%',
  height: '100%',
  position: 'absolute',
  top: '0',
  display: 'flex',
  justifyItems: 'center',
  alignItems: 'center',
  textAlign: 'center',
  fontSize: '35px',
  color: '#fff',
  zIndex: '1',
  backgroundColor: '#000000aa'
}
const ddayText={
  display:'inline-block',
  fontFamily: 'NotoSansCJKkr',
  fontSize: '35px',
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontStretch: 'normal',
  lineHeight: 'normal',
  letterSpacing: '-0.2px',
  color: '#ffffff',
  marginRight:'10px'
}
const timeText={
  display:'inline-block',
  fontFamily: 'NotoSansCJKkr',
  fontSize: '20px',
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontStretch: 'normal',
  lineHeight: 'normal',
  letterSpacing: '-0.2px',
  color: '#ffffff',
}
/**
 * 필수 지식 카드 뷰 입니다.
 */
class AssignedCardView extends Component {
  state = {
    showMsg:false,
    selected:0,
  }
  /**
   * 최상단 페이지 취소 눌렀을 경우 선택 체크박스 초기화
   * @param {*} nextProps 
   * @param {*} prevState 
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selectedItems.isChanging === false) {
      return { selected: 0 };
    }
    return null;
  }
  /**
   * 메시지 박스 클릭시 토글 이벤트 입니다.
   */
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }
  /**
   * 해당 카드를 선택 했을 경우 이벤트 입니다.
   */
  handleChecked = (item) =>{
    const { selectedItems , selectedCallback} = this.props
      if(this.state.selected === 0){
        const arr =selectedItems.idx.concat(item.timelineIdx)
        const originObj =selectedItems.originObj.concat(item)
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 1 });
      }else{
        const arr =selectedItems.idx.filter(_id=>{
            return _id !== item.timelineIdx
        })
        const originObj =selectedItems.originObj.filter(item=>{
            return item.timelineIdx !== item.timelineIdx
        })
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 0 });
      }
  }
  render() {
    const { item ,selectedItems, onClick ,userProfile , uploadVideoStatus ,isLocker,
      isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
      openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
    } = this.props;

    //day를 계산합니다.
    let endOrigin = moment(item.assignedInfo.endDate );
    let end = moment(item.assignedInfo.endDate ).hour(0).minutes(0).seconds(0)
    const now = moment();
    const d_days =now.diff (end, 'days');
    let d_days_str=''
    
    function fillzeros(num, len) {
      var str = '';
      num = num.toString();
      if (num.length < len){
        for(let i = 0 ; i < len-num.length;i++){
          str += '0';
        }
      }
      return str + num;
    }
    let hhMM = null; 
    if(d_days===0){
      const d_hour =now.diff (endOrigin, 'hours', false);
      const d_min =now.diff (endOrigin, 'minutes', false);
      var clearMinutes = d_min % 60;
      d_days_str='D-Day ';
      let signedNum='+'
      if(d_hour < 0 || d_min < 0){
        signedNum='-';
      }
      hhMM = signedNum + fillzeros(Math.abs(d_hour),2)+":"+fillzeros(Math.abs(clearMinutes),2);
    }else{      
      d_days_str='D'+ ((d_days > 0 ) ? '+'+ d_days : d_days) 
    }
    return (
      <div className="one-three">
          <div className="imageBox style4 clearfix">
              <div className="image img-hover-parent" >
                <div className="card_thumnail_wapper" onClick={ (item.videoStatusFlag==='C'? onClick:null)}>
                    <img src={item.thumbnail} alt="image"/>
                </div>
                <DivDeletedImage item={item}/>
                <div className="text assigned-text ">
                  <div className="seen">
                    {item.assignedInfo.completeDate ?
                      <Fragment>
                        <p>Completed</p>
                        <span style={{marginBottom:'10px',lineHeight:'13px'}}>{moment(item.assignedInfo.startDate).format('YYYY.MM.DD hh:mm')} | {T.translate('play_menu.deadline')}:{moment(item.assignedInfo.completeDate).format('YYYY.MM.DD hh:mm')}</span>
                      </Fragment>
                      :
                      <Fragment>
                        <span style={ddayText}>
                          {d_days_str}
                        </span>
                        {hhMM!=null && <span style={timeText}>{hhMM}</span>}
                      </Fragment>
                    }
                  </div>
                  <p><a >{item.name}</a></p>
                </div>
                <span className="time">{item.playTime}</span>
                {/* <DivImageMouseOver item={item} onClick={onClick}></DivImageMouseOver> */}
                <div className="bottom"><CardMore
                      item={item}
                      userProfile={userProfile}
                      isLocker={isLocker}
                      uploadVideoStatus={uploadVideoStatus}
                      isActiveSettingLayer={isActiveSettingLayer}
                      toggleActiveLayer={toggleActiveLayer}
                      toggleInactiveLayer={toggleInactiveLayer}

                      openClipBoard={openClipBoard}
                      deleteTimeline={deleteTimeline}
                      videoPublish={videoPublish}
                      openReport={openReport}
                      openChooseUsers={openChooseUsers}
                      openFolder={openFolder}
                    ></CardMore></div>
                  
                  <a className="message" onClick={()=>this.setState({ showMsg: true })}><img src="/images/v1/icon/ic-message-card.svg"/></a>
                  { this.state.showMsg ?
                  <div className="assigned-message" style={ {width:'50%',left:'50%'} }>
                      <div className="message-header">
                          <div className="title"></div>
                          <img onClick={()=>this.setState({ showMsg: false })} src="/images/v1/icon/ic-cancel.svg"/>
                      </div>
                      <div className="message-body">{item.assignedInfo.message}</div>
                      <span className="message-footer">{moment(item.assignedInfo.assignedDate).format('YYYY.MM.DD hh:mm')}</span>
                  </div>
                : null
                }
                <DivChooseCardView selectedItems={selectedItems} selected={this.state.selected} item={item} callBack={this.handleChecked}/>
              </div>
          </div>
      </div>
    );
  }
}

export default AssignedCardView;
