import React, { Component } from "react";
import { Link , withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import { textToHtml ,mergeName} from '../../util/card';
import moment from 'moment'
import T from "i18n-react";
/**
 * 유저 프로필 카드뷰 입니다.
 */
class UserCardView extends Component {
  state = {
    showMsg:false
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }
  render() {
    const { item , onClick} = this.props;
    return (
        <div className="one-four">
            <div className="imageBox image-top image-top-simple">
                <Link to={`/profile/${item.userKey}`}>
                <div className="content">
                    <div className="bottom clearfix">
                        <div className="wrap-avatar">
                            <div className="image-avatar" >
                                    <img src={item.thumbnailSmall} alt="image"/>
                                    <span className="userLevel"><img src={`/images/v1/user-ic-level-${item.level}-l-3-x@2x.png`} alt="image"/></span>
                            </div>
                            <div className="wrap-info">
                                <div className="name"><a href="#">{ReactHtmlParser(textToHtml(mergeName(item.nickname,item.position,item.department),this.props.keyword))}</a></div>
                                <div className="time">{T.translate('common.knowledge')}{item.timelineCount}개</div>
                            </div>
                        </div>
                    </div>
                </div>
                </Link>
            </div>
        </div>
    );
  }
}
export default connect(
    (state) => ({
      keyword: state.searchM.get('keyword'),
    }),
    (dispatch) => ({
    })
    )(withRouter(UserCardView));

