import React, { Component } from "react";
import moment from 'moment'
import CardMore from './CardMore'
import DivDeletedImage from './DivDeletedImage'
import DivChooseCardView from './DivChooseCardView'
import DivImageMouseOver from './DivImageMouseOver'
/**
 * 폴더 카드뷰 입니다.
 */
class FolderCardView extends Component {
  state = {
    showMsg:false,
    selected:0,
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }
  /**
   * 최상단 페이지 취소 눌렀을 경우 선택 체크박스 초기화
   * @param {*} nextProps 
   * @param {*} prevState 
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selectedItems.isChanging === false) {
      return { selected: 0 };
    }
    return null;
  }
  /**
   * 해당 카드를 선택 했을 경우 이벤트 입니다.
   */
  handleChecked = (item) =>{
    const { selectedItems , selectedCallback} = this.props
      if(this.state.selected === 0){
        const arr =selectedItems.idx.concat(item.timelineIdx)
        const originObj =selectedItems.originObj.concat(item)
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 1 });
      }else{
        const arr =selectedItems.idx.filter(_id=>{
            return _id !== item.timelineIdx
        })
        const originObj =selectedItems.originObj.filter(item=>{
            return item.timelineIdx !== item.timelineIdx
        })
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 0 });
      }
  }   
  render() {
    const { item , selectedItems, onClick ,userProfile , uploadVideoStatus ,isLocker,
      isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
      openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
    } = this.props;
    return (
      <div className="one-three">
        <div className="imageBox cardSize style4 clearfix">
            <div className="image img-hover-parent">
                <div className="card_thumnail_wapper" onClick={ (item.videoStatusFlag==='C'? onClick:null)} >
                  <img src={item.thumbnail} alt="image" style={{ height: "100%" }}/>
                </div>
                <DivDeletedImage item={item}/>
                <div className="text">
                    <div className="name"><a >{item.user.nickname}·{item.user.position}·{item.user.duty}</a></div>
                    <p><a className="timeline_title">{item.name}</a></p>
                </div>
                <span className="time">{item.playTime}</span>
                {/* <DivImageMouseOver item={item} onClick={onClick} ></DivImageMouseOver> */}
                <a onClick={()=>this.setState({ showMsg: true })}  className="message"><img src="/images/v1/icon/ic-message-card.svg"/></a>
                <div className="bottom">
                  <CardMore 
                    item={item} 
                    userProfile={userProfile}
                    isLocker={isLocker} 
                    uploadVideoStatus={uploadVideoStatus}
                    isActiveSettingLayer={isActiveSettingLayer} 
                    toggleActiveLayer={toggleActiveLayer}
                    toggleInactiveLayer={toggleInactiveLayer}

                    openClipBoard={openClipBoard}
                    deleteTimeline={deleteTimeline}
                    videoPublish={videoPublish}
                    openReport={openReport}
                    openChooseUsers={openChooseUsers}
                    openFolder={openFolder}
                ></CardMore>
                </div>
                <DivChooseCardView selectedItems={selectedItems} selected={this.state.selected} item={item} callBack={this.handleChecked}/>
                <div className="image-content" style={ {display:(this.state.showMsg ===true ? 'block' : 'none' )} }>                        
                    <div className="top clearfix">
                        <div className="section">{item.pinInfo.begin && item.pinInfo.end && `${item.pinInfo.begin}-${item.pinInfo.end}` }</div>
                        <a onClick={()=>this.setState({ showMsg: false })} className="cancel cancelMessage"><img src="/images/v1/icon/ic-cancel.svg"/></a>
                    </div>                                         
                    <div className="text">{item.pinInfo.message}</div>
                    <span className="time">{moment(item.pinInfo.regDate).format('YYYY.MM.DD HH:mm')}</span>
                </div>
            </div>
        </div>
    </div>
      // <div className="one-three">
      //   <div className="imageBox style4 clearfix">
      //     <div className="image" onClick={onClick}>
            // <div
            //   style={{
            //     height: "240px",
            //     backgroundColor: "#1f1f1f",
            //     textAlign: "center"
            //   }}
            // >
            //   <img
            //     src={item.thumbnail}
            //     alt="image"
            //     style={{ height: "100%" }}
            //   />
            // </div>

      //       <a className="message">
      //         <img src="/images/v1/icon/ic-message-card.svg" />
      //       </a>
      //       <div className="text">
      //         <div className="name">
      //           <a >
      //             {item.user.nickname}·{item.user.position}·{item.user.duty}
      //           </a>
      //         </div>
      //         <p>
      //           <a >{item.title}</a>
      //         </p>
      //       </div>
      //       <span className="time">{item.playTime}</span>
      //       <div className="bottom">
              // <CardMore 
              //     item={item} 
              //     userProfile={userProfile}
              //     isLocker={isLocker} 
              //     uploadVideoStatus={uploadVideoStatus}
              //     isActiveSettingLayer={isActiveSettingLayer} 
              //     toggleActiveLayer={toggleActiveLayer}
              //     toggleInactiveLayer={toggleInactiveLayer}

              //     openClipBoard={openClipBoard}
              //     deleteTimeline={deleteTimeline}
              //     videoPublish={videoPublish}
              //     openReport={openReport}
              //     openChooseUsers={openChooseUsers}
              //     openFolder={openFolder}
              // ></CardMore>
      //       </div>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

export default FolderCardView;
