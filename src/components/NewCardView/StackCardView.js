import React, { Component } from "react";
import { connect } from 'react-redux';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import { textToHtml ,mergeName} from '../../util/card';
import CardMore from './CardMore'
import DivImageMouseOver from './DivImageMouseOver'
import DivDeletedImage from './DivDeletedImage'

const textOverlay = {
  width: '100%',
  height: '100%',
  position: 'absolute',
  top: '0',
  display: 'flex',
  justifyItems: 'center',
  alignItems: 'center',
  textAlign: 'center',
  fontSize: '35px',
  color: '#fff',
  zIndex: '1',
  backgroundColor: '#00000002'
}
/**
 * 덧지식 카드뷰 입니다.
 */
class SectionCardView extends Component {
  state = {
    showMsg:false
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }  
  render() {
    const { item , onClick ,userProfile , uploadVideoStatus ,isLocker,
        isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
        openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
      } = this.props;
    return (
    <div className="one-three">
        <div className="imageBox cardSize image-top">
         <DivDeletedImage item={item}/>
              <div className="stack-card img-hover-parent">
                  <div className="top stack-top">                             
                      <div className="title"> <a href="#">{ReactHtmlParser(textToHtml(item.name,this.props.keyword))}</a></div>
                      <div className="timeline-name">{ReactHtmlParser(textToHtml(item.timelineName,this.props.keyword))}</div>
                  </div>
                  <div className="stack-bottom">
                    <div className="stack-info">
                      <div><img src="/images/v1/icon/card-ic-like-large-3-x-2@3x.png"></img><span>{item.likeCount}</span></div>
                      <div><img src="/images/v1/icon/card-ic-comm-large-3-x@3x.png"></img><span>{item.commentCount}</span></div>
                    </div>
                    <ul className="stack-icons">
                      <li>
                        <img src={`/images/v1/icon/card-ic-text-${item.card.indexOf('TEXT') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li>  
                      <li>
                        <img src={`/images/v1/icon/card-ic-camera-${item.card.indexOf('IMAGE') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li> 
                      <li>
                        <img src={`/images/v1/icon/card-ic-link-${item.card.indexOf('LINK') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li> 
                      <li>
                        <img src={`/images/v1/icon/card-ic-map-${item.card.indexOf('MAP') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li> 
                      <li>
                        <img src={`/images/v1/icon/card-ic-file-${item.card.indexOf('FIME') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li> 
                      <li>
                        <img src={`/images/v1/icon/card-ic-quiz-${item.card.indexOf('QUIZ') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li> 
                      <li>
                        <img src={`/images/v1/icon/card-ic-vote-${item.card.indexOf('VOTE') > 0?'s':'n'}-3-x@3x.png`}></img>
                      </li> 
                    </ul>
                     
                  </div>
                  <DivImageMouseOver item={item} onClick={onClick} ></DivImageMouseOver> 
                  {/* <div className="bottom clearfix" style={{border:'none'}}>
                    {item.regDate}
                      <CardMore 
                        item={item} 
                        userProfile={userProfile} 
                        isLocker={isLocker} 
                        uploadVideoStatus={uploadVideoStatus}
                        isActiveSettingLayer={isActiveSettingLayer} 
                        toggleActiveLayer={toggleActiveLayer}
                        toggleInactiveLayer={toggleInactiveLayer}

                        openClipBoard={openClipBoard}
                        deleteTimeline={deleteTimeline}
                        videoPublish={videoPublish}
                        openReport={openReport}
                        openChooseUsers={openChooseUsers}
                        openFolder={openFolder}
                      ></CardMore>
                  </div> */}
              </div>                                            
          </div>
    </div>
    );
  }
}
export default connect(
  (state) => ({
    keyword: state.searchM.get('keyword'),
  }),
  (dispatch) => ({
  })
  )(SectionCardView);

