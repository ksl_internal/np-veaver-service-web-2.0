import React, { Component } from "react";
import { Link } from 'react-router-dom';
import CardMore from './CardMore'
import T from 'i18n-react';
import DivDeletedImage from './DivDeletedImage'
import DivChooseCardView from './DivChooseCardView'
import DivImageMouseOver from './DivImageMouseOver'
const positionRelative = {
    position: 'relative'
}
const centCard = {
    display: 'inline-block',
    width: '27px',
    height: '27px',
    overflow: 'hidden',
    marginRight: '5px'
}
/**
 * 보낸 카드뷰 입니다.
 */
class CentCardView extends Component {
  state = {
    showMsg:false,
    isOpenMore:false,
    selected:0,
  }
  componentWillMount(){

  }
  /**
   * 최상단 페이지 취소 눌렀을 경우 선택 체크박스 초기화
   * @param {*} nextProps 
   * @param {*} prevState 
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selectedItems.isChanging === false) {
      return { selected: 0 };
    }
    return null;
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }
  toggleMore = () =>{
    this.setState({ isOpenMore: (this.state.isOpenMore) ? false : true });
  }
  /**
   * 해당 카드를 선택 했을 경우 이벤트 입니다.
   */
  handleChecked = (item) =>{
    const { selectedItems , selectedCallback} = this.props
    console.log(selectedItems)
      if(this.state.selected === 0){
        const arr =selectedItems.idx.concat(item.timelineIdx)
        const originObj =selectedItems.originObj.concat(item)
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 1 });
      }else{
        const arr =selectedItems.idx.filter(_id=>{
            return _id !== item.timelineIdx
        })
        const originObj =selectedItems.originObj.filter(item=>{
            return item.timelineIdx !== item.timelineIdx
        })
        selectedItems.idx = arr
        selectedItems.originObj = originObj
        selectedCallback(selectedItems)
        this.setState({ selected: 0 });
      }
  }
  render() {
    const { item ,selectedItems, onClick ,userProfile , uploadVideoStatus ,isLocker,
      isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
      openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
    } = this.props;
    const shareUsers = item.bySharedInfo.shareUsers.map(user=>{
        return (
            <li key={'shareUsers'+user.userKey}><img src={user.thumbnailSmall}/></li>
        )
    })
    return (
        <div className="one-three" >

            <div className="imageBox cardSize flex clearfix" style={positionRelative}>
                <DivDeletedImage item={item}/>
                <div className="content inner-flex img-hover-parent">
                    <div className="seen">
                        <p>
                            {
                                item.bySharedInfo.seenUserCount === item.bySharedInfo.sharedUserCount &&
                                <span style={centCard}><img src="/images/v1/icon/ic-check.svg"/></span>
                            }
                            <span className="first">{item.bySharedInfo.seenUserCount}/</span>
                            <span className="last">{item.bySharedInfo.sharedUserCount}</span>
                        </p>
                        <span className="time">{item.bySharedInfo.sharedDate}</span>
                    </div>
                    <ul className="gallery">
                        {shareUsers}
                    </ul>
                    {/* <DivImageMouseOver item={item} onClick={onClick}></DivImageMouseOver> */}
                    <a onClick={()=>this.setState({ showMsg: true })} className="message"><img src="/images/v1/icon/ic-message-card.svg"/></a>

                    <div className="wrap-message" style={ {display:(this.state.showMsg ? 'block' : 'none' )} }>
                        <div className="text">{item.bySharedInfo.message}</div>
                        <div className="bottom">
                            <div className="section">{item.bySharedInfo.begin && item.bySharedInfo.end && `${item.bySharedInfo.begin}-${item.bySharedInfo.end}` } &nbsp;</div>
                            <a onClick={()=>this.setState({ showMsg: false })} className="cancel cancelMessage"><img src="/images/v1/icon/ic-cancel.svg"/></a>
                        </div>
                    </div>
                </div>
                <div className="image inner-flex">
                    <div className="card_thumnail_wapper" onClick={ (item.videoStatusFlag==='C'? onClick:null)}>
                       <img src={item.thumbnail} alt="image"/>
                    </div>
                    <a className="group-card">
                        <img src="/images/v1/icon/img-group-card.svg" alt="image"/>
                    </a>
                    <DivChooseCardView selectedItems={selectedItems} selected={this.state.selected} item={item} callBack={this.handleChecked}/>
                    <div className="text"><p><a >{item.name}</a></p></div>
                    <span className="time">{item.playTime}</span>
                    <div className="bottom">
                        <CardMore
                            item={item}
                            userProfile={userProfile}
                            isLocker={isLocker}
                            uploadVideoStatus={uploadVideoStatus}
                            isActiveSettingLayer={isActiveSettingLayer}
                            toggleActiveLayer={toggleActiveLayer}
                            toggleInactiveLayer={toggleInactiveLayer}

                            openClipBoard={openClipBoard}
                            deleteTimeline={deleteTimeline}
                            videoPublish={videoPublish}
                            openReport={openReport}
                            openChooseUsers={openChooseUsers}
                            openFolder={openFolder}
                            rotate={true}
                        ></CardMore>
                    </div>
                </div>
            </div>
        </div>
        // <div className="one-three">
        //     <div className="imageBox style2 flex clearfix">
        //         <div className="content inner-flex">
        //             <div className="avatar">
        //                 <img src={item.user.thumbnail} alt="image"/>
        //                 <span className="userLevel"><img src={`/v1//images/v1/user-ic-level-${item.user.veaverIndiceInfo.level}-l-3-x@2x.png`} alt="image"/></span>
        //             </div>
        //             <div className="title">{item.user.nickname}</div>
        //             <div className="sub-title">{item.user.position ? item.user.position : ''}{item.user.department ? '·' : ''}{item.user.department ? item.user.department : ''}</div>
        //             <div className="time">{item.regDate}</div>
        //             <a className="message" onClick={()=>this.setState({ showMsg: true })} ><img src="/v1//images/v1/icon/ic-message-card.svg"/></a>
        //             <div className="wrap-message" style={ this.state.showMsg? {display:'block'} : {display:'none'} }>
        //                 <div className="text">{item.toSharedInfo.seenDate !== undefined && item.toSharedInfo.message}</div>
        //                 <div className="bottom">
        //                     <div className="section">A-B</div>
        //                     <a onClick={()=>this.setState({ showMsg: false })} className="cancel cancelMessage"><img src="/v1//images/v1/icon/ic-cancel.svg"/></a>
        //                 </div>
        //             </div>


        //         </div>
        //         <div className="image inner-flex">
        //             <div onClick={onClick} style={{backgroundColor: "#1f1f1f",height: "100%",textAlign: "center"}}>
        //                 <img src={item.thumbnail} alt="image" style={{ height: "27vh" }} />
        //             </div>
        //             <a className="group-card">
        //                 <img src="/v1//images/v1/icon/img-group-card.svg" alt="image"/>
        //             </a>
        //             <div className="text">
        //                 {item.toSharedInfo.seenDate !== null &&
        //                     <div className="seen"><p>Seen</p><span>{item.toSharedInfo.seenDate}</span></div>
        //                 }
        //                 <p>{item.title}</p>
        //             </div>
        //             <span className="time">{item.playTime}</span>
                    // <div className="bottom">
                    // <CardMore
                    //     item={item}
                    //     userProfile={userProfile}
                    //     isLocker={isLocker}
                    //     uploadVideoStatus={uploadVideoStatus}
                    //     isActiveSettingLayer={isActiveSettingLayer}
                    //     toggleActiveLayer={toggleActiveLayer}
                    //     toggleInactiveLayer={toggleInactiveLayer}

                    //     openClipBoard={openClipBoard}
                    //     deleteTimeline={deleteTimeline}
                    //     videoPublish={videoPublish}
                    //     openReport={openReport}
                    //     openChooseUsers={openChooseUsers}
                    //     openFolder={openFolder}
                    //   ></CardMore>
                    // </div>
        //             {/* <a className="more"><img src="/v1//images/v1/icon/ic-more-card.svg"/></a> */}
        //         </div>
        //     </div>
        // </div>
    );
  }
}

export default CentCardView;
