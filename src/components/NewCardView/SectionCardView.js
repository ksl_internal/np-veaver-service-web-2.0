import React, { Component } from "react";
import { connect } from 'react-redux';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import { textToHtml ,mergeName} from '../../util/card';
import CardMore from './CardMore'
import DivImageMouseOver from './DivImageMouseOver'
import DivDeletedImage from './DivDeletedImage'

const textOverlay = {
  width: '100%',
  height: '100%',
  position: 'absolute',
  top: '0',
  display: 'flex',
  justifyItems: 'center',
  alignItems: 'center',
  textAlign: 'center',
  fontSize: '35px',
  color: '#fff',
  backgroundColor: '#00000002',
  overflow: 'hidden',
  wordBreak: 'break-all'
}
/**
 * 섹션 카드뷰 입니다.
 * 검색 했을 경우 섹션 및 타임라인 서버에서 tag 값을 주지 않습니다. 
 * 그렇기 때문에 디자인 상에 tag 표현을 숨김 처리 했습니다
 */
class SectionCardView extends Component {
  state = {
    showMsg:false
  }
  openMsg = () =>{
    this.setState({ showMsg: (this.state.showMsg) ? false : true });
  }  
  render() {
    const { item , onClick ,userProfile , uploadVideoStatus ,isLocker,
        isActiveSettingLayer,toggleActiveLayer,toggleInactiveLayer,
        openClipBoard,deleteTimeline,videoPublish,openReport,openChooseUsers,openFolder
      } = this.props;
    return (
    <div className="one-three">
        <div className="imageBox image-top">
          <DivDeletedImage item={item}/>
          <div className="image img-hover-parent">
            <div className="card_thumnail_wapper">
              <img src={item.thumbnail} alt="image" />
            </div> 
            <div style={textOverlay}><div style={{width:'100%'}}>{ReactHtmlParser(textToHtml(item.timelineName,this.props.keyword))}</div></div>
            <DivImageMouseOver item={item} onClick={onClick} ></DivImageMouseOver> 
          </div>
              <div className="content">
                  <div className="top">                             
                      <div className="title"> <a href="#">{ReactHtmlParser(textToHtml(item.name,this.props.keyword))}</a></div>
                      {item.tags &&
                        <div className="wrap-btn">
                            {item.tags.map( (tag,i)=>{
                              return (
                                <a key={tag+'-'+i} className="btn">{ReactHtmlParser(textToHtml(tag,this.props.keyword))}</a>
                              )
                            })}
                        </div>
                      }
                  </div>
                  <div className="bottom clearfix" style={{border:'none'}}>
                    {item.regDate}
                      <CardMore 
                        item={item} 
                        userProfile={userProfile} 
                        isLocker={isLocker} 
                        uploadVideoStatus={uploadVideoStatus}
                        isActiveSettingLayer={isActiveSettingLayer} 
                        toggleActiveLayer={toggleActiveLayer}
                        toggleInactiveLayer={toggleInactiveLayer}

                        openClipBoard={openClipBoard}
                        deleteTimeline={deleteTimeline}
                        videoPublish={videoPublish}
                        openReport={openReport}
                        openChooseUsers={openChooseUsers}
                        openFolder={openFolder}
                      ></CardMore>
                  </div>
              </div>                                            
          </div>
    </div>
    );
  }
}
export default connect(
  (state) => ({
    keyword: state.searchM.get('keyword'),
  }),
  (dispatch) => ({
  })
  )(SectionCardView);

