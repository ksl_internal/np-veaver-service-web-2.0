import React, {Component ,Fragment } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { isLocker } from '../../util/api'
import {toastr} from 'react-redux-toastr';
import * as myPageActionCreators from "../../modules/mypage";
import * as makeActionCreators from "../../modules/make";
import * as openPopup from "../../modules/popup";
import * as timelinesActions from '../../modules/timelines'
/**
 * 모든 카드 리스트 뷰를 관리합니다.
 */
class TimelinesWrapper extends Component {
    state = {
        clickableIndex: -1,
      }  
    _checkAllow = (tIdx, beginIdx) => {
        const timelineIdx = tIdx;
        let isLockerPage = isLocker();
        const { location} = this.props
        if(location.pathname==='/make'){
            isLockerPage = true
        }
        if (isLockerPage) {
            this.props.history.push(`/locker/play/${timelineIdx}`)
        } else {
            this.props.MyPageActionCreators.checkAllow(timelineIdx)
            .then(res => {
            if (res.isSuccess) {
                if(beginIdx && beginIdx != 0)
                    this.props.history.push(`/play/${timelineIdx}?begin=${beginIdx}`)
                else
                    this.props.history.push(`/play/${timelineIdx}`)
            } else {
                toastr.light('확인 할수 없는 지식 입니다.');
            }
            })
            .catch(error => Error(error));
        }
    
      }
      _videoPublish = (e, timelineIdx) => {
        e.preventDefault()
        this.props.MakeActionCreators.uploadVideoInfoDataSet(timelineIdx)
      }
    
      _openClipBoard = (e, videoIdx, timelineIdx) => {
        e.preventDefault()
        this.props.MyPageActionCreators.openClipBoard(videoIdx, timelineIdx)
        this.setState({ clickableIndex: -1 })
      }
    
      _deleteTimeline = async(e, timelineIdx) => {
        e.preventDefault(timelineIdx)
        if(this.props.location.pathname==='/make' || this.props.location.pathname==='/profile'){
            this.props.MyPageActionCreators.deleteTimeline(timelineIdx,null,true)
        }else{
            this.props.MyPageActionCreators.deleteTimeline(timelineIdx)
            this.setState({ clickableIndex: -1 })
        }
      } 
    render(){
        const {data,CardView ,userProfile , uploadVideoStatus ,selectedItems ,selectedCallback} = this.props;
        let isLockerPage = isLocker();
        const { location} = this.props
        if(location.pathname==='/make'){
            isLockerPage = true
        }
        const { clickableIndex } = this.state
        const items = data.map( (item , index)=>{
            //각 카드뷰에 따른 리액트 key값을 설정 합니다.
            //클릭시 상세 페이지 이동은 기본으로 timelineIdx 로 상세페이지 이동하게 됩니다.
            let keyTemp = item.timelineIdx;
            if(CardView.name === 'CentCardView'){
                keyTemp = item.bySharedInfo.sharedIdx;
            }else if(CardView.name === 'ReceivedCardView'){
                keyTemp = item.toSharedInfo.sharedIdx
            }else if(CardView.name === 'FolderCardView'){
                keyTemp = item.pinInfo.idx
            }else if(CardView.name === 'UserCardView'){
                keyTemp = item.userKey
            }
            return (
                <CardView 
                    key={`${CardView.name}-${keyTemp}-${index}`} 
                    item={item} 
                    selectedItems={selectedItems}
                    selectedCallback={selectedCallback}
                    onClick={()=>this._checkAllow(item.timelineIdx, item.begin != null ? item.begin: '')} 
                    userProfile={userProfile}
                    isLocker={isLockerPage}
                    openClipBoard={(e) => this._openClipBoard(e, item.videoIdx, item.timelineIdx)}
                    deleteTimeline={(e) => this._deleteTimeline(e, item.timelineIdx)}
                    videoPublish={(e) => this._videoPublish(e, item.timelineIdx)} 
                    openReport={(e)=>{e.preventDefault();this.props.OpenPopup.openPopup('REPORT', {timelineIdx:item.timelineIdx,targetType:'T'} )}}
                    openChooseUsers={(e)=>{e.preventDefault(); this.props.OpenPopup.openPopup('CHOOSEUSERS',{
                        timelineIdxs:[item.timelineIdx],
                        returnType:'share'
                    } )} }
                    openFolder={(e)=>{e.preventDefault(); this.props.OpenPopup.openPopup('FOLDER',{
                        timelineIdxList:[item.timelineIdx],
                        folderIdx : null,
                        message : null,
                        type:'add'
                    } )} }
                  
                    openTempTimelineToUser={(e)=>{e.preventDefault(); this.props.OpenPopup.openPopup('CHOOSEUSERS',{
                        timelineIdx:item.timelineIdx,
                        returnType:'tempTimelineToUser'
                    } )} }

                  onLoaded={complete => {}}
                  uploadVideoStatus={uploadVideoStatus}
                  isActiveSettingLayer={clickableIndex === index}
                  toggleActiveLayer={() => this.setState((state) => ({ clickableIndex: index }))}
                  toggleInactiveLayer={() => this.setState((state) => ({ clickableIndex: -1 }))}
                  
                  
                  >
                </CardView>
            )
        })
        return (
            <Fragment>
                {items}
            </Fragment>
        )
    }
    
}
export default connect(
(state) => ({
    userProfile: state.user.userProfile,
    uploadVideoStatus: state.make.uploadVideoStatus,
}),
(dispatch) => ({
    MyPageActionCreators: bindActionCreators(myPageActionCreators,dispatch),
    MakeActionCreators: bindActionCreators(makeActionCreators,dispatch),
    OpenPopup: bindActionCreators(openPopup,dispatch),
    TimelinesActions : bindActionCreators(timelinesActions,dispatch)
})
)(withRouter(TimelinesWrapper));
