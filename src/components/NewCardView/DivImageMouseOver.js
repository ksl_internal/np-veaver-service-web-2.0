import React , { Component ,Fragment} from "react";
import T from "i18n-react";
 /**
   * 모든 카드뷰 마우스 오버 카드 레이어 표시합니다.
   * (요청에 의해 삭제)
   */
const DivImageMouseOver = ({item,onClick}) =>{
    if(item.videoStatusFlag===undefined)
        item.videoStatusFlag = "C"
    return(
        <a
            onClick={ (item.videoStatusFlag==='C'? onClick:null)}  
          style={{ cursor: 'pointer' }}
          className="img-hover-layer">
            {item.viewCount !==undefined &&
                <div className="view">
                <p className="title">View</p>
                <p>{item.viewCount} </p>
                </div>
            }
            {
               item.likeCount !== undefined && 
                <div className="like">
                <p className="title">Like</p>
                <p>{item.likeCount}</p>
                </div>
            }
            {
                item.commentCount !== undefined &&
                <div className="comment">
                <p className="title">Comment</p>
                <p>{item.commentCount}</p>
                </div>
            }
            {
                item.playTime !== undefined && item.playTime !== '00:00' &&
                <div className="video-length">
                <p>{item.playTime}</p>
                </div>
            }
          </a>
    )
}
export default DivImageMouseOver;