import React, { Component } from 'react';
import cx from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isMouseLeftButton } from '../../util/mouse';
import { updateStackRange } from '../../modules/make';
import {
	initTimeline,
	seekTimeline,
  stackMoved,
} from '../../modules/player';

class MakeTimeline extends Component {

  state = {
    seekbarWidth: 772,
    unitWidth: 0,
    unitLength: 0,
    isThumbDrag: false,
    stackDrag: false,
    stackPosition: [],
  }

  constructor(props) {
    super(props);
    this._onThumbMouseMove = this._onThumbMouseMove.bind(this);
    this._onThumbMouseUp = this._onThumbMouseUp.bind(this);
    this._onStackLeftThumbMove = this._onStackLeftThumbMove.bind(this);
    this._onStackLeftThumbUp = this._onStackLeftThumbUp.bind(this);
    this._onStackRightThumbMove = this._onStackRightThumbMove.bind(this);
    this._onStackRightThumbUp = this._onStackRightThumbUp.bind(this);
  }

  componentDidMount() {
    const unitLength = this.props.duration / 5;
    const unitWidth = (this.state.seekbarWidth - 8) / unitLength - 1;

    this.setState((state) => {
      return {
        ...state,
        unitWidth,
        unitLength: Math.floor(unitLength),
      }
    });

    this.props.initTimeline();
    // this.timelineInterval = setInterval(this._onTimelineTick.bind(this), 200);
  }

  _onTimelineTick() {
		// if (!this.state.isThumbDrag) {
		// 	const unitLength = this.props.duration / 5;
		// 	const unitWidth = (this.elSeekbar.offsetWidth - 8) / unitLength - 1;
		// 	const { player } = this.props;

		// 	if (this.props.paused) {
		// 		this.setState((state) => {
		// 			return {
		// 				...state,
		// 				seekbarWidth: this.elSeekbar.offsetWidth,
		// 				unitWidth,
		// 				unitLength: Math.floor(unitLength),
		// 			}
		// 		});
		// 		return;
		// 	}

		// 	if (this.state.repeatAB && this.state.repeatEnd <= player.currentTime) {
		// 		player.setCurrentTime(this.state.repeatStart);
		// 	}

		// 	if (this.state.isThumbDrag) {
		// 		return;
		// 	}

		// 	this.timelineInterval && this.setState((state) => {
		// 		return {
		// 			...state,
		// 			seekbarWidth: this.elSeekbar.offsetWidth,
		// 			unitWidth,
		// 			unitLength: Math.floor(unitLength),
		// 		}
    //   });
    // }
  }

  componentWillUnmount() {
    clearInterval(this.timelineInterval);
    this.timelineInterval = false;
  }

  _onSeekbarMouseDown(evt) {
    if (!isMouseLeftButton(evt) || !this.props.mediaLoaded) {
      return;
    }

    const { player, duration } = this.props;
    const { currentTarget } = evt;

    let seekLeft = evt.clientX - currentTarget.getClientRects()[0].left;
    seekLeft = this.calcLimitThumbPosition(seekLeft);

    const seekTime = seekLeft / (this.state.seekbarWidth / duration);
    // this.setState((state) => {
    //   return {
    //     ...state,
    //     currentTime: seekTime,
    //   }
    // });
    this.props.seekTimeline(seekTime);
    player.setCurrentTime(seekTime);
  }

  _onSeekbarMouseUp(evt) {
		// console.log('seek up')
		this.props.stackMoved();
  }
  
  _onThumbMouseDown(evt) {
    if (!isMouseLeftButton(evt)) {
      return;
    }
    
    const { target } = evt;
		if (target && target.className === 'rail-handle') {
			this.setState((state) => {
				return {
					...state,
					isThumbDrag: true
				}
			});
			this.dragStartX = this.elSeekbar.getClientRects()[0].left;
			document.addEventListener('mousemove', this._onThumbMouseMove);
			document.addEventListener('mouseup', this._onThumbMouseUp);
    }
    evt.preventDefault();
		evt.stopPropagation();
	}

	_onThumbMouseMove(evt) {
		let seekLeft = evt.clientX - this.dragStartX;
    seekLeft = this.calcLimitThumbPosition(seekLeft);
    const { player, duration } = this.props;
    const seekTime = seekLeft / (this.state.seekbarWidth / duration);
    this.props.seekTimeline(seekTime);
    player.setCurrentTime(seekTime);
		// this.setState((state) => {
		// 	return {
		// 		...state,
    //     currentTime: seekTime,
		// 	}
		// });
	}

	_onThumbMouseUp(evt) {
		document.removeEventListener('mousemove', this._onThumbMouseMove);
		document.removeEventListener('mouseup', this._onThumbMouseUp);
		this.setState((state) => {
			return {
				...state,
				isThumbDrag: false
			}
		});

		if (!this.props.mediaLoaded) {
			return;
		}

    const { player, currentTime } = this.props;
		// const seekLeft = evt.clientX - this.dragStartX;
		// const seekTime = seekLeft / (this.elSeekbar.offsetWidth / player.duration);
    this.props.stackMoved();
		player.setCurrentTime(parseFloat(currentTime));
    // this.props.seekTimeline(currentTime);
	}

  _onStackLeftThumbDown(evt) {
    evt.persist();
    

    if (!isMouseLeftButton(evt)) {
      return;
    }

    this.stackDrag = true;

    const { stacks, duration } = this.props;
    this.handleStackIdx = Number(evt.target.parentElement.dataset.idx);
    
    this.secPerWidth = this.state.seekbarWidth / duration;

    let begin = 0;
    let end = 0;

    if (this.handleStackIdx !== 0) {
      console.log(this.handleStackIdx)
      begin = stacks[this.handleStackIdx - 1].end;
    }

    begin *= this.secPerWidth;
    end = (stacks[this.handleStackIdx].end - 2) * this.secPerWidth;

    
    this.orgStackLeft = evt.target.parentElement.offsetLeft;
    this.stackAvailableRange = { begin, end };
    this.stackLeftDragX = evt.clientX;
    // console.log(begin, end, this.secPerWidth, this.stackLeftDragX, this.orgStackLeft)

    document.addEventListener('mousemove', this._onStackLeftThumbMove);
    document.addEventListener('mouseup', this._onStackLeftThumbUp);

    evt.preventDefault();
    evt.stopPropagation();
  }

  _onStackLeftThumbMove(evt) {
    if (!this.stackDrag) {
      return;
    }

    let stackLeft = this.orgStackLeft + evt.clientX - this.stackLeftDragX;
    if (stackLeft < this.stackAvailableRange.begin) {
      stackLeft = this.stackAvailableRange.begin
    }
    
    if (this.stackAvailableRange.end < stackLeft) {
      stackLeft = this.stackAvailableRange.end
    }
    // console.log(stackLeft, this.stackAvailableRange)
    const stacks = [...this.props.stacks]
    // const index = stacks.length === 1 ? this.handleStackIdx : this.handleStackIdx - 1
    // console.log(stacks, index)
    const oldBegin = stacks[this.handleStackIdx].begin;
    if (oldBegin !== Math.floor(stackLeft / this.secPerWidth)) {
      stacks[this.handleStackIdx].begin = Math.round(stackLeft / this.secPerWidth);
      this.props.updateStackRange(stacks);
    }
  }

  _onStackLeftThumbUp(evt) {
    document.removeEventListener('mousemove', this._onStackLeftThumbMove);
    document.removeEventListener('mouseup', this._onStackLeftThumbUp);
    this.stackDrag = false;
  }

  _onStackRightThumbDown(evt) {
    evt.persist();
    evt.preventDefault();
    evt.stopPropagation();
    if (!isMouseLeftButton(evt)) {
      return;
    }

    this.stackDrag = true;

    const { stacks, duration } = this.props;
    this.handleStackIdx = Number(evt.target.parentElement.dataset.idx);
    this.secPerWidth = this.state.seekbarWidth / duration;

    let begin = 0;
    let end = duration;

    if (this.handleStackIdx !== stacks.length - 1) {
      end = stacks[this.handleStackIdx + 1].begin;
    }
    begin = (stacks[this.handleStackIdx].begin + 2) * this.secPerWidth;
    end *= this.secPerWidth;

    this.orgStackLeft = evt.target.parentElement.offsetLeft + evt.target.parentElement.offsetWidth;
    this.stackAvailableRange = { begin, end };
    this.stackLeftDragX = evt.clientX;

    document.addEventListener('mousemove', this._onStackRightThumbMove);
    document.addEventListener('mouseup', this._onStackRightThumbUp);
  }

  _onStackRightThumbMove(evt) {
    if (!this.stackDrag) {
      return;
    }

    let stackRight = this.orgStackLeft + evt.clientX - this.stackLeftDragX;
    if (stackRight < this.stackAvailableRange.begin) {
      stackRight = this.stackAvailableRange.begin;
    }

    if (this.stackAvailableRange.end < stackRight) {
      stackRight = this.stackAvailableRange.end;
    }

    const { stacks } = this.props;
    const oldBegin = stacks[this.handleStackIdx].end;
    // console.log(this.secPerWidth, stackRight, oldBegin, Math.floor(stackRight / this.secPerWidth), oldBegin === Math.floor(stackRight / this.secPerWidth))
    
    stacks[this.handleStackIdx].end = Math.floor(stackRight / this.secPerWidth);
    oldBegin !== stacks[this.handleStackIdx].end && this.props.updateStackRange(stacks);
   
  }

  _onStackRightThumbUp(evt) {
    document.removeEventListener('mousemove', this._onStackRightThumbMove);
    document.removeEventListener('mouseup', this._onStackRightThumbUp);
    this.stackDrag = false;
  }

  calcLimitThumbPosition(seekLeft) {
    let leftLimit = 0;
    let rightLimit = this.state.seekbarWidth;

    if (seekLeft < leftLimit) {
      return leftLimit;
    } else if (rightLimit < seekLeft) {
      return rightLimit;
    }

    return seekLeft;
  }

  renderSections() {
    const { seekbarWidth } = this.state;
    const { sections } =  this.props;

    let remainWidth = seekbarWidth;

    const sectionItems = sections.map((section, idx) => {
      let sectionWidth = 0;

      if (idx + 1 < sections.length) {
        sectionWidth = (sections[idx + 1].begin - sections[idx].begin) / this.props.duration * seekbarWidth;
        remainWidth -= sectionWidth;
      } else {
        sectionWidth = remainWidth;
      }

      return (
        <div key={`section-${idx}`} className="section-block" style={{ width: `${sectionWidth}px`}}>
          <div className="section">
              <div className="section-title">
                  <p title={section.name}>{section.name}</p>
              </div>
          </div>
        </div>
      )
    })

    return (
      <div className="section-wrapper">
        {sectionItems}
      </div>
    );
  }

  renderStacksNEvents() {
    if (this.props.step < 2) {
      return null;
    }

    // const { stacks } = this.state;
    const { events, stacks } = this.props;

    const stackItems = stacks.map((stack, idx) => {
      const left = stack.begin / this.props.duration * this.state.seekbarWidth;
      const width = (stack.end - stack.begin) / this.props.duration * this.state.seekbarWidth;

      return (
        <div key={`stack-${idx}`}
          className="stack"
          style={{ left: `${left}px`, width: `${width}px`}}
          data-idx={idx}
        >
          <span className="left" onMouseDown={this._onStackLeftThumbDown.bind(this)}></span>
          <span className="right" onMouseDown={this._onStackRightThumbDown.bind(this)}></span>
        </div>
      );
    });

    const eventItems = events.map((event, idx) => {
      const marginValuePx = 8;
      const buttonCenterValuePx = 10;
      const left = Math.floor(event.begin) / Math.floor(this.props.duration) * (this.state.seekbarWidth - marginValuePx);

      return (
        <button key={`event-${idx}`} className="event-type" style={{ left: `${left - buttonCenterValuePx}px` }}></button>
      )
    });

    return (
      <div className="stack-wrapper" ref={el => this.stackWrapper = el}>
        {stackItems}
        {eventItems}
      </div>
    );
  }

  renderUnits() {
    const { unitWidth, unitLength } = this.state;

    const units = [];
    for (let i = 0; i < unitLength; i++) {
      units.push(<span key={i} className="unit" style={{ marginLeft: `${unitWidth}px`}}></span>)
    }

    return (
      <div className="sec-units">
        {units}
      </div>
    );
  }

  render() {
    const currentTime = Math.floor(this.props.currentTime);
    const sections = this.props.sections;
    const rangeTick = 2;

    return(
      <div className="make-timeline">
        <div className="timeline-wrapper">
          <div className="time-rail"
            ref={el => this.elSeekbar = el}
            onMouseDown={this._onSeekbarMouseDown.bind(this)}
            onMouseUp={this._onSeekbarMouseUp.bind(this)}
          >
            <span className="rail-total"></span>
            <span className="rail-current"
              style={{
                width: `${this.state.seekbarWidth / this.props.duration * (currentTime )}px`
              }}
            ></span>
            <button className="rail-handle"
              onMouseDown={this._onThumbMouseDown.bind(this)}
              style={{
                left: `${this.state.seekbarWidth / this.props.duration * (currentTime ) - 7}px`
              }}
            ></button>
            <span className="rail-timefloat"></span>
          </div>
          <button className={cx('timeline-handle', { 'combine-mode': sections.some((item, index) => ( item.begin !== 0 &&
          (item.begin - rangeTick <= currentTime && item.begin + rangeTick >= currentTime)))})}
            style={{
              left: `${this.state.seekbarWidth / this.props.duration * (currentTime )}px`
            }}
            ></button>
          <div className="timeline-rail"
            onMouseDown={this._onSeekbarMouseDown.bind(this)}
            onMouseUp={this._onSeekbarMouseUp.bind(this)}>
            <div className="timeline">
              {this.renderUnits()}
              {this.renderSections()}
              {this.renderStacksNEvents()}
            </div>
          </div>
        </div>
      </div>

    );
  }
}

const mapStateToProps = state => ({
  mediaLoaded: state.player.mediaLoaded,
	duration: state.player.duration,
  currentTime: state.player.currentTime,
	sections: state.make.sections,
  stacks: state.make.stacks,
  events: state.make.events,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  updateStackRange,
  seekTimeline,
  initTimeline,
  stackMoved,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
)(MakeTimeline);
