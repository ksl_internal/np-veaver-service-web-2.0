import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	initTimeline,
	seekTimeline,
	stackMoved,
} from '../../modules/player';
import { isMouseLeftButton } from '../../util/mouse';

class PlayTimeline extends Component {

  state = {
    isThumbDrag: false,
    thumbPosition: -7,
    seekbarWidth: 0,
    duration: this.props.duration || 0,
    repeatLeft: 0,
    repeatRight: 0,
    repeatStart: 0,
    repeatEnd: 0,
		unitWidth: 0,
		unitLength: 0,
  }

  constructor(props) {
    super(props);
    this._onThumbMouseMove = this._onThumbMouseMove.bind(this);
    this._onThumbMouseUp = this._onThumbMouseUp.bind(this);
    this._onRepeatLeftThumbMove = this._onRepeatLeftThumbMove.bind(this);
    this._onRepeatLeftThumbUp = this._onRepeatLeftThumbUp.bind(this);
    this._onRepeatRightThumbMove = this._onRepeatRightThumbMove.bind(this);
    this._onRepeatRightThumbUp = this._onRepeatRightThumbUp.bind(this);
  }

    componentDidMount() {
   
		const unitLength = this.props.duration / 5;
		const unitWidth = (this.elSeekbar.offsetWidth - 8) / unitLength - 1;

    this.setState((state) => {
      return {
        ...state,
        seekbarWidth: this.elSeekbar.offsetWidth,
				unitWidth,
				unitLength: Math.floor(unitLength),
      }
    });
    this.timelineInterval = setInterval(this._onTimelineTick.bind(this), 200);
    this.props.initTimeline();
    console.log("ply timeline")
  }

	componentWillUnmount() {
		clearInterval(this.timelineInterval);
		this.timelineInterval = false;
	}

  _onTimelineTick() {
		if (!this.state.isThumbDrag) {
			const unitLength = this.props.duration / 5;
			const unitWidth = (this.elSeekbar.offsetWidth - 8) / unitLength - 1;
			const { player } = this.props;

			if (this.props.paused) {
				this.setState((state) => {
					return {
						...state,
						seekbarWidth: this.elSeekbar.offsetWidth,
						unitWidth,
						unitLength: Math.floor(unitLength),
					}
				});
				return;
			}

			if (this.state.repeatAB && this.state.repeatEnd <= player.currentTime) {
				player.setCurrentTime(this.state.repeatStart);
			}

			if (this.state.isThumbDrag) {
				return;
			}

			this.timelineInterval && this.setState((state) => {
				return {
					...state,
					seekbarWidth: this.elSeekbar.offsetWidth,
					unitWidth,
					unitLength: Math.floor(unitLength),
				}
			});

		}
  }

  calcLimitThumbPosition(seekLeft) {
    let leftLimit = 0;
    let rightLimit = this.elSeekbar.offsetWidth;
    if (this.state.repeatAB) {
      leftLimit = this.state.repeatLeft - 4;
      rightLimit = this.elSeekbar.offsetWidth - this.state.repeatRight;
    }

    if (seekLeft < leftLimit) {
      return leftLimit;
    } else if (rightLimit < seekLeft) {
      return rightLimit;
    }

    return seekLeft;
  }

  _onRepeatLeftThumbDown(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    if (!isMouseLeftButton(evt) || !this.state.repeatAB || !this.props.operable) {
      return;
    }

    this.repeatThumbDrag = true;

    this.orgRepeatLeft = this.state.repeatLeft;
    this.repeatThumbDragX = evt.clientX;

    document.addEventListener('mousemove', this._onRepeatLeftThumbMove);
    document.addEventListener('mouseup', this._onRepeatLeftThumbUp);
  }

  _onRepeatLeftThumbMove(evt) {
    if (!this.repeatThumbDrag) {
      return;
    }
    const repeatLeftLimit = this.elSeekbar.offsetWidth - this.state.repeatRight - 1;
    let repeatLeft = this.orgRepeatLeft + evt.clientX - this.repeatThumbDragX;
    if (repeatLeft < 10) {
      repeatLeft = 10;
    }

    if (repeatLeftLimit < repeatLeft) {
      repeatLeft = repeatLeftLimit;
    }

    this.setState((state) => {
      return {
        ...state,
        repeatLeft,
      }
    });
  }

  _onRepeatLeftThumbUp(evt) {
    document.removeEventListener('mousemove', this._onRepeatLeftThumbMove);
    document.removeEventListener('mouseup', this._onRepeatLeftThumbUp);
    this.repeatThumbDrag = false;
    this.setState((state) => {
      return {
        ...state,
        repeatStart: this.state.repeatLeft / (this.state.seekbarWidth / this.props.duration),
      }
    });
  }

  _onRepeatRightThumbDown(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    if (!isMouseLeftButton(evt) || !this.state.repeatAB) {
      return;
    }

    this.repeatThumbDrag = true;

    this.orgRepeatRight = this.state.repeatRight;
    this.repeatThumbDragX = evt.clientX;

    document.addEventListener('mousemove', this._onRepeatRightThumbMove);
    document.addEventListener('mouseup', this._onRepeatRightThumbUp);
  }

  _onRepeatRightThumbMove(evt) {
    if (!this.repeatThumbDrag) {
      return;
    }

    const repeatRightLimit = this.elSeekbar.offsetWidth - this.state.repeatLeft - 1;
    let repeatRight = this.orgRepeatRight - (evt.clientX - this.repeatThumbDragX);
    if (repeatRight < 10) {
      repeatRight = 10;
    }

    if (repeatRight > repeatRightLimit) {
      repeatRight = repeatRightLimit;
    }

    this.setState((state) => {
      return {
        ...state,
        repeatRight,
      }
    });
  }

  _onRepeatRightThumbUp(evt) {
    document.removeEventListener('mousemove', this._onRepeatRightThumbMove);
    document.removeEventListener('mouseup', this._onRepeatRightThumbUp);
    this.repeatThumbDrag = false;
    this.setState((state) => {
      return {
        ...state,
        repeatEnd: (this.elSeekbar.offsetWidth - this.state.repeatRight) / (this.state.seekbarWidth / this.props.duration),
      }
    });
  }

  _onSeekbarMouseDown(evt) {
		// TODO 시크바 클릭시 리액션및 지식 위치 스크롤링 해야함.
    if (!isMouseLeftButton(evt) || !this.props.mediaLoaded || !this.props.operable) {
      return;
    }
    
    const { player , currentTime} = this.props;
    const { currentTarget } = evt;
    this.props._setMyRealPlaytime(currentTime,false) //사용자가 영상을 건너 뛰거나 시크바 클릭시 현재 값을 마지막 값으로 저장
    let seekLeft = evt.clientX - currentTarget.getClientRects()[0].left;
    seekLeft = this.calcLimitThumbPosition(seekLeft);

    const seekTime = seekLeft / (this.elSeekbar.offsetWidth / player.duration);
		this.props.seekTimeline(seekTime);
    player.setCurrentTime(seekTime);
    this.props._setMyRealPlaytime(seekTime,true) //영상이 새로운 위치에서 재생시 시작 값으로 저장.
    
  
  }

	_onSeekbarMouseUp(evt) {
		// console.log('seek up')
    this.props.stackMoved();
	}

  _onThumbMouseDown(evt) {
		evt.preventDefault();
		evt.stopPropagation();
    if (!isMouseLeftButton(evt)) {
      return;
    }
    console.log('_onThumbMouseDown')
		const { target } = evt;
		if (target && target.className === 'rail-handle') {
			this.setState((state) => {
				return {
					...state,
					isThumbDrag: true
				}
			});
			this.dragStartX = this.elSeekbar.getClientRects()[0].left;
			document.addEventListener('mousemove', this._onThumbMouseMove);
			document.addEventListener('mouseup', this._onThumbMouseUp);
		}
	}

	_onThumbMouseMove(evt) {
    let seekLeft = evt.clientX - this.dragStartX;
    seekLeft = this.calcLimitThumbPosition(seekLeft);
    const { player } = this.props;
    const seekTime = seekLeft / (this.elSeekbar.offsetWidth / player.duration);
		this.props.seekTimeline(seekTime);
    player.setCurrentTime(seekTime);
	}

	_onThumbMouseUp(evt) {
		// console.log('mouse up')
		document.removeEventListener('mousemove', this._onThumbMouseMove);
		document.removeEventListener('mouseup', this._onThumbMouseUp);
		this.setState((state) => {
			return {
				...state,
				isThumbDrag: false
			}
		});

		if (!this.props.mediaLoaded) {
			return;
		}

    const { player, currentTime } = this.props;
		player.setCurrentTime(parseFloat(currentTime));
		this.props.stackMoved();
	}

  setRepeat(repeatAB) {
    let repeatLeft = 0;
    let repeatRight = 0;
    let repeatStart = 0;
    let repeatEnd = 0;

    if (repeatAB) {
      const { duration, currentTime } = this.props;
      // const { currentTime } = this.state;
      const defaultRepeatWidth = this.elSeekbar.offsetWidth * 0.15;
      const currentPosition = this.state.seekbarWidth / duration * currentTime;
      repeatLeft = currentPosition - (defaultRepeatWidth / 2);
      repeatRight = this.elSeekbar.offsetWidth - currentPosition - (defaultRepeatWidth / 2);

      if (repeatLeft < 0) {
        repeatRight += repeatLeft;
        repeatLeft = 10;
      }

      if (repeatRight < 0) {
        repeatLeft += repeatRight;
        repeatRight = 10;
      }
      repeatStart = repeatLeft / (this.state.seekbarWidth / duration);
      repeatEnd = (this.elSeekbar.offsetWidth - repeatRight) / (this.state.seekbarWidth / duration);
      if (duration <= repeatEnd) {
        repeatEnd = duration;
      }
      console.log(repeatLeft, repeatRight, repeatStart, repeatEnd)
    }


    this.setState((state) => {
      return {
        repeatLeft,
        repeatRight,
        repeatStart,
        repeatEnd,
        repeatAB,
      }
    });
  }

  renderRepeatArea(repeatAB) {
    if (!this.state.repeatAB) {
      return null;
    }
    let [repeatLeft, repeatRight] = [this.state.repeatLeft, this.state.repeatRight];
    const isFullScreen = window.player.outerHTML.includes('1920')
    const weightAverage = 2.1768

    if (isFullScreen) {
      repeatLeft = repeatLeft * weightAverage
      repeatRight = repeatRight * weightAverage
    }
    return (
      <div>
        <div className="repeat-area left" style={{ width: `${repeatLeft}px` }}>
          <button onMouseDown={this._onRepeatLeftThumbDown.bind(this)}></button>
        </div>
        <div className="repeat-area right" style={{ width: `${repeatRight}px` }}>
          <button onMouseDown={this._onRepeatRightThumbDown.bind(this)}></button>
        </div>
      </div>
    )
  }

	renderSections() {
		const { sections } = this.props.source;
		if (!sections) {
			return null;
		}

		const sectionItems = [];

		let remainWidth = this.state.seekbarWidth;
		for (let i = 0; i < sections.length; i++) {
			let sectionWidth = 0;
			if (i + 1 < sections.length) {
				sectionWidth = (sections[i + 1].begin - sections[i].begin) / this.props.duration * this.state.seekbarWidth;
				remainWidth -= sectionWidth - 4;
			} else {
				sectionWidth = remainWidth;
			}

			const section = sections[i];

			sectionItems.push(
				<div key={section.sectionIdx}
					className="section-block"
					style={{ width: `${sectionWidth}px`}}
				>
					<div className="section-title">
						<p title={section.name}>{section.name}</p>
					</div>
					<div className="section"></div>
				</div>
			);
		}

		return (
			<div className="section-wrapper">
				{sectionItems}
			</div>
		);
	}

	renderStacks() {
		const { stacks } = this.props.source;
		if (!stacks) {
			return null;
		}

		const stackItems = [];

		for (let i = 0; i < stacks.length; i++) {
			const stack = stacks[i];
			const left = stack.begin / this.props.duration * this.state.seekbarWidth;
			const width = ((stack.end + 1) - stack.begin) / this.props.duration * this.state.seekbarWidth;
			stackItems.push(
				<button key={stack.stackIdx}
					className="stack"
					style={{ left: `${left}px`, width: `${width}px`}}
				></button>
			);
		}

		return (
			<div className="stack-wrapper">
				{stackItems}
			</div>
		)
	}

	renderUnits() {
		const { unitWidth, unitLength } = this.state;

		const units = [];
		for (let i = 0; i < unitLength; i++) {
			units.push(<span key={i} className="unit" style={{ marginLeft: `${unitWidth}px`}}></span>)
		}

		return (
			<div className="sec-units">
				{units}
			</div>
		);
	}

  render() {
    const { duration, currentTime } = this.props;
    return(
      <div className="timeline-wrapper">
        <div ref={el => this.elSeekbar = el}
          className="time-rail"
          onMouseDown={this._onSeekbarMouseDown.bind(this)}
					onMouseUp={this._onSeekbarMouseUp.bind(this)}>
          <span className="rail-total"></span>
          <span className="rail-current"
            style={{
              width: `${this.state.seekbarWidth / duration * (currentTime * 0.995)}px`
            }}
          />
          <button className="rail-handle"
            ref={el => this.thumb = el}
            style={{
              left: `${this.state.seekbarWidth / duration * (currentTime * 0.995) - 7}px`
            }}
            onMouseDown={this._onThumbMouseDown.bind(this)}
          />
          <span className="rail-timefloat"></span>
        </div>
        <button className="timeline-handle"
          style={{
            left: `${this.state.seekbarWidth / duration * (currentTime * 0.995)}px`
          }}
        />

			<div className="timeline-rail" onMouseDown={this._onSeekbarMouseDown.bind(this)} onMouseUp={this._onSeekbarMouseUp.bind(this)}>
          <div className="timeline">
						{this.renderSections()}
						{this.renderUnits()}
						{this.renderStacks()}
            {this.renderRepeatArea()}
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = state => ({
  mediaLoaded: state.player.mediaLoaded,
	duration: state.player.duration,
  currentTime: state.player.currentTime,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  initTimeline,
	seekTimeline,
	stackMoved,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
)(PlayTimeline);
