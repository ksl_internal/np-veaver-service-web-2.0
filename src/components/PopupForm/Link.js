import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  createOpenGraph,
  } from '../../modules/make';
import {
  closePopup,
  } from '../../modules/popup';
import T from "i18n-react";

class Link extends Component {
  state = {
    isUrl: false,
    url: '',
  }

  _checkUrl = (e) => {
    const value = e.target.value;
    const expUrl = /^((http(s?))\:\/\/)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?$/;
    const isTrue = expUrl.test(value);
    if (isTrue) {
      this.setState((state) => ({ isUrl: expUrl.test(value), url: value }));
    }
  }

  _onCreateLink = (e) => {
    const currentTime = Math.floor(+this.props.player.currentTime);
    const { url, isUrl } = this.state;
    this.props.createOpenGraph(isUrl, url, currentTime);
  }

  render() {
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{T.translate('make.add-link')}</p>
            <button
              type="button"
              className="btn btn-close"
              onClick={this.props.closePopup}></button>
          </header>
          <main>
            <div className="type-url">
              <input
              type="text"
              placeholder={T.translate('common.enter')}
              onBlur={this._checkUrl} />
            </div>
          </main>
          <footer>
            <button
              className="btn btn-popup"
              type="button"
              onClick={this._onCreateLink}
            >{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createOpenGraph,
  closePopup,
  }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Link);
