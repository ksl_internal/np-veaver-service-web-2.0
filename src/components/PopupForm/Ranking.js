import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { openPopup } from "../../modules/popup";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { UpperRankingRow,LowerRankingRow } from './RankingRow';
import T from "i18n-react";
import moment from 'moment';
import Loading from '../../components/LoadingForm';
import * as profileActions from '../../modules/profile';
/**
 * 랭킹 포인트 팝업입니다.
 */
class Ranking extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  
  handlePopup = (e) =>{
    e.preventDefault();
    this.props.openPopup('LEVELSACCORDINGPOINTS',{
        level:this.props.data.level,
        point:this.props.data.point,
    })
  }
  componentDidMount() {
    // this code will be always called when component is mounted in browser DOM ('after render')
    const { data ,veaverStackRankingsResult,veaverRankingsResult} = Object.assign({}, this.props);
      if(veaverStackRankingsResult==null && veaverRankingsResult==null) return null;
      let myUserInfo=null;
      if(data.type==='stack'){
        myUserInfo = this.props.veaverStackRankingsResult.myUserInfo
      }else{
        myUserInfo = this.props.veaverRankingsResult.myUserInfo
      }
    document.getElementById(`${myUserInfo.ranking}`).scrollIntoView();
  }
  render() {
      const { data ,veaverStackRankingsResult,veaverRankingsResult} = Object.assign({}, this.props);
      if(veaverStackRankingsResult==null && veaverRankingsResult==null) {
          return (<Loading isLoadingStoped={true} key="isLoading-key"/>);
      }
      let myUserInfo=null;
      let users=null;
      //현재 보여주는 뷰에 따라 데이터 세팅합니다.
      if(data.type==='stack'){
        myUserInfo = this.props.veaverStackRankingsResult.myUserInfo
        users = this.props.veaverStackRankingsResult.users
      }else{
        myUserInfo = this.props.veaverRankingsResult.myUserInfo
        users = this.props.veaverRankingsResult.users
      }
    let upperRanking=[];
    let lowerRanking =[];
    let index=0
    for(let i in users){
        if(index < 3){ //랭킹 123등 따로 배열에 추가
            if(users[i].ranking === myUserInfo.ranking){
               users[i].isMe = true;
            }
            upperRanking.push(users[i])
        }else{ //그외 다른 배열에 추가
            if(users[i].ranking === myUserInfo.ranking){
                users[i].isMe = true;
            }
            lowerRanking.push(users[i])
        }
        // 해당 서버에서 로딩시 2만 건이 넘는 유저로 인해 속도 느려 500명으로 제한.
        if(index >= 500) break
        index++
    }
    let temp = upperRanking[0];
    upperRanking[0] = upperRanking[1];
    upperRanking[1] = temp;
   
    return (
    <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
            <div className="popup-profile open">
                <form action="#" method="get" acceptCharset="utf-8">
                    <div className="title">
                        {T.translate('profile.level-rank')}
                    </div>
                    <div>

                    </div>
                    {/* <div className="divider"></div> */}
                    <div className="content">
                        {data.type==='' &&
                            <div>
                                <div className="title-profile">평가기간 {moment(this.props.usersIndicesResult.startDate).format('MM.DD')} - {moment(this.props.usersIndicesResult.endDate).format('MM.DD')}</div>
                                <div className="sub-title-profile">업데이트 매일 0시</div>
                            </div>
                        }
                        <div className="flex three-col">
                            <UpperRankingRow data={upperRanking}></UpperRankingRow>
                        </div>
                    </div>
                    {/* <div className="divider"></div> */}
                    <div style={{height:'1px',backgroundColor:'#e6e6e6'}}></div>
                    <div className="wrap-list-profile scrollbarStyle3">
                        <ul className="list-profile">
                            <LowerRankingRow data={lowerRanking}></LowerRankingRow>
                        </ul>
                    </div>
                    <div className="button-submit">
                        <button type="button" className="activities" onClick={this.handlePopup}>{T.translate('profile.activity-history')}</button>
                        <button type="button" className="confirm" onClick={e => this.props.onClose(false)}>{T.translate('common.confirm')}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    );
  }
}

Ranking.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Ranking.defaultProps = {
  data: {},
  onClose: () => {},
}

export default connect(
(state)=>({
    ...state,
    usersIndicesResult: state.profile.get("usersIndicesResult"),
    veaverStackRankingsResult: state.profile.get("veaverStackRankingsResult"),
    veaverRankingsResult: state.profile.get("veaverRankingsResult"),
}),
(dispatch)=>({
    openPopup:bindActionCreators(openPopup, dispatch),
    ProfileActions : bindActionCreators(profileActions,dispatch),
})
)(Ranking)
