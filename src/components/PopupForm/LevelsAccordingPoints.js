import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { openPopup } from "../../modules/popup";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import  * as profileActions  from '../../modules/profile'
import T from "i18n-react";
import Loading from '../../components/LoadingForm';
/**
 * 나의 포인트 확인 팝업입니다.
 */
class LevelsAccordingPoints extends Component {

  constructor(props) {
    super(props);
    this.state = {
        list : [
            {level:1,point:'100'},
            {level:2,point:'300'},
            {level:3,point:'500'},
            {level:4,point:'700'},
            {level:5,point:'1,000'},
            {level:6,point:'1,500'},
            {level:7,point:'1,700'},
            {level:8,point:'2,000'},
            {level:9,point:'2,000'},
        ]
    };
  }
  handlePopup = () =>{
    this.props.openPopup('LEVELSPOINTS',{
        level:this.props.data.level,
        point:this.props.data.point,
    })
  }
  handleRanking=async(type)=>{
    if(type === 'stack'){
        await this.props.ProfileActions.getVeaverStackRankings({});
        this.props.openPopup('RANKING',{
            level:this.props.data.level,
            point:this.props.data.point,
            type:type
        })
    }else{
        await this.props.ProfileActions.getVeaverRankings({});
        this.props.openPopup('RANKING',{
            level:this.props.data.level,
            point:this.props.data.point,
            type:type
        })
    }
  }
  render() {
      const { data } = Object.assign({}, this.props);
    return (
    <div className="popup-layer">
    <Loading isLoadingStoped={true}/>
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
            <div className="popupLevelsAccordingPoints open">
                <div className="add-folder-container">
                    <div className="title">
                        {T.translate('profile.level-rank')}
                    </div>
                    <div className="content">
                        <form action="#" method="get" acceptCharset="utf-8">
                            <div className="top">
                                <div className="title-top clearfix">
                                    <div className="left">{T.translate('profile.level')} & {T.translate('profile.point')} <img src="/images/v1/icon/ic-info.svg" onClick={this.handlePopup} /></div>
                                    {this.props.loginUser.userKey == this.props.userResult.user.userKey &&
                                        <div className="right" style={{cursor:'pointer'}} onClick={this.handleRanking.bind(this,'stack')}>Ranking</div>
                                    }
                                </div>
                                <div className="inner-content clearfix">
                                    <ul className="list-level">
                                        <li className={data.level===1 ? 'active' :'' }><img src="/images/v1/user-ic-level-1-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===2 ? 'active' :'' }><img src="/images/v1/user-ic-level-2-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===3 ? 'active' :'' }><img src="/images/v1/user-ic-level-3-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===4 ? 'active' :'' }><img src="/images/v1/user-ic-level-4-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===5 ? 'active' :'' }><img src="/images/v1/user-ic-level-5-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===6 ? 'active' :'' }><img src="/images/v1/user-ic-level-6-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===7 ? 'active' :'' }><img src="/images/v1/user-ic-level-7-l-3-x@2x.png" alt="image"/></li>
                                        <li className={data.level===8 ? 'active' :'' }><img src="/images/v1/user-ic-level-8-l-3-x@2x.png" alt="image"/></li>
                                    </ul>
                                    <div className="left">
                                        <div className="wrap">
                                            <div className="title-left">{T.translate('profile.level')}</div>
                                            <div className="level">{data.level}</div>
                                        </div>
                                    </div>
                                    <div className="right" style={{textAlign:'right'}}>
                                        <div className="wrap">
                                            <div className="title-right">{T.translate('profile.point')}</div>
                                            <div className="point">{data.point}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="bottom">
                                <div className="title-bottom clearfix">
                                    <div className="left">{T.translate('profile.duration')} {moment(this.props.usersIndicesResult.startDate).format('MM.DD')} ~ {moment(this.props.usersIndicesResult.endDate).format('MM.DD')}</div>
                                    {this.props.loginUser.userKey == this.props.userResult.user.userKey &&
                                        <div className="right" style={{cursor:'pointer'}} onClick={this.handleRanking.bind(this,'')}>Ranking</div>
                                    }
                                </div>
                                <div className="inner-content clearfix">
                                    <div className="left">
                                        <div className="wrap">
                                            <div className="title-left">{T.translate('profile.level')}</div>
                                            <div className="level">{this.props.usersIndicesResult.ranking}</div>
                                        </div>
                                    </div>
                                    <div className="right" style={{textAlign:'right'}}>
                                        <div className="wrap">
                                            <div className="title-right">{T.translate('profile.point')}</div>
                                            <div className="point">{this.props.usersIndicesResult.point}</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="button-submit">
                                <button type="button" className="confirm" onClick={e => this.props.onClose(false)}>{T.translate('common.confirm')}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
  }
}

LevelsAccordingPoints.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

LevelsAccordingPoints.defaultProps = {
  data: {},
  onClose: () => {},
}
export default connect(
(state)=>({
    ...state,
    loginUser : state.user.userProfile,
    userResult: state.profile.get("userResult"),
    usersIndicesResult: state.profile.get("usersIndicesResult"),
}),
(dispatch)=>({
    openPopup:bindActionCreators(openPopup, dispatch),
    ProfileActions : bindActionCreators(profileActions,dispatch),
})
)(LevelsAccordingPoints)
