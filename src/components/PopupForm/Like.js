import React, { Component } from 'react';

import {PropTypes} from "prop-types";
import FavoritesRow from "./FavoritesRow/FaovritesRow";
import {connect} from "react-redux";
import { convDuration } from "../../util/time";

class Like extends Component {


    render() {
      return (
         <div className="popup-layer">
            <div className="popup-dimmed-layer"></div>
            <div className="popup-wrapper">
                <header className="favorites-border">
                    <div style={{'display': 'flex'}}>
                        <span className="img_favorite_02"></span>
                        <div className="favorites-popup-title">좋아요 {this.props.data[0].reactionChildrens.length}</div>
                    </div>
                    <div className="favorites-popup-title-range">{convDuration(this.props.data[0].begin)} ~ {convDuration(this.props.data[0].end)} </div>
                </header>

                <main>
                    <div className="wrap-list-favorites"  >
                        <ul className="list-favorites" >
                            <FavoritesRow data={this.props.data[0].reactionChildrens}></FavoritesRow>
                        </ul>
                    </div>
                </main>
                <button-footer>
                    {<button className="btn button-submit" type="button" onClick={e => this.props.onClose(e)}>닫기</button>}
                </button-footer>
            </div>
         </div>
      )
    }
}


const mapStateToProps = (state) => ({
    ...state,
});

export default connect(
    mapStateToProps,
)(Like);

