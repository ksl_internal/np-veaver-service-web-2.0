import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import T from "i18n-react";

class Basic extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
      const { data } = Object.assign({}, this.props);
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{data.title}</p>
            {/* <button type="button" className="btn btn-close" onClick={e => this.props.onClose(false)}></button> */}
          </header>
          <main>
            <div className="type-normal">
              <p>
                {data.description}
              </p>
            </div>
          </main>
          <footer >
              {data.showcancel === 'showcancel' ? '' : <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(false)}>{T.translate('common.cancel')}</button>}
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(true)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

Basic.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Basic.defaultProps = {
  data: {},
  onClose: () => {},
}

export default Basic;
