import React, { Component } from "react";
/**
 * 유저 선택 팝업 트리 메뉴에서 즐겨찾기 뷰 입니다.
 * @param {*} param0 
 */
const BookMarkUserRow = ({bookMark , onClick}) =>{
  if(bookMark== undefined){
    return null;
  };
  return (
    <li className="no-sub">
        <span className="text">{bookMark.title}({bookMark.userCount})</span>
        {bookMark.userCount > 0 &&
          <span className="add" onClick={()=>{onClick(bookMark)} } ><img src="/images/v1/icon/ic-plus-popup.svg" alt="image"/></span>
        }
    </li>
  );
}

export default BookMarkUserRow;           