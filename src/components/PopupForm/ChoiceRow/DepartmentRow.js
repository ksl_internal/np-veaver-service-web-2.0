import React, { Component ,Fragment } from "react";
import { UserRow,DepartmentRow } from './index';
/**
 * 유저 선택 팝업 트리 메뉴에서 부서 뷰 입니다.
 * 현재 뎁스에 부서 리스트와 유저 리스트를 나타냅니다.
 * @param {*} param0 
 */
const DepartmentRowRoot = ({obj, onClick,selectedDepartmentAll , handleUserSelected , onlyOne=false}) =>{
  if(obj=== undefined || obj.id=== undefined){
    return null;
  };
  const _handleUserSelected = handleUserSelected;
  return (
    <Fragment>
      <li>

      
        <span className={`btn-submenu ${obj.isActive===true? 'active':''}`} onClick={()=>{ onClick(obj)} }></span>
        <span className="text">{obj.id}</span>
        {onlyOne==false?
          <span className="add" onClick={()=>{selectedDepartmentAll({searchDepthNumber:obj.number,department : obj.id  })} }>
            <img src="/images/v1/icon/ic-plus-popup.svg" alt="image"/>
          </span>
          :''
        }
        {obj.isActive===true ?
          (
            <ul className={`dropdown `}>
              {
                obj.children.map( (child,index) =>{
                  return (
                      <DepartmentRow key={`child-${index}-${child.id}`}  obj={child} onClick={onClick} selectedDepartmentAll={selectedDepartmentAll} handleUserSelected={handleUserSelected} onlyOne={onlyOne}>
                      </DepartmentRow>
                  )
                } )
              }
              { 
                obj.userList.map(_user=>{
                  return (
                      <UserRow key={_user.userKey} user={_user} onClick={_handleUserSelected}></UserRow>
                    )
                })
              }
            </ul>
          )
            
          :''
        }
      </li>
    </Fragment>
  )
}
export default DepartmentRowRoot;  