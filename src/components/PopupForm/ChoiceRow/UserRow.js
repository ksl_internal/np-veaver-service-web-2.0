import React, { Component } from "react";
/**
 * 유저 선택 팝업 트리 메뉴에서 유저뷰 입니다.
 * @param {*} param0 
 */
const UserRow = ({user , onClick}) =>{
  if(user== undefined){
    return null;
  };
  return (
    <li className="no-sub" style={{paddingLeft:'22px'}}>
        <span className="image" key={user.userKey}>
            <img src={user.thumbnailSmall} alt="image"/>
        </span>
        <span className="text">{user.nickname}{user.position ? '·' : ''}{user.position ? user.position : ''}</span>
        <span className="add" onClick={()=>{onClick(user)} }><img src="/images/v1/icon/ic-plus-popup.svg" alt="image"/></span>
    </li>
  );
}

// class List extends Component {
  
//   render() {
//     const { items } = this.props;  
//     let itemList =null  
//     if(items != undefined){
//       console.log('items : ',items)
//       itemList = items.list.map(item=>{
//         return (
//           <tr key={item.idx}>
//             <td>{item.idx}</td>
//             <td>{item.contactUsTypeInfo}</td>
//             <td><Link to={`/contact/${item.idx}`}>{item.subject}<div className="reception-number">{item.regNum}</div></Link></td>
//             <td>{moment(item.regDate).format('YYYY.MM.DD HH:mm')}</td>
//           </tr>
//         )
//       })
//     }
//     return (
//         <div className="list-wapper">
//           <table>
//               <colgroup>
//                 <col width="80px"/>
//                 <col width="150px"/>
//                 <col width="*"/>
//                 <col width="200px"/>
//               </colgroup>
//             <thead>
//               <tr>
//                 <th>No.</th>
//                 <th>분류</th>
//                 <th>제목</th>
//                 <th>접수 일시</th>
//               </tr>
//             </thead>
//             <tbody>
//               {itemList}
//             </tbody>
//           </table>
//          </div>
//     );
//   }
// }

export default UserRow;           