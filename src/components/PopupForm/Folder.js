import React, { Component , Fragment } from 'react';
import { PropTypes } from 'prop-types';
import { openPopup } from "../../modules/popup";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../modules/folderAndPin';
import * as notiActions from '../../modules/noti';
import FolderAdd from './FolderAdd';
import T from "i18n-react";
const inputStyle = {
    outline                   : 'none',
    border                    : 'none',
    marginTop                 : '24px',
    marginBottom              : '40px'
  }
  /**
   * 삭제 여부 단순 확인 팝업입니다.
   * @param {*} param0 
   */
const FolderRemove =({folderIdx,onCloseModal,callBack})=> {
    return (
        <div className="popup-wrapper">
            <header>
            <p className="popup-title">{T.translate('common.delete')}</p>
            <button type="button" className="btn btn-close" onClick={onCloseModal}></button>
            </header>
            <main>
            <div className="type-normal">
                <p>
                {T.translate('common.delete-check')}
                </p>
            </div>
            </main>
            <footer>
            <button className="btn btn-popup" type="button" onClick={e => callBack(folderIdx)}>{T.translate('common.confirm')}</button>
            </footer>
        </div>
    );
}
/**
 * 폴더 담기 시 팝업 입니다. 폴더 추가 와 폴더 선택 하여 담기 ,폴더 삭제 기능.
 */
class Folder extends Component {
  state = {
    folderIdx:'',
    open: false,
    removeOpen:false
  };
  componentDidMount() {
    this.getFolders();
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
    this.setState({ removeOpen: false });
  };
  /**
   * 폴더 리스트를 가져옵니다.
   */
  getFolders = async () =>{
    await this.props.Actions.getFolders()
  }
  /**
   * 폴더 담기 이벤트 입니다.
   */
  submit =async()=>{
    try { 
      if(this.state.folderIdx ==='') return;
      let body = {
        id: this.state.folderIdx,
        timelineIdxList:this.props.data.timelineIdxList,
        message: this.state.message,
      };
      await this.props.Actions.postPin(body);
      if(this.props.postPinResult != null){
        await this.props.Actions.getFolders()
        await this.props.Actions.getFoldersId({id:body.id})
        
        global.alert(T.translate('popup.saved'));
        this.props.onClose()
      }
    } catch (error) {

    }
  }
  /**
   * 폴더 삭제 이벤트 입니다.
   */
  removeFolder = async(_folderIdx)=>{
      if(_folderIdx =='') return false;
      await this.props.Actions.putFolderIdLeave({folderIdx:_folderIdx})
      await this.props.Actions.getFolders()
      this.onCloseModal()
  }
  render() {
    const { folders } = this.props;
    if(folders ==null) return null
    const folderList = folders.folders.map(folder=>{
        if(this.props.data.type==='add'){
            return(
                <div key={folder.idx} onClick={ ()=>{ this.setState({folderIdx:folder.idx}) } } className="row-item">
                    <input type="radio" id={folder.idx} name="folderIdx"/>
                    <label className="radio" htmlFor={folder.idx} >
                        <div className="box-folder-item">
                            <div className="image" style={{backgroundColor:'black',textAlign:'center'}}>
                                <img src={folder.recentThumbnail} style={{height:'50px'}} alt=""/>
                            </div>
                            <div className="folder-name">
                                {folder.folderName}
                            </div>
                        </div>
                    </label>
                </div>
            )
        }else{
            return (
                <div key={folder.idx} className="row-item clearfix">
                    <div className="box-folder-item" style={{float:'left'}}>
                        <div className="image" style={{backgroundColor:'black',textAlign:'center'}}>
                            <img src={folder.recentThumbnail} style={{height:'50px'}} alt=""/>
                        </div>
                        <div className="folder-name">
                            {folder.folderName}
                        </div>
                    </div>      
                    <span className="delete" onClick={()=>{this.setState({folderIdx:folder.idx});this.setState({removeOpen:true})}}>{T.translate('common.delete')}</span>
                </div>
            )
        }
    })
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <div className="popup-add-folder open">
            <div className="add-folder-container">
                <div className="title">
                    {T.translate('folder.my-folder')}
                </div>
                <div className="content">
                    <form action="#" method="get" acceptCharset="utf-8">
                        <div className="upload-folder">
                            <a title=""><img onClick={this.onOpenModal} src="/images/v1/icon/ic-plus-folder-popup.svg" style={{cursor:'pointer'}} alt=""/> {T.translate('folder.add-folder')}</a>
                        </div>
                        <div className="field-row">
                        {folderList}
                        </div>
                        {this.props.data.type==='add'&&
                            <div className="message-input">
                                <textarea name="message" onChange={(e)=> { this.setState({[e.target.name]:e.target.value}) }} placeholder={T.translate('common.message-input')}></textarea>
                            </div>
                        }
                        <div className="button-submit">
                            <button style={{width:`${this.props.data.type==='add'?'50%':'100%'}`}} type="button" className="cancle" onClick={e => this.props.onClose(e)} >{T.translate('common.cancel')}</button>
                            <button style={{display:`${this.props.data.type==='add'?'block':'none'}`}} type="button" className={`add ${this.state.folderIdx === '' ? 'dimmed' : ''}`}
                              onClick={e => this.submit(e)} >{T.translate('folder.add')}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
      </div>
      {
          this.state.removeOpen ?
            <Fragment>
                <div className="popup-dimmed-layer"></div>
                <FolderRemove
                    folderIdx={this.state.folderIdx}
                    callBack={this.removeFolder}
                    onCloseModal = {this.onCloseModal}></FolderRemove>
            </Fragment>
          : null
      }
      {this.state.open ?
          <Fragment>
            <div className="popup-dimmed-layer"></div>
                <FolderAdd
                FolderActions={this.props.Actions}
                onCloseModal = {this.onCloseModal}
                />
          </Fragment>
          : null
        }
    </div>
    );
  }
}

Folder.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Folder.defaultProps = {
  data: {},
  onClose: () => {},
}

export default connect(
(state)=>({
  folders : state.folderAndPin.get('folders'),
  postPinResult : state.folderAndPin.get('postPinResult')
}),
(dispatch)=>({
  openPopup:bindActionCreators(openPopup, dispatch),
  Actions:bindActionCreators(actions, dispatch),
  NotiActions:bindActionCreators(notiActions, dispatch)
})
)(Folder)
