import Basic from './Basic';
import Map from './Map';
import Url from './Url';
import Image from './Image';
import Quiz from './Quiz';
import Vote from './Vote';
import Report from './Report';
import LinkCopy from './LinkCopy';
import Link from './Link';
import Answer from './Answer';
import ServicePolicy from './ServicePolicy';
import PrivacyPolicy from './PrivacyPolicy';
import Like from './Like';
import InsertPopup from './InsertPopup';
import ChooseUsers from './ChooseUsers';
import Folder from './Folder';
import LevelsPoints from './LevelsPoints';
import LevelsAccordingPoints from './LevelsAccordingPoints';
import Ranking from './Ranking';
import UserBookMark from './UserBookMark';
import Cert from './Cert';
import Notice from './Notice';


export {
  Basic,
  Map,
  Url,
  Image,
  Quiz,
  Vote,
  Report,
  LinkCopy,
  Link,
  Answer,
  ServicePolicy,
  PrivacyPolicy,
  Like,
  InsertPopup,
  ChooseUsers,
  Folder,
  LevelsPoints,
  LevelsAccordingPoints,
  Ranking,
  UserBookMark,
  Cert,
  Notice
}
