import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import T from "i18n-react";
const inputStyle={
  width:'100%',
  height:'52px',
  borderRadius:'6px',
  border:'soild 1px #979797'
}
class Cert extends Component {

  constructor(props) {
    super(props);
    this.state = {
      certNumber:''
    };
  }

  render() {
    console.log('herehehrere');
      const { data } = Object.assign({}, this.props);
      //console.log(data)
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{data.title}</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose({type:'close',certNumber:''})}></button>
          </header>
          <main>
            <div className="type-normal">
              <p>
                {data.description}
              </p>
              <br></br>
              <br></br>
              <input autoFocus style={inputStyle} type="text" placeholder="인증코드" name="certNumber" onChange={(e)=> { this.setState({certNumber:e.target.value}) }} maxLength="6"></input>
              <br></br>
              {data.failCount>0 &&
                <p style={{color:'red'}}>{data.returnMsg}</p>
              }
            </div>
          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => data.resetCallback()}>다시전송</button>
            <button className="btn btn-popup" type="button" onClick={e => data.submitCallback(this.state.certNumber) }>{T.translate('common.enter')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

Cert.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Cert.defaultProps = {
  data: {},
  onClose: () => {},
}
export default connect(
  (state)=>({
    data: state.auth.postData,
  }),
  (dispatch)=>({
    
  })
  )(Cert)
//export default Cert;
