import React, { Component } from 'react';
import pt from 'prop-types';
import Swiper from 'react-id-swiper';
import cx from 'classnames'

class Image extends Component {
  static propTypes = {
    data: pt.arrayOf(pt.object.isRequired),
    onClose: pt.func.isRequired,
  }

  static defaultProps = {
    data: [{ orderValue: 0, thumbnail: 'Image Not Found' }],
    onClose: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      fileName: '',
      realIndex: 0,
      gallerySwiper: null,
      thumbnailSwiper: null,
      thumbnailClicked: false,
    }
    this.galleryRef = this.galleryRef.bind(this);
    this.thumbRef = this.thumbRef.bind(this);
  }

  componentDidMount() {
    const { data } = this.props
    const firstIndex = 0
    if (data.length && (/jpg|jpeg|png/g).test(data[firstIndex].thumbnail)) {
      this.setState({ fileName: this.getFileName(data[firstIndex]) })
    }
    console.log(data)
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.gallerySwiper && nextState.thumbnailSwiper) {
      const { gallerySwiper, thumbnailSwiper, thumbnailClicked, realIndex } = nextState
      if (thumbnailClicked) { 
        gallerySwiper.slideTo(realIndex)
      } else {
        gallerySwiper.controller.control = thumbnailSwiper
        thumbnailSwiper.controller.control = gallerySwiper
      }
    }
  }

  getFileName = (obj) => obj.thumbnail.split('/').pop()

  galleryRef(ref) {
    if (ref) this.setState({ gallerySwiper: ref.swiper })
  }

  thumbRef(ref) {
    if (ref) this.setState({ thumbnailSwiper: ref.swiper })
  }
  
  get gallerySwiperParams() {
    const that = this
    return {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      on: {
        slideChange: function() {
          const { realIndex } = this
          const { data } = that.props
          const targetObj = data[realIndex]
            that.setState({ realIndex, fileName: that.getFileName(targetObj) })
        }
      }
    }
  }

  get thumbnailSwiperParams() {
    const that = this
    return {
      slidesPerView: 'auto',
      slideToClickedSlide: false,
      on: {
        click: function() {
          const realIndex = this.clickedIndex
          const { data } = that.props
          const targetObj = data[realIndex]
          that.setState({ realIndex, fileName: that.getFileName(targetObj), thumbnailClicked: true })
        }
      }
    } 
  }

  get renderSwiper() {
    const { fileName, realIndex } = this.state
    const { data } = this.props
    const maximumSize = data.length
    const pageNumber = realIndex + 1
    return (<div className="type-img">
    <div className="img-area">
      <Swiper {...this.gallerySwiperParams} ref={this.galleryRef}>
         {data.map(({thumbnail, orderValue}, idx) => 
          <div>
            <img className="thumbnail-img" key={`gallery-images-${idx}`} data-src={thumbnail} src={thumbnail} alt={orderValue} />
          </div>            
          )}
      </Swiper>
    </div>

    <div className="img-list-area">
      <div className="img-info">
        <p className="img-name">{fileName}</p>
        <p className="counter">
          <span>{pageNumber}</span> / {maximumSize}
        </p>
      </div>

      <div className="img-list">
        <ul>
          <Swiper  {...this.thumbnailSwiperParams} ref={this.thumbRef}>
            {data.map(({thumbnail, orderValue}, idx) => 
              <li
              key={`thumbnail-images-${idx}`} 
              className={cx({ on: realIndex === idx })}>
                <img data-src={thumbnail} src={thumbnail} alt={orderValue} />
            </li>)}
          </Swiper>
        </ul>
      </div>
    </div>
  </div>)
  }


  componentDidUpdate(prevProps, prevState) {
    let img=document.getElementsByClassName('thumbnail-img');
    for(let i = 0; i < img.length; i++) {
        let tarImg=img[i];
        let h=tarImg.clientHeight;

        if(h<520) tarImg.style.marginTop=(520-h)/2+'px';
    }
  }

  render() {
    console.log('render');
    const { onClose } = this.props;

    return (
      <div className="popup-layer img-popup">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">이미지 보기</p>
            <button
            title="팝업 닫기"
            className="btn btn-close"
            onClick={onClose}></button>
          </header>
          <main>
           { this.renderSwiper }
          </main>
          <footer>
            <button
              title="팝업 닫기" 
              className="btn btn-popup" 
              onClick={onClose}>확인</button>
          </footer>
        </div>
      </div>
    );
  }
}

export default Image;
