import React, { Component } from 'react';
import T from "i18n-react";
class PopupForm extends Component {

  render() {
    return(
      // 이미지 팝업의 경우만 popup-layer에 img-popup 클래스 추가
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">팝업 타이틀</p>
            <button type="" className="btn btn-close"></button>
          </header>
          <main>
            <div className="type-normal hide">
              <p>
                가장 작은 사이즈의 팝업
              </p>
            </div>
            <div className="type-url">
              <input type="text" placeholder="입력" />
            </div>
            <div className="type-linkcopy hide">
              <input type="text" readOnly value="https://prompt-dev-pc.veaver.com/" />
              <button type="button">Copy</button>
            </div>
            <div className="type-report hide">
              <p>
                발생한 문제에 대해 알려주세요.
              </p>
              <p className="sub-text">
                작성된 정보는 관리자에게 전달됩니다.
              </p>
              <div className="report-list">
                <div className="type-radio">
                  <input type="radio" id="report-01" name="radio" />
                  <label htmlFor="report-01">
                    <span></span>
                    불쾌한 내용입니다.
                  </label>
                </div>
                <div className="type-radio">
                  <input type="radio" id="report-02" name="radio" />
                  <label htmlFor="report-02">
                    <span></span>
                    Veaver에 게시되기에 적절하지 않습니다.
                  </label>
                </div>
                <div className="type-radio">
                  <input type="radio" id="report-03" name="radio" />
                  <label htmlFor="report-03">
                    <span></span>
                    스팸입니다.
                  </label>
                </div>
                <div className="type-radio">
                  <input type="radio" id="report-04" name="radio" />
                  <label htmlFor="report-04">
                    <span></span>
                    회사 보안사항입니다.
                  </label>
                </div>
                <div className="type-radio">
                  <input type="radio" id="report-05" name="radio" />
                  <label htmlFor="report-05">
                    <span></span>
                    기타
                  </label>
                </div>
                <input type="text" className="dimmed" placeholder="기타 사유를 적어주세요." />
              </div>
            </div>
            <div className="type-quiz hide">
              <div className="quiz-area">
                <div className="question">
                  <p>
                    덧지식 퀴즈의 질문 내용입니다.
                    <br />
                    정답은 무엇일까요?
                  </p>
                </div>
                <div className="answer ox-type">
                  <p className="correct">
                    이것이 정답입니다.
                  </p>
                  <p className="incorrect">
                    이것이 오답입니다.
                  </p>
                </div>
              </div>
              <div className="reaction-area">
                <ul>
                  <li className="reaction-list">
                    <div className="user-info">
                      <span className="thumbnail">
                        <img src="/images/test.png" alt="User Profile" />
                      </span>
                      <div className="user-profile">
                        <p>
                          Nickname·직급·소속
                        </p>
                        <p>
                          2017.07.31 15:22
                        </p>
                      </div>
                    </div>
                    <p className="comment">
                      답변 내용
                    </p>
                  </li>
                  <li className="reaction-list">
                    <div className="user-info">
                      <span className="thumbnail">
                        <img src="/images/test.png" alt="User Profile" />
                      </span>
                      <div className="user-profile">
                        <p>
                          Nickname·직급·소속
                        </p>
                        <p>
                          2017.07.31 15:22
                        </p>
                      </div>
                    </div>
                    <p className="comment">
                      답변 내용
                    </p>
                  </li>
                </ul>
              </div>
            </div>
            <div className="type-vote hide">
              <div className="vote-area">
                <div className="question">
                  <p>
                    국민 프로듀서 분들의 선택은? 지금 투표해주세요!
                  </p>
                  <p className="deadline">
                    2017. 07.12 (수) 00:00까지
                  </p>
                </div>
                <div className="option">
                  <ul>
                    <li>
                      <p>
                        첫 번째 항목 - <span>1표</span>
                      </p>
                    </li>
                    <li>
                      <p>
                        두 번째 항목 - <span>0표</span>
                      </p>
                    </li>
                    <li className="majority">
                      <p>
                        세 번째 항목 - <span>3표</span>
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="reaction-area">
                <div className="option-list">
                  <p className="option-title">
                    첫 번째 항목
                  </p>
                  <ul>
                    <li className="reaction-list">
                      <div className="user-info">
                        <span className="thumbnail">
                          <img src="/images/test.png" alt="User Profile" />
                        </span>
                        <div className="user-profile">
                          <p>
                            Nickname·직급·소속
                          </p>
                          <p>
                            2017.07.31 15:22
                          </p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="option-list">
                  <p className="option-title">
                    두 번째 항목
                  </p>
                </div>
                <div className="option-list">
                  <p className="option-title">
                    세 번째 항목
                  </p>
                  <ul>
                    <li className="reaction-list">
                      <div className="user-info">
                        <span className="thumbnail">
                          <img src="/images/test.png" alt="User Profile" />
                        </span>
                        <div className="user-profile">
                          <p>
                            Nickname·직급·소속
                          </p>
                          <p>
                            2017.07.31 15:22
                          </p>
                        </div>
                      </div>
                    </li>
                    <li className="reaction-list">
                      <div className="user-info">
                        <span className="thumbnail">
                          <img src="/images/test.png" alt="User Profile" />
                        </span>
                        <div className="user-profile">
                          <p>
                            Nickname·직급·소속
                          </p>
                          <p>
                            2017.07.31 15:22
                          </p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="type-img">
              <div className="img-area">
                <button className="left"></button>
                <img src="/images/test.png" alt="Test" />
                <button className="right"></button>
              </div>
              <div className="img-list-area">
                <div className="img-info">
                  <p className="img-name">회사에서 해야할 일.jpg</p>
                  <p className="counter">
                    <span>5</span> / 10
                  </p>
                </div>
                <div className="img-list">
                  <button className="left"></button>
                  <ul>
                    <li>
                      <img src="/images/test.png" alt="Test"/>
                    </li>
                    <li>
                      <img src="/images/test.png" alt="Test"/>
                    </li>
                    <li>
                      <img src="/images/test.png" alt="Test"/>
                    </li>
                  </ul>
                  <button className="right"></button>
                </div>
              </div>
            </div>
          </main>
          <footer>
            <button className="btn btn-popup" type="reset">{T.translate('common.cancel')}</button>
            <button className="btn btn-popup" type="submit">보내기</button>
          </footer>
        </div>
      </div>
    );
  }
}

export default PopupForm;
