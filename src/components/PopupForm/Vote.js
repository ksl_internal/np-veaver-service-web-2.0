import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import moment from 'moment';
import {
  getBaseURL,
  validateResponse,
  axios,
} from '../../util/api';
import T from "i18n-react";

class Vote extends Component {
  constructor(props) {
    super(props);
    this.state = {
      voters: {
        limitDate: 0,
        title: '',
        voteResult: [],
      },
    };
    this.fetchVoteUserList();
  }

  fetchVoteUserList() {
    // console.log(this.props);
    axios.get(`/timelines/vote-answers/${this.props.data.cardIdx}`)
    .then(result => {
        // console.log(result.data);
        this.setState(state => {
          return {
            ...state,
            voters: result.data,
          }
        })
    });
  }

  render() {
    const { limitDate, title, voteResult } = Object.assign({}, this.state.voters);
    let maxCount = 0;
    if (voteResult) {
      voteResult.forEach(vote => {
        if (maxCount < vote.count) maxCount = vote.count;
      });
    }
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">투표 결과</p>
            <button type="" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>

          <div className="type-vote">
            <div className="vote-area">
              <div className="question">
                <p>
                  {title}
                </p>
                <p className={`deadline ${limitDate === 0 ? 'hide' : ''}`}>
                  {`${moment(limitDate).format('YYYY. MM. DD (ddd) HH:mm')} 까지`}
                  {/* 2017. 07.12 (수) 00:00까지 */}
                </p>
              </div>
              <div className="option">
                <ul>
                  {
                    voteResult.map(result => {
                      return (
                        <li key={result.answerId} className={`${maxCount === result.count && maxCount > 0 ? 'majority' : ''}`}>
                          <p>
                            {result.example} - <span>{result.count}표</span>
                          </p>
                        </li>
                      )
                    })
                  }
                </ul>
              </div>
            </div>
            <div className={`reaction-area ${this.props.data.data.anonymousFlag === 'Y' ? 'hide' : ''}`}>
              {
                voteResult.map((vote, i) => {
                  return (
                    <div className="option-list" key={vote.answerId}>
                      <p className="option-title">
                        {i + 1} 번째 항목 {` (${vote.voteUsers.length}명): ${vote.example}`}
                      </p>
                      <ul>
                        {
                          vote.voteUsers.map(user => {
                            return (
                              <li className="reaction-list" key={user.userKey}>
                                <div className="user-info">
                                  <span className="thumbnail">
                                    <img src={user.thumbnailSmall} alt="User Profile" />
                                  </span>
                                  <div className="user-profile">
                                    <p>
                                      {`${user.nickname}·${user.position} ${user.department !== null ? '·' + user.department : ''}`}
                                      {/* Nickname·직급·소속 */}
                                    </p>
                                    <p>
                                      {moment(user.regDate).format('YYYY.MM.DD HH:mm')}
                                    </p>
                                  </div>
                                </div>
                              </li>
                            );
                          })
                        }
                      </ul>
                    </div>
                  );
                })
              }
            </div>
          </div>

          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(e)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

Vote.propTypes = {
  data: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
};

Vote.defaultProps = {
  data: {},
  onClose: () => {},
}

export default Vote;
