import React, { Component } from 'react';
import moment from 'moment';
import T from "i18n-react";

class Answer extends Component {

  render() {
    const { data } = Object.assign({}, this.props);
    console.log(data);
    const event = data.event;
    const answers = data.answers;

    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">답변 보기</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>
            <div className="type-quiz">
              <div className="quiz-area">
                <div className="question">
                  <p>
                    {event.contents.title}
                  </p>
                </div>
                <div className={`answer ox-type ${event.contents.type !== 'MULTIPLE' && 'hide'}`}>
                  {event.contents.type === 'MULTIPLE' && event.contents.answers.map((answer, idx) => {
                    return (
                      <p key={idx} className={`correct ${answer.answerFlag === 'Y' && 'incorrect'}`}>
                        {answer.example}
                      </p>
                    )
                  })}
                </div>
                <div className={`answer ${event.contents.type !== 'SHORT' && 'hide'}`}>
                  <p>
                    {event.contents.type === 'SHORT' && event.contents.answer.example}
                  </p>
                </div>
              </div>
              <div className="reaction-area">
                <ul>
                  {answers.map((answer, idx) => {
                    return (
                      <li key={`answers-key-${idx}`} className="reaction-list">
                        <div className="user-info">
                          <span className="thumbnail">
                            <img src={`${answer.thumbnail}`} alt="User Profile" />
                          </span>
                          <div className="user-profile">
                            <p>
                              {answer.nickname}
                            </p>
                            <p>
                              {moment(answer.regDate).format('YYYY. MM. DD HH:mm')}
                            </p>
                          </div>
                        </div>
                        <p className="comment">
                          {event.contents.type === 'MULTIPLE' && event.contents.answers.map((eventAnswer, idx) => {
                            return answer.answerId === eventAnswer.answerId && eventAnswer.example
                          })}
                          {event.contents.type === 'SHORT' && answer.answer}
                        </p>
                      </li>
                    )
                  })}
                </ul>
              </div>
            </div>
          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(e)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

export default Answer;
