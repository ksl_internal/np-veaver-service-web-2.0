import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as folderAcions from '../../modules/folderAndPin';
import * as profileAcions from '../../modules/profile';
import * as bookmarkAcions from '../../modules/bookmark';
import T from 'i18n-react';
import {
  axios,
} from '../../util/api';
const inputStyle = {
  outline                   : 'none',
  border                    : 'none',
}
/**
 * 단순 추가 메세지 입력 팝업입니다.
 * 폴더 담기, 초대 , 공유 등등 추가 입력 메세지 팝업.
 */
class InsertPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message : ''
    };
  }
  handleOnChange = (e)=>{
    this.setState({
      message : e.target.value
    })
  }
  checkMaxLength = ()=>{
    const { data } = this.props;
    let maxLength=20;
    if(data.type==='share'){
      maxLength = 40;
    }else if (data.type==='userFavorites'){
      maxLength = 36;
    }
    return maxLength;
  }
  // submit = async(e) =>{
  //   let body = this.props.data.body;
  //   if(this.props.data.type==='share'){

  //     body.message = this.state.message;
  //     axios.post('/share', body).then(result => {
  //       this.props.onClose();
  //       this.props.ProfileAcions.getUsersMeBySharedTimelines()
  //       global.alert(T.translate('popup.shared'));
  //     });
  //   }else if(this.props.data.type==='createFolder'){
  //     body.folderName = this.state.message;
  //     await this.props.FolderActions.postFolders(body)
  //     const { postFoldersResult } = this.props;
  //     if(postFoldersResult.resultCode == 0){
  //       global.alert(T.translate('popup.created'));
  //     }
  //   }else if (this.props.data.type==='userFavorites'){
  //     if(this.state.message ==''){
  //       global.alert('즐겨찾기 명을 적어주세요.');
  //       return false;
  //     }
  //     const req ={
  //       title : this.state.message,
  //       userList : body.userKeys
  //     }
  //     await this.props.BookmarkActions.postUserBookmark(req)
  //     this.props.onClose();
  //     global.alert('등록 되었습니다.');
  //   }
  // }
  render() {
      const { data , onClose} = Object.assign({}, this.props);
      //console.log(data)
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{data.title}</p>
            {/* <button type="button" className="btn btn-close" onClick={e => this.props.onClose(false)}></button> */}
          </header>
          <main>
            {data.type == 'tempTimelineToUser' ? 
                <div>
                  <div className="to_user_div">
                    <div className="to_user_div_left">
                      <img src="/images/v1/card-ic-empower-2@3x.png"></img>
                      <img src="/images/v1/card-ic-empowering-2@3x.png"></img>
                    </div>
                    <div className="to_user_div_right">
                      <img className="profile_img" src={data.body.originUser.thumbnail}></img>
                      <div className="text_div">
                        <div>{data.body.originUser.nickname}</div>
                        <div>{data.body.originUser.position}</div>
                        <div>{data.body.originUser.department }</div>
                      </div>
                    </div>
                  </div>
                  <input className="to_user_msg" name='message' onChange={this.handleOnChange} placeholder={data.description} maxLength={this.checkMaxLength()}></input>
                </div>
             :
              <div className="type-normal">
                <textarea style={inputStyle} type="text" name='message' placeholder={data.description} onChange={this.handleOnChange} maxLength={this.checkMaxLength()}>
                </textarea>
              </div>
            }
          </main>
          <footer style={{marginTop:(data.type == 'tempTimelineToUser'?'0':'20px')}}>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(false)}>{T.translate('common.cancel')}</button>
            <button className="btn btn-popup" type="button" onClick={e => data.callBack(this.state.message)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

InsertPopup.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

InsertPopup.defaultProps = {
  data: {},
  onClose: () => {},
}
export default connect(
  (state)=>({
    postFoldersResult : state.folderAndPin.get('postFoldersResult'),
    bookmarkResult: state.bookmark.get('bookmarkResult'),
  }),
  (dispatch)=>({
    FolderActions:bindActionCreators(folderAcions, dispatch),
    ProfileAcions:bindActionCreators(profileAcions, dispatch),
    BookmarkActions : bindActionCreators(bookmarkAcions, dispatch),
  })
  )(InsertPopup)
