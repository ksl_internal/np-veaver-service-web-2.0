import React, { Component , Fragment} from 'react';
import T from "i18n-react";

const Row = ({item}) =>{

    return (
        <div className="row-item clearfix">
            <div className="box-folder-item">
                <div className="image round50">
                    <img src="/images/v1/people_01.jpg" alt=""/>
                </div>
                <div className="folder-name">
                    리더 그룹 (n)
                </div>
            </div>
            <span className="delete">{T.translate('common.delete')}</span>
        </div>
    );
}

const UserRow = ({data}) =>{
    const items = data.map(item=>{
        return (<Row key={item.user.userKey} item={item}></Row>)
    })
    return (
        <Fragment>
            {items}
        </Fragment>
    )

}
export default UserRow;
