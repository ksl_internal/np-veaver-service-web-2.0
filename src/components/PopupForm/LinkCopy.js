import React, { Component } from 'react';
import Clipboard from 'clipboard'
import T from 'i18n-react';

class LinkCopy extends Component {

  componentDidMount() {
    const clipboard = new Clipboard(this.refs.button, {
      text: (trigger) => {
        
        return this.refs.snippet.value;
      }
    });
    clipboard.on('success', function(e) {
      global.alert('링크가 복사되었습니다.');
    });
  }

  render() {
    const { data } = Object.assign({}, this.props);
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{T.translate('common.linkcopy')}</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>
            <div className="type-linkcopy">
              <input type="text" value={data.linkText} ref="snippet" readOnly/>
              <button className="copyButton" type="button" ref="button">Copy</button>
            </div>
          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(e)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

export default LinkCopy;
