import React, { Component , Fragment} from 'react';
import { PropTypes } from 'prop-types';
import { UserRow,DepartmentRow,BookMarkUserRow } from './ChoiceRow';
import { openPopup } from "../../modules/popup";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter,Link } from 'react-router-dom';
import * as searchActions from '../../modules/searchM';
import * as bookmarkActions from '../../modules/bookmark';
import * as folderAcions from '../../modules/folderAndPin';
import * as profileAcions from '../../modules/profile';
import * as timelinesActions from '../../modules/timelines'
import { toastr } from "react-redux-toastr";
import  DragScroll  from 'react-dragscroll';
import T from "i18n-react";
import {
  axios,
} from '../../util/api';
// const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const inputStyle = {
  outline                   : 'none',
  border                    : 'none',
  marginTop                 : '24px',
  marginBottom              : '40px'
}
/**
 * 즐겨 찾기 단순 확인 결과 페이지 입니다.
 * @param {*} param0 
 */
const ChooseUsersResult =({items, handleToggleResultModal ,removeSelectedUser , callback ,_props})=> {
  function registerBookmark (){
    //console.log('현재 선택된 유저로 즐겨찾기 추가해버리기')
    const _userKeys = items.map(u =>{
      return u.userKey;
    })
    let body = {
      userKeys:_userKeys
    };
    _props.openPopup('INSERTPOPUP',{
      title:T.translate('share.register-bookmark'),
      description:T.translate('common.bookmark-input'),
      body,
      callBack : async(_message) => { 
        if(_message ==''){
          global.alert('즐겨찾기 명을 적어주세요.');
          //toastr.light('즐겨찾기 명을 적어주세요.',{position:'top-center'})
          return false;
        }
        const req ={
          title : _message,
          userList : body.userKeys
        }
        try {
          const result = await _props.BookmarkActions.postUserBookmark(req)
          if(result.data.bookmarkIdx !==undefined){
            this.props.onClose();
            global.alert('등록 되었습니다.');
            //toastr.light('등록 되었습니다.',{position:'top-center'})
          }else{
            if(result.data.header.resultCode===7004){
              //toastr.light('최대 10개 항목까지 등록 가능합니다.',{position:'top-center'})
              global.alert('최대 10개 항목까지 등록 가능합니다.');
            }else{
              //toastr.light('',result.data.errorData,{position:'top-center'})
              global.alert(result.data.errorData);
            }
          } 
        } catch (error) {
          if(error.response.data.header.resultCode===400){
            if(error.response.data.errorData !== null && error.response.data.errorData.invalidMessages !==null){
              let errorMsg='';
              if(error.response.data.errorData.invalidMessages.length > 0){
                for(let i in error.response.data.errorData.invalidMessages){
                  errorMsg = errorMsg+ error.response.data.errorData.invalidMessages[i].message+'\n';
                }
              }
              //toastr.light('',errorMsg,{position:'top-center'})
              global.alert(errorMsg);
              // const toastrConfirmOptions = {
              //   onOk: () => console.log('OK: clicked'),
              //   onCancel: () => console.log('CANCEL: clicked')
              // };
            }
          }
        }
      },
    })
  }

  let total =0
    const users = items.map(user=>{
      total++;
      return (
          <div key={user.userKey} className="row-item clearfix">
            <div className="box-folder-item">
              <div className="image round50">
                <Link to={`/profile/${user.userKey}`}>
                  <img src={user.thumbnail} alt="User thumbnail" />
                </Link>
              </div>
              <div className="folder-name">
                {user.nickname}
                {user.position ? '·' : ''}
                {user.position ? user.position : ''}
                {user.department ? '·' : ''}
                {user.department ? user.department : ''}
              </div>
            </div>
            <span className="close" onClick={()=>removeSelectedUser(user.userKey)}><img src="/images/v1/icon/ic-delete-popup.svg" alt="image"/></span>
          </div>
      )
    })
  return (
    <div className="popup-wrapper">
      <div className="popupAddUserSelection open">
        <div className="add-folder-container">
          <div className="title">
            {T.translate('profile.select-user')} ({total})
          </div>
          <div className="content">
            <form method="get" acceptCharset="utf-8">
              <div className="addRegisterYourFavorites">
                {
                  // returnType:'userFavorites'
                }
                <a onClick={()=>registerBookmark()} className="registerYourFavorites">{T.translate('share.register-bookmark')}</a>
              </div>
              <div className="field-row scrollbarStyle1 style1">
                {users}
              </div>
              <div className="button-submit">
                <button type="button" onClick={handleToggleResultModal} className="cancle">{T.translate('common.cancel')}</button>
                <button type="button" onClick={(e)=>callback(e)} className="add">{T.translate('folder.add')}</button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  );
}
/**
 * 추가 요청.
 * 즐겨찾기 편집 누를 경우 추가 팝업 단순 안내 뷰.
 */
const CheckBookMarkModal =({callback , handleEditBookmark})=> {
  return (
    <div className="popup-wrapper">
        <header>
          <p className="popup-title">페이지 이동 안내</p>
        </header>
        <main>
          <div className="type-normal">
            사용자 선택 페이지가 닫히고, 사용자 즐겨찾기 관리 화면으로 이동합니다. 계속 하시겠습니까?
          </div>
        </main>
        <footer>
          <button className="btn btn-popup" type="button" onClick={handleEditBookmark}>{T.translate('common.cancel')}</button>
          <button className="btn btn-popup" type="button" onClick={(e)=>callback(e)}>{T.translate('common.confirm')}</button>
        </footer>
    </div>
  );
}

/**
 * 유저를 선택하는 팝업창 입니다.
 */
class ChooseUsers extends Component {
  state = {
    userKeys:[], //실제 선택유저
    priorityIdx:'1', //실제 중요도
    message:[], //실제 메세지
    keyword:'', // 현재 검색 하는 글자
    doSearch : false, 
    userList:[],
    node : [],
    userBookmarkList: [],
    userBookmarkResult: {
      title:T.translate('common.bookmark'),
      children:[],
      isActive:false
    },
    openResult : false,
    goToBookmark:false
  };
  /**
   * 초기값을 세팅합니다.
   */
  componentDidMount() {
    this.fetchDepartmentList(0,null);
    this.fetchBookmarkList();
    try {  
      if(this.props.data.returnType==='returnUsers'){
        this.setState({userKeys:this.props.data.userKeys})
      }
    } catch (error) {
      console.log(error)
    }
  }
  /**
   * 각 부서 최상단 카데고리를 가져와서 세팅합니다.
   */
  fetchDepartmentList = async (searchDepthNumber,department) => {
    const { SearchActions } =this.props;
    let query = {
      depthNumber : searchDepthNumber
    }
    if(department !== null){
      query.department = department
    }
    await SearchActions.getSearchUsersDepth(query)
    const { usersDepthResult } = this.props
    if(usersDepthResult.searchDepthNumber ===0){
      const list = usersDepthResult.departmentList.map( (depart,index)=>{
        return {
          id: depart, //현재 depth 이름
          number : usersDepthResult.searchDepthNumber, // 현재 depth 번호
          isActive: false,
          userList: [],
          children : []
        }
      })
      this.setState(state => {
        return {
          ...state,
          node : list
        }
      })
    }else{
      return usersDepthResult;
    }
  }
  /**
   * 현재 유저의 즐겨찾기 리스트를 가져옵니다.
   */
  fetchBookmarkList=async()=>{
    try {
      const { BookmarkActions } =this.props;
      await BookmarkActions.getUserBookmark();
      this.state.userBookmarkResult.children = this.props.userBookmarkResult.userBookmarkList
      this.setState({
        userBookmarkResult : Object.assign(this.state.userBookmarkResult)
      });
    } catch (error) {

    }
  }
  /**
   * lazy 로딩으로 자식 요소를 가져옵니다.
   * 한번 lazy 로딩 한 후 속도 개선을 위해 다시 요청하지 않습니다.
   */
  loadChildren = async (tree) => {
    if(!tree.isActive && (tree.children.length === 0 && tree.userList.length === 0) ) { //최초 로딩시 해당 자식 요소를 가져옵니다.
      tree.isActive = true; //현재 상태 수정.
      let query = {
        depthNumber : tree.number+1,
        department : tree.id
      }
      //하위 lazyloading
      await this.props.SearchActions.getSearchUsersDepth(query);
      const { usersDepthResult } = this.props
      const list = usersDepthResult.departmentList.map( (depart,index)=>{
        return {
          id: depart,
          isActive : false,
          number : usersDepthResult.searchDepthNumber,
          userList: [],
          children : []
        }
      })
      tree.children = list;
      tree.userList = usersDepthResult.userList;
    }else{ //현재 상태가 열려있고 하위 가 없을 경우 로딩 하지 않는다. 지금 열려있는 상태만 닫힘으로 바꿉니다.
      tree.isActive = tree.isActive===true? false:true;
    }
    this.setState(state => {
      return {
        ...state,
        node:Object.assign(this.state.node)
      }
    })
  }
  /**
   * 모든 사용자 검색 이후 이벤트 들입니다.
   * 사용자 검색 모달 요청시 returnType 에 의해 확인 이벤트 처리 
   */
  submit(e) {
    if(this.state.message == '' && this.state.userKeys.length > 0){
      e.preventDefault();
      const userKeys = this.state.userKeys.map(u =>{
        return u.userKey;
      })
      if(this.props.data.returnType === 'share'){
        let body = {
          timelineIdxs:this.props.data.timelineIdxs,
          message: this.state.message,
          priorityIdx:this.state.priorityIdx,
          userKeys:userKeys
        };
        this.props.openPopup('INSERTPOPUP',{
          type:this.props.data.returnType,
          title:T.translate('share.popup-share'),
          description:T.translate('common.message-input'),
          body,
          callBack : (_message) => {  
            body.message = _message;
            axios.post('/share', body).then(result => {
              this.props.onClose();
              this.props.ProfileAcions.getUsersMeBySharedTimelines({pageNum:1},false)
              global.alert(T.translate('popup.shared'));
            });
          },
        })
      }else if(this.props.data.returnType === 'userFavorites'){
        let body = {
          timelineIdxs:this.props.data.timelineIdxs,
          message: this.state.message,
          priorityIdx:this.state.priorityIdx,
          userKeys:userKeys
        };
        this.props.openPopup('INSERTPOPUP',{
          type:this.props.data.returnType,
          title:T.translate('share.register-bookmark'),
          description:T.translate('common.bookmark-input'),
          body,
          callBack : async(_message) => { 
            if(_message ==''){
              global.alert('즐겨찾기 명을 적어주세요.');
              return false;
            }
            const req ={
              title : _message,
              userList : body.userKeys
            }
            try {
              const result = await this.props.BookmarkActions.postUserBookmark(req)
              if(result.data.bookmarkIdx !==undefined){
                this.props.onClose();
                //toastr.light('등록 되었습니다.',{position:'top-center'})
                global.alert('등록 되었습니다.');
              }else{
                if(result.data.header.resultCode===7004){
                  //toastr.light('최대 10개 항목까지 등록 가능합니다.',{position:'top-center'})
                  global.alert('최대 10개 항목까지 등록 가능합니다.');
                }else{
                  //toastr.light('',result.data.errorData,{position:'top-center'})
                  global.alert(result.data.errorData);
                }
              } 
            } catch (error) {
              if(error.response.data.header.resultCode===400){
                if(error.response.data.errorData !== null && error.response.data.errorData.invalidMessages !==null){
                  let errorMsg='';
                  if(error.response.data.errorData.invalidMessages.length > 0){
                    for(let i in error.response.data.errorData.invalidMessages){
                      errorMsg = errorMsg+ error.response.data.errorData.invalidMessages[i].message+'\n';
                    }
                  }
                  //toastr.light('',errorMsg,{position:'top-center'})
                  global.alert(errorMsg);
                  // const toastrConfirmOptions = {
                  //   onOk: () => console.log('OK: clicked'),
                  //   onCancel: () => console.log('CANCEL: clicked')
                  // };
                }
              }
              
            }
            
          },
        })
      }else if(this.props.data.returnType === 'invite'){
        let body = {
          folderIdx:this.props.data.folderIdx,
          userList:userKeys,
        };
        this.props.openPopup('INSERTPOPUP',{
          body,
          type:this.props.data.returnType,
          title:'초대',
          description:T.translate('common.message-input'),
          callBack : async(_message) => {  
            if(_message ==''){
              global.alert(T.translate('common.message-input'));
              return false;
            }
            body.message = _message;
            this.props.FolderActions.postFolderIdInvite(body)
            //toastr.light('초대 되었습니다.',{position:'top-center'})
            global.alert('초대 되었습니다.');
            this.props.onClose();
          },
        })
      }else if(this.props.data.returnType === 'tempTimelineToUser'){
        let body = {
          idx:this.props.data.timelineIdx,
          targetUserKey:userKeys[0],
          originUser :this.state.userKeys[0]
        };
        this.props.openPopup('INSERTPOPUP',{
          body,
          type:this.props.data.returnType,
          title:T.translate('common.transfer')+" "+T.translate('common.message'),
          description:T.translate('common.message-input'),
          callBack : async(_message) => {  
            if(_message ==''){
              global.alert(T.translate('common.message-input'));
              return false;
            }
            body.message = _message;
            delete body.originUser;
            try {
              const resResult=await this.props.TimelinesActions.postTempTimelinesIdToUser(body)
              if(resResult.data.header.resultCode===0)
                global.alert(T.translate('popup.transfered'));
            } catch (error) {
              
            }
            if(this.props.location.pathname==='/make'){
                await this.props.TimelinesActions.getLockerTimelines({pageNum:1},false)
            }
            this.props.onClose();
          },
        })
      }else if(this.props.data.returnType === 'returnUsers'){
        //console.log('choose Users  ',this.state.userKeys)
        this.props.data.returnCallBack(this.state.userKeys)
        this.props.onClose();
      }
    }
  }

  inputText(e) {
    const etcDescription = e.currentTarget;
    this.setState(state => {
      return {
        ...state,
        etcDescription: etcDescription.value,
      }
    })
  }
  handleKeyPress = (e) => {
    if(e.key === 'Enter') {
      this.searchUsers();
    }
  }
  /**
   * 유저를 검색합니다.
   */
  searchUsers = async() =>{
    if(this.state.keyword ==''){
      this.setState(state => {
        return {
          ...state,
          userList: [],
          doSearch: false
        }
      })
      return ;
    }
    const {SearchActions} = this.props;
    await SearchActions.getSearchUsersKeyword({keyword:this.state.keyword});
    const {usersKeywordResult} = this.props;
    this.setState(state => {
      return {
        ...state,
        userList: usersKeywordResult.userList,
        doSearch : true
      }
    })
  }
  handleEditBookmark=()=>{
    this.setState({goToBookmark:(this.state.goToBookmark?false:true)})
  }
  openBookMark = ()=>{
    this.props.openPopup('USERBOOKMARK')
  }
  handleToggleResultModal=()=>{
    this.setState({openResult:(this.state.openResult?false:true)})
  }
  /**
   * 선택한 유저를 삭제합니다. 현재 뷰에서만 제외.
   */
  removeSelectedUser=(userKey)=>{
    this.setState(state => {
      return {
        ...state,
        userKeys: this.state.userKeys.filter(u => u.userKey !== userKey) ,
      }
    })
  }
  /**
   * 최종 선택된 유저의 뷰입니다.
   */
  viewSearchUsers = () =>{
    if(this.state.userKeys.length > 0){
      return (
          <div className="folder-name-member">
            <a onClick={this.handleToggleResultModal} className="add-folder" style={{position:'absolute'}}>
              <span className="image"><img src="/images/v1/icon/ic-plus-folder-popup.svg" alt="" style={{cursor:'pointer'}} /></span>
            </a>
            <DragScroll className="scrollbarStyle2 marginLeft50" height={'100%'} width={'100%'}>
              {
                this.state.userKeys.map( (user,index) => {
                  return (
                      <a key={user.userKey} className="folder-member">
                        <span className="image">
                            <img src={user.thumbnail} />
                            <span className="remove"onClick={()=>{
                              this.removeSelectedUser(user.userKey)
                            }}><img src="/images/v1/icon/ic-user-delete-popup.svg"/></span>
                        </span>
                        <span className="name">{user.nickname}</span>
                      </a>
                  );
                })
              }
            </DragScroll>
          </div>
      )
    }else{
      return null;
    }
  };

  handleUserSelected=(user)=>{
    if(this.state.userKeys.indexOf(user) == -1){
      this.setState(state => {
        return {
          ...state,
          userKeys: (this.props.data.returnType === 'tempTimelineToUser') ? [user] :  this.arrayUnique(this.state.userKeys.concat(user) ) ,
        }
      })
    }
  }
  /**
   * 중복 제거 
   */
  arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
      for(var j=i+1; j<a.length; ++j) {
        if(a[i].userKey === a[j].userKey)
          a.splice(j--, 1);
      }
    }
    return a;
  }
  ///부서 하위 모든 유저 선택.
  selectedDepartmentAll = async(obj) =>{
    let query = {
      depthNumber : obj.searchDepthNumber+1,
      department : obj.department
    }

    const {SearchActions} = this.props;
    await SearchActions.getSearchUsersAllDepth(query);
    const {usersAllDepthResult} = this.props;
    this.setState(state => {
      return {
        ...state,
        userKeys : this.arrayUnique(this.state.userKeys.concat(usersAllDepthResult.userList) )
      }
    })
  }
  /**
   * 게층 형식의 유저 리스트 볼 수 있고 선택 하는 뷰입니다.
   */
  viewResult= () =>{
    if(this.state.doSearch){
      if(this.state.userList.length ===0){
        return (
            <div className="wrapNoresultImage" style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100%',textAlign:'center'}}>
              <div className="noresultImage">
                <img src="/images/v1/icon/img-noresult.svg" alt="image"/>
                <p>{T.translate('common.no-result')}</p>
              </div>
            </div>
        )
      }else {
        const userList = this.state.userList.map(user=>{
          return (
              <UserRow key={user.userKey} user={user} onClick={this.handleUserSelected}></UserRow>
          )
        })
        return (
            <ul className="menu-add-folder">
              <ul className={`dropdown `}>{userList}</ul>
            </ul>
        )
      }
    }else{
      let departmentList;
      let _onlyOne=false
      if(this.props.data.returnType === 'tempTimelineToUser'){
        _onlyOne = true
      }
      if(this.state.node.length > 0 ){
        departmentList = this.state.node.map( (obj,index) => {
          
          return (
                <DepartmentRow key={index} obj={obj} onClick={this.loadChildren} selectedDepartmentAll={this.selectedDepartmentAll} handleUserSelected={this.handleUserSelected} onlyOne={_onlyOne}>
                </DepartmentRow>
          )
        })
      }
      return (
          <ul className="menu-add-folder">
            <li>
              <span onClick={this.handleToggleBookmark} className={`btn-submenu ${this.state.userBookmarkResult.isActive===true?'active':''}`}></span>
              <span className="text">{this.state.userBookmarkResult.title}</span>
              {this.props.data.returnType !=='userFavorites' &&
               <span className="edit" onClick={()=>this.handleEditBookmark()} >{T.translate('common.edit')}</span>
              }
              <ul className={`dropdown ${this.state.userBookmarkResult.isActive===true?'show':'hide'}`} style={{paddingLeft:'0px'}}>
                {this.state.userBookmarkResult.children.map(obj=>{
                  return (<Fragment key={obj.bookmarkIdx}>
                    <BookMarkUserRow bookMark={obj} onClick={this.handleBookmarkSelectedUser}></BookMarkUserRow>
                  </Fragment>)
                })}
              </ul>
            </li>
            {departmentList}
          </ul>
      )
    }
  }
  /**
   * 확인 버튼 이벤트 입니다.
   */
  callback = (e)=>{
    this.submit(e)
  }
  handleToggleBookmark = ()=>{
    this.state.userBookmarkResult.isActive = (this.state.userBookmarkResult.isActive? false:true)
    this.setState({userBookmarkResult:Object.assign(this.state.userBookmarkResult)})
  }
  /**
   * 즐겨 찾기에 있는 유저들을 가져와 선택 유저로 세팅합니다.
   */
  handleBookmarkSelectedUser = async (bookMark)=>{
    const {BookmarkActions} = this.props
    await BookmarkActions.getUsersByBookmarkId({bookmarkIdx:bookMark.bookmarkIdx});
    const {bookmarkUser} = this.props
    this.setState(state => {
      return {
        ...state,
        userKeys : this.arrayUnique(this.state.userKeys.concat(bookmarkUser.userList) )
      }
    })

  }
  render() {
    const { userKeys , userBookmarkResult , userList ,doSearch ,node, loadChildren , selectedDepartmentAll,handleUserSelected} = this.state;
    return (
        <div className="popup-layer">

          <div className="popup-dimmed-layer"></div>
          <div className="popup-wrapper">
            <div className="add-folder-name-member open">
              <div className="add-folder-container clearfix">
                <div className="title">
                  {T.translate('profile.select-user')}({userKeys.length})
                </div>
                <div className="content">
                  <form onSubmit={ e => { e.preventDefault(); }} acceptCharset="utf-8">
                    {this.viewSearchUsers()}
                    <div className="search-input">
                      <input type="text" className="search" placeholder={T.translate('common.search-input')}
                             name="keyword" value={this.state.keyword} onChange={(e)=>{e.preventDefault();this.setState({[e.target.name]:e.target.value}) }} onKeyPress={this.handleKeyPress}/>
                      <button onClick={this.searchUsers} className="btn-search"></button>
                      <button type="reset" className="reset" style={{visibility:'visible'}}>
                        <img onClick={()=>this.setState({keyword:'',doSearch:false})} src="/images/v1/icon/ic-cancel-popup.svg" alt="image"/>
                      </button>
                    </div>

                    <div className="wrap-menu-add-folder scrollbarStyle1">
                      {this.viewResult()}
                    </div>

                    <div className="button-submit">
                      <button type="button" onClick={e => this.props.onClose(e)} className="cancle">{T.translate('common.cancel')}</button>
                      <button type="button" className={`btn btn-popup add ${this.state.userKeys.length === 0 ? 'dimmed' : ''}`}
                              onClick={e => this.submit(e)} >
                        {T.translate('common.confirm')}</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          {this.state.openResult ?
              <Fragment>
                <div className="popup-dimmed-layer"></div>
                <ChooseUsersResult
                    items={userKeys}
                    handleToggleResultModal = {this.handleToggleResultModal}
                    removeSelectedUser = {this.removeSelectedUser}
                    callback={this.callback}
                    _props={this.props}
                />
              </Fragment>
              : null
          }
          {
            this.state.goToBookmark ? 
            <Fragment>
                <div className="popup-dimmed-layer"></div>
                <CheckBookMarkModal
                 handleEditBookmark={this.handleEditBookmark}
                 callback={this.openBookMark}
                ></CheckBookMarkModal>
              </Fragment>
            :null
          }
        </div>
    );
  }
}

ChooseUsers.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

ChooseUsers.defaultProps = {
  data: {},
  onClose: () => {},
}

export default withRouter(connect(
    (state)=>({
      pending: state.pender.pending,
      usersDepthResult : state.searchM.get('usersDepthResult'),
      usersKeywordResult : state.searchM.get('usersKeywordResult'),
      usersAllDepthResult: state.searchM.get('usersAllDepthResult'),
      userBookmarkResult: state.bookmark.get('userBookmarkResult'),
      bookmarkUser: state.bookmark.get('userResult'),
      postFoldersResult: state.folderAndPin.get('postFoldersResult'),
      bookmarkResult: state.bookmark.get('bookmarkResult'),
      bookmarkError : state.bookmark.get('error'),
    }),
    (dispatch)=>({
      openPopup:bindActionCreators(openPopup, dispatch),
      SearchActions:bindActionCreators(searchActions, dispatch),
      BookmarkActions:bindActionCreators(bookmarkActions, dispatch),
      FolderActions:bindActionCreators(folderAcions, dispatch),
      ProfileAcions:bindActionCreators(profileAcions, dispatch),
      TimelinesActions:bindActionCreators(timelinesActions, dispatch),
    })
)(ChooseUsers))
