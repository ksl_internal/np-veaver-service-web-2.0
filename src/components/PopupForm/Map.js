import { PropTypes } from 'prop-types';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  attachMapCard,
} from '../../modules/make';
import T from "i18n-react";
import {toastr} from "react-redux-toastr";

class Map extends Component {

  constructor(props) {
    super(props);
    this.state = {
      center: {
        lat: 37.5577138197085,
        lng: 126.98362071970848,
      },
      place: undefined,
      predicArray: [],
        selectedIdx : -1
    };
  }

  componentDidMount() {
    // console.log('mount', this.state.center);

    window.navigator.geolocation.getCurrentPosition(position => {
      // console.log(position.coords.latitude, position.coords.longitude);
      const center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      this.setState(state => {
        return {
          ...state,
          center,
        }
      });
      this.map.setCenter(this.state.center);
    });

    this.map = new window.google.maps.Map(document.getElementById('googleMap'), {
      center: this.state.center,
      zoom: 17,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false,
    });

    this.service = new window.google.maps.places.AutocompleteService();
  }

  _placeChanged() {
      if(this.marker)
        this.marker = this.marker.setMap(null);

      this.marker = new window.google.maps.Marker({
        map: this.map,
        position: this.state.place.geometry.location,
      });

      if (this.state.place.geometry.viewport) {
        this.map.fitBounds(this.state.place.geometry.viewport);
        // bounds.union(place.geometry.viewport);
      }
      else {
        this.map.setCenter(this.state.place.geometry.location);
        this.map.setZoom(17);
        // bounds.extend(place.geometry.location);
      }
  }

  _confirm(e) {
    if (this.state.place) {
      this.props.attachMapCard(this.props.currentTime, this.state.place);
      this.props.onClose();
    }
  }

  _searchKeyword() {
    function predictions(thisParam) {
        if (!thisParam.inputText.value.length) {
            thisParam.setState(state => {
                return {
                    ...state,
                    place: undefined,
                    predicArray: [],
                    selectedIdx: -1,
                }
            });
            if (thisParam.marker)
                thisParam.marker = thisParam.marker.setMap(null);
            return
        }
        thisParam.service.getQueryPredictions({input: thisParam.inputText.value}, function (predictions, status) {
            if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
                thisParam.setState(state => {
                    return {
                        ...state,
                        place: undefined,
                        predicArray: [],
                        selectedIdx: -1,
                    }
                });
                if (thisParam.marker)
                    thisParam.marker = thisParam.marker.setMap(null);

                return;
            }

            let predicArray = []
            predictions.forEach(function (prediction) {
                if (prediction.place_id)
                    predicArray.push(prediction)
            });

            thisParam.setState(state => {
                return {
                    ...state,
                    predicArray: predicArray,
                    selectedIdx: -1
                }
            });

            if (thisParam.marker)
                thisParam.marker = thisParam.marker.setMap(null);
        })
    }
    predictions(this)
  }


  _resultClick (idx) {
    function geocode(thisParam) {
        let geocoder = new window.google.maps.Geocoder();

        geocoder.geocode({'placeId': thisParam.state.predicArray[idx].place_id}, function(results, status) {
            if (status === window.google.maps.GeocoderStatus.OK) {
                const customPlace = results[0]
                customPlace.name = thisParam.state.predicArray[idx].structured_formatting.main_text

                thisParam.setState(state => {
                    return {
                        ...state,
                        place : customPlace,
                        selectedIdx : idx,
                    }
                });
                thisParam._placeChanged()
            } else {
            }
        })

    }
      geocode(this)

  }

  render() {
    const wrapperStyle = {'width': '600px'};
    const mapStyle = {'height': '350px'};

    const footerstyle1 = {'opacity' : '1.0', 'background' : '#2B43A8'}
      const footerstyle2 = {'opacity' : '1.0', 'background' : '#BDCCED'}

      return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper" style={wrapperStyle}>
          <header>
            <p className="popup-title">{T.translate('make.search-map')}</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>
            <div className="type-map">
              <div className="map-search">
                <input type="text" id="search" ref={input => this.inputText = input}
                       placeholder={T.translate('make.input-location')}
                       onChange={() => {this._searchKeyword()}}
                />
              </div>
              <div className="map-wrapper">
                <div id="googleMap" style={mapStyle}></div>
              </div>
               <div className="map-address-result1">
                 <ul>
                    {
                     this.state.predicArray.length ? this.state.predicArray.map((prediction, idx) => {
                        return (
                           <li key={idx}>
                              <div className="map-address-result" >
                                  <p className= {idx === this.state.selectedIdx ? "result-select" : "result"} onClick={()=>this._resultClick(idx)}>{prediction.description}
                                   </p>
                               </div>
                            </li>
                           );
                        }) : <div className="map-address-result" > <p className="no-result">{T.translate('make.search-result')}</p> </div>
                    }
                  </ul>
                </div>

            </div>
          </main>
          <footer>
            <button className={this.state.selectedIdx > -1 ? "btn btn-popup" : "btn btn-popup dimmed"}
                    style = {this.state.selectedIdx > -1 ? footerstyle1 : footerstyle2}
                    type="button"
                    onClick={e => this._confirm(e)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

Map.propTypes = {
  onClose: PropTypes.func.isRequired,
};

Map.defaultProps = {
  onClose: () => {},
}

const mapStateToProps = state => ({
  currentTime: state.player.currentTime,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  attachMapCard,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Map);
