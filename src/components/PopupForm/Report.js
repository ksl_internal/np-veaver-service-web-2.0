import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import T from 'i18n-react'
import {
  axios,
} from '../../util/api';
/**
 * 신고하기 팝업입니다.
 */
class Report extends Component {

  constructor(props) {
    super(props);
    this.state = {
      reportTypeList: [],
      reportType: 1,
      etcDescription: '',
    };
    this.fetchReportTypeList();
  }

  fetchReportTypeList() {
    axios.get('/report-type/ko')
    .then(result => {
        this.setState(state => {
          return {
            ...state,
            reportTypeList: result.data,
          }
        })
    });
  }

  selectReportType(e) {
    const reportType = Number(e.currentTarget.value);
    this.setState(state => {
      return {
        ...state,
        reportType,
      }
    });
  }

  report(e) {
    const { data } = Object.assign({}, this.props);
    let body = {
      reportTypeIdx: this.state.reportType,
      targetType: data.targetType,
    };

    switch (data.targetType) {
      case 'T':
        body.timelineIdx = data.timelineIdx;
      break;
      case 'C':
        body.reactionId = data.reactionId;
      break;
      case 'B':
        body.boardIdx = data.boardIdx;
      break;
      default: return;
    }

    if (this.state.reportType === 5) body.etcDescription = this.state.etcDescription;
    axios.post('/report', body)
    .then(result => {
        this.props.onClose();
        global.alert('신고내용이 접수되었습니다.');
    });
  }

  inputText(e) {
    const etcDescription = e.currentTarget;
    this.setState(state => {
      return {
        ...state,
        etcDescription: etcDescription.value,
      }
    })
  }

  render() {
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{T.translate('report.report')}</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>

          <div className="type-report">
            <p>
              {T.translate('report.description1')}
            </p>
            <p className="sub-text">
              {T.translate('report.description2')}
            </p>
            <div className="report-list">
              {
                this.state.reportTypeList.map((reportType, i) => {
                  return (
                    <div className="type-radio" key={reportType.idx}>
                      <input
                        type="radio" id={`report-${i}`} name='radio-report'
                        value={reportType.idx}
                        defaultChecked={this.state.reportType === reportType.idx}
                        onClick={e => this.selectReportType(e)}
                      />
                      <label htmlFor={`report-${i}`} className="report_label">
                        <span></span>
                        {T.translate('report.answer'+ reportType.idx)}
                      </label>
                    </div>
                  );
                })
              }
              <input type="text"
                className={this.state.reportType === 5 ? '' : 'dimmed'}
                placeholder={T.translate('report.other')}
                onChange={e => this.inputText(e)}
                maxLength="100"
              />
            </div>
          </div>

        </main>
        <footer>
          <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(e)}>{T.translate('common.cancel')}</button>
          <button className={`btn btn-popup ${this.state.reportType === 5 && this.state.etcDescription === '' ? 'dimmed' : ''}`}
            type="button" onClick={e => this.report(e)}
          >
            {T.translate('common.send')}
          </button>
        </footer>
      </div>
    </div>
    );
  }
}

Report.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Report.defaultProps = {
  data: {},
  onClose: () => {},
}

export default Report;
