import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { openPopup } from "../../modules/popup";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import T from "i18n-react";
/**
 * 단순 랭킹 포인트 확인 팝업입니다.
 */
class LevelsPoints extends Component {

  constructor(props) {
    super(props);
    this.state = {
        list : [
            {level:1,point:'100'},
            {level:2,point:'300'},
            {level:3,point:'500'},
            {level:4,point:'700'},
            {level:5,point:'1,000'},
            {level:6,point:'1,500'},
            {level:7,point:'1,700'},
            {level:8,point:'2,000'},
            {level:9,point:'2,000'},
        ]
    };
  }
  handlePopup = (e) =>{
    e.preventDefault();
    this.props.openPopup('LEVELSACCORDINGPOINTS',{
        level:this.props.data.level,
        point:this.props.data.point,
    })
  }
  render() {
      const { data } = Object.assign({}, this.props);
      const view = this.state.list.map(o =>{
        return (<div key={o.level} className="row-item clickPopupLevelsAccordingPoints clearfix">
            <div className="box-folder-item">
                <div className="image  round50">
                    <img src={`/images/v1/user-ic-level-${o.level}-l-3-x@2x.png`} alt="image"/>
                </div>
                <div className="folder-name" style={ (data.level==o.level ? {color:'blue'} :{}  ) }>
                    <span>LEVEL {o.level}.</span>
                    <span>{T.translate('profile.level'+o.level)}</span>
                </div>
            </div>
        </div>)
      })
    return (
    <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
            <div className="popupLevelsPoints open">
                <div className="add-folder-container">
                    <div className="title">
                        {T.translate('profile.level-disc')}
                    </div>
                    <div className="content">
                        <form action="#" method="get" acceptCharset="utf-8">
                            <div className="field-row scrollbarStyle1">
                                { view }
                                {/* <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image  round50">
                                            <img src="/images/v1/user-ic-level-1-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 1.</span>
                                            <span>100포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-2-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 2.</span>
                                            <span>300포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-3-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 3.</span>
                                            <span>500포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-4-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 4.</span>
                                            <span>700포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-5-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 5.</span>
                                            <span>1,000포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-6-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 6.</span>
                                            <span>1,500포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-7-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 7.</span>
                                            <span>1,700포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-8-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 8.</span>
                                            <span>2,000포인트 이하</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row-item clickPopupLevelsAccordingPoints clearfix">
                                    <div className="box-folder-item">
                                        <div className="image">
                                            <img src="/images/v1/user-ic-level-9-l-3-x@2x.png" alt="image"/>
                                        </div>
                                        <div className="folder-name">
                                            <span>LEVEL 9.</span>
                                            <span>2,000포인트 초과</span>
                                        </div>
                                    </div>
                                </div> */}

                            </div>
                            <div className="button-submit">
                                <button type="button" className="activities" onClick={this.handlePopup}>{T.translate('profile.activity-history')}</button>
                                <button type="button" className="confirm" onClick={e => this.props.onClose(false)}>{T.translate('common.confirm')}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
  }
}

LevelsPoints.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

LevelsPoints.defaultProps = {
  data: {},
  onClose: () => {},
}

export default connect(
(state)=>({
    ...state,
    usersIndicesResult: state.profile.get("usersIndicesResult"),
    veaverStackRankingsResult: state.profile.get("veaverStackRankingsResult"),
    veaverRankingsResult: state.profile.get("veaverRankingsResult"),
}),
(dispatch)=>({
    openPopup:bindActionCreators(openPopup, dispatch),
})
)(LevelsPoints)
