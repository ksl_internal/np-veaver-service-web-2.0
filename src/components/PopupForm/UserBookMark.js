import React, { Component ,Fragment } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter,Link } from 'react-router-dom';
import * as bookmarkActions from '../../modules/bookmark';
import * as popupActions from '../../modules/popup';
import { openPopup } from "../../modules/popup";
import T from "i18n-react";
import { toastr } from "react-redux-toastr";
/**
 * 추가요청
 * 즐겨찾기 팝업 의 로우 클릭시 -> 단순 확인 페이지 이동.
 * 유저 선택에서 기존 소스 복붙.
 */
class ChooseUsersResult extends Component { 
  state ={
    open:false,
    userKey:null,
    bookMark:null
  }
  toggleModal=(_userKey,_bookMark,_open)=>{
    if(_open){
      this.setState({open:_open,userKey:_userKey,bookMark:_bookMark})
    }else{
      this.setState({open:_open,userKey:null,bookMark:null})
    }
  }
    render (){
      const {items,bookMark, handleToggleResultModal ,removeSelectedUser ,_props} = this.props
      function registerBookmark (){
        const _userKeys = items.map(u =>{
          return u.userKey;
        })
        let body = {
          userKeys:_userKeys
        };
        _props.openPopup('INSERTPOPUP',{
          title:T.translate('share.register-bookmark'),
          description:T.translate('common.bookmark-input'),
          body,
          callBack : async(_message) => { 
            if(_message ==''){
              global.alert('즐겨찾기 명을 적어주세요.');
              //toastr.light('즐겨찾기 명을 적어주세요.',{position:'top-center'})
              return false;
            }
            const req ={
              title : _message,
              userList : body.userKeys
            }
            try {
              const result = await _props.BookmarkActions.postUserBookmark(req)
              if(result.data.bookmarkIdx !==undefined){
                _props.onClose();
                global.alert('등록 되었습니다.');
                //toastr.light('등록 되었습니다.',{position:'top-center'})
              }else{
                if(result.data.header.resultCode===7004){
                  //toastr.light('최대 10개 항목까지 등록 가능합니다.',{position:'top-center'})
                  global.alert('최대 10개 항목까지 등록 가능합니다.');
                }else{
                  //toastr.light('',result.data.errorData,{position:'top-center'})
                  global.alert(result.data.errorData);
                }
              } 
            } catch (error) {
              console.log('error',error)
              if(error.response.data.header.resultCode===400){
                if(error.response.data.errorData !== null && error.response.data.errorData.invalidMessages !==null){
                  let errorMsg='';
                  if(error.response.data.errorData.invalidMessages.length > 0){
                    for(let i in error.response.data.errorData.invalidMessages){
                      errorMsg = errorMsg+ error.response.data.errorData.invalidMessages[i].message+'\n';
                    }
                  }
                  //toastr.light('',errorMsg,{position:'top-center'})
                  global.alert(errorMsg);
                  // const toastrConfirmOptions = {
                  //   onOk: () => console.log('OK: clicked'),
                  //   onCancel: () => console.log('CANCEL: clicked')
                  // };
                }
              }
            }
          },
        })
      }
    
      let total =0
        const users = items.map(user=>{
          total++;
          return (
              <div key={user.userKey} className="row-item clearfix">
                <div className="box-folder-item">
                  <div className="image round50">
                    <Link to={`/profile/${user.userKey}`}>
                      <img src={user.thumbnail} alt="User thumbnail" />
                    </Link>
                  </div>
                  <div className="folder-name">
                    {user.nickname}
                    {user.position ? '·' : ''}
                    {user.position ? user.position : ''}
                    {user.department ? '·' : ''}
                    {user.department ? user.department : ''}
                  </div>
                </div>
                <span className="close" onClick={()=>this.toggleModal(user.userKey,bookMark,true)}><img src="/images/v1/icon/ic-delete-popup.svg" alt="image"/></span>
              </div>
          )
        })
      return (
      <div className="popup-layer">        
        <div className="popup-wrapper">
          <div className="popupAddUserSelection open">
            <div className="add-folder-container">
              <div className="title">
                {T.translate('profile.select-user')} ({total})
              </div>
              <div className="content">
                <form method="get" acceptCharset="utf-8">
                  <div className="addRegisterYourFavorites">
                    {
                      // returnType:'userFavorites'
                    }
                    <a onClick={()=>registerBookmark()} className="registerYourFavorites">{T.translate('share.register-bookmark')}</a>
                  </div>
                  <div className="field-row scrollbarStyle1 style1">
                    {users}
                  </div>
                  <div className="button-submit">
                    <button type="button" style={{width:'100%'}} onClick={()=>handleToggleResultModal(undefined)} className="cancle">{T.translate('common.cancel')}</button>
                  </div>
                </form>
              </div>
            </div>
    
          </div>
          </div>
          {
            this.state.open ?
            <Fragment>
              <div className="popup-dimmed-layer"></div>
              <div className="popup-wrapper">
                <header>
                  <p className="popup-title">{T.translate('common.delete')}</p>
                </header>
                <main>
                  <div className="type-normal">
                    <p>
                    선택한 사용자를 즐겨찾기에서 제외하시겠습니까?
                    </p>
                  </div>
                </main>
                <footer>
                  <button className="btn btn-popup" type="button" onClick={()=>this.toggleModal(this.state.userKey,this.state.bookMark,false)}>{T.translate('common.cancel')}</button>
                  <button className="btn btn-popup" type="button" onClick={()=>{this.setState({open:false}) ;removeSelectedUser(this.state.userKey,this.state.bookMark)}}>{T.translate('common.confirm')}</button>
                </footer>
              </div>
            </Fragment>
            :
            null
          }
        </div>
      );
    }
  }

/**
 * 즐겨찾기 팝업 입니다.
 */
  class UserBookMark extends Component {
  constructor(props) {
    super(props);
    this.state = {
        userKeys:[],
        openResult : false,
    };
  }
  componentDidMount() {
    this.fetchBookmarkList();
  }
  fetchBookmarkList=async()=>{
    try {
      const { BookmarkActions } =this.props;
      await BookmarkActions.getUserBookmark();
    } catch (error) {

    }
  }
  getThumbnail=(thumbnailUrlList)=>{
    if(thumbnailUrlList.length ===0){
        return (
            <div className="image round50">
                <img src="/images/v1/profile_empty.png" alt=""/>
            </div>
        )
    }else if (thumbnailUrlList.length ===1){
        return (
            thumbnailUrlList.map(thumbnail=>{
                return(<div key={thumbnail} className="image round50">
                    <img src={thumbnail} alt=""/>
                </div>)
            })
        )
    }else{
        return (
            <div className="image gallery">
                <ul>
                    <li className="image1 round50"><img src={thumbnailUrlList[0]} alt=""/></li>
                    <li className="image2 round50 borderWhite2"><img src={thumbnailUrlList[1]} alt=""/></li>
                </ul>
            </div>
        )
    }
}
    removeBookmark=(e,item)=>{
        const data = {
            title: T.translate('common.delete'),
            description: T.translate('common.delete-check'),
        };
        try {
            this.props.PopupActions.openPopup('CONFIRM', data, async(value) => {
                if (value) {
                    const { BookmarkActions } =this.props;
                    const req={
                        bookmarkIdx : item.bookmarkIdx
                    }
                    try {
                        const resResult = await BookmarkActions.deleteUserBookmark(req);
                        if(resResult.data.header.resultCode===0){
                            global.alert(T.translate('popup.deleted'));
                            await BookmarkActions.getUserBookmark();
                        }
                    } catch (error) {
                        
                    }
                }
            });
        } catch (error) {
            console.log(error)
        }
    }
    handleToggleResultModal=(_bookMark=undefined)=>{
      if(_bookMark !==undefined){
        this.handleBookmarkSelectedUser(_bookMark)
      }
      this.setState({openResult:(this.state.openResult?false:true),bookMark:_bookMark})
    }
    removeSelectedUser=async(userKey,_bookMark)=>{
     
    //   const data = {
    //     title: T.translate('common.delete'),
    //     description: T.translate('common.delete-check'),
    // };
    // try {
    //   this.setState({removePopup:(this.state.removePopup?false:true)})
    //   return RemoveConfirmModal( {data,  })
    // } catch (error) {
    //     console.log(error)
    // }
    //   try {
    //     const data = {
    //       title: T.translate('common.delete'),
    //       description: '선택한 사용자를 즐겨찾기에서 제외하시겠습니까?',
    //     };
        
    //     this.props.openPopup('CONFIRM', data, async(value) => {
    //         if (value) {
    //         }
    //       });
    //     } catch (error) {
    //       console.log(error)
    //     }
        try {
          const {BookmarkActions} = this.props
          const resResult = await BookmarkActions.deleteBookmarkIdUsers({bookmarkIdx:_bookMark.bookmarkIdx , targetUserKey : userKey});
          if(resResult.data.header.resultCode===0){
            this.setState(state => {
              return {
                ...state,
                userKeys: this.state.userKeys.filter(u => u.userKey !== userKey) ,
              }
            })
            await BookmarkActions.getUserBookmark();
            global.alert(T.translate('popup.deleted'));
          }
        } catch (error) {
          console.log('error',error)
        }
      }
      handleBookmarkSelectedUser = async (bookMark)=>{
        //console.log(bookMark)
    
        const {BookmarkActions} = this.props
        await BookmarkActions.getUsersByBookmarkId({bookmarkIdx:bookMark.bookmarkIdx});
        const {bookmarkUser} = this.props
        console.log('bookmarkUser',bookmarkUser)
        this.setState(state => {
          return {
            ...state,
            userKeys : bookmarkUser.userList
          }
        })
    
      }
  render() {
    const { data , userBookmarkResult} = this.props;
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <div className="popupUserFavorites popup-add-folder style2 open">
              <div className="add-folder-container">
                  <div className="title">
                      {T.translate('profile.user-bookmark')}
                  </div>
                  <div className="content">
                      <form method="get" acceptCharset="utf-8">
                          <div className="addUserSelection">
                              <a onClick={(e)=>{e.preventDefault(); this.props.PopupActions.openPopup('CHOOSEUSERS',{
                                timelineIdxs:[],
                                returnType:'userFavorites'
                                } )} } title=""><img src="/images/v1/icon/ic-plus-folder-popup.svg" style={{cursor:'pointer'}} alt=""/> {T.translate('profile.create-bookmark')}</a>
                          </div>
                          {userBookmarkResult.userBookmarkList.length === 0 ?
                            <div className="wrapNoresultImage">
                              <div className="noresultImage">
                                  <img src="/images/v1/icon/img-noresult.svg" alt="image"/>
                                  <p>{T.translate('profile.no-bookmark')}</p>
                              </div>
                            </div>
                           :
                           <div className="field-row scrollbarStyle1 style1">
                                {userBookmarkResult.userBookmarkList.map(bookmark=>{
                                    return (
                                        <div key={bookmark.bookmarkIdx} className="row-item clearfix" onClick={()=>this.handleToggleResultModal(bookmark)}>
                                            <div className="box-folder-item">
                                                {this.getThumbnail(bookmark.thumbnailUrlList)}
                                                <div className="folder-name">
                                                    {bookmark.title} ({bookmark.userCount})
                                                </div>
                                            </div>
                                            <span onClick={(e)=>this.removeBookmark(e,bookmark)} className="delete">{T.translate('common.delete')}</span>
                                        </div>
                                    )
                                })}
                           </div>
                          }
                          <div className="button-submit">
                              <button type="button" className="confirm" onClick={e => this.props.onClose(e)}>{T.translate('common.confirm')}</button>
                          </div>
                      </form>
                  </div>
              </div>

          </div>
        </div>
        {this.state.openResult ?
              <Fragment>
                <div className="popup-dimmed-layer"></div>
                <ChooseUsersResult
                    items={this.state.userKeys}
                    bookMark={this.state.bookMark}
                    handleToggleResultModal = {this.handleToggleResultModal}
                    removeSelectedUser = {this.removeSelectedUser}
                    _props={this.props}
                />
              </Fragment>
              : null
          }
      </div>
    );
  }
}

UserBookMark.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

UserBookMark.defaultProps = {
  data: {},
  onClose: () => {},
}

export default connect(
  (state)=>({
    userBookmarkResult: state.bookmark.get('userBookmarkResult'),
    bookmarkUser: state.bookmark.get('userResult'),
  }),
  (dispatch)=>({
    BookmarkActions:bindActionCreators(bookmarkActions, dispatch),
    PopupActions :bindActionCreators(popupActions, dispatch),
    openPopup :bindActionCreators(openPopup, dispatch),
  })
  )(UserBookMark)
