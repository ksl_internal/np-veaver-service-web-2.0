import React, { Component ,Fragment} from 'react';
/**
 * 랭킹 팝업에서의 1~3  등 유저 뷰
 * @param {*} param0 
 */
const Row = ({ item}) => {
    let rank ='';
    if(item.ranking===1){
        rank = 'first';
    }else if(item.ranking===2){
        rank = 'second';
    }else{
        rank = 'third';
    }
    return (
        <div className="col-item">
            <div className={`profile-ranking text-center ${rank}`}>
                <p className="number-ranking"><img src={`/images/v1/icon/${rank}.svg`} alt=""/></p>
                <div className="image-profile">
                    <img src={item.user.thumbnailMedium} style={{borderRadius:'90px'}} alt=""/>
                    <span className="level"><img src={`/images/v1/user-ic-level-${item.user.veaverIndiceInfo.level}-l-3-x@2x.png`} alt="image"/></span>
                </div>
                <div className="point">
                    <span>{item.user.veaverIndiceInfo.point}</span> p
                </div>
                <div className="option">
                    {item.user.nickname}<br />
                    {item.user.position}<br />
                    {item.user.duty} 
                </div>
            </div>
        </div>
    )
  }

const UpperRankingRow = ({data}) =>{
    const items = data.map(item=>{
        return (<Row key={item.user.userKey} item={item}></Row>)
    })
    return (
        <Fragment>
            {items}
        </Fragment>
    )
}
export default UpperRankingRow;