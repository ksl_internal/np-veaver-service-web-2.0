import React, { Component , Fragment} from 'react';
/**
 * 랭킹 팝업에서의 4~ 나머지 등수 뷰
 * @param {*} param0 
 */
const Row = ({item}) =>{
    return (
        <li style={{backgroundColor: item.isMe===true?'#ffcb04':''}} id={item.ranking} onUpdate={() => window.scrollTo(0, 0)} >
            <div className="orderNumber">{item.ranking}</div>
            <div className="image">
                <img src={item.user.thumbnailSmall} style={{borderRadius:'50px'}} alt="image"/>
                <span className="level"><img src={`/images/v1/user-ic-level-${item.user.veaverIndiceInfo.level}-l-3-x@2x.png`} alt="image"/></span>
            </div>
            <div className="name-position">
                <div className="number">{item.user.veaverIndiceInfo.point} <span>P</span></div>
                <div className="name">{item.user.nickname}·{item.user.position}·{item.user.duty}</div>
            </div>
        </li>
    );
}

const LowerRankingRow = ({data}) =>{
    const items = data.map(item=>{
        return (<Row key={item.user.userKey} item={item}></Row>)
    })
    return (
        <Fragment>
            {items}
        </Fragment>
    )
    
}
export default LowerRankingRow;