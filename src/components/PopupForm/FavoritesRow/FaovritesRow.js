import React, { Component , Fragment} from 'react';
import { toDateStr } from "../../../util/time";

const Row = ({item}) =>{
    const userimgUrl = item.thumbnailSmall
    const favImage = "/images/img-favorite.png"
    return (
        <li >
            <div style={{'display': 'flex'}}>

                <div className="user-image" style = {{background : 'url(' + userimgUrl + ')'}} >
                    <div className="image-alpha"  > </div>
                    <div className="image-favorite"  >
                        <img src = {favImage} />
                    </div>
                </div>

                <div className = "favorites-textview">
                    <div className="favorites-name-title">{item.nickname}</div>
                    <div className="favorites-date">{toDateStr(item.regDate)}</div>
                </div>


            </div>
        </li>
    );
}

const FavoritesRow = ({data}) =>{
    const items = data.map((item,idx) =>{
        return (<Row key={idx} item={item}></Row>)
    })
    return (
        <Fragment>
            {items}
        </Fragment>
    )

}
export default FavoritesRow;