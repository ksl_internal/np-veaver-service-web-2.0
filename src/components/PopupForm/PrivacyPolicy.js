import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import T from "i18n-react";

class PrivacyPolicy extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper popup-wrapper-policy">
          <header>
            <p className="popup-title popup-title-policy">개인정보처리방침</p>
          </header>
          <main>
            <div className="privacy-policy">
              <article>
              <p className="article-title">
                  제 1 조
              </p>
              <p className="article-subtitle">
                  [목적]
              </p>
              <ol className="article-list">
                  <li>
                  1. 주식회사 비버이엔티(Veaver, Inc)(이하 “회사”)는 회원에게 Veaver 서비스(이하 “서비스”)를 제공함에 있어 회원의 개인정보를 소중하게 생각하고 회원의 개인정보를 효과적으로 관리하고 안전하게 보호하기 위하여 최선을 다하며, 정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 개인정보와 관련된 법령 상의 개인정보보호 규정을 준수하고 있습니다. </li>
                  <li>
                  2. 회사는 개인정보취급방침을 명시하여 이용자가 제공하는 개인정보가 어떠한 용도와 방식으로 이용되고 있으며 개인정보보호를 위해 어떠한 조치가 취해지고 있는지 알려드립니다. 회사의 개인정보취급방침은 법령 및 고시 등의 변경 또는 회사의 약관 및 내부 정책에 따라 변경될 수 있으며 이를 개정하는 경우 회사는 변경사항에 대하여 서비스 화면 내에 게시하거나 이용자에게 고지합니다. </li>
                  <li>
                  3. 회사는 이용자가 본 취급방침을 언제나 쉽게 열람하실 수 있도록 애플리케이션 내 서비스 약관을 통해 제공하고 있습니다. </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 2 조 <span>
                  (수집하는 개인정보 항목 및 수집방법) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 서비스 이용 시 회원 식별 및 최적화된 서비스의 제공을 위해 아래와 같은 정보를 수집하며, 기본적 인권침해의 우려가 있는 개인정보(인종 및 민족, 사상, 출신지 등)는 요구하지 않습니다. </li>
              </ol>
              <p className="article-subtitle">
                  [수집하는 개인 정보]
              </p>
              <ol className="article-list">
                  <li>
                  1. (필수) 회사명, 이름, 이메일 주소, 소속, 비밀번호 </li>
              </ol>
              <p className="article-subtitle">
                  [서비스 이용 정보]
              </p>
              <ol className="article-list">
                  <li>
                  서비스 이용 과정에서 이용자 동의 하에 아래와 같은 정보들이 수집될 수 있습니다. </li>
                  <li>
                  1. 단말기 정보: 제조사, 모델명, OS 버전, 해상도, 이용자의 기기고유식별 번호(UUID , APNs Token),단말기 위치 정보, 서비스가 설치되는 이용자 단말기 내의 사진 및 사진에 포함된 메타정보(GPS 위치정보, 촬영 일시 등) </li>
                  <li>
                  서비스 이용과정이나 사업 처리과정에서 아래와 같은 정보들이 생성되어 수집될 수 있습니다. </li>
                  <li>
                  2. 서비스 이용 기록: 서비스 이용기록, 쿠키, 접속로그, IP 주소, 불량이용 기록, 국가코드, 표준시간대, 서비스 문의 등. </li>
              </ol>
              <p className="article-subtitle">
                  [수집 방법]
              </p>
              <ol className="article-list">
                  <li>
                  회사는 다음과 같은 방법으로 개인정보를 수집합니다. </li>
                  <li>
                  1. 회사와 계약을 맺은 사용자의 소속사가 관리시스템을 통해 일괄 등록 </li>
                  <li>
                  2. 생성정보 수집 툴을 통한 수집 </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 3 조 <span>
                  (개인정보의 수집 및 이용목적) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 수집한 개인정보를 다음의 목적으로 활용합니다. </li>
                  <li>
                  1. 이용자의 비밀번호 재등록 </li>
                  <li>
                  - 이용자의 요청에 따라 수집된 이메일로 인증메일을 발송 </li>
                  <li>
                  2. 서비스 제공에 관한 계약 이행 </li>
                  <li>
                  - 콘텐츠 업로드 및 재생 시 기업의 기밀사항 유출에 대한 사후 처리 </li>
                  <li>
                  3. 회원관리 </li>
                  <li>
                  - 회원제 서비스 제공, 개인 식별, 회원제 서비스 이용에 따른 불량회원의 부정 이용 방지, 가입의사 확인, 분쟁 조정을 위한 기록보존, 불만처리 등 민원처리, 고지사항 전달 </li>
                  <li>
                  4. 신규 서비스 개발, 기능 개선에 활용 </li>
                  <li>
                  - 신규 서비스 개발 및 맞춤 서비스 제공, 기능 개선, 서비스 유효성 확인, 접속 빈도 파악, 회원의 서비스 이용에 대한 통계 </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 4 조 <span>
                  (개인 정보 보존 및 파기) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 원칙적으로 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 파기하며 단, 회사 내부 방침이나 회원의 동의를 획득, 관계 법령의 규정에 의해 보존할 필요가 있을 경우 아래 명시한 기간 동안 보존합니다. </li>
                  <li>
                  1. 로그기록 관련 보존 개인정보 </li>
                  <li>
                  1) 보존항목: 접속기록 </li>
                  <li>
                  2) 보존근거: 통신비밀보호법 </li>
                  <li>
                  3) 보존기간: 3개월 </li>
                  <li>
                  관리시스템에서 회원 ID 삭제 시 수집된 개인정보는 완전히 삭제되며 어떠한 용도로도 이용할 수 없도록 처리됩니다. </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 5 조 <span>
                  (개인정보의 공유 및 제공) </span>
              </p>
              <ol className="article-list">
                  <li>
                  원칙적으로 회사는 이용자의 개인정보를 수집 및 이용목적에 고지한 범위에 한해서만 이용하며, 이용자의 사전 동의 없이 타인 또는 타기업•기관에 공개하지 않습니다. 다만, 아래의 경우에는 예외로 합니다. </li>
                  <li>
                  1. 이용자들이 사전에 공개에 동의한 경우 </li>
                  <li>
                  정보수집 또는 정보제공 이전에 이용자께 비즈니스 파트너가 누구인지, 어떤 정보가 왜 필요한지, 그리고 언제까지 어떻게 보호/관리되는지 알려드리고 동의를 구하는 절차를 거치게 되며, 이용자께서 동의하지 않는 경우에는 추가적인 정보를 수집하거나 비즈니스 파트너와 공유하지 않습니다. </li>
                  <li>
                  2. 법령의 규정에 의거하거나, 수사 목적으로 법령에 정해진 절차와 방법에 따라 수사기관의 요구가 있는 경우 </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 6 조 <span>
                  (개인정보 파기절차 및 방법) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 원칙적으로 개인정보 수집 및 이용목적이 달성되거나, 보유 및 이용기간이 경과된 후에는 해당 정보를 지체 없이 파기합니다. 회원의 개인정보는 법률(정보통신망 이용촉진 및 정보보호 등에 관한 법률)에 의한 경우가 아니고서는 보유되는 이외의 다른 목적으로 이용되지 않습니다. 개인정보 파기 절차 및 방법은 다음과 같습니다. </li>
                  <li>
                  1. 파기 방법 </li>
                  <li>
                  1) 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다. </li>
                  <li>
                  2) 전자적 파일 형태로 저장된 개인정보는 복원이 불가능한 방법으로 영구 삭제합니다. </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 7 조 <span>
                  (회원 및 법정대리인의 권리와 그 행사방법) </span>
              </p>
              <ol className="article-list">
                  <li>
                  1. 회사는 이용자의 연령을 수집하진 않지만, 향후 만 14세 미만의 아동임이 확인 되는 경우, 언제든 법정대리인의 동의를 받을 것 입니다. 이용자 및 법정 대리인은 언제든지 등록되어 있는 자신 혹은 당해 만 14세 미만 아동의 개인정보를 조회하거나 수정할 수 있으며, 가입 해지 또는 개인정보의 처리 정지를 요청할 수도 있습니다. 다만, 그러한 경우 서비스의 일부 또는 전부 이용이 제한될 수 있습니다. </li>
                  <li>
                  2. 이용자의 개인정보 조회•수정을 위해서는 애플리케이션 내 해당 메뉴를 통하여 직접 열람, 정정이 가능하며, 해지할 경우 회원의 개인정보는 모두 삭제됩니다. 단, 개인정보취급방침 제5조 제2항에 해당하는 경우에는 관계법령에 따라 일정기간 보관할 수 있습니다. </li>
                  <li>
                  3. 회원이 개인정보의 오류에 대한 정정을 요청하는 경우에는 정정을 완료하기 전까지 기존 개인정보를 이용할 수 없습니다. 또한 잘못된 개인정보를 제5조에 따라 제3자에게 제공한 경우에는 정정 처리결과를 제3자에게 제공하게 됩니다. </li>
                  <li>
                  4. 회사는 이용자 혹은 법정 대리인의 요청에 의해 해지 또는 삭제된 개인정보는 “개인정보의 보유 및 이용기간”에 명시된 바에 따라 처리하고 그 외의 용도로 열람 또는 이용할 수 없도록 처리하고 있습니다. </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 8 조 <span>
                  (개인정보 보호를 위한 기술 및 관리적 대책) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 회원의 개인정보를 취급함에 있어 개인정보가 분실, 도난, 누출, 변조 또는 훼손되지 않도록 안전성 확보를 위하여 다음과 같은 기술적, 관리적 대책을 강구하고 있습니다. </li>
                  <li>
                  1. 기술적 대책 </li>
                  <li>
                  1) 회사는 해킹이나 컴퓨터 바이러스 등에 의해 회원의 개인정보가 유출되거나 훼손되는 것을 막기 위해 관련 법률규정 및 내부정책에 따라 보안기능을 통해 안전하게 보호하고 있습니다. </li>
                  <li>
                  2) 개인정보의 훼손에 대비해서 자료를 수시로 백업하고 있고, 최신 백신프로그램을 이용하여 이용자들의 개인정보나 자료가 누출되거나 손상되지 않도록 방지하고 있습니다. 회사는 이용자의 비밀번호를 암호화하여 저장 및 관리하고 있으며, 네트워크 상의 개인정보를 안전하게 전송할 수 있도록 보안장치를 채택하고 있습니다. </li>
                  <li>
                  3) 회사는 침입차단시스템을 이용하여 외부로부터의 무단 접근을 통제하고 있으며, 기타 시스템적으로 보안성을 확보하기 위한 가능한 모든 기술적 장치를 갖추려 노력하고 있습니다. </li>
                  <li>
                  2. 관리적 대책 </li>
                  <li>
                  1) 회사의 개인정보관련 취급 직원은 담당자에 한정시키고 있고, 이를 위한 별도의 비밀번호를 부여하여 정기적으로 갱신하고 있으며, 담당자에 대한 수시 교육을 통하여 개인정보취급방침의 준수를 항상 강조하고 있습니다. </li>
                  <li>
                  2) 회사는 개인정보관리책임자를 두어 개인정보취급방침의 이행을 항상 확인하며, 문제가 발견될 경우 즉시 수정하고 바로 잡을 수 있도록 노력하고 있습니다. 단, 이용자 본인의 부주의나 인터넷상의 문제로 개인정보가 유출되어 발생한 문제에 대해 회사는 일체 책임을 지지 않습니다. </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 9 조 <span>
                  (개인정보 관리책임자 및 상담/신고) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 개인정보에 대한 의견 수렴 및 불만처리를 담당하는 개인정보 관리책임자를 지정하고 있습니다. 이용자의 개인정보와 관련한 문의사항이 있으시면 아래의 개인정보관리책임자 또는 개인정보관리담당자에게 연락 주시기 바랍니다. </li>
              </ol>
              </article>
              <article>
              <ol className="article-list">
                  <li>
                  1. 개인정보관리 책임자 및 담당자 </li>
                  <li>
                  1) 정: 박종진 차장, jjpark@veaver.com, tel) 82.70.7123.2232, fax) 02.3463.6701 </li>
                  <li>
                  2) 부: 김석현 부장, shkim@veaver.com, tel) 82.70.7123.2221, fax) 02.3463.6701 </li>
                  <li>
                  2. 개인정보 관리담당자 근무시간 </li>
                  <li>
                  – 평일 09:30 ~ 16:30 </li>
                  <li>
                  – 토요일 및 일요일, 공휴일은 휴무입니다. </li>
                  <li>
                  3. 기타 개인정보침해에 대한 신고나 상담 기관 </li>
                  <li>
                  – 개인정보침해신고센터(http://www.118.or.kr / 118) </li>
                  <li>
                  - 대검찰청 첨단범죄수사과(http://www.spo.go.kr / 02-3480-2000) </li>
                  <li>
                  - 경찰청 사이버테러대응센터(http://www.ctrc.go.kr / 02-392-0330) </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   제 10 조 <span>
                  (개인정보 취급 방침의 개정과 그 공지) </span>
              </p>
              <ol className="article-list">
                  <li>
                  회사는 본 개인정보취급방침을 변경하는 경우 그 변경 사유 및 적용일자를 명시하여 현행 개인정보취급방침과 함께 적용일자 7일전부터 적용일자까지 서비스 내 공지사항을 통해 고지합니다. 다만, 이용자의 권리 또는 의무에 중요한 내용의 변경이 있을 경우에는 최소 30일전에 고지합니다. 본 정책은 이용자의 편의를 고려하여 한국어, 영어로 제공됩니다. 만약 번역된 “약관 및 정책”이 한글 약관과 상이한 경우 한글로 된 “약관 및 정책”을 우선시 할 것 입니다. 본 개인정보취급방침에 관해 질문/불만/상담 내용이 있는 경우 jjpark@veaver.com으로 신고하실 수 있습니다 </li>
              </ol>
              </article>
              <article>
              <p className="article-title">
                   부칙
              </p>
              <ol className="article-list">
                  <li>
                  본 개인정보취급방침은 2018년 05월 04일부터 시행됩니다. </li>
              </ol>
              </article>
          </div>
          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(e)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

PrivacyPolicy.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

PrivacyPolicy.defaultProps = {
  data: {},
  onClose: () => {},
}

export default PrivacyPolicy;
