import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  axios,
} from '../../util/api';
import T from "i18n-react";

class Quiz extends Component {

  constructor(props) {
    super(props);
    this.state = {
      answers: [],
    };
    this.fetchAnswerList();
  }

  componentDidUpdate() {
    // document.getElementById(this.props.userProfile.userKey).scrollIntoView();
  }

  fetchAnswerList() {
    const { isEvent } = Object.assign({}, this.props.data);
    let fetchUri = '';
    if (isEvent) {
      const { source } = Object.assign({}, this.props.data);
      const { event } = source;
      const { contents } = event;
      const { type } = contents;
      const isMultiple = type === 'MULTIPLE';
      fetchUri = `/timelines/event-quiz-answers/${source.event.eventIdx}/${isMultiple ? 'multiple' : 'short'}`;
    } else {
      const { cardIdx, data } = Object.assign({}, this.props.data);
      fetchUri = `timelines/quiz-answers/${cardIdx}/${data.type.toLowerCase()}`;
    }

    axios.get(fetchUri)
    .then(result => {
        // 스크롤 이동 테스트용 답글
        // const answers = [];
        // const answerData = result.data.answers[0];
        // const meAnswer = result.data.answers[1];
        // let i = 0;
        // while(i < 100) {
        //   answers.unshift(answerData);
        //   if (i === 50) {
        //     answers.unshift(meAnswer);
        //   }
        //   i++;
        // }

        this.setState(state => {
          return {
            answers: result.data.quizResult,
          }
        })
    });
  }

  render() {
    const { isEvent } = Object.assign({}, this.props.data);
    let data;

    if (isEvent) {
      const { source } = Object.assign({}, this.props.data);
      const { event } = source;
      console.log(event);
      const { contents } = event;
      const { type, title, answers, answer } = contents;
      data = {
        type,
        title,
        answer,
        answers
      }
      console.log(data)
    } else {
      data = Object.assign({}, this.props.data).data;
    }

    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">정답보기</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>

          <div className="type-quiz">
            <div className="quiz-area">
              <div className="question">
                <p>
                  {data.title}
                </p>
              </div>
              <div className={`answer ${data.type === 'SHORT' ? '' : 'ox-type'}`}>
              {
                data.type === 'SHORT' ?
                <p>{data.answer.example}</p>
                :
                data.answers.map(answer => {
                  return (
                    <p key={answer.answerId} className={`${answer.answerFlag === 'Y' ? 'correct' : 'incorrect'}`}>
                      {answer.example}
                    </p>
                  );
                })
              }
              </div>
            </div>
            <div className="reaction-area">
              <ul>
              {
                data.type === 'SHORT' ?
                this.state.answers.map((answer, idx) => {
                  return (
                    <li className="reaction-list" key={`${answer.answerId}-${idx}`} id={answer.userKey}>
                      <div className="user-info">
                        <span className="thumbnail">
                          <img src={answer.thumbnailSmall} alt="User Profile" />
                        </span>
                        <div className="user-profile">
                          <p>
                            {answer.nickname}
                            {/* Nickname·직급·소속 */}
                          </p>
                          <p>
                            {moment(answer.regDate).format('YYYY.MM.DD HH:mm')}
                          </p>
                        </div>
                      </div>
                      <p className="comment">
                        {
                          data.type === 'SHORT' ?
                          answer.answer : data.answers.map(example => example.answerId === answer.answerId ? example.example : null)
                        }
                      </p>
                    </li>
                  );
                }) :
                this.state.answers.map((result, idx) => {
                  return result.quizUsers.map((answer) => {
                    return (
                      <li className="reaction-list" key={`${answer.answerId}-${idx}`} id={answer.userKey}>
                        <div className="user-info">
                          <span className="thumbnail">
                            <img src={answer.thumbnailSmall} alt="User Profile" />
                          </span>
                          <div className="user-profile">
                            <p>
                              {answer.nickname}
                              {/* Nickname·직급·소속 */}
                            </p>
                            <p>
                              {moment(answer.regDate).format('YYYY.MM.DD HH:mm')}
                            </p>
                          </div>
                        </div>
                        <p className="comment">
                          {
                            result.example
                          }
                        </p>
                      </li>
                    );
                  })
                })
              }
              </ul>
            </div>
          </div>

          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(e)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  userProfile: state.user.userProfile,
});

Quiz.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Quiz.defaultProps = {
  data: {},
  onClose: () => {},
}

export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(Quiz);
