import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import T from 'i18n-react';


const defaultStringValue = '';

export default class Url extends PureComponent {

  static propTypes = {
    data: PropTypes.object,
    popupTitle: PropTypes.string,
    onClose: PropTypes.func.isRequired,
  }

  static get defaultProps() {
    return {
      data: {},
      onClose: () => {},
      popupTitle: '팝업 타이틀',
    }
  }

  render() {
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">{this.props.popupTitle}</p>
            <button type="button" className="btn btn-close" onClick={e => this.props.onClose(e)}></button>
          </header>
          <main>

          <div className="type-url">
            <input ref={input => this.inputURL = input} type="text" placeholder={T.translate('common.enter')} />
          </div>

          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={() => this.props.onClose(this.inputURL.value)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}
