import React, { Component ,Fragment} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types';
import T from "i18n-react";
import Pagination from "react-js-pagination";
import "../../containers/ContactUs/index.css";
import moment from 'moment';

const List =({noticesData , getNextPage})=> {
  return (
    <Fragment>
        <div style={{padding:'0 30px 30px 30px'}}>
              <div className="list-wapper" >
                <table>
                  <colgroup>
                    <col width="80px"/>
                    <col width="150px"/>
                    <col width="*"/>
                    <col width="200px"/>
                  </colgroup>
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>noticeType</th>
                      <th>title</th>
                      <th>viewDate</th>
                    </tr>
                  </thead>
                  <tbody>
                    {noticesData.notices.map(item=>{
                      return(
                        <tr key={item.idx}  onClick={()=>window.open(item.contents, "_blank")}>
                          <td>{item.idx}</td>
                          <td>{item.noticeType}</td>
                          <td>{item.title}</td>
                          <td>{moment(item.viewDate).format('YYYY.MM.DD HH:mm')}</td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </div>
            <div style={{marginBottom:'36px',textAlign:'center'}}>
                <Pagination
                  activePage={ (noticesData.req.pageNum ) }
                  itemsCountPerPage={noticesData.req.pageSize}
                  totalItemsCount={noticesData.noticesCount}
                  pageRangeDisplayed={noticesData.req.pageSize}
                  onChange={getNextPage}
                  innerClass="pagination justify-content-center"
                  activeClassName="active"
                  activeLinkClassName="active"
                  />
            </div>
    </Fragment>
  );
}
const Detail =({noticesData})=> {
  return (
    <Fragment>
        <div className="notice-content" >
              <div className="title">{noticesData.title}</div>
              <div className="date">{moment(noticesData.viewDate).format('YYYY.MM.DD HH:mm')}</div>
        </div>
    </Fragment>
  );
}

class Notice extends Component {
  getNextPage=(num)=>{
    const { noticesData } = this.props;
    noticesData.req.pageNum = num
    noticesData.req.getNotices(noticesData.req)
  }
  render() {
    const { noticesData} = this.props;
    if(noticesData.notices.length === 0) return null;
    return (
      <div className="popup-layer">
        <div className="popup-dimmed-layer"></div>
        <div className="popup-wrapper">
          <header>
            <p className="popup-title">공지사항</p>
          </header>
          <main>
            <Detail noticesData={noticesData.notices[0]}></Detail>
            {/* <List noticesData={noticesData} getNextPage={this.getNextPage}></List> */}
          </main>
          <footer>
            <button className="btn btn-popup" type="button" onClick={e => this.props.onClose(true)}>{T.translate('common.confirm')}</button>
          </footer>
        </div>
      </div>
    );
  }
}

Notice.propTypes = {
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

Notice.defaultProps = {
  data: {},
  onClose: () => {},
}
export default connect(
  (state)=>({
    noticesData:state.noti.get('noticesData')
  }),
  (dispatch)=>({

  })
)(Notice)
