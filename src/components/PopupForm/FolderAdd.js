import React, { Component , Fragment } from 'react';
import T from "i18n-react";
import { toastr } from "react-redux-toastr";
const inputStyle = {
    outline                   : 'none',
    border                    : 'none',
    marginTop                 : '24px',
    marginBottom              : '40px'
  }
  /**
   * 폴더 추가 팝업입니다.
   */
  class FolderAdd extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        message : ''
      };
    }
    handleOnChange = (e)=>{
      this.setState({
        message : e.target.value
      })
    }
    submit = async(e) =>{
      let body = {
        folderName : this.state.message
      };
      await this.props.FolderActions.postFolders(body)
      global.alert(T.translate('popup.created'));
      //toastr.light('',T.translate('popup.created'))
      this.props.FolderActions.getFolders()
      this.props.onCloseModal();
    }
    render() {
      return (
        <div className="popup-wrapper">
            <header>
              <p className="popup-title">{T.translate('folder.add-folder')}</p>
              {/* <button type="button" className="btn btn-close" onClick={this.props.onCloseModal}></button> */}
            </header>
            <main>
              <div className="type-normal">                
                <input autoFocus style={inputStyle} type="text" name='message' placeholder={T.translate('folder.name-input')} onChange={this.handleOnChange} maxLength="10">
                </input>
              </div>
            </main>
            <footer>
              <button className="btn btn-popup" type="button" onClick={this.props.onCloseModal}>{T.translate('common.cancel')}</button>
              <button className="btn btn-popup" type="button" onClick={this.submit}>{T.translate('common.confirm')}</button>
            </footer>
        </div>
      );
    }
  }
  export default FolderAdd;
