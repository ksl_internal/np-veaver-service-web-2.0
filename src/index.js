import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux'; //https://github.com/ReactTraining/react-router/commit/85ad566a8811a7676ded5fd453daa11f61bab8d2
import App from './containers/App';
import MobileFrame from './components/MobileFrame';
import store, { history } from './store';
import device from 'current-device'
import 'react-app-polyfill/ie11';



const isIos = ['ios', 'iphone', 'ipad', 'ipod'].some(kindOfOs => kindOfOs === device.os);
const isAos = ['android'].some(kindOfOs => kindOfOs === device.os);
let downloadPath = '';

if (isIos) {
  downloadPath = 'https://itunes.apple.com/kr/app/prompt/id1223861186';
} else if (isAos) {
  downloadPath = 'market://details?id=com.prompt.android.veaver.enterprise';
}

const mobileRender = () => 
ReactDOM.render(
 <MobileFrame marketPath={downloadPath} />,
 document.getElementById('root'), () => {
   document.body.style.minWidth = '100%';
   document.body.style.background = 'rgb(50, 90, 237)';
 }
);



function snsLinkUrlCheck() {
    return window.location.pathname.includes('/snslink/') ? true : false
}

const desktopRender = () => 
ReactDOM.render(
  <Provider store={store}>
  <ConnectedRouter history={history}>
      <App />
  </ConnectedRouter>
</Provider>,
document.getElementById('root')
);
const executeRendering = /*device.mobile()*/snsLinkUrlCheck() ? desktopRender : ( device.mobile() ? mobileRender : desktopRender);

executeRendering();