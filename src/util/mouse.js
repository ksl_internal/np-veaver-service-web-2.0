export const isMouseLeftButton = (event) => {
  const e = event || window.event;

  if ('object' === typeof e) {
    return e.button === 0;
  }

  return false;
}
