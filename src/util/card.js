

export const textCard = () => ({ text: "" });
export const imageCardStack = (paths) => ({ images: [...paths] });
export const imageCardEvent = (paths) => ({ thumbnail: paths }); 
export const fileCard = (paths) => ([{
    path: paths[0].path,
    size: paths[0].size,
    name: paths[0].name,
}]);
export const quizCardShort = () => ({ title: '', type: 'SHORT', answer: getQuizAnswer('SHORT') })
export const quizCardMultiple = () => ({ title: '', type: 'MULTIPLE', answer: getQuizAnswer('MULTIPLE') })
export const voteCard = () => ({
    multipleFlag: "N",
    anonymousFlag: "N",
    addAnswerFlag: "N",
    answers: [
    {
        example: "",
        orderValue: 0
    },
    {
        example: "",
        orderValue: 1
    },
]})

export const mapCard = () => ({
    latitude: 0,
    longitude: 0,
    name: "",
    description: "",
})

export function getQuizAnswer(type) {
    switch (type) {
        case 'SHORT':
        return {
            example: "",
            answerFlag: "Y",
        };
        case 'MULTIPLE':
        return [
        {
            example: "",
            orderValue: 0,
            answerFlag: "Y"
        },
        {
            example: "",
            orderValue: 1,
            answerFlag: "N"
        }
        ];
        default: Error('unexpected');
    }
};

export function textToHtml(text,word){
    try {
        let regEx = new RegExp(word, "gi");
        return "<span>"+text.replace(regEx,"<span style='background-color:#aabdff'>"+word+"</span>") + "</span>";
    } catch (error) {
        return text
    }
}
export function mergeName(nickname,position,department){
    let result=nickname;
    result = result+(position!==undefined ? '·'+position : '')
    result = result+(department!==undefined ? '·'+department : '')
    return result;
} 
