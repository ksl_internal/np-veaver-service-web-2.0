import moment from 'moment';
import T from 'i18n-react'

export const timeSince = intervalMillis => {
  const intervalSeconds = intervalMillis / 1000;
  let interval = Math.floor(intervalSeconds / 3600);
  if (interval >= 1) {
    return interval + T.translate('common.hourago');
  }
  interval = Math.floor(intervalSeconds / 60);
  if (interval >= 1) {
    return interval + T.translate('common.minuteago');
  }
  return T.translate('common.now');
}
// 초 -> 시간:분:초 변환 (00:00:01)
export const convDuration = second => {
  const duration = moment.duration(second, 's');
  const hours = Math.floor(duration.asHours());
  let result = moment.utc(duration.asMilliseconds()).format("mm:ss");
  if (hours) result = `${hours}:${result}`;
  return result;
}

export const toDateStr = (time) => moment(time).format('YYYY.MM.DD HH:mm');


/**
 *
 * @param {*} originTimelines 서버에서 전달 받은 타임라인 리스트 입니다.
 *
 * 시분초를 00시간 전 문자열로 표시 및 추가 데이터 변환 합니다.
 */
export const convertTimelines =(originTimelines) =>{
  const currentMillis = new Date().getTime();
  const timelines = originTimelines.map(timeline => {
    const regMillis = timeline.regDate;
    const intervalMillis = currentMillis - regMillis;
    if(timeline.toSharedInfo !== undefined && timeline.toSharedInfo !==null){
      timeline.toSharedInfo.seenDate = timeline.toSharedInfo.seenDate !==null ? (intervalMillis < (24*60*60*1000)) ? timeSince(intervalMillis) : toDateStr(regMillis) : null;

      let toShareDateIntervalMillis = currentMillis - timeline.toSharedInfo.sharedDate;
      timeline.toSharedInfo.sharedDate = timeline.toSharedInfo.sharedDate !==null ? (toShareDateIntervalMillis < (24*60*60*1000)) ? timeSince(toShareDateIntervalMillis) : toDateStr(timeline.toSharedInfo.sharedDate) : null;
    }
    if(timeline.bySharedInfo !== undefined && timeline.bySharedInfo !==null){
      let byShareDateIntervalMillis = currentMillis - timeline.bySharedInfo.sharedDate;
      timeline.bySharedInfo.sharedDate = timeline.bySharedInfo.sharedDate !==null ? (byShareDateIntervalMillis < (24*60*60*1000)) ? timeSince(byShareDateIntervalMillis) : toDateStr(timeline.bySharedInfo.sharedDate) : null;
    }
    timeline.playTime = convDuration(timeline.playTime)
    timeline.regDate = (intervalMillis < (24*60*60*1000)) ? timeSince(intervalMillis) : toDateStr(regMillis);
    timeline.uptDate = ( (currentMillis- timeline.uptDate) < (24*60*60*1000)) ? timeSince(currentMillis- timeline.uptDate) : toDateStr(timeline.uptDate);
    timeline.tags = timeline.tag.split(',').map((tag) => {return '#'+tag});
    return timeline;
  });
  //console.log(timelines)
  return timelines;
}
