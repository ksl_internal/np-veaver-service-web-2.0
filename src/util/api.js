import { default as storage } from 'store';
import ax from 'axios';
export const API_RESPONSE_STATUS = {
  API_RESULT_SUCCESS: 0,
  FETCH_ERROR_BAD_REQUEST: 400,
  FETCH_ERROR_UNAUTHORIZED: 401,
  FETCH_ERROR_FORBIDDEN: 403,
  FETCH_ERROR_NOTFOUND: 404,
  FETCH_ERROR_INTERNAL_SERVER_ERROR: 500,
  API_RESULT_INVALID_TOKEN: 2006,
}

/* */
export const isLocker = () => window.location.pathname.includes('locker');

export const isCp = () => window.location.pathname.includes('cp');

export const getCpGroupNumber = () =>{
  let path=window.location.pathname.split('/');
  if(path.length > 2){
    return path[2];
  }else{
    return null;
  }
} 

export const getBaseDomain = () => process.env.NODE_ENV === 'production' ? window.location.origin : `https://${process.env.REACT_APP_SERVICE_ID}.ent.veaver.com`;

export const getBaseURL = () => process.env.NODE_ENV === 'production' ? '/api/v1' : `${getBaseDomain()}/api/v1`;

export const getAuthToken = () => storage.get('authorization');

export const APIError = (message, code) => {
  return {
    message,
    code
  }
}

export const validateResponse = response =>
  response.then(json => {
    const { resultMessage, resultCode } = json.header;
    switch (resultCode) {
      case API_RESPONSE_STATUS.API_RESULT_SUCCESS:
        return json;
      case API_RESPONSE_STATUS.FETCH_ERROR_BAD_REQUEST:
      case API_RESPONSE_STATUS.FETCH_ERROR_UNAUTHORIZED:
      case API_RESPONSE_STATUS.FETCH_ERROR_FORBIDDEN:
      case API_RESPONSE_STATUS.FETCH_ERROR_NOTFOUND:
      case API_RESPONSE_STATUS.FETCH_ERROR_INTERNAL_SERVER_ERROR:
      default:
        throw new APIError(resultMessage, resultCode);
    }
  });

  export const axios = process.env.NODE_ENV === 'test' ?
  ax.create({ 
    baseURL: `https://prompt-dev.veaver.com/api/v1`,
    withCredentials: true,
    headers: { Pragma: 'no-cache'}
  }) 
  :ax.create({
    baseURL: getBaseURL(),
    withCredentials: true, // send cookie
    headers: { Pragma: 'no-cache'},
    transformResponse: [function (data) {
      const parseData = JSON.parse(data);
      const essentialData = parseData.data || parseData; // items youtube
      return essentialData;
    }],
    validateStatus: function (status) {
      return status >= 200 && status < 300; 
    },
  });


export const commaSpliter = str => String(Math.floor(str)).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');