export const FETCH_ERROR = 'FETCH_ERROR';
export const FETCH_ERROR_BAD_REQUEST = 'FETCH_ERROR_BAD_REQUEST';
export const FETCH_ERROR_UNAUTHORIZED = 'FETCH_ERROR_UNAUTHORIZED';
export const FETCH_ERROR_FORBIDDEN = 'FETCH_ERROR_FORBIDDEN';
export const FETCH_ERROR_NOTFOUND = 'FETCH_ERROR_NOTFOUND';
export const FETCH_ERROR_INTERNAL_SERVER_ERROR = 'FETCH_ERROR_INTERNAL_SERVER_ERROR';

export const FIELD_MISSING = 'Please fill out the entire form.';
export const WRONG_PASSWORD = 'Wrong password.';
export const USER_NOT_FOUND = 'This username does not exist.';
export const USERNAME_TAKEN = 'Sorry, but this username is already taken';
export const GENERAL_ERROR = 'Something went wrong, please try again';
