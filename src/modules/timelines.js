import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import { timeSince, convDuration, toDateStr , convertTimelines} from '../util/time';
import {
    isLocker,
    axios,
  } from '../util/api';

/*

api 문서 타임라인 내용입니다

*/


const CP_TIMELINE_GROUPS = 'timelines/CP_TIMELINE_GROUPS';
const CP_TIMELINE_GROUPS_ID_TIMELINES = 'timelines/CP_TIMELINE_GROUPS_ID_TIMELINES';
const CP_TIMELINES = 'timelines/CP_TIMELINES'
const GET_LOCKER_TIMELINES = 'timelines/GET_LOCKER_TIMELINES'
const POST_TEMP_TIMELINES_ID_TO_USER = 'timelines/POST_TEMP_TIMELINES_ID_TO_USER'

export const getCpTimelineGroups = createAction(CP_TIMELINE_GROUPS, (req)=>{ 
  return axios.get(`/cp-timeline-groups`)
});
export const getCpTimelineGroupsIdTimelines = createAction(CP_TIMELINE_GROUPS_ID_TIMELINES, (req)=>{ 
    return axios.get(`/cp-timeline-groups/${req.id}/timelines`,{ params: req})
});
export const getCpTimelines= createAction(CP_TIMELINES, (req)=>{ 
  return axios.get(`/cp-timelines`,{ params: req})
});
export const getLockerTimelines= createAction(GET_LOCKER_TIMELINES, (req,attachMode=false)=>{ 
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  return axios.get(`/temp-timelines`,{ params: req})
},(req,attachMode=false)=>( { req ,attachMode}  ));

export const postTempTimelinesIdToUser= createAction(POST_TEMP_TIMELINES_ID_TO_USER, (req)=>{ 
  return axios.post(`/temp-timelines/${req.idx}/to-user`, req )
},(req)=>( { req }  ));


const initialState = Map({
  timelineResult:null,
    cpTimelineGroupsResult:null,
    cpTimelineGroupsIdTimelinesResult: null
})

const reducer = handleActions({
  // 다른 일반 액션들을 관리..
}, initialState);

export default applyPenders(reducer, [
  {
    type: CP_TIMELINE_GROUPS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('cpTimelineGroupsResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: CP_TIMELINE_GROUPS_ID_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('cpTimelineGroupsIdTimelinesResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: CP_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const timelines = convertTimelines(action.payload.data.timelines)
      action.payload.data.timelines = timelines
      return state.set('timelineResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_LOCKER_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const timelines = convertTimelines(action.payload.data.timelines)
      const result = state.get('tempItems');
      action.payload.data.timelines = action.meta.attachMode ? result.timelines.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.timelinesCount / 10) === action.meta.req.pageNum
      //action.payload.data.timelines = timelines
      return state.set('tempItems', action.payload.data).set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_TEMP_TIMELINES_ID_TO_USER,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('timelineToUserResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
]);
