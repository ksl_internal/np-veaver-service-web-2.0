const SET_ERROR_MESSAGE = 'error/SET_ERROR_MESSAGE';
const RESET_ERROR_MESSAGE = 'error/RESET_ERROR_MESSAGE';

export const setErrorMessage = (message, code) => ({
    type: SET_ERROR_MESSAGE,
    message,
    code,
});

export const resetErrorMessage = () => ({
    type: RESET_ERROR_MESSAGE
});

const initialState = {
  errorMessage: null,
  code: 200,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: null,
      }
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: action.message,
        code: action.code,
      }
    default:
      return state
  }
}
