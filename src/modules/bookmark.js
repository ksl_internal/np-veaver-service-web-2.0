import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import {
    isLocker,
    axios,
  } from '../util/api';

/*

api 문서 즐겨찾기 내용입니다

*/


const GET_USER_BOOKMARK = 'bookmark/GET_USER_BOOKMARK';
const POST_USER_BOOKMARK = 'bookmark/POST_USER_BOOKMARK';
const DELETE_USER_BOOKMARK = 'bookmark/DELETE_USER_BOOKMARK';
const GET_USER_BOOKMARK_ID = 'bookmark/GET_USER_BOOKMARK_ID';
const DELETE_BOOKMARK_ID_USER = 'bookmark/DELETE_USER_BOOKMARK_ID_USER';

export const getUserBookmark = createAction(GET_USER_BOOKMARK, ()=>{ 
  return axios.get(`/user-bookmark`)
});
export const postUserBookmark = createAction(POST_USER_BOOKMARK, (req)=>{ 
  return axios.post(`/user-bookmark`,req)
});
export const deleteUserBookmark = createAction(DELETE_USER_BOOKMARK, (req)=>{ 
  return axios.delete(`/user-bookmark/${req.bookmarkIdx}`,{data:req})
});
export const getUsersByBookmarkId = createAction(GET_USER_BOOKMARK_ID, (req)=>{ 
  return axios.get(`/user-bookmark/${req.bookmarkIdx}`)
});
export const deleteBookmarkIdUsers = createAction(DELETE_BOOKMARK_ID_USER, (req)=>{ 
  return axios.delete(`/user-bookmark/${req.bookmarkIdx}/${req.targetUserKey}`)
});




const initialState = Map({
  userBookmarkResult:{
    totalListCount: 0,
    userBookmarkList:[]
  },
  bookmarkResult:null,
  error:null
})

const reducer = handleActions({
  // 다른 일반 액션들을 관리..
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_USER_BOOKMARK,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => {    
      return state.set('userBookmarkResult', Object.assign(action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_USER_BOOKMARK,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => {
      return state.set('bookmarkResult', Object.assign(action.payload.data))
    },
    onFailure: (state, action) => {
        return state
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_USER_BOOKMARK_ID,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => {
      return state.set('userResult', Object.assign(action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
]);
