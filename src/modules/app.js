import { FETCH_USER_PROFILE_SUCCESS } from './user';
import {
  axios
} from '../util/api';

export const APP_INITIALIZE_START = 'app/APP_INITIALIZE_START';
export const APP_INITIALIZE_COMPLETE = 'app/APP_INITIALIZE_COMPLETE';
export const APP_INITIALIZE_FAILED = 'app/APP_INITIALIZE_FAILED';
export const FETCH_SERVER_STATUS = 'app/FETCH_SERVER_STATUS';
export const FETCH_SERVER_STATUS_SUCCESS = 'app/FETCH_SERVER_STATUS_SUCCESS';
export const PAGE_LOADING_START = 'app/PAGE_LOADING_START';
export const PAGE_LOADING_END = 'app/PAGE_LOADING_END';
export const MAKE_BACK_PREVENT_STATE_INITIAL = 'app/MAKE_BACK_PREVENT_STATE_INITIAL';
export const MAKE_BACK_PREVENT_STATE_MAKECONTAINER = 'app/MAKE_BACK_PREVENT_STATE_MAKECONTAINER';
export const MAKE_BACK_PREVENT_STATE_MAKESTEP2CONTAINER = 'app/MAKE_BACK_PREVENT_STATE_MAKESTEP2CONTAINER';
export const MAKE_BACK_PREVENT_STATE_ANOTHERCONTAINER = 'app/MAKE_BACK_PREVENT_STATE_ANOTHERCONTAINER';
export const MAKE_BACK_PREVENT_STATE_MODIFYCONTAINER = 'app/MAKE_BACK_PREVENT_STATE_MODIFYCONTAINER';

export const MAKE_BACK_PREVENT_STATE_SHOWPOPUP = 'app/MAKE_BACK_PREVENT_STATE_SHOWPOPUP';
export const MAKE_BACK_PREVENT_STATE_MAKEPUSH = 'app/MAKE_BACK_PREVENT_STATE_MAKEPUSH';

const initialState = {
  loading: true,
  appInitialized: 'start',
  appInitializeFailedPath: '',
  serverStatus: 'AWS',
  serviceName: process.env.REACT_APP_SERVICE_ID,
  userProfile: null,
  makebackPreventState: 'initial',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case APP_INITIALIZE_START:
      return {
        ...state,
      }
    case APP_INITIALIZE_COMPLETE: {
      return {
        ...state,
        serverStatus: action.serverStatus,
        appInitialized: 'complete',
        loading: false,
      }
    }
    case APP_INITIALIZE_FAILED: {
      const { redirectPath } = action;
      return {
        ...state,
        appInitialized: 'fail',
        appInitializeFailedPath: redirectPath,
        loading: false,
      }
    }
    case PAGE_LOADING_START:
      return {
        ...state,
        loading: true,
      }
    case PAGE_LOADING_END:
      return {
        ...state,
        loading: false,
      }
    case MAKE_BACK_PREVENT_STATE_INITIAL:
        return {
        ...state,
        makebackPreventState: 'initial',
      }
    case MAKE_BACK_PREVENT_STATE_MAKECONTAINER:
      return {
         ...state,
          makebackPreventState: 'make',
      }
      case MAKE_BACK_PREVENT_STATE_MAKESTEP2CONTAINER:
          return {
              ...state,
              makebackPreventState: 'makestep2',
          }

      case MAKE_BACK_PREVENT_STATE_ANOTHERCONTAINER:
          return {
              ...state,
              makebackPreventState: 'another',
      }

      case MAKE_BACK_PREVENT_STATE_SHOWPOPUP:
          return {
             ...state,
              makebackPreventState: 'showpopup',
       }
      case MAKE_BACK_PREVENT_STATE_MAKEPUSH:
          return {
              ...state,
              makebackPreventState: 'makepush',
      }
      case MAKE_BACK_PREVENT_STATE_MODIFYCONTAINER:
          return {
              ...state,
              makebackPreventState: 'modify',
          }

      default:
      return state
  }
}

export const appInitialize = () => {
  console.log('appInitialize')
  return async dispatch => {
    dispatch({ type: APP_INITIALIZE_START });
    const portalApiDomain = 'https://portal.ent.veaver.com/api/v1';
    const serviceId = process.env.NODE_ENV === 'production' ? window.location.hostname.split('.ent')[0] : process.env.REACT_APP_SERVICE_ID;
    const serverStatusQuery = `id=${serviceId}`;

    try {
      const me = await axios.get('/users/me');
      const auth = await axios.post('/users/valid-auth-token');
      const serverStatus = await fetch(`${portalApiDomain}/server-status?${serverStatusQuery}`).then(response => response.json());
      sessionStorage.setItem('Authorization', auth.data.userAuthToken);
      dispatch({ type: APP_INITIALIZE_COMPLETE, serverStatus: serverStatus.data.serverType });
      dispatch({ type: FETCH_USER_PROFILE_SUCCESS, userProfile: me.data.user  });
    } catch(e) {
      dispatch({ type: APP_INITIALIZE_FAILED, redirectPath: '/login' });
    }
  }
}

export const makebackPreventStateInit = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_INITIAL })
    }
}

export const makebackPreventStateMakeContainer = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_MAKECONTAINER });
    }
}

export const makebackPreventStateMakestep2Container = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_MAKESTEP2CONTAINER });
    }
}

export const makebackPreventStateAnotherContainer = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_ANOTHERCONTAINER });
    }
}

export const makebackPreventStateModifyContainer = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_MODIFYCONTAINER });
    }
}

export const makebackPreventStateShowpopup = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_SHOWPOPUP });
    }
}

export const makebackPreventStateMakePush = () => {
    return (dispatch) =>{
        dispatch({ type: MAKE_BACK_PREVENT_STATE_MAKEPUSH });
    }
}


export const getSessionStatus = () => (dispatch) => {
  dispatch({ type: APP_INITIALIZE_COMPLETE });
  return sessionStorage.getItem('key');
}

export const setPageLoading = (isLoading) => (dispatch) => {
  if (isLoading) {
    dispatch({ type: PAGE_LOADING_START });
  } else {
    dispatch({ type: PAGE_LOADING_END });
  }
}