/* 현재 사용되고 있지 않음 */
import {
  APIError,
  axios,
} from '../util/api';

export const FETCH_USER_PROFILE = 'user/FETCH_USER_PROFILE';
export const FETCH_USER_PROFILE_SUCCESS = 'user/FETCH_USER_PROFILE_SUCCESS';
export const FETCH_USER_PROFILE_FAILED = 'user/FETCH_USER_PROFILE_FAILED';

export const me = () => {
  return (dispatch, getState) => {
    dispatch({ type: FETCH_USER_PROFILE });
    axios.get('users/me')
    .then(result => {
      // releaseAuthToken();
      dispatch({ type: FETCH_USER_PROFILE_SUCCESS, result });
    })
    .catch((err) => {
      if (err instanceof APIError) {
        // 예외 코드 적용
        dispatch({ type: FETCH_USER_PROFILE_FAILED });
      } else {
        // dispatch(setErrorMessage(err.message));
      }
    });
  }
}

const initialState = {
  userProfile: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_PROFILE:
      return {
        ...state,
        loading: true,
      };
    case FETCH_USER_PROFILE_SUCCESS:
      const { userProfile } = action;
      return {
        ...state,
        loading: false,
        userProfile,
      }
    case FETCH_USER_PROFILE_FAILED:
      return {
        ...state,
        loading: false,
      }
    default:
      return state
  }
}
