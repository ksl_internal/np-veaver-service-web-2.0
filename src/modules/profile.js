import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import { timeSince, convDuration, toDateStr , convertTimelines} from '../util/time';
import {
    isLocker,
    axios,
  } from '../util/api';
import _adminAxios from 'axios';

/*

api 문서 회원 내용입니다

*/

const GET_USERS_TIMELINES = 'profile/GET_USERS_TIMELINES';
const GET_USERS = 'profile/GET_USERS';
const GET_USERS_ID_INDICES = 'profile/GET_USERS_ID_INDICES';

const GET_VEAVER_STACK_RANKINGS = 'profile/GET_VEAVER_STACK_RANKINGS';
const GET_VEAVER_RANKINGS = 'profile/GET_VEAVER_RANKINGS';

//필수 지식.////////////////////
const GET_USERS_ME_ASSIGNED_TIMELINES = 'profile/GET_USERS_ME_ASSIGNED_TIMELINES';
//받은 지식
const GET_USERS_ME_TO_SHARED_TIMELINES = 'profile/GET_USERS_ME_TO_SHARED_TIMELINES';
//내가 보낸 지식
const GET_USERS_ME_BY_SHARED_TIMELINES = 'profile/GET_USERS_ME_BY_SHARED_TIMELINES';

const UPDATE_SELECTED_ITEMS = 'profile/UPDATE_SELECTED_ITEMS';
const POST_CHANGE_PASSWORD = 'profile/POST_CHANGE_PASSWORD';


const POST_ASSIGNED_TIMELINES_HIDDEN = 'profile/POST_ASSIGNED_TIMELINES_HIDDEN';
const POST_TO_SHARED_TIMELINES_HIDDEN = 'profile/POST_TO_SHARED_TIMELINES_HIDDEN';
const POST_BY_SHARED_TIMELINES_HIDDEN = 'profile/POST_BY_SHARED_TIMELINES_HIDDEN';

const PUT_USERS_ME_THUMBNAIL = 'profile/PUT_USERS_ME_THUMBNAIL';


export const getUsersTimelines = createAction(GET_USERS_TIMELINES, (req,attachMode=false)=>{
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  req.pageSize = initialState.get('pageSize');
  console.log('get User timeline');
  return axios.get(`/users/${req.id}/timelines`,{params:req})
},(req,attachMode=false)=>( { req ,attachMode}  ));


export const getMyTimelines = createAction(GET_USERS_TIMELINES, (req,attachMode=false)=>{
    req.pageNum = attachMode ? req.pageNum + 1 : 1;
    req.pageSize = initialState.get('pageSize');
    console.log('get My timeline');
    return axios.get(`/users/me/timelines`,{params:req})
},(req,attachMode=false)=>( { req ,attachMode}  ));

export const getUsers = createAction(GET_USERS, (req)=>{ 
  return axios.get(`/users/${req.id}`)
});
export const getUsersIdIndices = createAction(GET_USERS_ID_INDICES, (req)=>{ 
  return axios.get(`/users/${req.id}/indices`)
});
export const getVeaverStackRankings = createAction(GET_VEAVER_STACK_RANKINGS, (req)=>{ 
  return axios.get(`/veaver-stack-rankings`)
});
export const getVeaverRankings = createAction(GET_VEAVER_RANKINGS, (req)=>{ 
  return axios.get(`/veaver-rankings`)
});

//////////////////////////
export const getUsersMeAssignedTimelines = createAction(GET_USERS_ME_ASSIGNED_TIMELINES, (req,attachMode=false)=>{ 
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  return axios.get(`/users/me/assigned-timelines`,{params:req})
},(req,attachMode=false)=>( { req ,attachMode}  ));
export const getUsersMeToSharedTimelines = createAction(GET_USERS_ME_TO_SHARED_TIMELINES, (req,attachMode=false)=>{ 
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  return axios.get(`/users/me/to-shared-timelines`,{params:req})
},(req,attachMode=false)=>( { req ,attachMode}  ));
export const getUsersMeBySharedTimelines = createAction(GET_USERS_ME_BY_SHARED_TIMELINES, (req,attachMode=false)=>{ 
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  return axios.get(`/users/me/by-shared-timelines`,{params:req})
},(req,attachMode=false)=>( { req ,attachMode}  ) )

export const updateSelectedItems = createAction(UPDATE_SELECTED_ITEMS, (data)=>{ 
  return new Promise(function (resolve, reject) {
    resolve(data);
  });
});

export const postAssignedTimelinesHidden = createAction(POST_ASSIGNED_TIMELINES_HIDDEN, (req)=>{ 
  return axios.post(`/assigned-timelines/hidden`,req)
});
export const postToSharedTimelinesHidden = createAction(POST_TO_SHARED_TIMELINES_HIDDEN, (req)=>{ 
  return axios.post(`/to-shared-timelines/hidden`,req)
});
export const postBySharedTimelinesHidden = createAction(POST_BY_SHARED_TIMELINES_HIDDEN, (req)=>{ 
  return axios.post(`/by-shared-timelines/hidden`,req)
});

const getBaseDomain = () => process.env.NODE_ENV === 'production' ? window.location.origin : `https://${process.env.REACT_APP_SERVICE_ID}.ent.veaver.com`;
const getBaseURL = () => process.env.NODE_ENV === 'production' ? '/api/admin/v1' : `${getBaseDomain()}/api/admin/v1`;
export const postChangePassword = createAction(POST_CHANGE_PASSWORD, (req)=>{ 
  const adminAxios = process.env.NODE_ENV === 'test' ?
  _adminAxios.create({ 
    baseURL: `https://prompt-dev.veaver.com/api/admin/v1`,
    withCredentials: true,
    headers: { Pragma: 'no-cache'}
  }) 
  :_adminAxios.create({
    baseURL: getBaseURL(),
    withCredentials: true, // send cookie
    headers: { Pragma: 'no-cache'},
    transformResponse: [function (data) {
      const parseData = JSON.parse(data);
      const essentialData = parseData.data || parseData; // items youtube
      return essentialData;
    }],
    validateStatus: function (status) {
      return status >= 200 && status < 300; 
    },
  });
  return adminAxios.post(`/change-password`,req)
})

export const uploadThumbnail = createAction(PUT_USERS_ME_THUMBNAIL, (req)=>{ 
  var formData = new FormData();
  formData.append("thumbnail", req.thumbnail);
  return axios.put(`/users/me/thumbnail`,formData)
  // formData.append("files", req.thumbnail);
  // return axios.post(`upload-temp-images`,formData)
//   Accept: application/json, text/plain, 
// Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryFPGth4U7gMBpk7L4
// Origin: http://localhost:3000
// Pragma: no-cache
// Referer: http://localhost:3000/make
// User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36
});

const initialState = Map({
  pageNum:1,
  pageSize:20,
  userTimelinesResult:null,
  userResult:null,
  usersIndicesResult:null,
  veaverStackRankingsResult: null,
  veaverRankingsResult:null,
  assignedResult:null,
  receivedResult:null,
  sentResult:null,
  selectedItems:{ //선택된 아이템
    idx:[],  // 리터널 형식 문자열
    isChanging:false,
    originObj:[] // 실제 선택된 객체 자제 
  },
  changeResult:null,
  hiddenResult:null
})

const reducer = handleActions({
  // 다른 일반 액션들을 관리..
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_USERS_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const timelines = convertTimelines(action.payload.data.timelines)
      const result = state.get('userTimelinesResult');
      action.payload.data.timelines = action.meta.attachMode ? result.timelines.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.timelinesCount / initialState.get('pageSize')) === action.meta.req.pageNum
      return state.set('userTimelinesResult', Object.assign({},action.payload.data)).set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_USERS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('userResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_USERS_ID_INDICES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('usersIndicesResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_VEAVER_STACK_RANKINGS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('veaverStackRankingsResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_VEAVER_RANKINGS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('veaverRankingsResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_USERS_ME_ASSIGNED_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const timelines = convertTimelines(action.payload.data.timelines)
      const sentResult = state.get('sentResult');
      action.payload.data.timelines = action.meta.attachMode ? sentResult.timelines.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.timelinesCount / 20) === action.meta.req.pageNum
      return state.set('assignedResult', action.payload.data)
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_USERS_ME_TO_SHARED_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const timelines = convertTimelines(action.payload.data.timelines)
      const sentResult = state.get('sentResult');
      action.payload.data.timelines = action.meta.attachMode ? sentResult.timelines.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.timelinesCount / 20) === action.meta.req.pageNum
      return state.set('receivedResult', action.payload.data)
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_USERS_ME_BY_SHARED_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const timelines = convertTimelines(Object.assign([],action.payload.data.timelines))
      const sentResult = state.get('sentResult');
      action.payload.data.timelines = action.meta.attachMode ? sentResult.timelines.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.timelinesCount / 20) === action.meta.req.pageNum
      return state.set('sentResult', Object.assign({},action.payload.data))
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: UPDATE_SELECTED_ITEMS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      // let sentResult = state.get('selectedItems');
      // sentResult.timelines = action.payload;
      return state.set('selectedItems', Object.assign({},action.payload))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_CHANGE_PASSWORD,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      
      return state.set('changeResult', Object.assign({},action.payload.data))
    },
    onFailure: (state, action) => {
        return state.set('changeResult', Object.assign({},action.payload.response.data)); // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_ASSIGNED_TIMELINES_HIDDEN,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => {      
      return state.set('hiddenResult', Object.assign({},action.payload.data)).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_TO_SHARED_TIMELINES_HIDDEN,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('hiddenResult', Object.assign({},action.payload.data)).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_BY_SHARED_TIMELINES_HIDDEN,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('hiddenResult', Object.assign({},action.payload.data)).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: PUT_USERS_ME_THUMBNAIL,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state;
    },
    onFailure: (state, action) => {
        return state
    },
    onCancel: (state, action) => {
      return state
    }
  },
]);
