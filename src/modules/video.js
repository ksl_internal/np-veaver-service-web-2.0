import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import {
    isLocker,
    axios,
  } from '../util/api';
import { timeSince, convDuration, toDateStr } from '../util/time';

/*

api 문서 비디오 내용입니다

*/

const GET_VIDEO_VIEW_CATEGORIES = 'video/GET_VIDEO_VIEW_CATEGORIES';
const GET_VIDEO_VIEW_CATEGORIES_ID = 'video/GET_VIDEO_VIEW_CATEGORIES_ID';
const MODEL_UPDATE_CATEGORY = 'video/MODEL_UPDATE_CATEGORY';
export const getVideoViewCategories = createAction(GET_VIDEO_VIEW_CATEGORIES, (req) =>{ 
  return axios.get(`/videos/view-categories`,{ params: req})
});
export const getVideoViewCategoriesId = createAction(GET_VIDEO_VIEW_CATEGORIES_ID, (req)=>{ 
  return axios.get(`/videos/view-categories/${req.id}`,{ params: req})
});
export const modelUpdateCategory = createAction(MODEL_UPDATE_CATEGORY, (query) =>{ 
  return new Promise(function (resolve, reject) {
      resolve(query);
  });
});




const initialState = Map({
  root:[],
  child:[]
})
const reducer = handleActions({
  getVideoViewCategoriesId
}, initialState);
export default applyPenders(reducer, [
  {
    type: GET_VIDEO_VIEW_CATEGORIES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      let categories = action.payload.data.mainViewCategories.map(o=>{
        o.selected=false;
        o.isOpen=false;
        return o;
      })
      return state.set('root',Object.assign([],categories) )
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_VIDEO_VIEW_CATEGORIES_ID,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      // let root = state.get('root');
      // const newRoot = root.map( (rootCate,i)=>{
      //   rootCate.isOpen =false;
      //   if(rootCate.idx === action.payload.config.params.id){
      //     rootCate.children = action.payload.data.subViewCategories;
      //     if(action.payload.data.subViewCategories.length > 0){
      //       rootCate.isOpen =true;
      //     }
      //   }
      //   return rootCate;
      // });
      let categories = action.payload.data.subViewCategories.map(o=>{
        o.selected=false;
        o.isOpen=false;
        return o;
      })
      return state.set('child',Object.assign([],categories))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: MODEL_UPDATE_CATEGORY,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      let root = state.get('root');
      function clearCategory(list , afterCategory){
        const result=  list.map(o=>{
          if(afterCategory.isRoot){
            o.isOpen = false;
          }
          o.selected=false;
          if(o.children !== undefined && o.children.length > 0){
            o.selected=false;
            o.children = clearCategory(o.children , afterCategory)
          }
          if(o.idx == afterCategory.idx){
            o = afterCategory
          }
          return o;
        })
        return result;
      }
      root = clearCategory(root , action.payload.category)
      // const newRoot = root.map( (rootCate,i)=>{
      //   rootCate.selected=false // 부모 선택 초기화
      //   if(action.payload.category.isRoot){ 
      //     rootCate.isOpen =false;
      //   }
      //   if(rootCate.idx === action.payload.category.idx){
      //     rootCate = action.payload.category; // 부모를 현재 객체로 변경
      //   }
      //   if(rootCate.children !==undefined && rootCate.children.length > 0){
      //     const newChildren = rootCate.children.map(child=>{
      //       child.selected = false; // 자식 선택 초기화
      //       if(child.idx === action.payload.category.idx){
      //         child = action.payload.category;
      //       }
      //       return child;
      //     })
      //     rootCate.children = newChildren //자식을 현재 객체로 변경
      //   }
      //   return rootCate;
      // });
      return state.set('root',Object.assign([],root))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  
]);
