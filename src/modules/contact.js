import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import {
    isLocker,
    axios,
  } from '../util/api';

/*

api 문서 세팅 문의하기 내용입니다

*/

  
const GET_CONTACTUS = 'contact/GET_CONTACTUS';
const GET_CONTACTUS_DETAIL = 'contact/GET_CONTACTUS_DETAIL';
const POST_CONTACTUS = 'contact/POST_CONTACTUS';
const GET_CONTACTUS_CATEGORIES = 'contact/GET_CONTACTUS_CATEGORIES';
const DELETE_CONTACTUS = 'contact/DELETE_CONTACTUS';


function _getContactUs(req) {
  req.me = 'Y'
  return axios.get(`/contact-us`,{ params: req})
}
function _getDetail(id) {
  return axios.get(`/contact-us/${id}`)
}

function _post(req) {
  return axios.post(`/contact-us`,req)
}
function _getCategories() {
  return axios.get(`/contact-us/categories`)
}
function _deleteContact(id) {
  return axios.delete(`/contact-us`,{data:{idx:id}})
}

export const getContactUs = createAction(GET_CONTACTUS, _getContactUs);
export const getDetail = createAction(GET_CONTACTUS_DETAIL, _getDetail);
export const postContactUs = createAction(POST_CONTACTUS, _post);
export const getCategories = createAction(GET_CONTACTUS_CATEGORIES, _getCategories);
export const deleteContact = createAction(DELETE_CONTACTUS, _deleteContact);


const initialState = Map({
  items:null,
  item:null,
  postItem:Map({
    categoryIdx:'', //1 : content / 2 : profile / 3 : etc / 4 : veaver
    subject: '',
    content : '',
    appVersion : '',
    deviceOs : '',
    deviceModel : '',
    importantFlag : '',
  }),
  categories : null,
  result : null
})

const reducer = handleActions({
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_CONTACTUS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('items', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }

  },
  {
    type: GET_CONTACTUS_DETAIL,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('item', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }

  },
  {
    type: POST_CONTACTUS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('item', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }

  },
  {
    type: GET_CONTACTUS_CATEGORIES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('categories', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }

  },
  {
    type: DELETE_CONTACTUS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('result', action.payload.data.header)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }

  }
  /*
    다른 pender 액션들
    { type: GET_SOMETHING, onSuccess: (state, action) => ... },
    { type: GET_SOMETHING, onSuccess: (state, action) => ... }
  */
]);
