/** 사용하고 있지 않음 */
import { setErrorMessage } from './error';
import {
  axios,
} from '../util/api';

export const FETCH_SERVER_STATUS = 'portal/FETCH_SERVER_STATUS';
export const FETCH_SERVER_STATUS_SUCCESS = 'portal/FETCH_SERVER_STATUS_SUCCESS';

export const fetchServerStatus = () => {
  return dispatch => {
    dispatch({ type: FETCH_SERVER_STATUS });
    axios.get(`https://portal.ent.veaver.com/api/v1/server-status?id=${process.env.REACT_APP_SERVICE_ID}`)
      .then(result => dispatch({
        type: FETCH_SERVER_STATUS_SUCCESS,
        result,
      }))
      .catch(err => dispatch(setErrorMessage(err.message)));
  }
}

const initialState = {
  serverStatus: 'R',
  serviceName: process.env.REACT_APP_SERVICE_ID,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SERVER_STATUS_SUCCESS:
      return {
        ...state,
        serverStatus: action.result.data.status
      }

    default:
      return state
  }
}
