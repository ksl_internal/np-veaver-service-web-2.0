import {
  getBaseDomain,
  isLocker,
  isCp,
  axios,
  getCpGroupNumber
} from '../util/api';
import { timeSince, convDuration, toDateStr } from '../util/time';
import { openPopup } from './popup';
import * as ProfileActions from './profile';
import T from 'i18n-react';

export const CHECK_ALLOW_SUCCESS = 'mypage/CHECK_ALLOW_SUCCESS';
export const TIMELINES_REQUEST = 'mypage/TIMELINES_REQUEST';
export const TIMELINES_SUCCESS = 'mypage/TIMELINES_SUCCESS';
export const DELETE_TIMELINE_SUCCESS = 'mypage/DELETE_TIMELINE_SUCCESS';
export const CREATE_TIMELINE_LINK_SUCCESS = 'mypage/CREATE_TIMELINE_LINK_SUCCESS';

const initialState = {
    totalCount: 0,
    items: [],
    pageNum: 1,
    pageSize: 20,
    isHideMoreLayer: true,
    isShowLoadingCards: true,
    categoryIdx:0
};

export const checkAllow = (timelineIdx) => (dispatch, getState) => {
    return axios.get(`/timelines/${timelineIdx}/allow`).then(res => {
    const resultCode = res.data.header.resultCode
    const isSuccess = resultCode === 0
      return dispatch({ type: CHECK_ALLOW_SUCCESS, isSuccess })
    }).catch(error => Error(error))
}

/**
 * @method 타임라인 목록 호출
 * @field dynamicUriPart : 임시저장과 api 를 공유한다.
 */
export const getTimelines = (attachMode = false,_categoryIdx=null) => (dispatch, getState) => {
  const pageNum = attachMode ? getState().mypage.pageNum + 1 : 1;
  const pageSize = getState().mypage.pageSize;
  let dynamicUriPart = isLocker() ? 'temp' : (isCp() ? 'cp' : 'recent');
  let realPath = `/${dynamicUriPart}-timelines`;
  const groupNum = getCpGroupNumber();
  if(isCp() && groupNum !=null){
    realPath = `/cp-timeline-groups/${groupNum}/timelines`
  }
  let params = {
    pageNum,
    pageSize
  }
  if(_categoryIdx!==null && _categoryIdx>0){
    params.categoryIdx = _categoryIdx
  }
  axios
  .get(realPath, {params})
  .then(result => {
      const currentMillis = new Date().getTime();
      const totalPageNum = Math.ceil(result.data.timelinesCount / pageSize);
      const isHideMoreLayer = totalPageNum === pageNum;
      const delay = 100;
      const timelines = result.data.timelines.map(timeline => {
      const regMillis = timeline.regDate;
      const intervalMillis = currentMillis - regMillis;
      return {
          sendStatus: timeline.sendStatus,
          timelineIdx: timeline.timelineIdx,
          videoIdx: timeline.videoIdx,
          user: timeline.user,
          title: timeline.name,
          thumbnail: timeline.thumbnail,
          publicFlag: timeline.publicFlag,
          viewCount: timeline.viewCount,
          likeCount: timeline.likeCount,
          commentCount: timeline.commentCount,
          videoStatusFlag: 'C',//timeline.videoStatusFlag,
          playTime: convDuration(timeline.playTime),
          regDate: (intervalMillis < (24*60*60*1000)) ? timeSince(intervalMillis) : toDateStr(regMillis),
          uptDate: ( (currentMillis- timeline.uptDate) < (24*60*60*1000)) ? timeSince(currentMillis- timeline.uptDate) : toDateStr(timeline.uptDate),
          tags: timeline.tag.split(',').map((tag) => {return '#'+tag})
      };
      });

      setTimeout(() => dispatch({ type: TIMELINES_SUCCESS, payload: {
        totalCount: result.data.timelinesCount,
        items: timelines,
        isHideMoreLayer: isHideMoreLayer,
        attachMode: attachMode,
        categoryIdx:_categoryIdx
        }}), delay);
  })
  .catch((error) => Error(error));
};
async function userTimelineRefresh (id){
    await ProfileActions.getUsersTimelines({id:id},false)
}
/**
 * @method 타임라인 삭제
 * @description 타임라인을 삭제하면, 리스트는 초기화가 트리거 된다.
 * @param {*} timelineIndex
 * 임시저장 삭제는 같은 api 를 사용 한다.
 */
export const deleteTimeline =  (timelineIndex, history,reload=false) => (dispatch, getState) => {
    const data = {
        title: T.translate('common.delete'),
        description: T.translate('common.delete-warn')+' '+T.translate('common.delete-check')
    };
    openPopup('CONFIRM', data, (value) => {
        if (value) {
            //userTimelineRefresh(userKey)
            axios.delete('/timelines', { data: { targetTimelineIdx: (typeof timelineIndex ==='object' ? timelineIndex : [timelineIndex] ) }})
                .then(result => dispatch({ type: DELETE_TIMELINE_SUCCESS }))
                .then(() => {
                    if (history) {
                        global.alert(T.translate('popup.deleted'));
                        history.goBack();
                    } else {
                        if(reload ===true){
                            global.alert(T.translate('popup.deleted'));
                            window.location.reload(true);
                        }else{
                            getTimelines()(dispatch, getState);
                            global.alert(T.translate('popup.deleted'));
                        }
                    }
                })
                .catch((error) => Error(error));
        }
    })(dispatch, getState);
}
/**
 * @method 클립보드 카피링크 호출
 */
export const openClipBoard = (videoIdx, timelineIdx) => (dispatch, getState) => {
    axios.post('/short-url', {
        url: `${getBaseDomain()}/timeline-deeplink?action=play&videoIdx=${videoIdx}&timelineIdx=${timelineIdx}`
      })
      .then(result => {
        const linkText = result.data.url;
        openPopup('LINK_COPY', { linkText })(dispatch, getState);
        dispatch({ type: CREATE_TIMELINE_LINK_SUCCESS });
      })
      .catch((error) => Error(error));
}

export default (state = initialState , action) => {
  switch (action.type) {
    case TIMELINES_REQUEST: {
        const items = action.attachMode ? state.items : [];
        const totalCount = action.attachMode ? state.totalCount : 0;
        return {
            ...state,
            items: items,
            totalCount: totalCount,
            isShowLoadingCards: true,
        };
    }

    case TIMELINES_SUCCESS: {
      const { isHideMoreLayer, items, totalCount, attachMode ,categoryIdx} = action.payload;
      const pageNum = attachMode ? (isHideMoreLayer ? state.pageNum : state.pageNum += 1) : 1;
      return {
          ...state,
          items: attachMode ? state.items.concat(items) :items,
          isShowLoadingCards: false,
          isHideMoreLayer,
          totalCount,
          pageNum,
          categoryIdx
      };
    }

    case DELETE_TIMELINE_SUCCESS: {
        return {
            ...state,
            items: [],
            pageNum: 1,
        };
    }

    case CHECK_ALLOW_SUCCESS:
    case CREATE_TIMELINE_LINK_SUCCESS:
    default: return state;
  }
}
