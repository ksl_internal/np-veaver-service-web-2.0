const SET_LANGUAGE = 'SET_LANGUAGE';

/*

다국어 적용을 위한 스토어 입니다.

*/

export const setLanguage = ( $language ) => ( {
    type: SET_LANGUAGE,
    language: $language
} );

const initialState = {
    language: 'ko'
};

export default (state = initialState, action) => {
    switch ( action.type ) {
        case SET_LANGUAGE:
            return {
                ...state,
                language: action.language,
            };
        default:
            return state;
    }
};


