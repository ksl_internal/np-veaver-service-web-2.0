import { LOCATION_CHANGE } from 'react-router-redux';
import { default as storage } from 'store';
import {
  axios,
} from '../util/api';
import { openPopup } from './popup';
import { appInitialize } from './app';

export const VALIDATE_AUTH_TOKEN = 'auth/VALIDATE_AUTH_TOKEN';
export const AUTH_TOKEN_NOT_VALIDATED = 'auth/AUTH_TOKEN_NOT_VALIDATED';
export const SAVE_AUTH_TOKEN = 'auth/SAVE_AUTH_TOKEN';
export const LOGIN = 'auth/LOGIN';
export const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS';
export const LOGOUT = 'auth/LOGOUT';
export const LOGOUT_SUCCESS = 'auth/LOGOUT_SUCCESS';
export const REMEMBER_ID_CHECKED = 'auth/REMEMBER_ID_CHECKED';
export const REMEMBER_ID_UNCHECKED = 'auth/REMEMBER_ID_UNCHECKED';

export const LOGIN_GET_CERT = 'auth/LOGIN_GET_CERT';
export const LOGIN_CONFIRM_CERT = 'auth/LOGIN_CONFIRM_CERT';

const saveId = (id) => {
  storage.set('remember', true);
  storage.set('rememberId', id);
};
const releaseId = () => {
  storage.set('remember', false);
  storage.set('rememberId', null);
}

export const rememberIdCheck = (checked) => {
  return dispatch => {
    checked ?
      dispatch({ type: REMEMBER_ID_CHECKED })
      : dispatch({ type: REMEMBER_ID_UNCHECKED })
  };
}
/**
 * 로그인시 
 * 1. /users/login 요청
 * 2. /users/cert-number 요청
 * 3. /users/confirm-cert-number 유효성 검사
 * 4. 로그인 완료.
 */
export const login = (email, password, autoLogin) =>(dispatch, getState) => {
  //(dispatch, getState) => {
    dispatch({ type: LOGIN });
    const requestObject = {
      email,
      password,
    };
    if (autoLogin) requestObject.autoLoginFlag = "Y";

    return axios
    .post('/users/login', requestObject)
    .then(res => {
      return res.data
    })
    .then((response) => {
      // /users/login 성공   서버에서 http status 200 성공  resultCode 로 분리 하기로 하였습니다.
      if(response.header.resultCode ===0){ 
        axios.post('/users/cert-number', requestObject)
          .then((res) => {
            // /users/cert-number 성공
            if(response.header.resultCode ===0){
              // 리덕스 state
              let postData = Object.assign({},getState().postData);
              postData = {
                title: '인증 코드 입력',
                description: 'PC 로그인을 위해서는 모바일로 전송된 인증 코드입력이 필요 합니다. 모바일 버전으로 로그인 후 중요알림 메뉴에서 인증코드를 확인 후 입력해 주세요.',
                failCount:0,
                returnMsg:'',
                requestObject,
                resetCallback: () =>{ //인증코드 다시 보내는 콜백 함수 입니다.
                  postData.failCount =0;
                  postData.returnMsg ='';
                  axios.post('/users/cert-number', requestObject).then((res) => {
                    dispatch({ type: LOGIN_GET_CERT ,postData});
                  }).catch((error) => Error(error));
                },
                submitCallback:(_certNumber)=>{ // 인증 코드 적은 후 확인 버튼 콜백 함수입니다.
                  if(_certNumber.replace(/(\s*)/g,'')!==''){
                    requestObject.certNumber = _certNumber
                    axios.post('/users/confirm-cert-number', requestObject).then((confirm_res) => {
                      // 로그인 모든 과정이 성공했을 경우입니다
                      if(confirm_res.data.header.resultCode===0){
                        dispatch({ type: LOGIN_SUCCESS });
                        getState().auth.checkRemember ? saveId(email) : releaseId()
                      }else if(confirm_res.data.header.resultCode===2004){
                        postData.failCount ++;
                        postData.returnMsg =`인증 코드를 ${postData.failCount}회 틀리셨습니다.\n인증코드 확인해주세요.`;
                        if(postData.failCount>=5){
                          const data = {
                            title: '코드 틀림',
                            description: '인증코드를 5회 틀리셨습니다. 인증코드 재발송 후 로그인을 시도해 주세요.',
                          };
                          openPopup('BASIC', data, (value) => {}) (dispatch, getState);
                        }
                        dispatch({ type: LOGIN_CONFIRM_CERT , postData });
                      }
                    }).catch((confirm_error) => {
                      console.log('confirm-cert-number error',confirm_error.response)
                    });
                  }
                }
              };
              dispatch({ type: LOGIN_GET_CERT ,postData});
              openPopup('CERT', {}, (value) => {}) (dispatch, getState); //postData redux 에서 관리 하도록 수정.
            }else {   // otp 요청에 관한 에러 처리 http status 200 >> resultCode ? 아직 모름.   최상단 /login 에서 먼저 에러처리 

            }
          }).catch((error) => {
            //console.log('cert-number catch : ',error.response)
            //모바일 설치 안되있으면, 경고창
            // if (error.response.data.header.resultCode == 400) {
            //   const data = {
            //     title: '중요알림',
            //     description: error.response.data.errorData,
            //   };
            //   openPopup('BASIC', data, (value) => {}) (dispatch, getState);
            // }
            Error(error)
          });
      }else if(response.header.resultCode===2004){  // 모든 에러 처리를 http status 200 , resultCode 로 분기
        const data = {
          title: '정보 불일치',
          description: '입력하신 패스워드가 잘못 되었습니다.',
        };
        openPopup('BASIC', data, (value) => {}) (dispatch, getState);
      }else if(response.header.resultCode===2007){
        const data = {
          title: '비활성 유저',
          description: '해당아이디는 비활성 상태입니다. 관리자에게 문의해주세요.',
        };
        openPopup('BASIC', data, (value) => {}) (dispatch, getState);
      }else if(response.header.resultCode===2009){
        const data = {
          title: '패스워드 제한',
          description: '패스워드 입력횟수 초과하였습니다. 관리자에게 문의해주세요.',
        };
        openPopup('BASIC', data, (value) => {}) (dispatch, getState);
      }else if(response.header.resultCode===2001){
        if(response.errorData ==null){
          const data = {
            title: '중요알림',
            description: '회원 정보가 없습니다. 관리자에게 문의해주세요.',
          };
          openPopup('BASIC', data, (value) => {}) (dispatch, getState);
        }else if(response.errorData==='userDevice'){
          const data = {
            title: '중요알림',
            description: 'PC버전 인증을 위해서는 모바일용 비버를 설치하셔야 합니다. 설치 후 다시 로그인 해주세요.',
          };
          openPopup('BASIC', data, (value) => {}) (dispatch, getState);
        }
        
      }
    })
    .catch(err => {
      // console.log('login catch : ',err.response)
      //모바일 설치 안되있으면, 모바일 설치 유도 창   // 2019 - 04-15 직접 미팅 후 강제 에러 처리 에서 http status 200 , resultCode 로 처리.
      // if (err.response.data.header.resultCode == 400) {
      //   const data = {
      //     title: '중요알림',
      //     description: 'PC버전 인증을 위해서는 모바일용 비버를 설치하셔야 합니다. 설치 후 다시 로그인 해주세요.',
      //     //description: err.response.data.errorData, //서버에서 에러 메세지 리턴 
      //   };
      //   openPopup('BASIC', data, (value) => {}) (dispatch, getState);
      // }
    });
  //}
}

export const logout = () => {
  return (dispatch, getState) => {
    dispatch({ type: LOGOUT });

    axios
    .post('/users/logout')
    .then(() => {
      window.history.replaceState(null, null, "/");
      window.location.reload(true);
    }).catch(err => Error(err));
  }
}

const initialState = {
  isAuthenticated: false,
  checkRemember: (storage.get('remember') || false),
  postData:{}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state;
    case REMEMBER_ID_CHECKED:
      return {
        ...state,
        checkRemember: true,
      }
    case REMEMBER_ID_UNCHECKED:
      return {
        ...state,
        checkRemember: false,
      }
    case LOGIN_SUCCESS: {
      
      return {
        ...state,
        isAuthenticated: true,
      }
    }
    case LOGOUT_SUCCESS: {
      return {
        ...state,
        isAuthenticated: false,
      }
    }
    case LOGIN_GET_CERT: {
      return {
        ...state,
        postData:Object.assign({},action.postData)
      }
    }
    case LOGIN_CONFIRM_CERT : {
      return {
        ...state,
        postData:Object.assign({},action.postData)
      }
    }
    default:
      return state
  }
}
