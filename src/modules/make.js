import { arrayMove } from "react-sortable-hoc";
import { toastr } from "react-redux-toastr";
import { isLocker, axios } from "../util/api";
import * as Cards from "../util/card";
import AWS from "aws-sdk";
import { closePopup } from './popup';
import { initPlayer } from './player';
import T from 'i18n-react';

// const RESUME_UPLOAD = 'make/RESUME_UPLOAD';
const UPLOADING_REQUEST = 'make/UPLOADING_REQUEST';
const UPLOADING_SUCCESS = 'make/UPLOADING_SUCCESS';

const UPDATE_FOCUS = 'make/UPDATE_FOCUS'
const METADATA_TO_TEMP = 'make/METADATA_TO_TEMP';
const OVERRIDE_METADATA = 'make/OVERRIDE_METADATA';
const H263NOT_SUPPORTED = 'make/H263NOT_SUPPORTED';
const UPLOAD_VIDEOINFO_DATA_SET = 'make/UPLOAD_VIDEOINFO_DATA_SET';
const UPDATE_TIMELINE = 'make/UPDATE_TIMELINE';
const UPDATE_FORMDATA = 'make/UPDATE_FORMDATA';
const UPDATE_CATEGORIES = 'make/UPDATE_CATEGORIES';
const UPDATE_DETAIL_DATA = 'make/UPDATE_DETAIL_DATA';
const UPDATE_STEP = 'make/UPDATE_STEP';
const UPDATE_VIDEO_SOURCE = "make/UPDATE_VIDEO_SOURCE";
const UPDATE_METADATA = "make/UPDATE_METADATA";

const SPLIT_SECTION = "make/SPLIT_SECTION";
const SPLIT_SECTION_FAILED = "make/SPLIT_SECTION_FAILED";
const COMBINE_SECTION = "make/COMBINE_SECTION";
const UPDATE_SECTION_NAME = "make/UPDATE_SECTION_NAME";

const SPLIT_STACK = "make/SPLIT_STACK";
const SPLIT_STACK_FAILED = "make/SPLIT_STACK_FAILED";
const DETACH_STACK = "make/DETACH_STACK";
const DETACH_STACK_IN_CARD = "make/DETACH_STACK_IN_CARD";
const SORT_STACK_IN_CARD = "make/SORT_STACK_IN_CARD";
const UPDATE_STACK_IN_CARD_TEXT = "make/UPDATE_STACK_IN_CARD_TEXT";
const UPDATE_EVENT_IN_CARD_TEXT = "make/UPDATE_EVENT_IN_CARD_TEXT";
const UPDATE_STACK_NAME = "make/UPDATE_STACK_NAME";
const UPDATE_STACK_RANGE = "make/UPDATE_STACK_RANGE";
const CREATE_STACK_IN_CARD_LINK = "make/CREATE_STACK_IN_CARD_LINK";

const GET_SUB_CATEGORIES = 'make/GET_SUB_CATEGORIES';
const ATTACH_CARD = "make/ATTACH_CARD";

const ATTACH_EVENT = "make/ATTACH_EVENT";
const DETACH_EVENT = "make/DETACH_EVENT";

const UPDATE_QUIZ_CARD_AT_EVENTS = 'make/UPDATE_QUIZ_CARD_AT_EVENTS';
const UPDATE_IMAGE_CARD_AT_EVENTS = 'make/UPDATE_IMAGE_CARD_AT_EVENTS';
const FAILED_IMAGE_CARD_AT_EVENTS = 'make/FAILED_IMAGE_CARD_AT_EVENTS';
const UPDATE_QUIZ_CARD_AT_STACKS = 'make/UPDATE_QUIZ_CARD_AT_STACKS';
const UPDATE_VOTE_CARD_AT_STACKS = "make/UPDATE_VOTE_CARD_AT_STACKS";
const UPDATE_IMAGE_CARD_AT_STACKS = 'make/UPDATE_IMAGE_CARD_AT_STACKS';
const FAILED_IMAGE_CARD_AT_STACKS = 'make/FAILED_IMAGE_CARD_AT_STACKS';
const UPDATE_FILE_CARD_AT_STACKS = 'make/UPDATE_FILE_CARD_AT_STACKS';

const UPDATE_VIDEO_THUMBNAIL = 'make/UPDATE_VIDEO_THUMBNAIL';
const FAILED_UPDATE_VIDEO_THUMBNAIL = 'make/FAILED_UPDATE_VIDEO_THUMBNAIL';

const UPLOADING_VIDEO = "make/UPLOAD_VIDEO";
const UPLOADING_AZURE_PROGRESS = "make/UPLOADING_AZURE_PROGRESS";
const COMPLETE_UPLOAD_VIDEO = "make/COMPLETE_UPLOAD_VIDEO";
const CANCEL_AWS_UPLOAD = "make/CANCEL_AWS_UPLOAD";
const CANCEL_AZURE_UPLOAD = "make/CANCEL_AZURE_UPLOAD";
const INIT_UPLOAD_VIDEO = "make/INIT_UPLOAD_VIDEO";

const GET_CATEGORIES_SUCCESS = "make/GET_CATEGORIES_SUCCESS";
const GET_SUB_CATEGORIES_SUCCESS = "make/GET_SUB_CATEGORIES_SUCCESS";

const initialState = {
  sections: [
    {
      begin: 0,
      name: T.translate('make.section-name')+"1"
    }
  ],
  stacks: [],
  events: [],
  azureUploadCancel: false,
  S3ManagedUpload: undefined,
  videoSource: null,
  uploadVideoInfo: {
    uploadProgress: 0
  },
  uploadVideoStatus: "READY",
  categories: [],
  viewVideoCategoryNames: [],
  recommendTags: "",
  step: 1,
  formdata: {
    thumbnail: '/images/test.png',
    title: '',
    description: '',
    categoryIdx: 0,
    tag: [],
    recommendTags: [],
    accountStatus: 'admin',
    publicRange: 'A',
  },
  boardAccessFlag: '', // modify
  videoIdx: 0, // modify
  playTime: 0, // modify
  timelineIdx: 0, //modify
  tempMetaData: {
    stacks: [],
    events: [],
    secitons: [],
  },
  uploading: false,
  focused: false,
  mainCategoryIdx: 0,
  smallCategoryIdx: 0,
  selectVideoStepDone: false, //메이킹의 첫번째 스탭이 완료되었는지 평가 한다.
};

const beforeAtSort = array => array.sort((a, b) => {
  if (a.begin > b.begin) return 1;
  else return -1;
});

// AWS 업로드
function uploadingAWS( S3ManagedUpload,uploadVideoInfo,videoSource,dispatch) {
  S3ManagedUpload.on("httpUploadProgress", evt => {
    const percentage = +((evt.loaded / evt.total) * 100).toFixed(0);
    if (uploadVideoInfo.uploadProgress !== percentage) {
      uploadVideoInfo.uploadProgress = percentage;
        dispatch({
          type: UPLOADING_VIDEO,
          S3ManagedUpload,
          uploadVideoInfo,
          videoSource,
          uploadVideoStatus: "PROGRESS"
        });
    }
  }).send((err, data) => {
    if (err) {
      console.log("Error:", err.code, err.message);
    } else {
      axios
      .post(`/timelines/${uploadVideoInfo.timelineIdx}/complete-video-upload`)
        .then(() => {
          uploadVideoInfo.uploadProgress = 100;
          dispatch({
            type: COMPLETE_UPLOAD_VIDEO,
            uploadVideoInfo,
            uploadVideoStatus: "COMPLETE"
          });
        });
    }
  });
}

// 이미지 업로드
function getTempImages(files, dispatch) {
  const data = new FormData();
  if (files instanceof Blob) {
    data.append("files", files, 'file.jpg')
  } else if (files instanceof File && files.length === 1) {
    data.append("files", files[0]);
  } else if ((files instanceof Array && files.length > 1) || files instanceof FileList) {
    Array.prototype.forEach.call(files, (image => { data.append("files", image); }))
  }
  dispatch({ type: UPLOADING_REQUEST });
  return axios
  .post('/upload-temp-images', data)
  .then(res => {
    dispatch({ type: UPLOADING_SUCCESS });
    return res.data.paths;
   })
  .catch(err => Error(err));
}
// 이벤트 이미지 업로드
function updateSingleImageFile(dispatch, getState, file, lastIndex) {
  getTempImages(file, dispatch)
  .then((images) => {
    const events = getState().make.events;
    events[lastIndex].contents = { thumbnail: images[0] };

    dispatch({ type: UPDATE_IMAGE_CARD_AT_EVENTS, events });
  }).catch(err => {
    toastr.light('', T.translate('make.upload-fail-error'));
    setTimeout(() => dispatch({ type: FAILED_IMAGE_CARD_AT_EVENTS, lastIndex}), 2000)
  });
}
// 덧지식 이미지 업로드
function updateMultiImageFile(dis, getState, files, stackIndex, lastIndex) {
  const dispatch = dis;
  getTempImages(files, dispatch)
  .then((images) => {
    const stacks = getState().make.stacks;
    stacks[stackIndex].cards[lastIndex].contents = {
      images: images.map((image, idx) => ({
      orderValue: idx,
      thumbnail: image
    }))};
    dispatch({ type: UPDATE_IMAGE_CARD_AT_STACKS, stacks });
  })
  .catch(err => {
    toastr.light('', T.translate('make.upload-fail-error'));
    setTimeout(() => dispatch({ type: FAILED_IMAGE_CARD_AT_STACKS, stackIndex, lastIndex}), 2000)
  });
}
// 썸네일 업로드
export const updateVideoThumbNail = file => (dispatch, getState) => {
  const formdata = getState().make.formdata;
  getTempImages(file, dispatch)
  .then((images) => {
    dispatch({
      type: UPDATE_VIDEO_THUMBNAIL,
      payload: {...formdata, thumbnail: images[0] },
      selectVideoStepDone: true,
     });
  }).catch(() => dispatch({ type: FAILED_UPDATE_VIDEO_THUMBNAIL, selectVideoStepDone: true }));
};

// 덧지식 - 파일 업로드
function attachFileUpload(dispatch, file, stackIndex, lastIndex) {
  const newFile = file;
  dispatch({ type: UPLOADING_REQUEST });
  const data = new FormData();
  data.append("files", newFile);
  axios.post('/upload-temp-files', data)
    .then(res => {
      dispatch({
        type: UPDATE_FILE_CARD_AT_STACKS,
        payload: {
          filePath: res.data.paths[0],
          size: newFile.size,
          name: newFile.name,
          lastIndex: lastIndex,
          stackIndex: stackIndex
        }
      });
      dispatch({ type: UPLOADING_SUCCESS });
    })
    .catch(err => Error(err));
}

// 타임라인 등록
export const uploadVideo = () => (dispatch, getState) => {
  const formdata = getState().make.formdata;
  const videoSource = getState().make.videoSource;
  const recommendTags = getState().make.recommendTags;
  const adminFlag = getState().user.userProfile.adminFlag;
  const duration = getState().player.duration;
  const sections = getState().make.sections;
  const stacks = getState().make.stacks;
  const events = getState().make.events;
  const flagValues = { admin: 'Y', user: 'N' };
  let usersIds=null;
  if(formdata.publicRange==='U'){
    usersIds = formdata.publicUsers.map(u=>{
      return u.userKey;
    })
  }
  const uploadInfo = {
    thumbnail: formdata.thumbnail,
    videoCategoryIdx: formdata.categoryIdx,
    name: formdata.title,
    description: formdata.description === "" ? null : formdata.description,
    tag: formdata.tag.length > 0 ? formdata.tag.join(",") : recommendTags,
    publicFlag: formdata.publicRange,
    publicUsers: usersIds,
    adminFlag: adminFlag === 'Y' ? flagValues[formdata.accountStatus] : 'N',
    publishFlag: "N",
    boardAccessFlag: "Y",
    timelineSyncSegment: null,
    resolutionWidth: null,
    resolutionHeight: null,
    playTime: duration,
    sections: sections,
    stacks: stacks,
    events: events
  };
  if (['YOUTUBE', 'LINK'].some(type => type === videoSource.type)) {
    uploadInfo.fileUrl = videoSource.url;
    uploadInfo.linkVideoType = videoSource.type;
  } else if ('NP' === videoSource.type) {
    uploadInfo.videoFileName = videoSource.resource.name;
  }
  const exec = {
    YOUTUBE: () => linkTypeVideoUpload(uploadInfo)(dispatch, getState),
    LINK: () => linkTypeVideoUpload(uploadInfo)(dispatch, getState),
    NP: () => npTypeVideoUpload(uploadInfo, videoSource)(dispatch, getState),
  };
  stepUpdate('direct', 3)(dispatch, getState);
  exec[videoSource.type]();
};

// 링크타입 비디오 업로드
function linkTypeVideoUpload(uploadInfo) {
  return function(dispatch, getState) {
    axios
    .post('/link-timelines',{...uploadInfo})
    .then(result => {
        const info = Object.assign({}, uploadInfo);
        info.timelineIdx = result.data.timelineIdx;
        info.uploadProgress = 100;
        completeVideoUpload(info, dispatch);
    })
    .catch(error => console.error(error));
  };
}

// 애저 업로드
function azureUploadModel(info, videoSource, dispatch, getState) {
  const { storageUri, mediaStroageSasToken, containerName } = info.videoUploadInfo;
  const { resource } = videoSource;
  const { AzureStorage } = window || global;
  const cancelUploadFilter = {
    nextCounter: 0,
    returnCounter: 0,
    handle: function (requestOptions, next) {
        var self = this;
        const {azureUploadCancel } = getState().make
        if (azureUploadCancel) {
            return;
        }
        if (next) {
            self.nextCounter++;
            next(requestOptions, function (returnObject, finalCallback, nextPostCallback) {
                self.returnCounter++;
                if (azureUploadCancel) {
                    if (self.nextCounter === self.returnCounter) {
                    }
                    // REALLY ??? Is this the right way to stop the upload?
                    return;
                }
                if (nextPostCallback) {
                    nextPostCallback(returnObject);
                } else if (finalCallback) {
                    finalCallback(returnObject);
                }
            });
        }
    }
  };
  const blobService = AzureStorage.Blob.createBlobServiceWithSas(storageUri, mediaStroageSasToken).withFilter(cancelUploadFilter);
  const blockSize = 1024 * 1000; // 1MB
  blobService.singleBlobPutThresholdInBytes = blockSize;
  const speedSummary = blobService.createBlockBlobFromBrowserFile(containerName, resource.name, resource, { blockSize }, progressCallback);
  const progressInterval = setInterval(() => {
    const {azureUploadCancel} = getState().make;
    if (!azureUploadCancel) {
      dispatch({ type: UPLOADING_AZURE_PROGRESS,
        uploadVideoInfo: { uploadProgress: speedSummary.getCompletePercent() },
        azureUploadCancel: azureUploadCancel,
        uploadVideoStatus: "PROGRESS"
      });
    }
  }, 200);

  function progressCallback(error, result, response) {
    clearInterval(progressInterval);
    if (response.isSuccessful) {
      axios
      .post(`/timelines/${info.timelineIdx}/complete-video-upload`)
        .then(() => {
          info.uploadProgress = 100;
          dispatch({
            type: COMPLETE_UPLOAD_VIDEO,
            uploadVideoInfo: info,
            uploadVideoStatus: "COMPLETE"
          });
        });
    } else {
      throw new Error(error);
    }
  }
};

function awsUploadModel(info, videoSource, dispatch) {
    const s3 = new AWS.S3({
      accessKeyId: info.videoUploadInfo.tempAccessKeyId,
      secretAccessKey: info.videoUploadInfo.tempSecretAccessKey,
      sessionToken: info.videoUploadInfo.tempSessionToken,
      region: info.videoUploadInfo.region
    });
    const upload = new AWS.S3.ManagedUpload({
      params: {
        Bucket: info.videoUploadInfo.bucketName,
        Key: info.videoUploadInfo.fileKey,
        Body: videoSource.resource
      },
      leavePartsOnError: true,
      service: s3
    });
    uploadingAWS(upload, info, videoSource, dispatch);
}

// 로컬 타입 업로드
function npTypeVideoUpload(uploadInfo, videoSource) {
  return function(dispatch, getState) {
    const command = {
      AZURE: azureUploadModel,
      AWS: awsUploadModel,
    };

    return axios
    .post('/timelines', {...uploadInfo})
    .then(result => {
      const info = Object.assign({}, uploadInfo);
      const { serverStatus } = getState().app;
      info.timelineIdx = result.data.timelineIdx;
      info.resourceUrl = result.data.resourceUrl;
      info.videoUploadInfo = result.data.videoUploadInfo;
      command[serverStatus](info, videoSource, dispatch, getState);
    }).catch(error => console.error(error));
  };
}

// 업로드 완료
function completeVideoUpload(uploadVideoInfo, dispatch) {
  dispatch({
    type: COMPLETE_UPLOAD_VIDEO,
    uploadVideoInfo,
    uploadVideoStatus: "COMPLETE"
  });
};

// 타임라인 게시하기
export const videoPublish = () => (dispatch, getState) => {
  const info = getState().make.uploadVideoInfo;
  info.publishFlag = "Y";
  axios
  .put(`/temp-timelines/${info.timelineIdx}`,{...info})
  .then(() => {
    initVideoInfo()(dispatch, getState);
    window.location.href = '/';
  })
  .catch(error => console.log(error));
};

// 영상 정보 초기화
export const initVideoInfo = () => (dispatch, getState) =>
  dispatch({ type: INIT_UPLOAD_VIDEO });

// 메이킹 스텝 업데이트
export const stepUpdate = (key, value) => (dispatch, getState) => {
  const step = getState().make.step;
  const isInterger = Number.isInteger(step);
  const decreaseStep = isInterger ? step - 1 : +(step - 0.1).toFixed(1);
  const increaseStep = isInterger ? step + 1 : +(step + 0.1).toFixed(1);
  const exec = {
    prev: () => dispatch({ type: UPDATE_STEP, payload: { step: decreaseStep }}),
    next: () => dispatch({ type: UPDATE_STEP, payload: { step: increaseStep }}),
    direct: () => dispatch({ type: UPDATE_STEP, payload: { step: value }}),
  };
  exec[key]();
};

// 수정 - 타임라인 정보 설정
export const getModifyInfo = (timelineIdx) => (dispatch, getState) => {
  axios
  .get(`/${isLocker() ? 'temp-' : ''}timelines/${timelineIdx}`)
  .then(rootRequest => {

    axios
    .get(`/videos/${rootRequest.data.videoIdx}`)
    .then(res => {
      const { mainCategoryIdx, smallCategoryIdx } = getState().make;
      const categories = { mainCategoryIdx, smallCategoryIdx };
        categories.mainCategoryIdx = rootRequest.data.videoCategories[0].videoCategoryIdx;
        if (rootRequest.data.videoCategories.length === 2) {
        categories.smallCategoryIdx = rootRequest.data.videoCategories[1].videoCategoryIdx;
        }

      const videoSourceType = {
        Y: { type: 'YOUTUBE', url: res.data.fileUrl, hlsFlag: res.data.hlsFlag },
        N: { type: 'NP', file: res.data.fileUrl, hlsFlag: res.data.hlsFlag },
        L: { type: 'LINK', file: res.data.fileUrl, hlsFlag: res.data.hlsFlag },
      };
      const formdata = {
        thumbnail: rootRequest.data.thumbnail,
        title: rootRequest.data.name,
        description: rootRequest.data.description,
        categoryIdx: rootRequest.data.videoCategoryIdx,
        tag: rootRequest.data.tag === '' ? [] : rootRequest.data.tag.split(','),
        accountStatus: rootRequest.data.adminFlag === 'Y'? 'admin' : 'user',
        publicRange: rootRequest.data.publicFlag,
        publicUsers: rootRequest.data.publicUsers
      };

      dispatch({ type: UPDATE_DETAIL_DATA, payload: {
        formdata: formdata,
        sections: rootRequest.data.sections,
        stacks: rootRequest.data.stacks,
        events: rootRequest.data.events,
        videoSource: videoSourceType[res.data.fileType],
        boardAccessFlag: rootRequest.data.boardAccessFlag, // modify
        videoIdx: rootRequest.data.videoIdx, // modify
        playTime: rootRequest.data.playTime, // modify
        timelineIdx: timelineIdx,
        deployFlag: isLocker() ? 'N' : res.data.deployFlag,
        step: 2,
        ...categories, // modify
      }});
      initPlayer(true, rootRequest.data.playTime)(dispatch, getState);
    }).catch(error => console.log(error));
  }).catch(error => console.log(error));
}
// 수정 - 타임라인 업데이트
export const updateTimeline = () => (dispatch, getState) => {
  const timelineIdx = getState().make.timelineIdx;
  const videoIdx = getState().make.videoIdx;
  const formdata = getState().make.formdata;
  const videoSource = getState().make.videoSource;
  const boardAccessFlag = getState().make.boardAccessFlag;
  const sections = getState().make.sections;
  const stacks = getState().make.stacks;
  const events = getState().make.events;
  const playTime = getState().make.playTime;
  const flagValues = { admin: 'Y', user: 'N' };
  let usersIds=null;
  if(formdata.publicRange==='U'){
    usersIds = formdata.publicUsers.map(u=>{
      return u.userKey;
    })
  }
  const requestBody = {
    videoIdx: videoIdx,
    videoCategoryIdx: formdata.categoryIdx,
    type: videoSource.type,
    name: formdata.title,
    description: formdata.description,
    tag: formdata.tag.join(','),
    publicFlag: formdata.publicRange,
    publicUsers: usersIds,
    adminFlag: flagValues[formdata.accountStatus],
    boardAccessFlag: 'Y',
    thumbnail: formdata.thumbnail,
    publishFlag: "N",
    playTime: playTime,
    sections: sections,
    stacks: stacks,
    events: events,
  };
  axios
  .put(`/${isLocker() ? 'temp-' : ''}timelines/${timelineIdx}`, {...requestBody})
  .then(res => {
    dispatch({ type: UPDATE_TIMELINE , payload: {
      step: 3,
      uploadVideoInfo: {
        uploadProgress: 100,
        name: formdata.title
      },
      uploadVideoStatus: 'MODIFY'
    }})
  }).catch(error => console.log(error));
}

export const onFocus = () => (dispatch, getState) => {
  dispatch({ type: UPDATE_FOCUS, focused: true })
}

export const onFocusOut = () => (dispatch, getState) => {
  dispatch({ type: UPDATE_FOCUS, focused: false })
}

// 정보입력 - sections, stacks, events 편집 취소
export const overrideMetaData = () => (dispatch, getState) => {
  dispatch({ type: OVERRIDE_METADATA });
  stepUpdate('direct', 2)(dispatch, getState);
}

// 정보입력 - 폼컨트롤
export const updateFormData = (key, value) => (dispatch, getState) => {
  const formdata = getState().make.formdata;
  const exec = {
    image: (file) => {
      const URL = window.URL || window.webkitURL;
      const tempUrl = URL.createObjectURL(file);
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, thumbnail: tempUrl }});
      getTempImages(file, dispatch)
      .then((images) => {
        dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, thumbnail: images[0] }});
      }).catch(err => Error(err));
    },
    title: (value) => {
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, title: value }});
    },
    description: (value) => {
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, description: value }});
    },
    categoryIdx: ({result ,recommendTags}) => {
      let tags = (recommendTags !== '') ? recommendTags.split(',') : [];
      const { categoryIdx, mainCategoryIdx, smallCategoryIdx } = result;
      dispatch({ type: UPDATE_CATEGORIES, mainCategoryIdx, smallCategoryIdx});
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, categoryIdx, recommendTags: tags }});
    },
    tag: (value) => {
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, tag: value }});
    },
    accountStatus: (value) => {
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, accountStatus: value }});
    },
    publicRange: (value) => {
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, publicRange: value }});
    },
    publicUsers: (value) => {
      dispatch({ type: UPDATE_FORMDATA, payload: {...formdata, publicUsers: value }});
    },
  }
  exec[key](value);
}

// 임시보관함에 있는 비디오를 게시 하기 위해 메타정보를 셋한다.
export const uploadVideoInfoDataSet = (timelineIdx) => (dispatch, getState) => {
  axios
  .get(`/temp-timelines/${timelineIdx}`)
  .then(result => {
    dispatch({ type: UPLOAD_VIDEOINFO_DATA_SET, uploadVideoInfo: result.data });
    videoPublish()(dispatch, getState);
  })
  .catch(error => console.log(error));
}


// 영상 선택 - h263미지원
export const H263NotSupported = () => (dispatch, getState) => {
  toastr.light('', T.translate('make.not-support-error'));
  dispatch({ type: H263NOT_SUPPORTED })
}

// 영상 선택 - 비디오 소스를 업데이트 한다.
export const updateVideoSource = (sourceObj) => (dispatch, getState) =>
  dispatch({ type: UPDATE_VIDEO_SOURCE, payload: sourceObj })

// 영상 선택 - 유튜브 영상 정보 호출
export const extractYoutubeDuration = (url) => (dispatch, getState) => {
  const googleYotubeApiV3Origin = 'https://www.googleapis.com/youtube/v3';
  let extractedId = '';
  if (url.includes('youtu.be')) {
    const splitedUrl = url.split('/');
    const lastValue = splitedUrl[splitedUrl.length - 1];
    extractedId = lastValue;
  } else {
    extractedId = url.split('v=')[1].split('&')[0];
  }

  return axios.get(`${googleYotubeApiV3Origin}/videos`, {
    params: {
      part: 'contentDetails, snippet',
      id: extractedId,
      key: 'AIzaSyCpBE_wzdh4CyAP4aw0E9tUN0tLOgxIqZo',
    }})
    .then(res => {
      // if(res.data.items[0].contentDetails.licensedContent == true){
      //   toastr.light('', T.translate('make.copyright-error'));
      //   return new Error ('error');
      // }
      if (res.data.items[0].snippet.liveBroadcastContent === 'live') {
         toastr.light('', T.translate('make.live-error'));
         return new Error ('error');
      }
        // const responseThumbnail = res.data.items[0].snippet.thumbnails.medium.url;
        const responseDuration = res.data.items[0].contentDetails.duration;
        const duration = YTDurationToSeconds(responseDuration);
        initPlayer(true, duration)(dispatch, getState);
        const formdata = getState().make.formdata;
        dispatch({
          type: UPDATE_VIDEO_THUMBNAIL,
          payload: {...formdata},
          selectVideoStepDone: true,
        });
    });
}
// 영상 선택 - 유튜브 플레이 타임을 추출 한다 type:PT14M32S
function YTDurationToSeconds(duration) {
  const match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/).slice(1).map((x) => {
    if (x === undefined) return 0;
    return x.replace(/\D/, '');
  });
  const hours = (parseInt(match[0], 10) || 0);
  const minutes = (parseInt(match[1], 10) || 0);
  const seconds = (parseInt(match[2], 10) || 0);
  return hours * 3600 + minutes * 60 + seconds;
}

export const updateSectionName = ({ target }, idx) => (dispatch, getState) => {
  const sections = getState().make.sections;
  sections[idx].name = target.value;
  if (target.value.length >= 20) {
    toastr.light('', T.translate('make.length-error'))
  }
  dispatch({ type: UPDATE_SECTION_NAME, payload: sections });
};
// 섹션 나누기
export const splitSection = () => (dispatch, getState) => {
  const currentTime = Math.floor(getState().player.currentTime);
  const duration = Math.floor(getState().player.duration);
  const makeSections = getState().make.sections;
  const prevTime = currentTime - 5;
  const nextTime = currentTime + 5;

  if (
    duration <= 10 ||
    currentTime === duration ||
    makeSections.some(item => prevTime <= item.begin && item.begin <= nextTime)
  ) {
    dispatch({ type: SPLIT_SECTION_FAILED });
    toastr.light('', T.translate('make.section-least-error'));
  } /*else if (makeSections.length === 1 && currentTime >= 5) {
    makeSections.push({
      sectionIdx: -1,
      begin: currentTime,
      name: `섹션명${makeSections.length + 1}`
    });
    dispatch({ type: SPLIT_SECTION, payload: beforeAtSort(makeSections) });
  } */else {

    makeSections.push({
      sectionIdx: -1,
      begin: currentTime,
      name: `${T.translate('make.section-name')}${makeSections.length + 1}`
    });

    dispatch({ type: SPLIT_SECTION, payload: beforeAtSort(makeSections) });
  }
};

// 영상 업로드 취소
export const cancelVideoUpload = () => (dispatch, getState) => {
    const { serverStatus } = getState().app;
    const dispatchServer = {
      AWS: () =>  dispatch({ type: CANCEL_AWS_UPLOAD }),
      AZURE: () => dispatch({ type: CANCEL_AZURE_UPLOAD }),
    }
    dispatchServer[serverStatus]();
};
// 섹션 결합
export const onCombineSection = () => (dispatch, getState) => {
  const sections = getState().make.sections;
  const currentTime = Math.floor(getState().player.currentTime);
  const {duration} = getState().player;
  const rangeTick = duration < 60 ? 2 : Math.floor(duration * 0.03);

  sections.forEach((item, idx) => {
    const possibleCombineRange = Array.from(
      { length: rangeTick * 2 },
      (dump, idx, begin = -rangeTick) => item.begin + (begin + idx)
    );
    if (possibleCombineRange.some(num => num === currentTime)) {
      sections.splice(idx, 1);
    }
  });
  dispatch({ type: COMBINE_SECTION, payload: sections });
};
/**
 * 스텍 나누기
 * 1. 스텍은 이미 생성된 구간내에서 생성이 되지 않는다.
 * 2. 스텍의 시작값은 (현재시간 - 조정값) 종료값은 (현재시간 + 조정값) 이다.
 * 3. 현재시간이 초기값이거나 시작값이 음수일때 시작값은 0, 종료값은 조정값 * 2 이다.
 * 4. 현재시간이 영상의 최종길이와 일치하면 시작값은 최종길이 - 조정값 * 2  종료값은 최종길이 이다.
 * 5. 현재시간에 조정값을 더한값이 전체 영상길이보다 크거나 같으면 종료값은 영상길이 이다.
 */
export const splitStack = () => (dispatch, getState) => {
  const currentTime = Math.floor(getState().player.currentTime);
  const duration = Math.floor(getState().player.duration);
  const rateTimeValue = duration <= 60 ? 1 : Math.round(duration * 0.05);
  const sections = getState().make.sections;
  const stacks = getState().make.stacks;

  const end = currentTime + rateTimeValue *2;
  const begin = currentTime;


  if (stacks.every((stack) => {
    if (stack.begin <= end && begin <= stack.begin) return false;
    else if (begin <= stack.end && stack.end <= end) return false;
    else if (stack.begin <= begin && end <= stack.end) return false;
    else if (begin <= stack.begin && stack.end <= end) return false;

    return true;
  })) {
    const pushObj = {
      stackIdx: -1,
      begin: currentTime ,
      end: currentTime + rateTimeValue * 2,
      tag: "static",
      name: "",
      cards: []
    };

    if (currentTime === duration) {
      pushObj.begin = duration - rateTimeValue * 2;
      pushObj.end = duration;
    } else if (currentTime + rateTimeValue * 2 >= duration) {
      pushObj.end = duration;
    }
    stacks.push(pushObj);
  } else {
    toastr.light(T.translate('make.additional-error'));
  }

  dispatch({ type: SPLIT_STACK, payload: beforeAtSort(stacks) });
  window.player.pause();
};

export const createOpenGraph = (isUrl, typedUrl, currentTime) => (dispatch, getState) => {
  if(isUrl) {
    const theTime = currentTime;
    const stacks = getState().make.stacks;
    const index = stacks.findIndex(item => (item.begin <= theTime && theTime <= item.end));
    const data = new FormData();
    data.append('url', typedUrl);

    const linkModel = (title = '', thumbUrl = '', url = typedUrl, desc = '') => ({
      cardIdx: -1,
      type: "LINK",
      orderValue: stacks[index].cards.length,
      ratingValue: 0,
      contents: {
        title: title,
        thumbUrl: thumbUrl,
        url: url,
        desc,
      },
    });

    axios
    .post('/open-graph', data)
    .then(res => {
      stacks[index].cards.push(linkModel(
        res.data["title"] ? res.data["title"] : res.data["og:title"],
        res.data["image"] ? res.data["image"] : res.data["og:image"],
        typedUrl, // res.data["url"] ? res.data["url"] : res.data["og:url"],
        res.data["description"] ? res.data["description"] : res.data["og:description"]));
      dispatch({ type: CREATE_STACK_IN_CARD_LINK, payload: stacks });
      closePopup()(dispatch);
     })
    .catch(error => {
        stacks[index].cards.push(linkModel());
        dispatch({ type: CREATE_STACK_IN_CARD_LINK, payload: stacks });
        closePopup()(dispatch);
    });
  } else {
    toastr.warning(T.translate('make.notification'), T.translate('make.url-error'));
  }
};

export const detachStack = idx => (dispatch, getState) => {
  const stacks = getState().make.stacks;
  stacks.splice(idx, 1);
  dispatch({ type: DETACH_STACK, payload: stacks });
};

export const detachStackInCard = (cidx, idx, event) => (dispatch, getState) => {
  event.stopPropagation()
  event.preventDefault()
  const stacks = getState().make.stacks;
  stacks[cidx].cards.splice(idx, 1);
  stacks[cidx].cards.forEach((item, idx) => {
    stacks[cidx].cards[idx].orderValue = idx;
  });
  onFocusOut()(dispatch, getState);
  dispatch({ type: DETACH_STACK_IN_CARD, payload: stacks });
};

export const onSortEndStackInCard = (cidx, oldIndex, newIndex) => (
  dispatch,
  getState
) => {

  const stacks = getState().make.stacks;
  stacks[cidx].cards = arrayMove(stacks[cidx].cards, oldIndex, newIndex);
  stacks[cidx].cards.forEach((item, idx) => {
    stacks[cidx].cards[idx].orderValue = idx;
  });

  dispatch({ type: SORT_STACK_IN_CARD, payload: stacks });
};

export const onChangeCardTypeText = (value, cidx, idx) => (
  dispatch,
  getState
) => {
  const stacks = getState().make.stacks;
  stacks[cidx].cards[idx].contents.text = value;
  dispatch({ type: UPDATE_STACK_IN_CARD_TEXT, payload: stacks });
};

export const updateStackRange = stacks => (dispatch, getState) => {
  dispatch({ type: UPDATE_STACK_RANGE, payload: stacks });
};

export const onChangeStackName = (value, idx) => (dispatch, getState) => {
  const stacks = getState().make.stacks;
  if (value.length >= 20) {
    toastr.light('', T.translate('make.length-error'));
  }
  stacks[idx].name = value;
  dispatch({ type: UPDATE_STACK_NAME, payload: stacks });
};

export const attachMapCard = (currentTime, place) => {
  return (dispatch, getState) => {
    const curTime = Math.floor(currentTime);
    const stacks = getState().make.stacks;
    const index = stacks.findIndex(
      item => item.begin <= curTime && curTime <= item.end
    );
    stacks[index].cards.push({
      cardIdx: -1,
      type: "MAP",
      orderValue: stacks[index].cards.length,
      ratingValue: 0,
      contents: {
        latitude: place.geometry.location.lat(),
        longitude: place.geometry.location.lng(),
        name: place.name,
        description: place.formatted_address
      }
    });
    dispatch({ type: ATTACH_CARD, payload: stacks });
  };
};

export const metadataToTemp = () => (dispatch, getState) => {
  dispatch({ type: METADATA_TO_TEMP });
}
//덧지식 추가
export const attachCard = (type, file, staticCurrentTime) => (
  dispatch,
  getState
) => {
  const stacks = getState().make.stacks;
  const currentTime = Math.floor(getState().player.currentTime);
  const index = stacks.findIndex(
    item => item.begin <= currentTime && currentTime <= item.end
  );
  const lastIndex = stacks[index].cards.length;
  const resultObj = {
    cardIdx: -1,
    type: type,
    orderValue: stacks[index].cards.length,
    ratingValue: 0
  };
  switch (type) {
    case 'TEXT': resultObj.contents = Cards.textCard(); break;
    case 'IMAGE': {
      const URL = window.URL || window.webkitURL;
      const limitCount = 10;
      const filesCountLimit = file.length > limitCount;
      if (filesCountLimit) toastr.light('', T.translate('make.image-number-error'))
      const files = filesCountLimit ? Array.prototype.slice.call(file, 0, 10) : file
      const thumbnails = Object.keys(files).map(idx => ({
        orderValue: +idx,
        thumbnail: URL.createObjectURL(files[idx])
      }))
      resultObj.contents = Cards.imageCardStack(thumbnails);
      updateMultiImageFile(dispatch, getState, files, index, lastIndex);
      break;
    }
    case 'FILE': {
      const URL = window.URL || window.webkitURL;
      const previewUrl = [
        { path: URL.createObjectURL(file), size: file.size, name: file.name }
      ];
      resultObj.contents = { files: previewUrl };
      attachFileUpload(dispatch, file, index, lastIndex);
      break;
    }
    case 'QUIZ': resultObj.contents = Cards.quizCardShort(); break;
    case 'VOTE': resultObj.contents = Cards.voteCard(); break;
    default: throw Error('unexpect');
  }
  stacks[index].cards.push(resultObj);
  onFocusOut()(dispatch, getState);
  dispatch({ type: ATTACH_CARD, payload: stacks });
};

export const updateStackName = sections => (dispatch, getState) => {
  dispatch({ type: UPDATE_SECTION_NAME, payload: sections });
};

// 이벤트 추가
export const attachEvent = (type, file, currentTimeSec) => (
  dispatch,
  getState
) => {
  const events = getState().make.events;
  const currentTime = Math.floor(currentTimeSec);
  if (!events.every(item => item.begin !== currentTime)) {
    return;
  }
  const resultObj = {
    eventIdx: -1,
    begin: currentTime,
    type: type,
  };
  switch (type) {
    case 'TEXT': resultObj.contents = Cards.textCard(); break;
    case 'IMAGE':{
      const URL = window.URL || window.webkitURL;
      const previewUrl = URL.createObjectURL(file);
      resultObj.contents = Cards.imageCardEvent(previewUrl);
      resultObj.contents.tempUrl = previewUrl;
      setTimeout(() => {
        const lastIndex = events.length - 1;
        updateSingleImageFile(dispatch, getState, file, lastIndex);
      }, 1500);
      break;
    }
    case 'QUIZ': resultObj.contents = Cards.quizCardShort(); break;
    default: throw Error('unexpected');
  }
  events.push(resultObj);
  events.sort((a, b) => { if (a.begin > b.begin) return 1; else return -1;})
  dispatch({ type: ATTACH_EVENT, payload: events });
};

export const detachEvent = idx => (dispatch, getState) => {
  const events = getState().make.events;
  events.splice(idx, 1);
  dispatch({ type: DETACH_EVENT, payload: events });
};

// 스텍에 있는 퀴즈 카드의 상태를 갱신
export const deleteQuizCardAtStack = (event, idx, cardIndex , obj) => (dispatch, getState) => {
  event.stopPropagation()
  event.preventDefault()
  const stacks = getState().make.stacks;
  const prevQuizContents = Object.assign({}, stacks[cardIndex].cards[idx].contents);
  stacks[cardIndex].cards[idx].contents = obj;
  dispatch({ type: UPDATE_QUIZ_CARD_AT_STACKS, stacks });
  if (prevQuizContents.type !== obj.type) {
    onFocusOut()(dispatch, getState);
  }
}

// 스텍에 있는 퀴즈 카드의 상태를 갱신
export const updateQuizCardAtStack = (obj, idx, cardIndex ) => (dispatch, getState) => {

  const stacks = getState().make.stacks;
  const prevQuizContents = Object.assign({}, stacks[cardIndex].cards[idx].contents);
  stacks[cardIndex].cards[idx].contents = obj;
  dispatch({ type: UPDATE_QUIZ_CARD_AT_STACKS, stacks });
  if (prevQuizContents.type !== obj.type) {
    onFocusOut()(dispatch, getState);
  }
}
// 이벤트에 있는 퀴즈 카드의 상태를 갱신
export const updateQuizCardAtEvents = (obj, idx) => (dispatch, getState) => {
  const events = getState().make.events;
  events[idx].contents = obj;
  dispatch({ type: UPDATE_QUIZ_CARD_AT_EVENTS, events });
}
// 스텍에 있는 투표 카드의 상태를 갱신
export const updateVoteCardAtStack = (obj, idx, cardIndex) => (dispatch, getState) => {
  const stacks = getState().make.stacks;
  stacks[cardIndex].cards[idx].contents = obj;
  dispatch({ type: UPDATE_VOTE_CARD_AT_STACKS, stacks });
}

export const onChangeEventTypeText = (value, idx) => (dispatch, getState) => {
  const events = getState().make.events;
  events[idx].contents.text = value;
  dispatch({ type: UPDATE_EVENT_IN_CARD_TEXT, payload: events });
}


/**
 * reducer *
 */
export default (state = initialState, action) => {
  switch (action.type) {
    case UPLOAD_VIDEOINFO_DATA_SET: {
      return {
        ...state,
        uploadVideoInfo: {
          ...state.uploadVideoInfo,
          ...action.uploadVideoInfo
        }
      };
    }
    case UPDATE_TIMELINE: {
      return {
        ...state,
        ...action.payload,
      }
    }
    case UPDATE_DETAIL_DATA: {
      return {
        ...state,
        ...action.payload,
      }
    }
    case UPDATE_METADATA:
      return {
        ...state,
        ...action.payload
      };
    case GET_SUB_CATEGORIES:
      return {
        ...state,
        ...action.payload
      };
    case UPDATE_FORMDATA: {
      return {
        ...state,
        formdata: action.payload,
      };
    }
    case UPDATE_CATEGORIES: {
      const { mainCategoryIdx, smallCategoryIdx } = action;
      return {
        ...state,
        mainCategoryIdx,
        smallCategoryIdx,
      }
    }
    case METADATA_TO_TEMP: {
      const tempMetaData = {
        sections: state.sections.slice(0),
        stacks: state.stacks.slice(0),
        events: state.events.slice(0),
      };
      return {
        ...state,
        tempMetaData,
      };
    }
    case UPDATE_STEP: {
      return {
        ...state,
        step: action.payload.step,
      };
    }
    case UPLOADING_VIDEO: {
      return {
        ...state,
        S3ManagedUpload: action.S3ManagedUpload,
        uploadVideoInfo: Object.assign({} ,action.uploadVideoInfo),
        videoSource: action.videoSource,
        uploadVideoStatus: action.uploadVideoStatus
      };
    }
    case UPLOADING_AZURE_PROGRESS: {
      return {
        ...state,
        azureUploadCancel: action.azureUploadCancel,
        uploadVideoInfo: Object.assign({} ,action.uploadVideoInfo),
        uploadVideoStatus: action.uploadVideoStatus
      };
    }
    case COMPLETE_UPLOAD_VIDEO:
      return {
        ...state,
        uploadVideoInfo: action.uploadVideoInfo,
        uploadVideoStatus: action.uploadVideoStatus,
      };
    case CANCEL_AWS_UPLOAD: {
      return {
        ...state,
        uploadVideoStatus: "CANCEL"
      }
    }
    case CANCEL_AZURE_UPLOAD: {
       return {
        ...state,
        azureUploadCancel: true,
        uploadVideoStatus: "CANCEL"
      };
    }
    case INIT_UPLOAD_VIDEO:
      return {
        ...initialState,
        S3ManagedUpload: undefined,
        uploadVideoInfo: {
          uploadProgress: 0
        },
        uploadVideoStatus: "READY",
        sections: [
          {
            begin: 0,
            name: T.translate('make.section-name')+"1"
          }
        ],
        stacks: [],
        events: [],
      };
    case SPLIT_SECTION:
    case COMBINE_SECTION:
    case UPDATE_SECTION_NAME:
      return {
        ...state,
        sections: [...action.payload]
      };
    case SPLIT_STACK:
    case DETACH_STACK:
    case DETACH_STACK_IN_CARD:
    case SORT_STACK_IN_CARD:
    case UPDATE_STACK_IN_CARD_TEXT:
    case CREATE_STACK_IN_CARD_LINK:
    case UPDATE_STACK_NAME:
    case UPDATE_STACK_RANGE:
    case ATTACH_CARD:
      return {
        ...state,
        stacks: [...action.payload],
      };
    case UPDATE_EVENT_IN_CARD_TEXT:
    case ATTACH_EVENT:
    case DETACH_EVENT: {
      return {
        ...state,
        events: [...action.payload]
      };
    }
    case UPDATE_QUIZ_CARD_AT_STACKS:
      return {
        ...state,
        stacks:[...action.stacks],
      }
    case UPDATE_QUIZ_CARD_AT_EVENTS:
      return {
        ...state,
        ...action.payload,
        formdata: {
          ...state.formdata,
        }
      };
    case GET_SUB_CATEGORIES_SUCCESS: {
      return {
        ...state,
        ...action.payload,
        formdata: {
          ...state.formdata,
        }
      };
    }
    case GET_CATEGORIES_SUCCESS: {
      return {
        ...state,
        categories: action.categories
      }
    }

    case UPDATE_VOTE_CARD_AT_STACKS:
      return {
        ...state,
        stacks: [...action.stacks],
      }
    case UPDATE_VIDEO_SOURCE:
      return {
        ...state,
        videoSource: action.payload,
        selectVideoStepDone : action.payload ? true : false
      };

    case UPDATE_VIDEO_THUMBNAIL:
      return {
        ...state,
        formdata: action.payload,
        selectVideoStepDone: action.selectVideoStepDone,
      };
    case FAILED_UPDATE_VIDEO_THUMBNAIL: {
      return {
        ...state,
        selectVideoStepDone: action.selectVideoStepDone,
      }
    }
    case UPDATE_IMAGE_CARD_AT_EVENTS:
      return {
        ...state,
        events: [...action.events],
      };
    case UPDATE_IMAGE_CARD_AT_STACKS: {
      return {
        ...state,
        stacks: [...action.stacks],
      };
    }
    case UPDATE_FILE_CARD_AT_STACKS: {
      const stacks = Object.assign([], state.stacks);
      const { filePath, size, name, stackIndex, lastIndex } = action.payload;
      stacks[stackIndex].cards[lastIndex].contents = { files: [{ path: filePath, name, size }] };
      return {
        ...state,
        stacks,
      };
    }
    case OVERRIDE_METADATA: {
      return {
        ...state,
        sections: state.tempMetaData.sections,
        stacks: state.tempMetaData.stacks,
        events: state.tempMetaData.events,
      }
    }
    case UPLOADING_REQUEST:
    return {
      ...state,
      uploading: true,
    };

    case UPLOADING_SUCCESS:
    return {
      ...state,
      uploading: false,
    };
    case UPDATE_FOCUS:
    return {
      ...state,
      focused: action.focused,
    }
    case FAILED_IMAGE_CARD_AT_EVENTS: {
      const events = Object.assign([], state.events);
      const { lastIndex } = action;
      delete events[lastIndex];
      return {
        ...state,
        events,
      }
    }
    case FAILED_IMAGE_CARD_AT_STACKS: {
      const stacks = Object.assign([], state.stacks);
      const { stackIndex, lastIndex } = action;
      delete stacks[stackIndex].cards[lastIndex];
      return {
        ...state,
        stacks,
      }
    }
    case H263NOT_SUPPORTED:
    case SPLIT_SECTION_FAILED:
    case SPLIT_STACK_FAILED:
    default:
      return state;
  }
};
