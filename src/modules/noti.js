import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import {
    isLocker,
    axios,
  } from '../util/api';
/*

api 문서 푸시 내용입니다.

*/
const GET_NOTIFICATIONS = 'noti/GET_NOTIFICATIONS';
const GET_NOTIFICATION_COUNT = 'noti/GET_NOTIFICATION_COUNT';
const PUT_NOTIFICATIONS = 'noti/PUT_NOTIFICATIONS';
const PUT_VIDEO_SEND_STATUS = 'noti/PUT_VIDEO_SEND_STATUS';
const PUT_FOLDER_RESPOND = 'noti/PUT_FOLDER_RESPOND';
const PUT_CONFIRM_NOTIFICATION = 'noti/PUT_CONFIRM_NOTIFICATION';

const GET_NOTICES = 'noti/GET_NOTICES';

export const getNotifications = createAction(GET_NOTIFICATIONS, (req)=>{ 
  return axios.get(`/users/notifications`,{params:req})
},(req)=>(req));
export const getNotificationCount = createAction(GET_NOTIFICATION_COUNT, (req)=>{ 
  return axios.get(`/users/notification-count`,{params:req})
});
export const putNotifications = createAction(PUT_NOTIFICATIONS, (req)=>{ 
  return axios.put(`/users/notifications/${req.idx}`)
});
export const putVideoSendStatus = createAction(PUT_VIDEO_SEND_STATUS, (req)=>{ 
  return axios.put(`/temp-timelines/send-status`,req)
});
export const putFolderRespond = createAction(PUT_FOLDER_RESPOND, (req)=>{ 
  return axios.put(`/folders/${req.folderIdx}/${req.inviteGroupFolderIdx}/respond`,req)
});
export const putConfirmNotification = createAction(PUT_CONFIRM_NOTIFICATION, (req)=>{ 
  return axios.put(`/users/confirm-notification/${req.idx}`)
});
export const getNotices = createAction(GET_NOTICES, (req)=>{ 
  const query={
    pageNum:req.pageNum,
    pageSize : req.pageSize
  }
  return axios.get(`/notices`,{params : query})
},(req)=>({req}));



const initialState = Map({
  notificationsResult:{
    I:{},
    G:{}
  },
  notificationCountResult:{
    general: 0,
    important: 0,
    sum: 0
  },
  noticesData:{
    notices:[],
    noticesCount:0
  }
})

const reducer = handleActions({
  // 다른 일반 액션들을 관리..
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_NOTIFICATIONS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      const notiType = action.meta.notiType;
      let notiResult = state.get('notificationsResult');
      notiResult[notiType] = Object.assign(action.payload.data)
      return state.set('notificationsResult', Object.assign({},notiResult))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_NOTIFICATION_COUNT,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('notificationCountResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: PUT_NOTIFICATIONS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: PUT_VIDEO_SEND_STATUS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      return state.set('notificationCountResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: PUT_FOLDER_RESPOND,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_NOTICES,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      action.payload.data.req= action.meta.req
      return state.set('noticesData', Object.assign({},action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  }
]);
