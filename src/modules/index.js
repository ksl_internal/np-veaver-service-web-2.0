import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as toastrReducer} from 'react-redux-toastr';
import {penderReducer} from 'redux-pender';
import app from './app';
import mypage from './mypage'
import auth from './auth';
import user from './user';
import error from './error';
import portal from './portal';
import player from './player';
import make from './make';
import popup from './popup';
import reaction from './reaction';
import contact from './contact';
import searchM from './searchM';
import folderAndPin from './folderAndPin';
import noti from './noti';
import profile from './profile';
import bookmark from './bookmark';
import timelines from './timelines';
import video from './video';
import language from './language';
import navbar from './navbar';


export default combineReducers({
  routing: routerReducer,
  toastr: toastrReducer,
  pender: penderReducer,
  mypage,
  app,
  auth,
  user,
  error,
  portal,
  player,
  make,
  popup,
  reaction,
  contact,
  searchM,
  folderAndPin,
  noti,
  profile,
  bookmark,
  timelines,
  video,
  language,
  navbar
});
