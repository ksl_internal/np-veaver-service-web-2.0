import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import { convertTimelines } from '../util/time';
import {
    isLocker,
    axios,
  } from '../util/api';

/*

api 문서 폴더 및 핀 내용입니다

*/


const GET_FOLDERS = 'folder/GET_FOLDERS';
const GET_FOLDERS_ID = 'folder/GET_FOLDERS_ID';
const POST_FOLDERS = 'folder/POST_FOLDERS';
const POST_PIN = 'folder/POST_PIN';
const GET_FOLDERS_ID_USERS = 'folder/GET_FOLDERS_ID_USERS';
const CURRENT_INFO = 'folder/CURRENT_INFO';
const PUT_FOLDER_ID_LEAVE = 'folder/PUT_FOLDER_ID_LEAVE';
const POST_FOLDER_ID_INVITE = 'folder/POST_FOLDER_ID_INVITE';
const UPDATE_SELECTED_ITEMS = 'folder/UPDATE_SELECTED_ITEMS';
const DELETE_FOLDERS_ID_PIN = 'folder/DELETE_FOLDERS_ID_PIN';
function _getFolders() {
  return axios.get(`/folders`)
}
function _getFoldersId(req,attachMode=false) {
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  return axios.get(`/folders/${req.id}`,{params:req})
}
function _postFolder(req) {
  return axios.post(`/folders`,req)
}
function _postPin(req) {
  return axios.post(`/folders/${req.id}`,req)
}
function _getFoldersIdUsers(req) {
  return axios.get(`/folders/${req.id}/users`)
}
function _currentInfo(folder) {
  return new Promise(function (resolve, reject) {
    resolve(folder);
  });
}

export const getFolders = createAction(GET_FOLDERS, _getFolders);
export const getFoldersId = createAction(GET_FOLDERS_ID, _getFoldersId,(req,attachMode=false)=>( { req ,attachMode}  ));
export const postFolders = createAction(POST_FOLDERS, _postFolder);
export const postPin = createAction(POST_PIN, _postPin);
export const getFoldersIdUsers = createAction(GET_FOLDERS_ID_USERS, _getFoldersIdUsers);
export const currentInfo = createAction(CURRENT_INFO, _currentInfo);

export const putFolderIdLeave = createAction(PUT_FOLDER_ID_LEAVE, (req)=>{
  return axios.put(`/folders/${req.folderIdx}/leave`)
});
export const postFolderIdInvite = createAction(POST_FOLDER_ID_INVITE, (req)=>{
  return axios.post(`/folders/${req.folderIdx}/invite`,req)
});
export const deleteFolderIdPin = createAction(DELETE_FOLDERS_ID_PIN, (req)=>{
  return axios.delete(`/folders/${req.folderIdx}`,{data:req})
},(req)=>({req}));
export const updateSelectedItems = createAction(UPDATE_SELECTED_ITEMS, (data)=>{ 
  return new Promise(function (resolve, reject) {
    resolve(data);
  });
});


const initialState = Map({
  folders:null,
  pins:null,
  users:null,
  postFoldersResult:null,
  postPinResult:null,
  currentInfo:null,
  selectedItems:{ //선택된 아이템
    idx:[],  // 리터널 형식 문자열
    isChanging:false,
    originObj:[] // 실제 선택된 객체 자제 
  },
})

const reducer = handleActions({
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_FOLDERS,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => { 
      let currentInfo;
      if(action.payload.data.folders.length > 0){
        currentInfo = action.payload.data.folders[0]
      }
      return state.set('folders', action.payload.data).set('currentInfo',currentInfo)
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_FOLDERS,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => { 
      return state.set('postFoldersResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: POST_PIN,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => { 
      return state.set('postPinResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_FOLDERS_ID,
    onPending: (state, action) => {
        return state;
    }, 
    onSuccess: (state, action) => {
      const timelines = action.payload.data.pins.map(pin=>{
        pin.timeline.pinInfo = pin
        return pin.timeline
      })
      const result = state.get('pins');
      action.payload.data.timelines = action.meta.attachMode ? result.timelines.concat(convertTimelines(timelines)) :convertTimelines(timelines)
      const isHideMoreLayer = Math.ceil(action.payload.data.timelinesCount / 20) === action.meta.req.pageNum      
      return state.set('pins',Object.assign({} ,action.payload.data) ).set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('selectedItems', Object.assign({idx:[],isChanging:false,originObj:[]}))
    },
    onFailure: (state, action) => {
        return state;
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_FOLDERS_ID_USERS,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => {
      return state.set('users', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: CURRENT_INFO,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => { 
      return state.set('currentInfo', action.payload)
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: UPDATE_SELECTED_ITEMS,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => { 
      return state.set('selectedItems', Object.assign({},action.payload))
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: DELETE_FOLDERS_ID_PIN,
    onPending: (state, action) => {
        return state; 
    }, 
    onSuccess: (state, action) => { 
      getFoldersId({id:action.meta.req.id},false);
      getFoldersIdUsers({id:action.meta.req.id});
      return state
    },
    onFailure: (state, action) => {
        return state; 
    },
    onCancel: (state, action) => {
      return state
    }
  },
]);
