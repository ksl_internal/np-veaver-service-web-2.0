import {
  isLocker,
  axios,
} from '../util/api';


export const PLAYER_LOADED = 'player/PLAYER_LOADED';
export const PLAYER_TIMELINE_TICK = 'player/PLAYER_TIMELINE_TICK';
export const PLAYER_CURRENT_CHANGED = 'player/PLAYER_CURRENT_CHANGED';
export const PLAYER_TIMELINE_LOADED = 'player/PLAYER_TIMELINE_LOADED';
export const PLAYER_LIKE_REACTION = 'player/PLAYER_LIKE_REACTION';
export const PLAYER_COMMENT_REACTION = 'player/PLAYER_COMMENT_REACTION';
export const PLAYER_ACTIVE_COMMENT_ICON= 'player/PLAYER_ACTIVE_COMMENT_ICON';
export const PLAYER_STACK_MOVED = 'player/PLAYER_STACK_MOVED';
export const PLAYER_STACK_NEED_MOVE = 'player/PLAYER_STACK_NEED_MOVE';
export const FETCH_TIMELINE_DETAIL = 'player/FETCH_TIMELINE_DETAIL';
export const FETCH_TIMELINE_DETAIL_SUCCESS = 'player/FETCH_TIMELINE_DETAIL_SUCCESS';
export const FETCH_TIMELINE_DETAIL_FAILED = 'player/FETCH_TIMELINE_DETAIL_FAILED';
export const ADD_VOTE_EXAMPLE = 'player/ADD_VOTE_EXAMPLE';
export const TIMELINE_LIKE_COUNT = 'player/TIMELINE_LIKE_COUNT';
export const TIMELINE_COMMENT_COUNT = 'player/TIMELINE_COMMENT_COUNT';
export const FIND_SCROLL_POSITION = 'player/FIND_SCROLL_POSITION';
export const CLEAR_PLAYER_DATA = 'player/CLEAR_PLAYER_DATA';
export const SET_PLAYER_DATA = 'player/SET_PLAYER_DATA';
export const CHANGE_ACTIVE_INFO_TAB = 'player/CHANGE_ACTIVE_INFO_TAB';
export const RELOAD_DETAILS = 'player/RELOAD_DETAILS';
export const CHANGE_TIMELINE = 'player/CHANGE_TIMELINE';
export const PREVENT_SCROLL_FALSE = 'player/PREVENT_SCROLL_FALSE';

export const SET_PLAYER = 'player/SET_PLAYER';
const initialState = {
  currentTime: 0,
  duration: 0,
  mediaLoaded: false,
  commentCount: 0,
  likeCount: 0,
  likeReaction: false,
  commentReaction: false,
  activeCommentIcon: false,
  needMoveStack: false,
  addVoteEvent: false,
  scrolling: false,
  timelineDetail: {},
  videoDetail: {},
  viewVideoCategoryNames: '',
  activeTab: 'KNOWLEDGE',
  cachedTimelineIdx: 0,
  player: null,
  preventScroll: false,
}

/**
 * @description 컨테이너의 모든 정보를 셋팅 한다.
 * @param {*} timelineIdx 
 */
export const getDetails = (timelineIdx) => async (dispatch, getState) => {
  try {
    await axios.put(`/timelines/${timelineIdx}/view-count`);
  } catch(e) {
    console.error(e);
  }
    const { data } = await axios.get(`/${isLocker() ? 'temp-' : ''}timelines/${timelineIdx}`);
    const lastIndexOfSections = data.sections.length - 1;
    const sections = data.sections.map((section, idx, target) => {
      const end = lastIndexOfSections === idx ? data.playTime :  target[idx + 1].begin;
      const stacks = data.stacks.filter(stack => (section.begin <= stack.begin && stack.begin < end));
      return ({
        ...section,
        end,
        stacks,
      });
    });
    data.sections = sections;
    const video = await axios.get(`/videos/${data.videoIdx}`);
    dispatch({ type: SET_PLAYER_DATA, 
      timelineDetail: data,
      videoDetail: video.data, 
      viewVideoCategoryNames: data.videoCategories,
      cachedTimelineIdx: timelineIdx,
    });
}

export const clearDetails = (timelineIdx) => (dispatch, getState) => {
  dispatch({ type: CLEAR_PLAYER_DATA });
}

export const setPlayer = (player) => {
    return (dispatch) => {
        dispatch({ type: SET_PLAYER, player });
    }
}

  
export const getDetailsReload = () => (dispatch, getState) => {
  const { cachedTimelineIdx } = getState().player;
  dispatch({ type: 'RELOAD_DETAILS' })
  getDetails(cachedTimelineIdx)(dispatch, getState);
}

export const changeActiveInfoTab = (activeTab) =>
 (dispatch) => 
 new Promise((resolve, reject) => resolve(dispatch({ type: CHANGE_ACTIVE_INFO_TAB,  activeTab })));

export const initPlayer = (mediaLoaded, duration) => {
  return (dispatch) => {
    dispatch({ type: PLAYER_LOADED, mediaLoaded, duration });
  }
}

export const initTimeline = (seekbarWidth) => {
  return (dispatch) => {
    dispatch({ type: PLAYER_TIMELINE_LOADED });
  }
}

export const refreshCurrentTime = (currentTime) => {
  return (dispatch) =>{
    dispatch({ type: PLAYER_TIMELINE_TICK, currentTime });
  }
}

export const seekTimeline = (seekTime) => {
  return (dispatch) => {
    dispatch({ type: PLAYER_CURRENT_CHANGED, seekTime });
  }
}

export const changeTimeline = (seekTime) => {
    return (dispatch) => {
        dispatch({ type: CHANGE_TIMELINE, seekTime });
    }
}

export const preventScrollFalse = () => {
    return (dispatch) =>{
        dispatch({ type: PREVENT_SCROLL_FALSE });
    }
}

export const stackNeedMove = () => {
  console.log('stackNeedMove ');
    return (dispatch) =>{
        dispatch({ type: PLAYER_STACK_NEED_MOVE });
    }
}

export const stackMoved = () => {
  return (dispatch) =>{
    dispatch({ type: PLAYER_STACK_MOVED });
  }
}

export const likeReaction = (likeReaction) => {
  return (dispatch) =>{
    dispatch({ type: PLAYER_LIKE_REACTION, likeReaction });
  }
}

export const commentReaction = (commentReaction) => {
  return (dispatch) =>{
    dispatch( {type: PLAYER_COMMENT_REACTION, commentReaction});
  }
}

export const activeCommentIcon = (activeCommentIcon) => {
  return (dispatch) =>{
    dispatch( {type: PLAYER_ACTIVE_COMMENT_ICON, activeCommentIcon});
  }
}

export const findTabScrollPositionFlag = (activeFlag) => {
  return (dispatch) => {
    dispatch( {type: FIND_SCROLL_POSITION, activeFlag});
  }
}
/**
 * 내가 본 영상의 시간을 서버에 요청하여 저장합니다.
 */
export const setMyPlaytime = (timelineIdx,data) => async (dispatch, getState) => {
  try {
    // 총 재생된 시간 체크.
    let calculatingTime = 0
    try{
      for(let i in data.realPlaytimes){
        for(let j = data.realPlaytimes[i].begin; j < data.realPlaytimes[i].end; j++){
          calculatingTime++
        }
      }
    }catch(e){
      calculatingTime =0
    }
    data.realPlaytime = calculatingTime
    axios.put(`/timelines/${timelineIdx}/my-playtime`,data);
  } catch(e) {
    console.error(e);
  }
}

// export const onTimelineDetail = (timelineDetail) => {
//   console.log(timelineDetail);
//   return (dispatch, getState) => {
//     dispatch({type: FETCH_TIMELINE_DETAIL_SUCCESS, timelineDetail});
//   }
// }

export const onAddVoteExample = (status) => {
  return (dispatch, getState) => {
    dispatch({ type: ADD_VOTE_EXAMPLE, status });

    // const { timelineData } = getState().player;
    // // timelineData.sections.forEach(section => {
    //   timelineData.stacks.forEach(stack => {
    //     stack.cards.forEach(card => {
    //       if (card.cardIdx === Number(cardIdx)) {
    //         card.contents.answers.push({
    //           answerId,
    //           example,
    //           orderValue: card.contents.answers.length + 1,
    //         });
    //         console.log('card :: ', card);
    //       }
    //     })
    //   })
    // // })
  }
}


// 투표하기
export const onVote = (cardIdx, answerId) => (dispatch, getState) => {
  return axios.post(`timelines/vote-answers/${cardIdx}`, { answerId }).then(() => {
    getDetailsReload()(dispatch, getState);
  }).catch(err => Error(err));
}

// vote reload
export const onVoteReload = () => (dispatch, getState) => {
   getDetailsReload()(dispatch, getState);

}


export const refreshLikeCount = likeCount => dispatch => dispatch({ type: TIMELINE_LIKE_COUNT, likeCount });
export const refreshCommentCount = commentCount => dispatch => dispatch({ type: TIMELINE_COMMENT_COUNT, commentCount });

export default (state = initialState, action) => {
  // console.log(state, action);
  switch (action.type) {
    case SET_PLAYER_DATA: {
      const {  timelineDetail, videoDetail, viewVideoCategoryNames, cachedTimelineIdx} = action;
      return {
        ...state,
        timelineDetail,
        videoDetail,
        viewVideoCategoryNames,
        cachedTimelineIdx,
      };
    }
    case CLEAR_PLAYER_DATA: {
      return {
        ...state,
        timelineDetail: {},
        videoDetail: {}, 
        viewVideoCategoryNames: '',
        cachedTimelineIdx: 0,
        activeTab: 'KNOWLEDGE',
      };
    }
      case SET_PLAYER:
        return {
            ...state,
            player : action.player,
        }
    case PLAYER_LOADED:
      return {
        ...state,
        mediaLoaded: action.mediaLoaded,
        duration: action.duration,
      }
    case PLAYER_TIMELINE_LOADED:
      return {
        ...state,
        currentTime: 0,
      }
    case PLAYER_TIMELINE_TICK:
      return {
        ...state,
        needMoveStack: false,
        currentTime: action.currentTime
      }
    case PLAYER_CURRENT_CHANGED:
      return {
        ...state,
        currentTime: action.seekTime,
        needMoveStack: true,
      }
      case CHANGE_TIMELINE:
        console.log('change_timeline ',state);
        const {player, currentTime} = state;
        player.setCurrentTime(action.seekTime);
        return {
            ...state,
            currentTime: action.seekTime,
            needMoveStack: true,
            preventScroll : true,
        }
     case PREVENT_SCROLL_FALSE:
       return {
           ...state,
           preventScroll : false,
       }
      case PLAYER_STACK_NEED_MOVE:
        console.log('needMoveStack ',state);
        return {
            ...state,
            needMoveStack: true,

        }
    case PLAYER_STACK_MOVED:
      return {
        ...state,
        needMoveStack: false,
      }
    case PLAYER_LIKE_REACTION:
      return {
        ...state,
        likeReaction: action.likeReaction,
      }
    case PLAYER_COMMENT_REACTION:
      return {
        ...state,
        commentReaction: action.commentReaction,
      }
    case PLAYER_ACTIVE_COMMENT_ICON:
      return {
        ...state,
        activeCommentIcon: action.activeCommentIcon,
      }
    case FETCH_TIMELINE_DETAIL:
      return {
        ...state,
        loading: true,
      };
    case ADD_VOTE_EXAMPLE:
      const { status } = action;
      return {
        ...state,
        addVoteEvent: status,
      };
    // case FETCH_TIMELINE_DETAIL_SUCCESS:
    //   const { timelineDetail } = action;
    //   return {
    //     ...state,
    //     loading: false,
    //     timelineData: timelineDetail,
    //   }
    case FETCH_TIMELINE_DETAIL_FAILED:
      return {
        ...state,
        loading: false,
      }
    case TIMELINE_LIKE_COUNT:
      return {
        ...state,
        likeCount: action.likeCount,
      }
    case TIMELINE_COMMENT_COUNT:
      return {
        ...state,
        commentCount: action.commentCount,
      }
    case FIND_SCROLL_POSITION:
      return {
        ...state,
        scrolling: action.activeFlag,
      }
    case CHANGE_ACTIVE_INFO_TAB: {
      return {
        ...state,
        activeTab: action.activeTab,
      }
    }
    default:
      return state
  }
}
