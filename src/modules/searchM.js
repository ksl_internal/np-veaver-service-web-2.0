import {handleActions, createAction} from 'redux-actions';
import { applyPenders } from 'redux-pender';
import { Map, List, fromJS } from 'immutable';
import {
    isLocker,
    axios,
  } from '../util/api';
import { timeSince, convDuration, toDateStr } from '../util/time';

/*

api 문서 검색 내용입니다

*/

const GET_SEARCH_CATEGORIES_TIMELINE_COUNT = 'search/GET_SEARCH_CATEGORIES_TIMELINE_COUNT';
const GET_SEARCH_CATEGORIES_SECTION_COUNT = 'search/GET_SEARCH_CATEGORIES_SECTION_COUNT';
const GET_SEARCH_CATEGORIES_STACK_COUNT = 'search/GET_SEARCH_CATEGORIES_STACK_COUNT';
// const GET_SEARCH_CATEGORIES_TAG_COUNT = 'search/GET_SEARCH_CATEGORIES_TAG_COUNT';
const GET_SEARCH_CATEGORIES_USERS_COUNT = 'search/GET_SEARCH_CATEGORIES_USERS_COUNT';
const GET_SEARCH_CATEGORIES_CHILDREN = 'search/GET_SEARCH_CATEGORIES_CHILDREN';
const MODEL_UPDATE_CATEGORY = 'search/MODEL_UPDATE_CATEGORY';
const GET_SEARCH_TIMELINES = 'search/GET_SEARCH_TIMELINES';
const GET_SEARCH_TIMELINES_SECTION = 'search/GET_SEARCH_TIMELINES_SECTION';
const GET_SEARCH_TIMELINES_STACK = 'search/GET_SEARCH_TIMELINES_STACK';
const GET_SEARCH_TIMELINES_TAG = 'search/GET_SEARCH_TIMELINES_TAG';
const GET_SEARCH_TAG = 'search/GET_SEARCH_TAG';
const GET_SEARCH_USERS = 'search/GET_SEARCH_USERS';
const GET_SEARCH_USERS_DEPTH = 'search/GET_SEARCH_USERS_DEPTH';
const GET_SEARCH_USERS_KEYWORD = 'search/GET_SEARCH_USERS_KEYWORD';
const GET_SEARCH_USERS_KEYWORD_PAGE = 'search/GET_SEARCH_USERS_KEYWORD_PAGE';
const GET_SEARCH_USERS_ALL_DEPTH = 'search/GET_SEARCH_USERS_ALL_DEPTH';

function _getSearchCategoriesTimeline(req) {
  return axios.get(`/search/${req.id}/count`,{ params: req})
}
function _getSearchCategoriesSection(req) {
  return axios.get(`/search/${req.id}/count`,{ params: req})
}
function _getSearchCategoriesStack(req) {
  return axios.get(`/search/${req.id}/count`,{ params: req})
}
// function _getSearchCategoriesTag(req) {
//   return axios.get(`/search/${req.id}/count`,{ params: req})
// }
function _getSearchCategoriesChildren(req){
  return axios.get(`/search/${req.id}/count`,{ params: req})
}
function _getSearchTimelines(req,attachMode=false) {
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  query.from = attachMode ? req.pageNum * 20 : 0;
  delete query.pageNum
  return axios.get(`/search/timelines`,{ params: query})
}
function _getSearchTimelinesSection(req,attachMode=false) {
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  query.from = attachMode ? req.pageNum * 20 : 0;
  delete query.pageNum
  return axios.get(`/search/timelines/section`,{ params: query})
}
function _getSearchTimelinesStack(req,attachMode=false) {
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  query.from = attachMode ? req.pageNum * 20 : 0;
  delete query.pageNum
  return axios.get(`/search/timelines/stack`,{ params: query})
}
function _getSearchTimelinesTag(req,attachMode=false) {
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  query.from = attachMode ? req.pageNum * 20 : 0;
  delete query.pageNum
  return axios.get(`/search/timelines/tag`,{ params: query})
}
function _getSearchTag(req,attachMode=false) {
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  query.from = attachMode ? req.pageNum * 20 : 0;
  delete query.pageNum
  return axios.get(`/search/tag`,{ params: query})
}
function _getSearchUsers(req,attachMode=false) {
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 0;
  query.from = attachMode ? req.pageNum : 0;
  query.orderType='L'
  delete query.pageNum
  return axios.get(`/search/users`,{ params: query})
}
function _getSearchUsersDepth(req){
    return axios.get('/search/users-depth',{params:req})
}
function _getSearchUsersKeyword(req){
  return axios.get('/search/users-keyword',{params:req})
}
function _getSearchUsersAllDepth(req){
  return axios.get('/search/users-all-depth',{params:req})
}

export const getSearchCategoriesTimeline = createAction(GET_SEARCH_CATEGORIES_TIMELINE_COUNT, _getSearchCategoriesTimeline);
export const getSearchCategoriesSection = createAction(GET_SEARCH_CATEGORIES_SECTION_COUNT, _getSearchCategoriesSection);
export const getSearchCategoriesStack = createAction(GET_SEARCH_CATEGORIES_STACK_COUNT, _getSearchCategoriesStack);
// export const getSearchCategoriesTag = createAction(GET_SEARCH_CATEGORIES_TAG_COUNT, _getSearchCategoriesTag);
export const getSearchCategoriesChildren = createAction(GET_SEARCH_CATEGORIES_CHILDREN, _getSearchCategoriesChildren);
export const getSearchTimelines = createAction(GET_SEARCH_TIMELINES, _getSearchTimelines,(req,attachMode=false) => ({req,attachMode}));
export const getSearchTimelinesSection = createAction(GET_SEARCH_TIMELINES_SECTION, _getSearchTimelinesSection,(req,attachMode=false) => ({req,attachMode}));
export const getSearchTimelinesStack = createAction(GET_SEARCH_TIMELINES_STACK, _getSearchTimelinesStack,(req,attachMode=false) => ({req,attachMode}));
export const getSearchTimelinesTag = createAction(GET_SEARCH_TIMELINES_TAG, _getSearchTimelinesTag,(req,attachMode=false) => ({req,attachMode}));
export const getSearchTag = createAction(GET_SEARCH_TAG, _getSearchTag,(req,attachMode=false) => ({req,attachMode}));
export const getSearchUsers = createAction(GET_SEARCH_USERS, _getSearchUsers,(req,attachMode=false) => ({req,attachMode}));
export const getSearchUsersDepth = createAction(GET_SEARCH_USERS_DEPTH, _getSearchUsersDepth);
export const getSearchUsersKeyword = createAction(GET_SEARCH_USERS_KEYWORD, _getSearchUsersKeyword);
export const getSearchUsersAllDepth = createAction(GET_SEARCH_USERS_ALL_DEPTH, _getSearchUsersAllDepth);
export const modelUpdateCategory = createAction(MODEL_UPDATE_CATEGORY, (query) =>{
  return new Promise(function (resolve, reject) {
      resolve(query);
  });
});
export const getSearchCategoriesUsers = createAction(GET_SEARCH_CATEGORIES_USERS_COUNT, (req,attachMode=false) =>{
  let query = JSON.parse(JSON.stringify(req));
  req.pageNum = attachMode ? req.pageNum + 1 : 1;
  query.from = attachMode ? req.pageNum : 0;
  query.orderType='L'
  delete query.pageNum
  return axios.get(`/search/users`,{ params: query})
});
/**
 * 리스트를 검색 데이터에 맞게 변환 합니다.
 * @param {*} originTimelines 서버에서 전달 받은 타임라인 리스트
 * @param {*} word 사용하지 않습니다
 */
function convertSearchTimelines(originTimelines,word){

  const currentMillis = new Date().getTime();
  //console.log(originTimelines)
  const timelines = originTimelines.map(item=>{
    const regMillis = item.fields.regDate;
    const intervalMillis = currentMillis - regMillis;
    if(item.fields.user === undefined){
      item.fields.user = {
        userKey :item.fields.userKey
      }
    }
    if(item.fields.timelineIdx===undefined){
      item.fields.timelineIdx = item.id;
    }else{
      item.fields.id = item.id;
    }
    item.fields.playTime = convDuration(item.fields.playTime);
    item.fields.regDate = (intervalMillis < (24*60*60*1000)) ? timeSince(intervalMillis) : toDateStr(regMillis);
    if(item.fields.tag !== undefined)
      item.fields.tags = item.fields.tag.map((tag) => {return '#'+tag})
    return item.fields;
  })
  return timelines;
}
/**
 * 변경된 객체를 카테고리에서 찾아 변경합니다.
 * @param {*} state state 값
 * @param {*} action action 값
 */
function updateRootNode(state,action){
  const root = state.get('root');
  function updateNode(root){
    return root.map( (rootCate,i)=>{
      if(action.payload.category.idx > 0){
        if(rootCate.idx === action.payload.category.idx){
          rootCate = action.payload.category;
        }else{
          rootCate.selected=false
        }
        if(rootCate.children !==undefined && rootCate.children.length > 0){
          updateNode(rootCate.children)
        }
      }else{
        if(rootCate.name === action.payload.category.name){
          rootCate = action.payload.category;
          if(rootCate.children !==undefined && rootCate.children.length > 0){
            updateNode(rootCate.children)
          }
        }else{
          rootCate.selected=false
          if(rootCate.children !==undefined && rootCate.children.length > 0){
            updateNode(rootCate.children)
          }
        }
      }
      return rootCate;
    });
  }
  return updateNode(root)
}
const initialState = Map({
  keyword:'',
  orderType:'',
  categoryIdx:0,
  from:0,
  limit:20,
  items:{
    search : [],
    count : 0
  },
  currentPageType:'',
  result:{
    timeline:{
      search : [],
      count : 0
    },
    section:{
      search : [],
      count : 0
    },
    stack:{
      search : [],
      count : 0
    },
    tag:{
      search : [],
      count : 0
    },
    users:{
      search : [],
      count : 0
    },
  },
  item:null,
  usersDepthResult:null,
  usersKeywordResult:null,
  usersAllDepthResult:null,
  root:[ //검색 LNB 에 나타나는 카테고리 입니다. 최초에 모든 자식 카테고리 검색합니다. 이유는 해당 카테고리의 자식 요소를 알수 없기에 서버에 요청을 모두 한번씩 합니다.
    {
      idx:0,  //idx = 0 인경우 최상단 루트로 체크 합니다. 이후 LNB 클릭시 카테고리별 타임라인 서버에 요청합니다
      name : '지식명',
      languagePack:'search_result.knowledge-name',
      count:0,
      isRoot:true, 
      isOpen:false,  //해당 카테고리가 열렸는지 여부
      selected:false,  // 해당 카테고리를 선택 했는지 여부
      timelineCount:0,
      type:'timeline'
    },
    {
      idx:0,
      name : '섹션',
      languagePack:'search_result.section',
      count:0,
      isRoot:true,
      isOpen:false,
      selected:false,
      timelineCount:0,
      type:'section'
    },
    {
      idx:0,
      name : '덧지식',
      languagePack:'search_result.additional-knowledge',
      count:0,
      isRoot:true,
      isOpen:false,
      selected:false,
      timelineCount:0,
      type:'stack'
    },
    {
      idx:0,
      name : '태그',
      languagePack:'search_result.tag',
      count:0,
      isRoot:true,
      isOpen:false,
      selected:false,
      timelineCount:0,
      type:'tag'
    },
    {
      idx:0,
      name : '사용자',
      languagePack:'common.user',
      count:0,
      isRoot:false,
      isOpen:false,
      selected:false,
      timelineCount:0,
      type:'users'
    }
  ]
})

const reducer = handleActions({
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_SEARCH_CATEGORIES_TIMELINE_COUNT,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      //return state.set('root', Object.assign([],initRootNode(state,action,'timeline')))
      const idx = action.payload.config.params.categoryIdx;
      const type = action.payload.config.params.id;
      const children = action.payload.data.categoryResult.map(_child=>{
        _child.isOpen=false;
        _child.selected= false;
        _child.timelineCount = _child.count;
        _child.type = type;
        return _child
      })
      action.payload.data.idx = idx
      action.payload.data.type = type
      action.payload.data.children = children
      return state.set(type+'Category', Object.assign([],action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_CATEGORIES_SECTION_COUNT,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const idx = action.payload.config.params.categoryIdx;
      const type = action.payload.config.params.id;
      const children = action.payload.data.categoryResult.map(_child=>{
        _child.isOpen=false;
        _child.selected= false;
        _child.timelineCount = _child.count;
        _child.type = type;
        return _child
      })
      action.payload.data.idx = idx
      action.payload.data.type = type
      action.payload.data.children = children
      return state.set(type+'Category', Object.assign([],action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_CATEGORIES_STACK_COUNT,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const idx = action.payload.config.params.categoryIdx;
      const type = action.payload.config.params.id;
      const children = action.payload.data.categoryResult.map(_child=>{
        _child.isOpen=false;
        _child.selected= false;
        _child.timelineCount = _child.count;
        _child.type = type;
        return _child
      })
      action.payload.data.idx = idx
      action.payload.data.type = type
      action.payload.data.children = children
      return state.set(type+'Category', Object.assign([],action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_TAG,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const idx = action.payload.config.params.categoryIdx;
      const type = action.payload.config.params.id;
      const children = action.payload.data.map( (_child,i)=>{
        _child.isOpen=false;
        _child.selected= false;
        _child.timelineCount = _child.count;
        _child.type = 'tag';
        _child.name = _child.tag;
        _child.idx =_child.tag+'-'+i;
        return _child
      })
      action.payload.data.idx = idx
      action.payload.data.type = type
      action.payload.data.children = children
      return state.set(type+'Category', Object.assign([],action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  // {
  //   type: GET_SEARCH_CATEGORIES_TAG_COUNT,
  //   onPending: (state, action) => {
  //       return state; // do something
  //   },
  //   onSuccess: (state, action) => {
  //     const root = action.payload.data.categoryResult.map(_root=>{
  //       _root.isOpen=false;
  //       _root.selected= false;
  //       _root.timelineCount = _root.count;
  //       _root.type = 'tag';
  //       return _root
  //     })
  //     return state.set('categoryTag', root)
  //   },
  //   onFailure: (state, action) => {
  //       return state; // do something
  //   },
  //   onCancel: (state, action) => {
  //     return state
  //   }
  // },
  {
    type: GET_SEARCH_CATEGORIES_USERS_COUNT,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      console.log(action.payload.data)
      action.payload.data.type = "users"
      return state.set('usersCategory', Object.assign([],action.payload.data))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_CATEGORIES_CHILDREN,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      // var type = action.payload.config.params.id.substring(0,1).toUpperCase() + action.payload.config.params.id.substring(1);
      // let root = state.get('category'+type);
      // const newRoot = root.map( (rootCate,i)=>{
      //   rootCate.isOpen =false;
      //   if(rootCate.idx === action.payload.config.params.categoryIdx){
      //     rootCate.children = action.payload.data.categoryResult;
      //     if(action.payload.data.categoryResult.length > 0){
      //       rootCate.isOpen =true;
      //     }
      //     //rootCate.children.unshift({idx:rootCate.idx,name:'전체',timelineCount:rootCate.timelineCount})
      //   }
      //   return rootCate;
      // });
      // return state.set('category'+type, newRoot)
      //console.log(' fanal ?? ',addChildrenRootNode(state,action) )
      const type = action.payload.config.params.id;
      const children = action.payload.data.categoryResult.map(_child=>{
        _child.isOpen=false;
        _child.selected= false;
        _child.type = type;
        _child.timelineCount = _child.count
        return _child
      })
      //return state.set('root', Object.assign([],addChildrenRootNode(state,action)))
      return state.set('child', Object.assign([],children))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: MODEL_UPDATE_CATEGORY,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      // console.log(action.payload)
      // var type = action.payload.category.type.substring(0,1).toUpperCase() + action.payload.category.type.substring(1);
      // let root = state.get('category'+type);
      // const newRoot = root.map( (rootCate,i)=>{
      //   rootCate.selected=false
      //   if(rootCate.idx === action.payload.category.idx){
      //     rootCate = action.payload.category;
      //   }
      //   if(rootCate.children !==undefined && rootCate.children.length > 0){
      //     const newChildren = rootCate.children.map(child=>{
      //       child.selected = false;
      //       if(child.idx === action.payload.category.idx){
      //         child = action.payload.category;
      //       }
      //       return child;
      //     })
      //     rootCate.children = newChildren
      //   }

      //   console.log(rootCate)
      //   return rootCate;
      // });
      // return state.set('category'+type,newRoot)
      return state.set('root', Object.assign([],updateRootNode(state,action)))
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_TIMELINES,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const timelines = convertSearchTimelines(Object.assign([],action.payload.data.search) , action.payload.config.params.keyword)
      let result = state.get('result');
      result.timeline.count = action.payload.data.count
      result.timeline.type = 'timeline'
      result.timeline.search = action.meta.attachMode ? result.timeline.search.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.count / 20) ===  action.meta.req.pageNum
      return state.set('items', Object.assign(result.timeline))
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('keyword', action.payload.config.params.keyword).set('orderType', action.payload.config.params.orderType)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_TIMELINES_SECTION,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const timelines = convertSearchTimelines(Object.assign([],action.payload.data.search), action.payload.config.params.keyword)
      let result = state.get('result');
      result.section.count = action.payload.data.count
      result.section.type = 'section'
      result.section.search = action.meta.attachMode ? result.section.search.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.count / 20) ===  action.meta.req.pageNum
      return state.set('items', Object.assign(result.section))
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('keyword', action.payload.config.params.keyword).set('orderType', action.payload.config.params.orderType)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_TIMELINES_STACK,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const timelines = convertSearchTimelines(Object.assign([],action.payload.data.search), action.payload.config.params.keyword)
      let result = state.get('result');
      result.stack.count = action.payload.data.count
      result.stack.type = 'stack'
      result.stack.search = action.meta.attachMode ? result.stack.search.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.count / 20) === action.meta.req.pageNum
      return state.set('items', Object.assign(result.stack))
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('keyword', action.payload.config.params.keyword).set('orderType', action.payload.config.params.orderType)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_TIMELINES_TAG,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      const timelines = convertSearchTimelines(Object.assign([],action.payload.data.search), action.payload.config.params.keyword)
      let result = state.get('result');
      result.tag.count = action.payload.data.count
      result.tag.type = 'tag'
      result.tag.search = action.meta.attachMode ? result.tag.search.concat(timelines) :timelines
      const isHideMoreLayer = Math.ceil(action.payload.data.count / 20) === action.meta.req.pageNum
      return state.set('items', Object.assign(result.tag))
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('orderType', action.payload.config.params.orderType)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_USERS,
    onPending: (state, action) => {
        return state; // do something
    }, 
    onSuccess: (state, action) => { 
      let result = state.get('result');
      result.users.count = action.payload.data.userCount
      result.users.type = 'users'
      result.users.search = action.meta.attachMode ? result.users.search.concat(Object.assign([],action.payload.data.userList)) :Object.assign([],action.payload.data.userList)
      const isHideMoreLayer = Math.ceil(action.payload.data.userCount/20) === action.meta.req.pageNum
      return state.set('items', Object.assign(result.users))
      .set('req',action.meta.req).set('attachMode',action.meta.attachMode).set('isHideMoreLayer',isHideMoreLayer).set('keyword', action.payload.config.params.keyword).set('orderType', action.payload.config.params.orderType)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_USERS_DEPTH,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      return state.set('usersDepthResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_USERS_KEYWORD,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      return state.set('usersKeywordResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  },
  {
    type: GET_SEARCH_USERS_ALL_DEPTH,
    onPending: (state, action) => {
        return state; // do something
    },
    onSuccess: (state, action) => {
      return state.set('usersAllDepthResult', action.payload.data)
    },
    onFailure: (state, action) => {
        return state; // do something
    },
    onCancel: (state, action) => {
      return state
    }
  }

]);
