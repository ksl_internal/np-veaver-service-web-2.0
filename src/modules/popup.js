export const POPUP_OPEN = 'popup/POPUP_OPEN';
export const POPUP_CLOSE = 'popup/POPUP_CLOSE';

const initialState = {
  type: null,
  data: null,
  onClose: null,
}

export const openPopup = (popupType, data, onClose) => {
  return (dispatch) => {
    dispatch({ type: POPUP_OPEN, popupType, data, onClose });
  }
}

export const closePopup = () => {
  return (dispatch) => {
    dispatch({ type: POPUP_CLOSE });
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case POPUP_OPEN:
      return {
        ...state,
        type: action.popupType,
        data: action.data,
        onClose: action.onClose,
      }
      case POPUP_CLOSE:
      return {
        ...state,
        type: null,
        data: null,
        onClose: null,
      }
    default:
      return state
  }
}
