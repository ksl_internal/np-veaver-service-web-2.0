import {handleActions, createAction} from 'redux-actions';

/*

LNB 열림 여부 내용입니다.

*/


const NAVBAR_CLOSE = '/navbar/NAVBAR_CLOSE';
const NAVBAR_TOOGLE = '/navbar/NAVBAR_TOOGLE';
const NAVBAR_SET_STATUS = '/navbar/NAVBAR_SET_STATUS';

export const navbarToggle = createAction(NAVBAR_TOOGLE, ()=>{
    document.body.classList.toggle("hide-content-left")
    return document.body.classList.contains("hide-content-left")?false:true
});
export const navbarClose = createAction(NAVBAR_CLOSE, ()=>{
    document.body.classList.add("hide-content-left")
    return false
});

export const setStatus = createAction(NAVBAR_SET_STATUS, (status)=>{
    return status
});
const initialState = {
    isOpen: true,
    status:1   // 1.LNB 있으면서 열려있음 , 2.LNB 있으면서 닫혀있음 , 3.LNB 없는 페이지
};

export default (state = initialState, action) => {
    switch ( action.type ) {
        case NAVBAR_CLOSE:
            return {
                ...state,
                isOpen: action.payload,
                status : 2
            };  
        case NAVBAR_TOOGLE:
            return {
                ...state,
                isOpen: action.payload,
                status : (action.payload) ? 1 : 2
            };
        case NAVBAR_SET_STATUS:
            return {
                ...state,
                status : action.payload
            };               
        default:
            return state;
    }
};