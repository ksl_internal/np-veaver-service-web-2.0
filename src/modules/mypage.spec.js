import mypage, { 
    checkAllow, 
    CHECK_ALLOW_SUCCESS,
    TIMELINES_REQUEST,
    TIMELINES_SUCCESS,
    DELETE_TIMELINE_SUCCESS,
 } from './mypage';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk'
import { axios } from '../util/api';
//import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
const middlewares = [thunk]
const mockStore = configureStore(middlewares);
const mock = new MockAdapter(axios);

//https://medium.com/@ljs0705/react-state%EA%B0%80-%EB%B6%88%EB%B3%80%EC%9D%B4%EC%96%B4%EC%95%BC-%ED%95%98%EB%8A%94-%EC%9D%B4%EC%9C%A0-ec2bf09c1021

describe('async action creators', () => {
  afterEach(() => {
    mock.restore()
    mock.reset()
  })

  test('해당 유저가 해당 타임라인(지식)에 대한 접근권한이 있다.', () => {
    const timelineIdx = 11;
    mock.onGet(`/timelines/${timelineIdx}/allow`).reply(200, { header: { resultCode: 0 }})

    const expectedActions = [{ type: CHECK_ALLOW_SUCCESS, isSuccess: true }]
    const store = mockStore({})
    return store.dispatch(checkAllow(timelineIdx)).then(res => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})


describe('reducer', () => {
    test('초기 상태값 확인', () => {
        expect(mypage(undefined, {})).toEqual({
            totalCount: 0,
            items: [],
            pageNum: 1,
            pageSize: 20,
            isHideMoreLayer: true,
            isShowLoadingCards: true,
        })
    })

    test('타임라인 리스트 요청', () => {
        expect(mypage([], {
            type: TIMELINES_REQUEST,
            attachMode: false,
        })).toEqual({
            items: [],
            totalCount: 0,
            isShowLoadingCards: true,
        })
    })

    test('타임라인 리스트 응답 성공', () => {
        expect(mypage([], {
            type: TIMELINES_SUCCESS,
            payload: {
                isHideMoreLayer: false,
                items: [],
                totalCount: 20,
                attachMode: false,
            }
        })).toEqual({
            items: [],
            isShowLoadingCards: false,
            isHideMoreLayer: false,
            totalCount: 20,
            pageNum: 1,
        })
    })

    test('타임라인 삭제 성공', () => {
        expect(mypage([], {
            type: DELETE_TIMELINE_SUCCESS,
        })).toEqual({
            items: [],
            pageNum: 1,
        })
    })
})