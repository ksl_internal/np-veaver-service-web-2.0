  import { axios } from '../util/api';
  
  const SET_SHOWTYPE = 'reaction/SET_SHOWTYPE';
  const SET_LISTCONDITION = 'reaction/SET_LISTCONDITION';
  const SET_VIRTUAL_TIMELINE = 'reaction/SET_VIRTUAL_TIMELINE';
  const GET_PLAYTIME_BASED_LIST = 'reaction/GET_PLAYTIME_BASED_LIST';
  const GET_RECENTTIME_BASED_LIST = 'reaction/GET_RECENTTIME_BASED_LIST';
  const GET_MORE_COMMENT = 'reaction/GET_MORE_COMMENT';
  const REQUEST_CHANGE_COMMENT = 'reaction/REQUEST_CHANGE_COMMENT';
  const DELETE_REACTION = 'reaction/DELETE_REACTION';
  const INCREASE_LIKE_COUNT = 'reaction/INCREASE_LIKE_COUNT';

  const initialState = {
    reactions: [],
    virtualTimeline: [],
    likeCount: 0,
    commentCount: 0,
    showType: 'TEXT', // [ALL, TEXT, LIKE]
    listCondition: 'RECENTTIME', // or RECENTTIME
    tempVideoIdx: undefined, // 플레이어 컨테이너를 전역상태로 분리하기 전까지 videoIdx를 담아두기 위한 임시 프로퍼티.
  }

  // const getVirtualTimelineIdx = (time, tic = 5) => (time % tic === 0 && time !== 0) ? ((time / tic) - 1) : Math.floor(time / tic);
  const getVirtualTimelineIdx = (time, tic = 5) => Math.floor(time / tic);
  
  const listSortAndFilterd = (childrens = [], showType = 'ALL', sorted = true) => {
    const listTypeCondition = (condition) => showType === 'ALL' || condition.reactionType === showType;
    const sortCondtions = (prev, next) => (next.regDate - prev.regDate);
    let filterdMap = childrens.filter(listTypeCondition);
    return filterdMap;
  }

  const playTimeBasedList = (videoIdx, showType, virtualTimeline, dispatch) => {
    return axios
    .get(`/videos/${videoIdx}/reactions`)
    .then(res => {
      const { reactionTimes, commentCount, likeCount } = res.data;
      const reactionMap = new Map()
      reactionTimes.forEach((metadata, index) => {
        const virtualIdx = getVirtualTimelineIdx(metadata.reactionTime);
        const reactionChildrens = listSortAndFilterd(metadata.reactions, showType);
        if (reactionMap.has(virtualIdx)) {
          const reactionValue = reactionMap.get(virtualIdx);
          reactionMap.set(virtualIdx, {
            ...reactionValue, 
            reactionChildrens: [
                ...reactionValue.reactionChildrens,
                ...reactionChildrens,
            ]});
        } else {
          if (reactionChildrens.length) reactionMap.set(virtualIdx, {...virtualTimeline[virtualIdx], reactionChildrens})
        }
      });
      dispatch({ 
        type: GET_PLAYTIME_BASED_LIST,
        likeCount, commentCount,
        reactions: [...reactionMap.values()] });
    }).catch(err => Error(err));
  }

  const recentTimeBasedList = (videoIdx, showType, dispatch) => {
    return axios
    .get(`/videos/${videoIdx}/latest-reactions`)
    .then(response => {
      const sorted = false;
      const { reactions, begin, end, likeCount, commentCount } = response.data;
      const filterdReactions = listSortAndFilterd(reactions, showType, sorted).sort((a, b) => a.regDate - b.regDate)
      const reactionsList = [{
        begin,
        end,
        reactionChildrens: [...filterdReactions]
      }];
      dispatch({ 
         type: GET_RECENTTIME_BASED_LIST,
         likeCount, commentCount,
         reactions:  reactionsList });
    }).catch(err => Error(err));
  }

  const createComment = (basedPath, basedParams) => {
    return axios.post(`${basedPath}/reactions/comment`, basedParams)
    .then(() => 'success')
    .catch(err => Error(err))
  }
  const createNestedComment = (basedPath, basedParams, reactionId) => {
    return axios.post(`${basedPath}/reactions/${reactionId}`, basedParams)
    .then(() => 'success')
    .catch(err => Error(err))
  }

  const updateComment = (basedPath, basedParams, reactionId) => {
    return axios.put(`${basedPath}/reactions/${reactionId}`, basedParams)
    .then(() => 'success')
    .catch(err => Error(err))
  }
  const updateNestedComment = (basedPath, basedParams, reactionId) => {
    return axios.put(`${basedPath}/nested-reactions/${reactionId}`, basedParams)
    .then(() => 'success')
    .catch(err => Error(err))
  }

  export const setShowType = (value) => (dispatch, getState) =>
    dispatch({ type: SET_SHOWTYPE, showType: value });

  export const setListCondition = (value) => (dispatch, getState) =>
    dispatch({ type: SET_LISTCONDITION, listCondition: value });

  export const setVirtualTimeline = (videoIdx, duration) => (dispatch, getState) => {
    const { listCondition } = getState().reaction;
    const each = 5, min = 1;
    let virtualTimeline = [];
    if (listCondition === 'RECENTTIME') {
      virtualTimeline.push({ begin:0 , end: duration });
    } else {
      const remainderResult = duration % each;
      const divisionResult = Math.floor(duration / each) + 1;
      virtualTimeline = Array.from({ length: divisionResult - 1 },
        (v, i) => ({ 
          begin: i * each,
          end: (i + 1) * each,
        }))
        .concat({ 
          begin: (divisionResult - 1) * each,
          end: duration,
        });
    }
    dispatch({ type: SET_VIRTUAL_TIMELINE, virtualTimeline, videoIdx });
  }

  export const getReactions = (videoIdx) => (dispatch, getState) => {
      const { listCondition, showType, virtualTimeline, tempVideoIdx } = getState().reaction;
      const videoIndex = videoIdx || tempVideoIdx;
    (listCondition === 'PLAYTIME') ?
    playTimeBasedList(videoIndex, showType, virtualTimeline, dispatch) :
    recentTimeBasedList(videoIndex, showType, dispatch);
  }

  export const getMoreComment = (reactionIdx, childrenIdx, reactionId, nestedReactionId) => (dispatch, getState) => {
    const { tempVideoIdx } = getState().reaction;
    return axios.get(`/videos/${tempVideoIdx}/reactions/${reactionId}?nestedCommentId=${nestedReactionId}`)
    .then(res => {
      const { reactions } = getState().reaction;
      const { moreNestedComments, nestedComments } = res.data;
      reactions[reactionIdx].reactionChildrens[childrenIdx].moreNestedComments = moreNestedComments;
      // reactions[reactionIdx].reactionChildrens[childrenIdx].nestedComment = nestedComments[nestedComments.length - 1];
      reactions[reactionIdx].reactionChildrens[childrenIdx].nestedComments = [...nestedComments]
      dispatch({ type: GET_MORE_COMMENT, reactions })
    })
    .catch(err => Error(err))
  }

  export const changeComment = (actionType, isNested, value, reactionId) => (dispatch, getState) => {
      const videoIdx = getState().reaction.tempVideoIdx;
      const basedPath = `/videos/${videoIdx}`;
      const basedParams = { comment: value };
      if (actionType === 'CREATE' && !isNested) {
        const currentTime = getState().player.currentTime;
        basedParams.reactionTime = Math.floor(currentTime);
      } else if (reactionId || isNested) {
        basedParams.reactionType = 'TEXT';
      }
      const exec = {
          CREATE: isNested ? createNestedComment : createComment,
          UPDATE: isNested ? updateNestedComment : updateComment,
      };
      dispatch({ type: REQUEST_CHANGE_COMMENT });
      return exec[actionType](basedPath, basedParams, reactionId);
  }

  export const deleteReaction = (videoIdx, nested, reactionId) => (dispatch, getState) => {
      const nestedPath = nested ? 'nested-' : '';
      return axios
      .delete(`/videos/${videoIdx}/${nestedPath}reactions/${reactionId}`)
      .then(() => dispatch({ type: DELETE_REACTION }))
      .then(() => 'success')
      .catch(err => Error(err));
  }

  export const increaseLikeCount = (videoIdx) => (dispatch, getState) => {
    const { currentTime} = getState().player;
    return axios
    .post(`/videos/${videoIdx}/reactions/emotion`, {
      reactionTime: Math.floor(currentTime),
      reactionType: 'LIKE',
    })
    .then(() => dispatch({ type: INCREASE_LIKE_COUNT }))
    .then(() => 'success')
    .catch(err => Error(err));
  }

  export const getLike = (videoIdx) => (dispatch, getState) => {

      const { virtualTimeline , tempVideoIdx } = getState().reaction;
      const videoIndex = videoIdx || tempVideoIdx;


      return axios
          .get(`/videos/${videoIdx}/latest-reactions`)
          .then(res => {
              const { reactionTimes  } = res.data;

              const reactionMap = new Map()
              // reactionTimes.forEach((metadata, index) => {
                  // const virtualIdx = getVirtualTimelineIdx(metadata.reactionTime);
                  // const reactionChildrens = listSortAndFilterd(metadata.reactions, 'LIKE');
                  //
                  // if (reactionMap.has(virtualIdx)) {
                  //     const reactionValue = reactionMap.get(virtualIdx);
                  //     reactionMap.set(virtualIdx, {
                  //         ...reactionValue,
                  //         reactionChildrens: [
                  //             ...reactionValue.reactionChildrens,
                  //             ...reactionChildrens,
                  //         ]});
                  // } else {
                  //     if (reactionChildrens.length) reactionMap.set(virtualIdx, {...virtualTimeline[virtualIdx], reactionChildrens})
                  // }


              // });


              const sorted = false;
              const { reactions, begin, end, likeCount, commentCount } = res.data;
              const filterdReactions = listSortAndFilterd(reactions, 'LIKE', sorted).sort((a, b) => a.regDate - b.regDate)
              const reactionsList = [{
                  begin,
                  end,
                  reactionChildrens: [...filterdReactions]
              }];
              console.log('reactionsList ',reactionsList)



              return reactionsList;

          }).catch(err => Error(err));


  }


  export default (state = initialState, action) => {
    switch (action.type) {
      case SET_SHOWTYPE: {
          return {
              ...state,
              showType: action.showType,
          }
      }
      case SET_LISTCONDITION: {
          return {
              ...state,
              listCondition: action.listCondition,
          }
      }
      case SET_VIRTUAL_TIMELINE: {
          return {
              ...state,
              virtualTimeline: action.virtualTimeline,
              tempVideoIdx: action.videoIdx,
          }
      }
      case GET_MORE_COMMENT: {
        return {
          ...state,
          reactions: [...action.reactions],
        }
      }
      case GET_PLAYTIME_BASED_LIST:
      case GET_RECENTTIME_BASED_LIST: {
          return {
              ...state,
              reactions: action.reactions,
              likeCount: action.likeCount,
              commentCount: action.commentCount,
          }
      }
      case REQUEST_CHANGE_COMMENT:
      case INCREASE_LIKE_COUNT:
      case DELETE_REACTION:
      default:
        return state
    }
  }
  