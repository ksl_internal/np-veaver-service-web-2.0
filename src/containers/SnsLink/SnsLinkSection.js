import React, { Component } from 'react';
import { File, Image, Link, CPLink, Map, Text, Quiz } from './Knowledge';
import { convDuration } from '../../util/time';
import device from 'current-device'
class Section extends Component {

  renderStacks = this.renderStacks.bind(this)

    get isEmpty() {
        const { section, index, isFocusSectionList} = Object.assign({}, this.props);
        return (section.stacks.length === 0 || isFocusSectionList)
  }


  render() { 
    const { section, index, isFocusSectionList} = Object.assign({}, this.props);
    let smallDevice=false;

    if(device.mobile() || device.ipad() || device.ipod() || device.tablet())
        smallDevice=true;

    return (
      <div className="sectionWrap" data-sectionidx={section.sectionIdx}>

          {(this.isEmpty && smallDevice)?
              <div className="sectionAreaEmpty">
                  <span className="time" >
                    {`${convDuration(section.begin)} - ${convDuration(section.end)}`}
                  </span>
                  <p className="tit">
                      {`${section.name}`}
                  </p>

              </div>

              :
            <div className="sectionArea">

              <span className="time" >
                {`${convDuration(section.begin)} - ${convDuration(section.end)}`}
              </span>
              <p className="tit">
                {`${section.name}`}
              </p>
            </div>
          }
          {section.stacks.length > 0 && !this.isEmpty && this.renderStacks(section.stacks)}
      </div>
    );
  }

  renderStacks(stacks) {
    return stacks.map(stack => (
      <div className="infoArea" key={`stack-${stack.stackIdx}`} data-stackidx={stack.stackIdx}>
        <div className="infoHead">
          <span className="time" >
            {`${convDuration(stack.begin)} - ${convDuration(stack.end)}`}
          </span>
          <p className="tit">
            {stack.name}
          </p>
          {stack.cards.length > 0 ? this.renderCards(stack.cards) : null}
        </div>
      </div>
    ));
  }

  renderCards(cards) {
    const { onAnswer, onVote, onAddExample, isOwner } = Object.assign({}, this.props);
    cards.sort((a, b) => a.orderValue - b.orderValue);
    return cards.map(card => {
      switch (card.type) {
        case 'FILE': return <File key={`file-key-${card.cardIdx}`} data={card.contents.files ? card.contents.files[0] : undefined} make={false} />;
        case 'IMAGE': return <Image key={`image-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'LINK': return <Link key={`link-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'CP_LINK': return <CPLink key={`link-key-${card.cardIdx}`} data={card.contents} make={false} />
        case 'MAP': return  <Map key={`map-key-${card.cardIdx}`} data={card.contents} make={false} />
         case 'QUIZ': return <Quiz key={`quiz-key-${card.cardIdx}`} timelineIdx={this.props.timelineIdx} cardIdx={card.cardIdx} data={card.contents} make={false} onAnswer={onAnswer} isOwner={isOwner}/>
        case 'TEXT': return <Text key={`text-key-${card.cardIdx}`} data={card.contents} make={false} />
        default: return '';
      }
    });
  }

}

export default Section;
