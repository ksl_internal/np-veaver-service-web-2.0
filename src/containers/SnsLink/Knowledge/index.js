import File from './File';
import Image from './Image';
import Link from './Link';
import CPLink from './CPLink';
import Map from './Map';
import Quiz from './Quiz';
import Text from './Text';
import Vote from './Vote';

export {
  File,
  Image,
  Link,
  CPLink,
  Map,
  Quiz,
  Text,
  Vote
}
