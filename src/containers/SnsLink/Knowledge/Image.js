import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { PropTypes } from "prop-types";
import { openPopup } from "../../../modules/popup";
import cx from 'classnames';

class Image extends Component {
  _onPopup(e) {
    e.preventDefault();
    this.props.openPopup("IMAGE", this.props.data.images);
  }

  renderImage(imageArea) {
    return this.props.data.images.map((image, index) =>
      <div className={imageArea} key={`img-data-${index}`}>
        <img src={image.thumbnail} alt={image.orderValue} role="presentation" />
      </div>
    );
  }

  renderEventImageCard() {
    return (
      <div className="card-item img-card">
        {/* 덧지식 및 이벤트 카드 개별로딩 */}
        <div
        className={cx('card-loading', { hide: !this.props.data.thumbnail.includes('blob:http') })}
        >
          <img src="/images/card-loading.png" alt="images loading" />
        </div>

        <div className="img-wrapper">
          <div className="img-area">
            <img
              src={this.props.data.thumbnail}
              role="presentation"
              style={{ height: "108px" }}
              alt="이미지"
            />
          </div>
        </div>
      </div>
    );
  }

  renderMakeImageCard() {
    const viewCount = 4;
    const length = (this.props.data !== null && this.props.data.images !== null) ? this.props.data.images.length : 0;
    
    return (
      <div className="card-item img-card">
        {/* 덧지식 및 이벤트 카드 개별로딩 */}
        <div
        className={cx('card-loading', { hide: !this.props.data.images.some(item => item.thumbnail.includes('blob:http')) })}
        >
          <img src="/images/card-loading.png" alt="images loading" />
        </div>

        <div className="img-wrapper">
          {length > 0 ? this.renderImage('img-area') : () => {}}
          <button
            className={`more ${length > viewCount
              ? ""
              : "hide"}`}
            onClick={e => this._onPopup(e)}
          >
            +{length - viewCount}
          </button>
        </div>
        <div className="card-button">
          <button
            className="delete"
            onClick={e => this.props.onDelete(e, true)}
          />
        </div>
      </div>
    );
  }

  renderImageCard() {
    const viewCount = 2;
    const length = (this.props.data !== null && this.props.data.images !== null) ? this.props.data.images.length : 0;
    return (
      <div className="card type-img">
        <div className="image-wrapper" onClick={e => this._onPopup(e)} style={{ cursor: 'pointer' }}>
          {length > 0 ? this.renderImage('image-area') : () => {}}
        </div>
        <button
          type="button"
          onClick={e => this._onPopup(e)}
          className={`more ${length > viewCount
            ? ""
            : "hide"}`}
        >
          +{length - viewCount}
        </button>
      </div>
    );
  }

  render() {
    // console.log(this.props)
    if (this.props.make && this.props.mode !== "events")
      return this.renderMakeImageCard();
    if (this.props.make && this.props.mode === "events")
      return this.renderEventImageCard();
    return this.renderImageCard();
  }
}

Image.propTypes = {
  make: PropTypes.bool,
  mode: PropTypes.string,
  begin: PropTypes.number,
  data: PropTypes.shape({
    images: PropTypes.arrayOf(PropTypes.object.isRequired)
  }),
  onDelete: PropTypes.func
};

Image.defaultProps = {
  make: false,
  mode: "all",
  begin: 0,
  data: {
    images: [""]
  },
  onDelete: () => {}
};

const mapStateToProps = state => ({
  openPopup: state.openPopup
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openPopup
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Image);
