import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

class Link extends Component {

  renderMakeLinkCard() {
    return(
      <div className="card-item link-card">
        <div className="link-info-wrapper">
          <span className="link-icon">
            {this.props.data.thumbUrl && <img src={this.props.data.thumbUrl} alt="product icon" /> }
          </span>
          <div className="link-info">
            <p className="link-name">{this.props.data.title}</p>
            <p className="link-context">{this.props.data.desc}</p>
            <a href={this.props.data.url} target="_blank" rel="noopener noreferrer" className="link-address">{this.props.data.url}</a>
          </div>
        </div>
        <div className="card-button">
          <button className="delete" onClick={e => this.props.onDelete(e, true)}></button>
        </div>
      </div>
    );
  }

  renderLinkCard() {
    return (
      <div className="card type-link">
        <div className="link-thumbnail">
          <img src={this.props.data.thumbUrl} alt="Website Preview" onError={e => e.target.parentElement.style.display = 'none'}/>
        </div>
        <div className="link-info">
          <p className="link-name">
            {this.props.data.title}
          </p>
          <p className="link-context">
            {this.props.data.desc}
          </p>
          <a className="link-address" href={this.props.data.url} target="_blank" rel="noopener noreferrer">
            {this.props.data.url}
          </a>
        </div>
      </div>
    );
  }

  render() {
    return this.props.make ? this.renderMakeLinkCard() : this.renderLinkCard();
  }
}

Link.propTypes = {
  make: PropTypes.bool,
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    desc: PropTypes.string,
    thumbUrl: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
  }),
  onDelete: PropTypes.func,
};

Link.defaultProps = {
  make: false,
  data: {
    title: '무제',
    desc: '',
    thumbUrl: '',
    url: '',
  },
  onDelete: () => {},
}

export default Link;
