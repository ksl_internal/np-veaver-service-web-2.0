import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toastr } from "react-redux-toastr";
import moment from 'moment';
import {
  openPopup,
} from '../../../modules/popup';
import cx from 'classnames';
import { getQuizAnswer } from '../../../util/card';
/**
 *
 * 질문을 입력한다.
 * 답변을 선택할수있다.
 *  - 주관식 -> 객관식 -> 주관식 값이 초기화 된다.
 *  - 최초 라벨선택시 더블클릭 되는 문제 발생
 * 주관식 답변을 작성한다.
 * 객관식 답변을 작성한다.
 * 객관식 답변을 추가한다.
 * 객관식 답변을 제거한다. = 2개 부터 제거 안됨. 
 * 답변을 체크한다.
 *  - 활성화, 비활성화가 되어야 한다.
 *  - 답변이 한개일때 체크가 해제 되면 안됨.(적어도 한개)
 */
class Quiz extends Component {
  constructor(props) {
    super(props);
    moment.updateLocale('ko', {
      weekdays: ["일요일","월요일","화요일","수요일","목요일","금요일","토요일"],
      weekdaysShort: ["일","월","화","수","목","금","토"],
    });

    this.state = {
      title: '',
      type: 'SHORT',
      answer: getQuizAnswer('SHORT'),
    };
  }

  _showAnswer(e) {
    e.preventDefault();
    this.props.openPopup('QUIZ', {...this.props, isEvent: false});
  }
   /**
   * 질문 수정
   */
  onChangeQuestion = ({ target }) => {
    
    this.limitStringHandler(target);
    this.props.exportQuizModel(Object.assign(this.props.data, { title: target.value }));
  }
  /**
   * 답변 타입 변경
   */
  onChangeAnswerType = ({ target }) => {
    const data = Object.assign({}, this.props.data);
    data.type = target.value;
    if (target.value === 'SHORT') {
      delete data.answers;
      data.answer = getQuizAnswer(target.value);
    } else {
      delete data.answer;
      data.answers = getQuizAnswer(target.value);
    }
    this.props.exportQuizModel(data);
  }
  /**
   * 답변 수정 - 주관식일때
   */
  onChangeShortAnswer = ({ target }) => {
    this.limitStringHandler(target);
    const data = Object.assign({}, this.props.data);
    data.answer.example = target.value;
    this.props.exportQuizModel(data);
  }
  /**
   * 답변 수정 - 객관식일때
   */
  onChangeMultipleAnswer = ({ target }, i) => {
    const data = Object.assign({}, this.props.data);
    this.limitStringHandler(target);
    data.answers[i].example = target.value;
    this.props.exportQuizModel(data);
  }
  /**
   * 답변에 대한 중복 평가.
   */
  onAnswerValidator = ({target}, i) => {
    const data = Object.assign({}, this.props.data);
    const answers = data.answers.filter(a => a.example !== '').map(a => a.example);
    const alreadyExists = answers.filter(a => a === target.value).length > 1;
    if (target.value !== '' && alreadyExists) {
      target.value = '';
      target.focus();
      data.answers[i].example = '';
      this.props.exportQuizModel(data);
      toastr.light('', '정답문항이 동일한 항목이 있습니다.')
    }
  }
  /**
   * 정답 선택
   */
  onCheckAnswer = ({ target }, i) => {
    const data = Object.assign({}, this.props.data);
    data.answers = data.answers.map((answer, idx) => ({...answer, answerFlag: idx === i ? 'Y' : 'N'}));
    this.props.exportQuizModel(data);
    this.props.onFocusOut();
  }
  /**
   * 답변 삭제
   */
  onDeleteAnswer = (e , i) => {
    const data = Object.assign({}, this.props.data);
    if (data.answers.length > 2) {
      data.answers.splice(i, 1);
    } else {
      toastr.light('', '최소 2개 항목은 등록해야 합니다.')
    }
    this.props.deleteQuizModel(data , e);
    this.props.onFocusOut();
    
  }
  /**
   * 답변 추가
   */
  onAddAnswer = (e) => {
    e.preventDefault()
    const data = Object.assign({}, this.props.data);
    const answersLimit = 5
    if (data.answers.length < answersLimit) {
      data.answers.push({
        example: '',
        orderValue:  Math.max(...data.answers.map(item => item.orderValue)) + 1,
        answerFlag: 'N',
      });
      this.props.exportQuizModel(data);
      this.props.onFocusOut();
    } else {
      toastr.light('', '최대 5개 항목까지 등록 가능합니다.')
    }
  }

  limitStringHandler = (target) => {
    const { maxLength, value } = target;
    if (value.length >= maxLength) {
      toastr.light('', `${maxLength}자 이내로 입력해주세요.`);
      value.substring(0, maxLength);
    }
  }

  renderQuizContent() {
    const { data, cardIdx } = Object.assign({}, this.props);
    switch(data.type) {
      case 'MULTIPLE':
        return (
          <div className="quiz-form single-choice">
            <div className="context">
              <p>
                {data.title}
              </p>
              <p className="limit hide">
                2017. 07.19 (수) 00:00까지
              </p>
            </div>
            {
              data.answers.map((answer, i) => {
                if (this.props.isOwner) {
                  return (
                    <div className={`dimmed`} key={`quiz-answer-${answer.answerId}`}>
                      <input
                        type="radio"
                        id={`quiz-${cardIdx}-${i}`}
                        name={`quiz-${cardIdx}`}
                        value={answer.answerId}
                        defaultChecked={answer.answerId.indexOf(data.myAnswer) !== -1 ? true : false}
                      />
                      <label htmlFor={`quiz-${cardIdx}-${i}`}>
                        <span></span>
                        {answer.example}
                      </label>
                    </div>
                  );
                } else {
                  return (
                    <div className={`type-radio answers ${data.myAnswer === null ? '' : 'dimmed'}`} key={`quiz-answer-${answer.answerId}`}>
                      <input
                        type="radio"
                        id={`quiz-${cardIdx}-${i}`}
                        name={`quiz-${cardIdx}`}
                        value={answer.answerId}
                        defaultChecked={answer.answerId.indexOf(data.myAnswer) !== -1 ? true : false}
                        disabled
                      />
                      <label htmlFor={`quiz-${cardIdx}-${i}`}>
                        <span></span>
                        {answer.example}
                      </label>
                    </div>
                  );
                }
                
              })
            }
          </div>
        );
      case 'SHORT': {
        if (this.props.isOwner) {
          return (
            <div className="quiz-form multiple-choice">
              <div className="context">
                <p>
                  {data.title}
                </p>
                <p className="limit hide">
                  2017. 07.19 (수) 00:00까지
                </p>
              </div>
              <div className="quiz-input">
                <input
                  type="text"
                  maxLength={100}
                  name={`quiz-${cardIdx}`}
                  className={`dimmed`}
                  placeholder="답변 입력"
                  defaultValue={data.myAnswer === null ? '' : data.myAnswer}
                  disabled={data.myAnswer === null ? false : true}
                />
              </div>
            </div>
          );
        } else {
          return (
            <div className="quiz-form multiple-choice">
              <div className="context">
                <p>
                  {data.title}
                </p>
                <p className="limit hide">
                  2017. 07.19 (수) 00:00까지
                </p>
              </div>
              <div className="quiz-input">
                <input
                  type="text"
                  maxLength={100}
                  name={`quiz-${cardIdx}`}
                  className={`answers ${data.myAnswer === null ? '' : 'dimmed'}`}
                  placeholder="답변 입력"
                  defaultValue={data.myAnswer === null ? '' : data.myAnswer}
                  disabled
                />
              </div>
            </div>
          );
        }
      }
        
      default: return null;
    }
  }


  get shortAnswerArea() {
    const conditionOfDimmed = this.props.isModification && (this.props.item.cardIdx || this.props.item.eventIdx) > 0;
    let readValShort = false
    if(this.props.item.eventIdx > 0){
      readValShort = true
    }
    return (<div className={cx('answer-list short-answer', { dimmed: conditionOfDimmed })}>
    <input 
     type="text" 
     placeholder="정답을 입력하세요."
     maxLength={100}
     defaultValue={this.props.data.answer.example} 
     onChange={this.onChangeShortAnswer} 
     readOnly={readValShort}
     />
  </div>);
  }

  get multipleAnswerArea() {
    const conditionOfDimmed = this.props.isModification && (this.props.item.cardIdx || this.props.item.eventIdx) > 0;
    let readValMulti = false
    if(this.props.item.eventIdx > 0){
      readValMulti = true
    }
    return (<div className={cx('answer-list multiple-answer', { dimmed: conditionOfDimmed })}>
    {
      this.props.data.answers && this.props.data.answers.map((answer, i) => {
        return (
          <div key={i} className="answer-item">
            <button className="except" onClick={e => this.onDeleteAnswer(e,i)} disabled={readValMulti}></button> 
            <input
              type="text"
              maxLength={50}
              placeholder={`항목을 입력해주세요.`}
              defaultValue={answer.example}
              onBlur={e => this.onAnswerValidator(e, i)}
              onChange={e => this.onChangeMultipleAnswer(e, i)} 
              readOnly={readValMulti}/>
            <label className="submit">
              <input
              type="checkbox"
              readOnly
              checked={answer.answerFlag === 'Y'}
              onClick={e => this.onCheckAnswer(e, i)}
              disabled={readValMulti}/>
              <span></span>
            </label>
          </div>
        );
      })
    }
    <div className="answer-item">
      <button className="add" onClick={this.onAddAnswer} disabled={readValMulti}></button>
      <input
      maxLength={50}
      type="text"
      placeholder={`항목을 입력해주세요.`}
      className="dimmed" />
      <label className="submit dimmed">
        <input type="checkbox" />
        <span></span>
      </label>
    </div>
  </div>)
  }

  get deleteCardOnlyStack() {
    return (<div className="card-button">
    <button className="delete" onClick={() => {
      this.props.onDelete(true);
      this.props.onFocusOut();
    }}></button>
  </div>);
  }

  renderMakeQuizCard() {
    const { props }  = this;
    const conditionOfDimmed = props.isModification && (props.item.cardIdx || props.item.eventIdx) > 0;
    let readVal = false
    if(this.props.item.eventIdx > 0){
      readVal = true
    }
    return (
      <div className="card-item quiz-card" onMouseOver={this.props.onFocus} onMouseLeave={this.props.onFocusOut}>
        <div className="quiz-wrapper">
          <div className="question">
            <span className="q-icon"></span>
            <textarea
              rows="3"
              className={cx({ dimmed: conditionOfDimmed })}
              placeholder="질문을 입력하세요."
              maxLength={100}
              onChange={this.onChangeQuestion}
              defaultValue={props.data.title}
              title="질문입력"
              readOnly={readVal}
            />
          </div>
          <div className="answer">
            <div className="answer-mode-wrapper">
              <span className="a-icon"></span>
              <div className="answer-mode">
                <div className={cx('type-radio', { dimmed: conditionOfDimmed })}>
                  <input
                    type="radio"
                    id={`short-answer-${props.cardIdx}-${props.unique}`}
                    name={`answer-${props.cardIdx}-${props.unique}`}
                    defaultChecked={props.data.type === 'SHORT'}
                    onClick={this.onChangeAnswerType}
                    value='SHORT'
                    disabled={readVal}
                  />
                <label htmlFor={`short-answer-${props.cardIdx}-${props.unique}`}>
                    <span></span>
                    주관식
                  </label>
                </div>
                <div className={cx('type-radio', { dimmed: conditionOfDimmed })}>
                  <input
                    type="radio"
                    id={`multiple-answer-${props.cardIdx}-${props.unique}`}
                    name={`answer-${props.cardIdx}-${props.unique}`}
                    defaultChecked={props.data.type === 'MULTIPLE'}
                    onClick={this.onChangeAnswerType}
                    value='MULTIPLE'
                    disabled={readVal}
                  />
                <label htmlFor={`multiple-answer-${props.cardIdx}-${props.unique}`}>
                    <span></span>
                    객관식
                  </label>
                </div>
              </div>
            </div>
            {props.data.type === 'SHORT' && this.shortAnswerArea}
            {props.data.type === 'MULTIPLE' && this.multipleAnswerArea}
          </div>
          {
            props.isDeployed &&  <div className="modify-dimmed"></div>
          }
          {
            props.mode !== 'events' &&
            <div className="card-button">
              <button className="delete" onClick={e => {
                props.onDelete(e, true);
                this.props.onFocusOut();
              }}></button>
            </div>
          }
        </div>
      </div>
    );
  }

  renderQuizCard() {
   // console.log('isOwner: '+this.props.isOwner)
    if (this.props.isOwner) {
      return (
        <div className="card type-quiz" data-cardidx={this.props.cardIdx} data-type={this.props.data.type}>
          <div className="quiz-header">
            <span className="quiz-icon"></span>
          </div>
          {this.renderQuizContent()}
          <div className="quiz-submit">
            <button onClick={e => this._showAnswer(e)}>정답 보기</button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="card type-quiz" data-cardidx={this.props.cardIdx} data-type={this.props.data.type}>
          <div className="quiz-header">
            <span className="quiz-icon"></span>
            <a className={`view-result ${this.props.data.myAnswer === null ? 'dimmed' : ''}`} href="#" onClick={e => this._showAnswer(e)}>
              답변 보기
            </a>
          </div>
          {this.renderQuizContent()}
          <div className="quiz-submit">
          <button className={'dimmed'}>답변 하기</button>
          {/*  <button className={this.props.data.myAnswer === null ? '' : 'dimmed'} onClick={e => this.props.onAnswer(e)}>답변 하기</button> */}
          </div>
        </div>
      );
    }
  }

  render() {
    return this.props.make ? this.renderMakeQuizCard() : this.renderQuizCard();
  }
}

Quiz.propTypes = {
  make: PropTypes.bool,
  cardIdx: PropTypes.number,
  data: PropTypes.shape({
    type: PropTypes.string.isRequired,
    title: PropTypes.string,
    myAnswer: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    answers: PropTypes.arrayOf(PropTypes.shape({
      answerId: PropTypes.string,
      orderValue: PropTypes.number,
      answerFlag: PropTypes.string,
      example: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    })),
    answer: PropTypes.shape({
      answerId: PropTypes.string,
      answerFlag: PropTypes.string,
      example: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    })
  }),
  onInput: PropTypes.func,
  onDelete: PropTypes.func,
  onAddExample: PropTypes.func,
  onDeleteExample: PropTypes.func,
  onAnswer: PropTypes.func,
}

Quiz.defaultProps = {
  make: false,
  mode: 'all',
  unique: 0,
  cardIdx: 0,
  isModification: false,
  data: {
    type: 'SHORT', // MULTIPLE, SHORT
    title:'',
    answers: [],
    answer: {},
    myAnswer: '',
  },
  onInput: () => {},
  onChangeQuizType: () => {},
  onDelete: () => {},
  onAddExample: () => {},
  onDeleteExample: () => {},
  onAnswer: ()=> {},
  onChangeQuestion: () => {},
  deleteQuizModel: () => {},
  exportQuizModel: () => {},
}

const mapStateToProps = state => ({
  openPopup: state.openPopup,
  userProfile: state.user.userProfile,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  openPopup,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Quiz);
