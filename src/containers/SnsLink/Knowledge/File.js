import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Spinner from 'react-spinkit';
import cx from 'classnames';

const styles = {
  spinner: {
    color: 'blue',
    top: '12px',
    left: '50%',
  }
};

class File extends Component {

  renderMakeFileCard() {
    return (
      <div className="card-item file-card">
        {/* 덧지식 및 이벤트 카드 개별로딩 */}
        <div
        className={cx('card-loading', { hide: !this.props.data.path.includes('blob:http') })}
        >
          <img src="/images/card-loading.png" alt="file loading" />
        </div>

        <a href={this.props.data.path} target="_blank" rel="noopener noreferrer">
        <div className="download-area">
          <span className="file-icon"></span>
          <div className="file-info">
            <p className="file-name">{this.props.data.name}</p>
            <p className="file-size">{`${(this.props.data.size/1024/1024).toFixed(1)}mb`}</p>
          </div>
        </div>
        <div className="card-button">
          <button className="delete" onClick={e => this.props.onDelete(e, true)}></button>
        </div>
        </a>
      </div>
    );
  }

  renderFileCard() {
    return (
      <div className="card type-file">
        <a href={this.props.data.path} target="_blank" rel="noopener noreferrer">
        <div className="download-area">
          <span className="file-icon"></span>
          <div className="file-info">
            <p className="file-name">
              {this.props.data.name}
            </p>
            <p className="file-size">
              {`${(this.props.data.size/1024/1024).toFixed(1)}mb`}
            </p>
          </div>
        </div>
        </a>
      </div>
    )
  }

  render() {
    return this.props.make ? this.renderMakeFileCard() : this.renderFileCard();
  }
}

File.propTypes = {
  make: PropTypes.bool,
  data: PropTypes.shape({
    path: PropTypes.string,
    name: PropTypes.string.isRequired,
    size: PropTypes.number.isRequired
  }),
  onDelete: PropTypes.func,
};

File.defaultProps = {
  make: false,
  data: {
    path: '',
    name: '무제',
    size: 0.0,
  },
  onDelete: () => {},
}

export default File;
