import { PropTypes } from 'prop-types';
import React, { Component } from 'react';
import { toastr } from "react-redux-toastr";

class Text extends Component {

  renderTextCard() {
    return (
      <p className="text">
          {this.props.data.text}
      </p>
    );
  }

  render() {
    return this.renderTextCard();
  }
}

Text.propTypes = {
  make: PropTypes.bool,
  placeholder: PropTypes.string,
  data: PropTypes.shape({
    text: PropTypes.string,
  }),
  onDelete: PropTypes.func,
  onInput: PropTypes.func
};

Text.defaultProps = {
  make: false,
  placeholder: '입력하세요',
  data: {
    text: ''
  },
  onDelete: () => {},
  onInput: () => {},
  begin: 1,
  mode: 'all',
  val: '',
}

export default Text;
