import React,{Component} from "react";
import {connect} from "react-redux";
import { withRouter } from "react-router-dom";
import { Scrollbars } from 'react-custom-scrollbars';
import {axios} from "../../util/api";
import T from "i18n-react/dist/i18n-react";
import * as playerActions from '../../modules/player';
import { bindActionCreators } from 'redux';
import MediaElement from '../../components/Player/MediaElement';
import moment from "moment/moment";
import SnsLinkSection from "./SnsLinkSection";
import device from "current-device";



class SnsLink extends Component {

    state = {
        timelineDetail: {},
        videoDetail: {},
        viewVideoCategoryNames: '',
        isFocusSectionList : false,
        bannerModel: null,

    }

    componentDidMount() {
        this._isMounted = true;
        const { match } = this.props;
        const { publickey } = match.params;

        this.getPublicData(publickey)
//d78ec4298b02b9
        //d78eaed72ba280
        // this.props.getDetails(timelineIdx);
        //
        // console.log('match ',timelineIdx)
    }

    componentWillMount(){
                
        if(device.mobile() || device.ipad() || device.ipod() || device.tablet()){
            document.getElementsByClassName("snslink-wrapper").style=import ('./css/mobileLayout.css');
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    getPublicData(publicKey) {

        axios.get(`play/${publicKey}`)
            .then(result =>{
                const { timeline } = result.data
                if(timeline === undefined ) return
                const lastIndexOfSections = timeline.sections.length - 1;
                const sections = timeline.sections.map((section, idx, target) => {
                    const end = lastIndexOfSections === idx ? timeline.playTime :  target[idx + 1].begin;
                    const stacks = timeline.stacks.filter(stack => (section.begin <= stack.begin && stack.begin < end));
                    return ({
                        ...section,
                        end,
                        stacks,
                    });
                });
                timeline.sections = sections;

                axios.get(`/play/videos/${timeline.videoIdx}`)
                    .then(result =>{
                        if(this._isMounted) {
                            this.setState(state => {
                                return {
                                    ...state,
                                    timelineDetail : timeline,
                                    videoDetail: result.data,
                                    viewVideoCategoryNames: timeline.videoCategories,
                                }
                            })
                        }
                    })
                    .catch(error =>Error(error));
            })
            .then(() => {
                axios.get(`/banners`)
                    .then(result =>{
                        if(result.data) {
                            this.setState(state => {
                                return {
                                    ...state,
                                    bannerModel :result.data
                                }
                            })
                        }
                    })
                    .catch(error =>Error(error));

            })
    }




    renderCategories() {
        return this.state.viewVideoCategoryNames.map((category, index) => <span key={`categoty-${index}`}>{category.videoCategoryName}</span>);
    }


    renderSections() {
      //  console.log('Sns Render Section');
        const { sections, timelineIdx } = Object.assign({}, this.state.timelineDetail);
        const isOwner = false;
        return (<div>
            {
                sections.map((section, i) => (
                    <SnsLinkSection key={`section-${section.sectionIdx}`}
                             timelineIdx = {timelineIdx}
                             section={section}
                             index={i}
                             isOwner={isOwner}
                             isFocusSectionList={this.state.isFocusSectionList}

                    />
                ))
            }
        </div>);
    }

    BannerTextAndImage(imageSource, text) {
        return (
            <div>
                <div className="Image">
                    <img src={imageSource} />
                </div>
                <div className="textWithImage">
                    {text}
                </div>
            </div>
        )        
    }

    BannerImage(imageSource){
        return (     
             <div className="Image">
                 <img src={imageSource} />
            </div>     
        )    
    }

    bannerWithLink(){
        const {bannerModel} = this.state;
        return (
            bannerModel.bannerValues.length > 1
                            ?
                            <a href = {bannerModel.link} target="_blank">
                                {this.BannerTextAndImage(bannerModel.bannerValues[1].value, bannerModel.bannerValues[0].value)}
                            </a>                            
                            :
                            <a href = {bannerModel.link} target="_blank">
                                { bannerModel.bannerValues[0].type == 'image' ? this.BannerImage(bannerModel.bannerValues[0].value):
                                 <div className="textOnly">
                                    {bannerModel.bannerValues[0].value}
                                 </div>}
                            </a>
                            
        )  
    }   

    bannerWithoutLink(){
        const {bannerModel} = this.state; 
        return (
            bannerModel.bannerValues.length > 1
                            ?
                            this.BannerTextAndImage(bannerModel.bannerValues[1].value, bannerModel.bannerValues[0].value)
                            :( bannerModel.bannerValues[0].type == 'image' ? this.BannerImage(bannerModel.bannerValues[0].value):
                            <div className="textOnly">
                                {bannerModel.bannerValues[0].value}
                            </div>)
        )        
    }

    renderBanner() {
        const {bannerModel} = this.state;

        return (
            <div className="banner">
                {bannerModel === null ? <div className="empty"> </div> : (
                    bannerModel.link === null ?
                            this.bannerWithoutLink() :
                            this.bannerWithLink()
                )   
                }
            </div>
        )
    }


    get isNotEmpty() {
        const { videoDetail, timelineDetail } = this.state;
        const hasProperties = (obj) => Object.keys(obj).length !== 0;
        return [
            hasProperties(videoDetail),
            hasProperties(timelineDetail),
        ].every(condition => condition);
    }

    render () {
        const { videoDetail, timelineDetail,bannerModel} = this.state;



        const videoSourceType = {
            Y: 'video/youtube',
            N: videoDetail.hlsFlag === 'Y' ? 'application/x-mpegURL' : 'video/mp4',
            L: 'video/mp4',
        };

        const sources = [
            { src: videoDetail.fileUrl, type: videoSourceType[videoDetail.fileType] }
        ];

        const FullScreenStyle = {width : '100%'}

        return (

            <div className="snslink-wrapper">

                <header>
                    <h1>Veavaer</h1>
                    <p>Veavaer - 지식을 Make, Play, Share</p>

                </header>

                {!this.isNotEmpty  ? '' :
                <section className="container">
                    <div className="content">
                        <div className="player" style = {FullScreenStyle}>
                            {(videoDetail && timelineDetail ) ?
                                <MediaElement
                                        id="player"
                                        mediaType="video"
                                        preload
                                        width="890"
                                        height="500"
                                        poster=""
                                        hlsFlag={videoDetail.hlsFlag}
                                        sources={sources}
                                        timelineSource={timelineDetail}
                                        makeMode={false}
                                        snsFlag={true}
                                /> : ''}
                        </div>

                        <div className="contArea">
                            <dl className="tit">
                                <dt>{timelineDetail.name}</dt>
                                <dd><span>{timelineDetail.publicFlag === 'A' && T.translate('play.public')}</span>
                                    <span>{timelineDetail.publicFlag === 'H' && T.translate('play.job-position')}</span>
                                    <span>{timelineDetail.publicFlag === 'D' && T.translate('play.department')}</span>
                                    <span>{timelineDetail.publicFlag === 'F' && T.translate('play.following')}</span>
                                    <span>{timelineDetail.publicFlag === 'P' && T.translate('play.private')}</span>
                                    <span>{timelineDetail.publicFlag === 'U' && T.translate('play.selected-opening')}</span>
                                    <span> | {T.translate('play.register-date')} {moment(timelineDetail.regDate).format('YYYY. MM. DD HH:mm')}</span></dd>
                            </dl>
                            <div className="subTit">
                                {this.renderCategories()}
                            </div>

                            <p className="hashtag">
                                {timelineDetail.tag.split(',').map( (tag,index)=>{
                                    return (
                                            <span key={`tag-${index}`}>{`${tag} `}</span>
                                    )
                                })}
                            </p>
                            <div className="scrollArea">
                                {timelineDetail.description !== null && timelineDetail.description.replace(/(\r\n|\n|\r)/gm, ',').split(',').map((line, index) =>
                                    <span ref={el => this.spnDescription = el} key={`description-${index}`}>{`${line}`}<br /></span>
                                )}
                            </div>
                            <p className="mBtn"><button type="button">열기</button></p>
                        </div>

                    </div>

                    <div className="sideWrap">
                        <p className="sideTit">덧지식</p>
                        <div className="mSideTab">
                            <button type="button" className={`stList ${this.state.isFocusSectionList === true ? 'on' : ''}`} onClick={() => {  this.setState({isFocusSectionList: true})}}><span>섹션리스트보기</span></button>
                            <button type="button" className={`stDetail ${this.state.isFocusSectionList === false ? 'on' : ''}`}  onClick={() => {  this.setState({isFocusSectionList: false})}}><span>덧지식 함께보기</span></button>
                        </div>

                        <div className="sideCont" id="sideScroll">
                            <Scrollbars
                                style={{ padding : '20px', overflow:'scroll' }}
                                        renderThumbVertical={() => <div className='scroll-bar'/>}
                                        renderView={() => <div className = 'custom' style={{position : 'relative', wordBreak: 'break-all', top:'0px',left:'0px',right:'0px',bottom:'0px',marginRight:'0px',marginBottm:'0px'}} />}
                                        ref={ref => { this.konwledgeScroll = ref; }}
                            >
                                {/* 섹션 Wrapper */}
                                {timelineDetail && this.renderSections()}

                            </Scrollbars>
                            {this.renderBanner()}
                            {/*<div className="banner">*/}
                                {/*<div className="textOnly">*/}
                                    {/*{bannerModel.bannerValues.length > 0 ? bannerModel.bannerValues[0].value : 'shit'}*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        </div>
                    </div>

                </section>}

                <footer>
                    <div className ="footArea">                        
                        <span>&copy; 2018 Veaver, Inc All rights reserved</span>
                    </div>
                </footer>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    videoDetail: state.videoDetail,
    timelineDetail: state.timelineDetail,
    categories: state.viewVideoCategoryNames,
    isFocusSectionList : state.isFocusSectionList,
    bannerModel: state.bannerModel,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    ...playerActions,
}, dispatch)

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(SnsLink));