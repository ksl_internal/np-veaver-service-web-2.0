import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingForm from '../../components/LoadingForm';

class PageLoading extends Component {

  render() {
    return (
      <LoadingForm isLoadingStoped={!this.props.loading} />
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(
  mapStateToProps,
)(PageLoading);
