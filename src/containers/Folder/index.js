import React, { Component , Fragment} from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import * as folderActions from "../../modules/folderAndPin";
import * as profileActions from "../../modules/profile";
import {openPopup} from "../../modules/popup";
import TimelineButtonForm from "../../components/TimelineButtonForm"
import  {NotFound , TimelinesWrapper , FolderCardView , ListMoreView}  from '../../components/NewCardView'
import T from "i18n-react";
import {makebackPreventStateInit,makebackPreventStateAnotherContainer} from "../../modules/app";
/**
 * 폴더 페이지 입니다.
 */
class FolderPage extends Component {
  state = {
    title : ''
  };

    componentWillMount() {
        if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
            this.props.makebackPreventStateAnotherContainer()
            window.history.back()
        }
    }


  componentDidMount(){
      if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
          this.props.makebackPreventStateInit()
          this.init();
          window.scrollTo(0, 0);
      }
  }
  /**
   * 초기값 세팅
   */
  init = async ()=>{
    try {
      const { FolderActions } = this.props;
      await FolderActions.getFolders();
      const { currentInfo } = this.props; //현재 선택된 폴더 객체. 폴더 리스트 중 0번째 인덱스를 현재 선택된 객체로 지정한다.
      if(currentInfo !== null && currentInfo !== undefined) {
        const folerRes =await FolderActions.getFoldersId({id:currentInfo.idx},false);
        const folerUserRes = await FolderActions.getFoldersIdUsers({id:currentInfo.idx});
        if(folerUserRes.data === null){
          window.location.reload(true)
        }
      }
    } catch (error) {
      console.log(error)
    }
  }
  /**
   * 해당 폴더의 핀과 유저를 가져온다.
   */
  fetchData = async (query,attachMode)=>{
    try {
      const { FolderActions,currentInfo } = this.props;
      if(currentInfo !== null && currentInfo !== undefined) {
        const folerRes = await FolderActions.getFoldersId(query,attachMode);
        const folerUserRes = await FolderActions.getFoldersIdUsers({id:query.id});
        if(folerUserRes.data === null){
          window.location.reload(true)
        }
      }
    } catch (error) {
      console.log(error)
    }
  }
  updateCallback =(selectedItems) =>{
    const { FolderActions } = this.props;
    FolderActions.updateSelectedItems(selectedItems)
  }
  /**
   * 폴더에 유저를 초대 합니다.
   */
  userAdd =()=>{
    const { currentInfo } = this.props;
    this.props.openPopup('CHOOSEUSERS',{
      folderIdx:currentInfo.idx,
      returnType:'invite'
      })
  }
  /**
   * 폴더에서 나갑니다.
   */
  getOut=async(e)=>{
    const { currentInfo } = this.props;
    try {
        const data = {
            title: T.translate('common.leave'),
            description: "'"+currentInfo.folderName+"' 폴더에서 나가시겠습니까?"
        };
        const {FolderActions} = this.props
        this.props.openPopup('CONFIRM', data, async(value) => {
            if (value) {
                await FolderActions.putFolderIdLeave({folderIdx:currentInfo.idx})
                window.location.reload(true);
            }
        })
    } catch (error) {
        console.log(error)
    }
  }
  render() {
    const {currentInfo, pins , folderUsers} = this.props
    console.log(folderUsers)
    if(currentInfo==undefined|| pins == null || folderUsers == null){
      return (
        <div className="right">
          <div className="tab-content-two">
            <div className="tab-content-inner paddingLeft30 paddingRight30">
                <div className="title-inner-top clearfix">
                  <h3 className="title">폴더가 없습니다.</h3>
                </div>
            </div>
          </div>
        </div>
      ) 
    } 
    const buttonData ={
      default:{
        openButton:T.translate('common.select'),
        closeButton:T.translate('common.cancel')
      },
      pageButton:[
        {
          name:T.translate('common.share'),
          className:'btn',
          fucOnClick:(e,selectedIdx)=>{
            this.props.openPopup('CHOOSEUSERS',{
              timelineIdxs:selectedIdx.idx,
              returnType:'share'
            })
          }
        },
        {
          name:T.translate('common.add-to-folder'),
          className:'btn add-to-folder',
          fucOnClick:(e,selectedIdx)=>{
            this.props.openPopup('FOLDER',{
              timelineIdxList:selectedIdx.idx,
              folderIdx : null,
              message : null,
              type:'add'
            })
          }
        },
        {
          name:T.translate('common.delete'),
          className:'btn',
          fucOnClick:async(e,selectedIdx)=>{
            const data = {
              title: T.translate('common.delete'),
              description: T.translate('timeline.delete-check'),
            };
            const req={
              folderIdx: currentInfo.idx,
              pinIdxList:selectedIdx.originObj.map(obj=>{return obj.pinInfo.idx})
            }
            try {
                this.props.openPopup('CONFIRM', data, async(value) => {
                    if (value) {
                      try {
                        const {FolderActions} = this.props
                        await FolderActions.deleteFolderIdPin(req)
                        window.location.reload(true);
                      } catch (error) {
                        
                      }
                    }
                });
            } catch (error) {
                console.log(error)
            }
          }
        },
        // {
        //   name:"나가기",
        //   className:'btn',
        //   lengthCheck:true,
        //   fucOnClick:async(e,selectedIdx)=>{
        //     try {
        //         const data = {
        //             title: "나가기",
        //             description: "'"+currentInfo.folderName+"' 폴더에서 나가시겠습니까?"
        //         };
        //         const {FolderActions} = this.props
        //         this.props.openPopup('CONFIRM', data, async(value) => {
        //             if (value) {
        //                 await FolderActions.putFolderIdLeave({folderIdx:currentInfo.idx})
        //                 window.location.reload(true);
        //             }
        //         })
        //     } catch (error) {
        //         console.log(error)
        //     }
        //   }
        // }
      ]
    }
    if(folderUsers.header != null) return window.location.reload(true)
    return (
      <div className="right">
        <div className="tab-content-two">
          <div className="tab-content-inner paddingLeft30 paddingRight30">
                <div className="title-inner-top clearfix">
                  <h3 className="title">{currentInfo.folderName}({pins.totalCount})</h3>
                  <TimelineButtonForm buttonData={buttonData} selectedItems={this.props.selectedItems} updateCallback={this.updateCallback} disabled={pins.timelines.length===0?true:false}>
                  </TimelineButtonForm>
                  {!this.props.selectedItems.isChanging &&
                    <div className="control-btn">
                      <button className="btn active" onClick={this.getOut}>{T.translate('common.leave')}</button>
                    </div>
                  }
                </div>
            <div className="wrap-btn-add-member">
                {folderUsers.users.map(user=>{
                    return (
                        <div key={user.userKey} className="add-memeber" onClick={this.userAdd}>
                            <div className="avatar">
                              <img src={user.thumbnailSmall} alt="image" />
                              <div className="add-hover" />
                              {/* <span className="number">4</span> */}
                            </div>
                            <div className="role">{user.nickname}</div>
                        </div>
                    )
                })}
            </div>
            {pins.timelines.length===0 ?
                <div style={{height:'80vh',position:'relative'}}>
                    <NotFound></NotFound>
                </div>
                :
                <Fragment>
                    <div className="wrap-one-three">
                        <TimelinesWrapper data={pins.timelines} CardView={FolderCardView} selectedItems={this.props.selectedItems} selectedCallback={this.updateCallback}></TimelinesWrapper>
                    </div>
                    <ListMoreView req={this.props.req} isHideMoreLayer={this.props.isHideMoreLayer} callBack={this.fetchData}></ListMoreView>
                </Fragment>
                
            }        
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    folders: state.folderAndPin.get("folders"),
    selectedItems : state.folderAndPin.get("selectedItems"),
    pins: state.folderAndPin.get("pins"),
    folderUsers: state.folderAndPin.get("users"),
    currentInfo: state.folderAndPin.get("currentInfo"),
    req: state.folderAndPin.get("req"),
    attachMode: state.folderAndPin.get("attachMode"),
    isHideMoreLayer : state.folderAndPin.get("isHideMoreLayer"),
      makebackPreventState:state.app.makebackPreventState,
  }),
  dispatch => ({
    FolderActions: bindActionCreators(folderActions, dispatch),
    ProfileActions: bindActionCreators(profileActions, dispatch),
    openPopup: bindActionCreators(openPopup, dispatch),
    makebackPreventStateAnotherContainer: bindActionCreators(makebackPreventStateAnotherContainer, dispatch),
      makebackPreventStateInit:bindActionCreators(makebackPreventStateInit,dispatch)
  })
)(withRouter(FolderPage));
