import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import {
  Play,
  Make,
  Edu,
  Folder,
  Search,
  Profile,
  Noti
} from "../../components/Navbar";
import { openPopup, closePopup } from "../../modules/popup";
import * as navbarActions from "../../modules/navbar";

/**
 * LNB 메인 페이지 입니다.
 */
class Navbar extends Component {
  //url 별 LNB 를 그립니다.
  getView = () => {
    const { match } = this.props;
    if(match.path.length === 1 || match.path.indexOf("/assigned") !== -1 || match.path.indexOf("/received") !== -1 || match.path.indexOf("/sent") !== -1){
      this.props.NavbarActions.setStatus(1)
      return <Play handleToggle={this.handleToggle} isOpen={this.props.isOpen}/>;
    } else if (match.path.indexOf("/profile") !== -1) {
      this.props.NavbarActions.setStatus(1)
      return <Profile handleToggle={this.handleToggle} isOpen={this.props.isOpen}/>;
    } else if (match.path.indexOf("/cp") !== -1) {
      this.props.NavbarActions.setStatus(1)
      return <Edu handleToggle={this.handleToggle} isOpen={this.props.isOpen}/>;
    } else if(match.path.indexOf("/folder") !== -1){
      this.props.NavbarActions.setStatus(1)
      return <Folder handleToggle={this.handleToggle} {...this.props} isOpen={this.props.isOpen}/>;
    } else if(match.path.indexOf("/make") !== -1 ){
      this.props.NavbarActions.setStatus(1)
      return <Make handleToggle={this.handleToggle} isOpen={this.props.isOpen}/>;
    } else if(match.path.indexOf("/search") !== -1){
      this.props.NavbarActions.setStatus(1)
      return <Search handleToggle={this.handleToggle} isOpen={this.props.isOpen}/>;
    } else if(match.path.indexOf("/noti") !== -1){
      this.props.NavbarActions.setStatus(1)
      return <Noti handleToggle={this.handleToggle} isOpen={this.props.isOpen}/>;
    } else {
      this.props.NavbarActions.setStatus(3)
      return null;
    }
  };
  /**
   * 열고 닫는 토글 함수입니다.
   */
  handleToggle = () => {
    this.props.NavbarActions.navbarToggle();
  }
  moveMake = (e, loc) => {
    e.stopPropagation();
    this.props.initPlayer(false, 0);
    // this.props.initVideoInfo();
    // this.props.history.push(`/${loc}`);

      if(loc === 'make') {
          if (this.props.makebackPreventState === 'makestep2') {
              this.props.makebackPreventStateMakePush()
          } else {
              this.props.initVideoInfo();
              this.props.history.push(`/${loc}`);
          }

      } else {
          if (this.props.makebackPreventState === 'makestep2') {
          } else {
              this.props.initVideoInfo();
          }
          this.props.history.push(`/${loc}`);
      }

  };
  // shouldComponentUpdate(nextProps, nextState){
  //   if(nextProps.categories !== this.props.categories) {
  //     document.documentElement.scrollTop = 0;
  //     return true;
  //   }else{

  //     return true;
  //   }
  // }
  render() {
    return (
      <Fragment>
        <nav>
          <ul className="lnb">
            <li>
              <a className="lnb-logo" ></a>
            </li>
            <li>
              <a
                className="lnb-play"
                style={{
                  cursor: "pointer"
                }}
                onClick={e => this.moveMake(e, "")}
              >
                Play
              </a>
            </li>
            <li>
              <a
                className="lnb-make"
                style={{
                  cursor: "pointer"
                }}
                onClick={e => this.moveMake(e, "make")}
              >
                Make
              </a>
            </li>
            <li>
              <a
                className="lnb-edu"
                style={{
                  cursor: "pointer"
                }}
                onClick={e => this.moveMake(e, "cp")}
              >
                Edu
              </a>
            </li>
            <li>
              <a
                className="lnb-folder"
                style={{
                  cursor: "pointer"
                }}
                onClick={e => this.moveMake(e, "folder")}
              >
                Folder
              </a>
            </li>
          </ul>
          {this.getView()}
        </nav>
      </Fragment>
    );
  }
}

// const mapStateToProps = state => ({
//   isAuthenticated: state.auth.isAuthenticated,
// 	popupType: state.popup.type,
// 	popupStatus: state.popup.status,
// 	popupData: state.popup.data,
//   onClose: state.popup.onClose,
// });

// const mapDispatchToProps = dispatch => bindActionCreators({
//   openPopup,
// 	closePopup,
// }, dispatch);

// export default connect(
//   mapStateToProps,
// 	mapDispatchToProps,
// 	ProfileActions : bindActionCreators(profileActions,dispatch),
// )(Navbar);
export default withRouter(connect(
  state => ({
    // pending: state.pender.pending,
   // userResult: state.profile.get("userResult"),
    // usersIndicesResult: state.profile.get("usersIndicesResult"),
    // veaverStackRankingsResult: state.profile.get("veaverStackRankingsResult"),
    // veaverRankingsResult: state.profile.get("veaverRankingsResult"),
    // userTimelinesResult: state.profile.get("userTimelinesResult")
    // veaverStackRankingsResult:state.profile.get('veaverStackRankingsResult'),
    // veaverRankingsResult:state.profile.get('veaverRankingsResult'),
    // notificationsResult : state.noti.get('notificationsResult')
    isOpen:state.navbar.isOpen
  }),
  dispatch => ({
    openPopup: bindActionCreators(openPopup, dispatch),
    closePopup: bindActionCreators(closePopup, dispatch),
    NavbarActions: bindActionCreators(navbarActions, dispatch)
  })
)(Navbar));
