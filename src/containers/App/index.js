import React from "react";
import { Route, Redirect, Switch, withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import MainFrame from "../../components/MainFrame";
// import Home from '../Home';
import Mypage from "../Mypage";
import Play from "../Play";
import Make from "../Make";
import Modify from "../Modify";
import Login from "../Login";
import Popup from "../Popup";
import ErrorPage from "../ErrorPage";
import Loading from "../Loading";

import Folder from "../Folder";
import Profile from "../Profile";
import Password from "../Profile/Password";
import Search from "../Search";
import {ContactUsList,ContactUsWrite,ContactUsDetail} from "../ContactUs";
import Assigned from "../Mypage/Assigned";
import Received from "../Mypage/Received";
import Sent from "../Mypage/Sent";
import T from 'i18n-react';
import SnsLink from "../SnsLink"

import ReduxToastr from "react-redux-toastr";
import { appInitialize } from "../../modules/app";
import { resetErrorMessage } from "../../modules/error";
import { NotiList } from "../Noti";

const mainFrameHoc = (WrappedComponent, appStatus, isTempMode) => class extends React.Component {
  render() {

    if (appStatus !== 'complete' && appStatus !== 'start') {
      return <Login {...this.props} />;
    }else{
      return (<MainFrame>
                <WrappedComponent {...this.props}/>
            </MainFrame>);
    }
  }
};


const snsLinkComponent = (WrappedComponent,appStatus) => class extends React.Component {
    render() {


            return (<div>
                <WrappedComponent {...this.props}/>

            </div>)


    }
};

class App extends React.Component {
  static defaultProps = {
    appInitialized: 'start',
  }
  componentWillMount() {
    this.props.appInitialize();
    const userLang = navigator.language || navigator.userLanguage;
    this.selectLang( userLang );
  }
    componentWillReceiveProps(nextProps){
        this.selectLang(nextProps.language);
    };
  handleDismissClick = e => {
    this.props.resetErrorMessage();
    e.preventDefault();
  };

  renderErrorMessage() {
    const { errorMessage } = this.props;
    if (!errorMessage) {
      return null;
    }

    return (
      <p style={{ backgroundColor: "#e99", padding: 10 }}>
        <b>{errorMessage}</b>{" "}
        <button onClick={this.handleDismissClick}>Dismiss</button>
      </p>
    );
  }
  /**
   * 다국어 json 세팅 
   */
    selectLang( $lang ) {
        let lang = 'ko';
        switch ( $lang ) {
            case 'en' :
                lang = 'en';
                break;
            case 'zh' :
                lang = 'zh';
                break;
            case 'ru' :
                lang = 'ru';
                break;
            default:
                lang = 'ko';
        }
        T.setTexts( require( '../../languages/' + lang + '.json' ) );
    }

  initialzeRouter() {
    const { appInitialized } = this.props;
    return (
      <div key="react-router-unique-key">
        <Switch>
         <Route exact path="/" component={mainFrameHoc(Mypage, appInitialized)} />
         <Route exact path="/cp" component={mainFrameHoc(Mypage, appInitialized)} />
         <Route exact path="/cp/:groupIdx" component={mainFrameHoc(Mypage, appInitialized)} />
         <Route exact path="/locker" component={mainFrameHoc(Mypage, appInitialized)} />
         <Route exact path="/locker/modify/:timelineIdx" component={mainFrameHoc(Modify, appInitialized)} />
         <Route exact path="/locker/play/:timelineIdx" component={mainFrameHoc(Play, appInitialized)} />
         <Route exact path="/modify/:timelineIdx" component={mainFrameHoc(Modify, appInitialized)} />
         <Route exact path="/play/:timelineIdx" component={mainFrameHoc(Play, appInitialized)} />
         <Route exact path="/make" component={mainFrameHoc(Make, appInitialized)} />

          {/* 추가 작업된 페이지 리스트 입니다. */}
         <Route exact path="/contact" component={mainFrameHoc(ContactUsList, appInitialized)} />
         <Route exact path="/contact/write" component={mainFrameHoc(ContactUsWrite, appInitialized)} />
         <Route exact path="/contact/:id" component={mainFrameHoc(ContactUsDetail, appInitialized)} />
         <Route exact path="/profile" component={mainFrameHoc(Profile, appInitialized)} />
         <Route exact path="/profile/password" component={mainFrameHoc(Password, appInitialized)} />
         <Route exact path="/profile/:id" component={mainFrameHoc(Profile, appInitialized)} />
         <Route exact path="/folder" component={mainFrameHoc(Folder, appInitialized)} />
         <Route exact path="/search/:type" component={mainFrameHoc(Search, appInitialized)} />
         <Route exact path="/assigned" component={mainFrameHoc(Assigned, appInitialized)} />
         <Route exact path="/received" component={mainFrameHoc(Received, appInitialized)} />
         <Route exact path="/sent" component={mainFrameHoc(Sent, appInitialized)} />
         <Route exact path="/noti/:type" component={mainFrameHoc(NotiList, appInitialized)} />

         {/*<Route exact path="/test" component={mainFrameHoc(Search, appInitialized)} />*/}

         <Route exact path="/snslink/:publickey" component={snsLinkComponent(SnsLink,appInitialized)} />

         <Redirect from="/login" to="/" />
       </Switch>
      </div>
    );
  }

  render() {
    return (
      [
        <Loading loading={this.props.loading} key="isLoading-key" />,
        <Popup  key="popup-key" />,
        <ErrorPage key="error-page-key" />,
        !this.props.loading && this.initialzeRouter(),
        <ReduxToastr
          key="toastr-key"
          timeOut={3000}
          newestOnTop={false}
          position="bottom-center"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          showCloseButton={false}
          preventDuplicates
        />,
      ]
    );
  }
}

const mapStateToProps = state => ({
  loading: state.app.loading,
  appInitialized: state.app.appInitialized,
  serverStatus: state.app.serverStatus,
  serviceName: state.app.serviceName,
  userProfile: state.user.userProfile,
  errorMessage: state.error.errorMessage,
  language:state.language.language
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      appInitialize,
      resetErrorMessage
    },
    dispatch
  );

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
