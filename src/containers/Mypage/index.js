import React, { Component } from 'react'
import pt from 'prop-types'
import { Element, animateScroll } from 'react-scroll' // Imports all Mixins
import { bindActionCreators } from "redux"
import { connect } from 'react-redux'
import cx from 'classnames'
import { isLocker } from '../../util/api'
import NodataForm from '../../components/NodataForm'
import LoadingCard from '../../components/CardForm/LoadingCard'
import MypageCard from '../../components/CardForm/MypageCard'
import * as MyPageActionCreators from "../../modules/mypage"
import { videoPublish, uploadVideoInfoDataSet } from "../../modules/make"
import {toastr} from 'react-redux-toastr';
import {makebackPreventStateAnotherContainer} from "../../modules/app";
import { openPopup } from "../../modules/popup";
import T from 'i18n-react';
import {axios} from "../../util/api";

class MypageContents extends Component {
  static propTypes = {
    items: pt.array,
    totalCount: pt.number,
    isHideMoreLayer: pt.bool,
    isShowLoadingCards: pt.bool,
  }

  static defaultProps = {
    items: [],
    totalCount: 99,
    isHideMoreLayer: false,
    isShowLoadingCards: false,
  }

  state = {
    clickableIndex: -1,
  }

  componentDidMount() {
      if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
        this.props.getTimelines(false, 0)
    }
    window.scrollTo(0, 0);
    // this.dummyGetCall();
  }

  // dummyGetCall(){
  //   console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
  //   axios.get(`/banners`)
  //           .then(result =>{
  //             console.log(result);
  //         })
  //         .catch(error =>{ console.log(error); Error(error) });
  //
  //   axios.get(`/videos/4534`)
  //           .then(result =>{
  //             console.log(result.data.fileUrl);
  //             if(result.data.fileUrl){
  //               var xhr = new XMLHttpRequest();
  //               xhr.open('GET', result.data.fileUrl);
  //               xhr.setRequestHeader('some-header', 'some-value')
  //               xhr.onload = function() {
  //                 var responseText = xhr.responseText;
  //                 console.log(responseText);
  //                 // process the response.
  //                };
  //
  //                xhr.onerror = function() {
  //                  console.log('There was an error!');
  //                };
  //                xhr.send();
  //             }
  //
  //         })
  //         .catch(error =>{ console.log(error); Error(error) });
  // }

  componentWillMount() {
      if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
       this.props.makebackPreventStateAnotherContainer()
       window.history.back()
    }
  }

  getTimelines(attachmode, categoryIdx) {
    // animateScroll.scrollToBottom()
    console.log('view more for category- ' + categoryIdx);
    this.props.getTimelines(attachmode,categoryIdx)
  }

  scrollTop() {
    animateScroll.scrollToTop()
  }

  _videoPublish = (e, timelineIdx) => {
    e.preventDefault()
    this.props.uploadVideoInfoDataSet(timelineIdx)
  }

  _openClipBoard = (e, videoIdx, timelineIdx) => {
    e.preventDefault()
    this.props.openClipBoard(videoIdx, timelineIdx)
    this.setState({ clickableIndex: -1 })
  }

  _deleteTimeline = (e, timelineIdx) => {
    e.preventDefault(timelineIdx)
    this.props.deleteTimeline(timelineIdx)
    this.setState({ clickableIndex: -1 })
  }

  get loadingCards() {
    return Array.from({ length: 20 }, (item, idx) =>
    <LoadingCard key={`loading-cards-key-${idx}`} />)
  }

  _checkAllow = (tIdx) => {
    const timelineIdx = tIdx;
    const isLockerPage = isLocker();
    if (isLockerPage) {
      this.props.history.push(`/locker/play/${timelineIdx}`)
    } else {
      this.props.checkAllow(timelineIdx)
      .then(res => {
        if (res.isSuccess) {
          this.props.history.push(`/play/${timelineIdx}`)
        } else {
          toastr.light('확인 할수 없는 지식 입니다.');
        }
      })
      .catch(error => Error(error));
    }

  }

  render() {
    const isLockerPage = isLocker()
    const {
       isShowLoadingCards,
       isHideMoreLayer,
       userProfile,
       uploadVideoStatus,
       totalCount,
       items, } = this.props
    const { clickableIndex } = this.state
    const isEmptyOfItems = items.length === 0
    return (
      <div className="right">
        <div className="tab-content-two">
          <div className="tab-content-inner paddingLeft30 paddingRight30">
            <div className="title-inner-top clearfix">
              <h3 className="title">{isLockerPage ? T.translate("make.temp-storage") : T.translate("play_menu.all")}(<mark>{totalCount}</mark>)</h3>
            </div>
              <Element
              className={cx('wrap-one-three image-top mypage', { hide: isEmptyOfItems })} style={{padding:'20px 5px'}}
              name="firstInsideContainer">
                {
                  items.map((item, index) =>
                  <MypageCard
                  data={item}
                  key={`mypage-card-key-${index}`}
                  checkAllow={() => this._checkAllow(item.timelineIdx)}
                  onLoaded={complete => {}}
                  isLocker={isLockerPage}
                  uploadVideoStatus={uploadVideoStatus}
                  userProfile={userProfile}
                  isActiveSettingLayer={clickableIndex === index}
                  toggleActiveLayer={() => this.setState((state) => ({ clickableIndex: index }))}
                  toggleInactiveLayer={() => this.setState((state) => ({ clickableIndex: -1 }))}
                  openClipBoard={(e) => this._openClipBoard(e, item.videoIdx, item.timelineIdx)}
                  deleteTimeline={(e) => this._deleteTimeline(e, item.timelineIdx)}
                  videoPublish={(e) => this._videoPublish(e, item.timelineIdx)}
                  openReport={(e)=>{e.preventDefault();this.props.openPopup('REPORT', {timelineIdx:item.timelineIdx,targetType:'T'} )}}
                  openChooseUsers={(e)=>{e.preventDefault(); this.props.openPopup('CHOOSEUSERS',{
                    timelineIdxs:[item.timelineIdx],
                    returnType:'share'
                  } )} }
                  openFolder={(e)=>{e.preventDefault(); this.props.openPopup('FOLDER',{
                    timelineIdxList:[item.timelineIdx],
                    folderIdx : null,
                    message : null,
                    type:'add'
                  } )} }
                  />)
                }
                {(isShowLoadingCards && !isEmptyOfItems) && this.loadingCards}
                </Element>
              {(!isShowLoadingCards && isEmptyOfItems) && <div style={{height:'80vh',position:'relative'}}><NodataForm /></div>}
            <div className={cx('more', {
              dimmed: isShowLoadingCards,
              hide: isEmptyOfItems,
            })}>
            {
              isHideMoreLayer ?
              <button
              onClick={() => this.scrollTop()}>
                {T.translate('common.view-up')}<span
                className="more-arrow"
                style={{ transform: 'rotate(180deg)'}}></span>
              </button> :
              <button
              onClick={() => this.getTimelines(true,this.props.categoryIdx)}>
                {T.translate('common.view-more')}<span
                className="more-arrow"></span>
              </button>
            }<br></br><br></br>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  items: state.mypage.items,
  totalCount: state.mypage.totalCount,
  isHideMoreLayer: state.mypage.isHideMoreLayer,
  isShowLoadingCards: state.mypage.isShowLoadingCards,
  uploadVideoStatus: state.make.uploadVideoStatus,
  userProfile: state.user.userProfile,
  makebackPreventState:state.app.makebackPreventState,
  openPopup: state.openPopup,
  categoryIdx:state.mypage.categoryIdx
})

const mapDispatchToProps = dispatch =>
bindActionCreators({...MyPageActionCreators, videoPublish, uploadVideoInfoDataSet,openPopup,makebackPreventStateAnotherContainer}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MypageContents)
