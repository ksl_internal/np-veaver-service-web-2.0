import React, { Component ,Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import TimelineButtonForm from "../../components/TimelineButtonForm"
import * as profileActions from "../../modules/profile";
import  {NotFound , TimelinesWrapper , ReceivedCardView,ListMoreView}  from '../../components/NewCardView'
import T from 'i18n-react';
import { openPopup } from "../../modules/popup";
class Received extends Component {
  state = {
  };
  componentDidMount(){
    this.fetchData({pageNum:1},false)
  }
  fetchData = async (query,attachMode)=>{
    try {
      const { ProfileActions } = this.props;
      await ProfileActions.getUsersMeToSharedTimelines(query,attachMode);
    } catch (error) {
      console.log(error)
    }
  }
  /**
   * 타임라인 체크 여부를 수정하는 콜백 함수입니다.
   */
  updateCallback =(selectedItems) =>{
    const { ProfileActions } = this.props;
    ProfileActions.updateSelectedItems(selectedItems)
  }
  render() {
    const {items,ProfileActions} = this.props
    if(items == null || items == null) return null;
    //우측 상단 버튼 객체 입니다.
    const buttonData ={
      default:{
        openButton:T.translate('common.edit'),
        closeButton:T.translate('common.cancel')
      },
      pageButton:[
        {
          name:T.translate('common.share'),
          className:'btn',
          fucOnClick:(e,selectedIdx)=>{ //공유 콜백 함수입니다
            this.props.openPopup('CHOOSEUSERS',{
              timelineIdxs:selectedIdx.idx,
              returnType:'share'
            })
          }
        },
        {
          name:T.translate('common.add-to-folder'),
          className:'btn add-to-folder',
          fucOnClick:(e,selectedIdx)=>{ //폴더 콜백 함수입니다
            this.props.openPopup('FOLDER',{
              timelineIdxList:selectedIdx.idx,
              folderIdx : null,
              message : null,
              type:'add'
            })
          }
        },
        {
          name:T.translate('common.delete'),
          className:'btn',
          fucOnClick:async(e,selectedIdx)=>{ //삭제 콜백 함수입니다
            const data = {
                title: T.translate('common.delete'),
                description: T.translate('timeline.delete-check'),
            };
            const req={
              hiddenIdxList:selectedIdx.originObj.map(obj=>{return obj.toSharedInfo.sharedIdx})
            }
            try {
                this.props.openPopup('CONFIRM', data, async(value) => {
                    if (value) {
                      try {
                        await ProfileActions.postToSharedTimelinesHidden(req)
                        const {hiddenResult} = this.props
                        if(hiddenResult.header.resultCode===0){
                          this.fetchData({pageNum:1},false)
                          global.alert(T.translate('popup.deleted'));
                        }
                      } catch (error) {
                        
                      }
                    }
                });
            } catch (error) {
                console.log(error)
            }
          }
        }
      ]
    }
    return (
      <div className="right">
        <div className="tab-content-two">
          <div className="tab-content-inner paddingLeft30 paddingRight30">
            <div className="title-inner-top clearfix">
              <h3 className="title">{T.translate('play_menu.receive')}({items.timelinesCount})</h3>
              <TimelineButtonForm buttonData={buttonData} selectedItems={this.props.selectedItems} updateCallback={this.updateCallback} disabled={items.timelines.length===0?true:false}></TimelineButtonForm>
            </div>
            {items.timelines.length==0 ?
                <div style={{height:'80vh',position:'relative'}}>
                    <NotFound></NotFound>
                </div>
                :
                <Fragment>
                  <div className="wrap-one-three">
                      <TimelinesWrapper data={items.timelines} CardView={ReceivedCardView} selectedItems={this.props.selectedItems} selectedCallback={this.updateCallback}></TimelinesWrapper>
                  </div>
                  <ListMoreView req={this.props.req} isHideMoreLayer={this.props.isHideMoreLayer} callBack={this.fetchData}></ListMoreView>
                </Fragment>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    items: state.profile.get("receivedResult"),
    selectedItems : state.profile.get("selectedItems"),
    req: state.profile.get("req"),
    attachMode: state.profile.get("attachMode"),
    isHideMoreLayer : state.profile.get("isHideMoreLayer"),
    hiddenResult : state.profile.get("hiddenResult")
  }),
  dispatch => ({
    ProfileActions: bindActionCreators(profileActions, dispatch),
    openPopup: bindActionCreators(openPopup, dispatch)
  })
)(withRouter(Received));
