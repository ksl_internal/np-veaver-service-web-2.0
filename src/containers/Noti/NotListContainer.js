import React, { Component ,Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import T from 'i18n-react';
import List from '../../components/Noti/List';
import "./list.css";

import * as notiActions from '../../modules/noti';
import  {NotFound}  from '../../components/NewCardView'
import {makebackPreventStateInit,makebackPreventStateAnotherContainer} from "../../modules/app";
/**
 * 알림 메인 페이지 입니다.
 */
class NotListContainer extends Component {
    componentWillMount() {
        if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
            this.props.makebackPreventStateAnotherContainer()
            window.history.back()
        }
    }

    componentDidMount() {
        if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
            this.props.makebackPreventStateInit()
        }
    }

  componentDidUpdate(prevProps, prevState) {
    document.documentElement.scrollTop = 0;

  }
  handlePageChange = (pageNumber) =>{
  }
  /**
   * 해당 로우 별 클릭 이벤트 콜백 함수입니다.
   * 수락, 거절에 관련 한 이벤트 입니다.
   */
  callBack= async(item,type)=>{
    const {NotiActions,match,items} = this.props
    let req ={
      pushHistoryIdx : item.idx   //알림 idx
    }
    try {
      if(item.data=='null') return false;
      await NotiActions.putConfirmNotification({idx:item.idx})
      const itemData = JSON.parse(item.data)  // 해당 api문서상  문자열 로 넘어옵니다 객체로 변환 하여 사용합니다.
      if(itemData.hasOwnProperty('timelineSendIdx')){
        req.timelineSendIdx = itemData.timelineSendIdx    //비디오 전달 idx(비디오 idx 가 아님)
        req.timelineSendStatus = type   //비디오 전달 상태값(Y : 수락 , N : 거절)
        await NotiActions.putVideoSendStatus(req)
        global.alert((type==='Y'?'수락':'거절')+' 되었습니다.');
      }else if (itemData.hasOwnProperty('folderIdx')){
        req.folderIdx = itemData.folderIdx    //폴더 IDX
        req.inviteGroupFolderIdx =itemData.inviteGroupFolderIdx     //그룹 폴더 초대 IDX
        req.approveFlag = type   //초대 수락 여부 (Y : 수락 , N: 거절)
        await NotiActions.putFolderRespond(req)
        global.alert((type==='Y'?'수락':'거절')+' 되었습니다.');
      }
      await NotiActions.getNotifications({notiType:match.params.type.toUpperCase(),limit:100});
    } catch (error) {
      
    }
  }
  /**
   * 알림 리스트 클릭할경우 페이지 이동 함수 입니다.
   */
  movePage=async(item)=>{
    const { NotiActions ,match ,history} = this.props
    if(item.confirmFlag==='N'){
      await NotiActions.putConfirmNotification({idx:item.idx})
    }
    try{
      if(item.data=='null') {
        await NotiActions.getNotifications({notiType:match.params.type.toUpperCase(),limit:100});
      }else{
        const itemData = JSON.parse(item.data)
        if(itemData.hasOwnProperty('timelineIdx') || itemData.hasOwnProperty('timelineSendIdx')){
          history.push(`/play/${itemData.hasOwnProperty('timelineIdx') ? itemData.timelineIdx : itemData.timelineSendIdx}`)
        }else if(itemData.hasOwnProperty('userKey')){
          history.push(`/profile/${itemData.userKey}`)
        }else if (itemData.hasOwnProperty('folderIdx')){
          history.push(`/folder`)
        }
      }
    }catch (error){
      console.log('move page error ',error)
    }
  }
  render() {
    const { location , match ,loading, data, page , items} = this.props;
    if(loading ===undefined || loading || items ===undefined) return null;
    const noti = items[match.params.type.toUpperCase()]
    return (
      <div>
        <div className="inquiry-board-wapper">
          <div className="board-content">
            <div className="board-title">{(match.params.type==='i'? T.translate('noti.important'):T.translate('noti.normal'))} {T.translate('make.notification')}</div>
            <Fragment>
                {
                  noti.count === 0 
                  ? (
                    <div style={{height:'80vh',position:'relative'}}>
                            <NotFound></NotFound>
                        </div>
                  )
                  : (<List items={noti.notifications} title={(match.params.type==='i'? T.translate('noti.important'):T.translate('noti.normal'))} callBack={this.callBack} movePage={this.movePage} />)
                }
                <div className="board-submit-div text-center">
                  <div style={{marginBottom:'36px'}}>
                  </div>
                </div>
            </Fragment>
          </div>
        </div>
      </div>
    );
  }
}


export default connect(
  (state) => ({
    items: state.noti.get('notificationsResult'),
    loading: state.pender.pending['noti/GET_NOTIFICATIONS'],
    makebackPreventState:state.app.makebackPreventState,
  }),
  (dispatch) => ({
    NotiActions: bindActionCreators(notiActions, dispatch),
      makebackPreventStateAnotherContainer: bindActionCreators(makebackPreventStateAnotherContainer, dispatch),
      makebackPreventStateInit: bindActionCreators(makebackPreventStateInit, dispatch)
  })
)(NotListContainer);
