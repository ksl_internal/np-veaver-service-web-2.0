import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import T from "i18n-react";
import * as profileActions from "../../modules/profile";
import { openPopup } from "../../modules/popup";
class PasswordPage extends Component {
    state = {
        error:{
            code:false,
            msg:""
        },
        currentpassword:'',
        newpassword:'',
        confirmpassword:'',
        loading:false
    }
    componentDidMount(){
        this.fetchInitialData()
    }
    /**
     * 초기 데이터를 세팅합니다. 
     */
    fetchInitialData = async () =>{
        const { userProfile, ProfileActions } = this.props;
        await ProfileActions.getUsers({id: userProfile.userKey});
        await this.props.ProfileActions.getUsersTimelines({id: userProfile.userKey},false);
        await this.props.ProfileActions.getUsersIdIndices({id: userProfile.userKey});
    }
    checkValid=()=>{
        this.setState({
            error: {
              code:false,
              msg:''
            }
          });
        const passwordRegex = /^(?=.*[a-zA-Z])(?=.*[$@$!%*?&\d])[A-Za-z\d$@$!%*?&]{6,20}/;
        if (!passwordRegex.test(this.state.currentpassword) || !passwordRegex.test(this.state.newpassword)) {
            this.setState(state => {
                return {
                  ...state,
                  error: {
                    code:true,
                    msg:T.translate('profile.pw-validation')
                  }
                };
              });
            return false; // false
        }
        if ((this.state.newpassword !== this.state.confirmpassword) ) {
            this.setState(state => {
                return {
                  ...state,
                  error: {
                    code:true,
                    msg:'비밀번호를 동일하게 입력해주세요.'
                  }
                };
              });
            return false; // false
        }
        return true;
    }
    handleOnChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        })
    }
    handleOnSubmit=async()=>{
        if(!this.checkValid() || this.state.loading){
            return false;
        }
        const req = {
            email:this.props.userProfile.email,
            password:this.state.currentpassword,
            newPassword:this.state.newpassword
        }
        const {ProfileActions} = this.props
        this.setState({
            loading:true
        })
        try{

            await ProfileActions.postChangePassword(req)
            const { changeResult } = this.props
            this.setState({
                loading:false
            })
            if(changeResult.header.resultCode===0){
                this.setState(state => {
                    return {
                      ...state,
                      currentpassword:'',
                        newpassword:'',
                        confirmpassword:'',
                    };
                  });
                  global.alert('비밀번호가 변경되었습니다.');  
                // const data = {
                //     title: '비밀번호 변경',
                //     description: '비밀번호가 변경되었습니다.',
                //   };
                // this.props.openPopup('BASIC', data, (value) => {});
            }
        }catch(e){
            const { changeResult } = this.props
            this.setState(state => {
                return {
                  ...state,
                  loading:false,
                  error: {
                    code:true,
                    msg:changeResult.errorData
                  }
                };
              });
        }
    }
    render() {
      const {userProfile} = this.props
      return (
        <div className="right">
            <div className="tab-content-two">
                <div className="tab-content-inner paddingLeft30 paddingRight30">
                    <div className="title-inner-top clearfix">
                        <h3 className="title">{T.translate('profile.change-pw')}</h3>
                        <div className="control-btn">
                        </div>
                    </div>

                    <div className="wrapFormChangePassword">
                        <div className="wrap-input">
                            <div className="row-input">
                                <div className="row-left">{T.translate('profile.pw-name')}</div>
                                <div className="row-right">{userProfile.nickname}</div>
                            </div>
                            <div className="row-input currentPassword">
                                <div className="row-left">{T.translate('profile.current-pw')}</div>
                                <div className="row-right"><input type="password" name="currentpassword" value={this.state.currentpassword} onChange={(e)=>this.handleOnChange(e)}/></div>
                            </div>
                            <div className="row-input">
                                <div className="row-left">{T.translate('profile.new-pw')}</div>
                                <div className="row-right"><input type="password" name="newpassword" value={this.state.newpassword} onChange={(e)=>this.handleOnChange(e)} /></div>
                            </div>
                            <div className="row-input">
                                <div className="row-left">{T.translate('profile.confirm-pw')}</div>
                                <div className="row-right"><input type="password" style={{borderTop:'none'}} name="confirmpassword" value={this.state.confirmpassword} onChange={(e)=>this.handleOnChange(e)}/></div>
                            </div>
                            {this.state.error.code == true ? (
                                <div className="row-input">
                                    <div className="row-left"></div>
                                    <div className="row-right"><p className="note" style={{color:'red'}}>{this.state.error.msg}</p></div>
                                </div>
                            )
                            :
                                <div className="row-input">
                                    <div className="row-left"></div>
                                    <div className="row-right"><p className="note">{T.translate('profile.pw-validation')}</p></div>
                                </div>
                            }
                        </div>
                        
                        <div className="wrap-btn-submit">
                            <button type="button" onClick={()=>this.handleOnSubmit()}>{T.translate('profile.change-pw')}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      );
    }
  }

  export default connect(
    (state) => ({
       userProfile : state.user.userProfile,
       changeResult : state.profile.get('changeResult'),
       userResult : state.profile.get('userResult'),
       userTimelinesResult : state.profile.get('userTimelinesResult'),
       usersIndicesResult : state.profile.get('usersIndicesResult'),
    }),
    (dispatch) => ({
        ProfileActions : bindActionCreators(profileActions,dispatch),
        openPopup: bindActionCreators(openPopup, dispatch),
    })
  )(withRouter(PasswordPage));
