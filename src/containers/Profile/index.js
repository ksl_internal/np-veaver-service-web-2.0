import React, { Component , Fragment} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import TimelineButtonForm from "../../components/TimelineButtonForm"
import { openPopup } from "../../modules/popup";
import  {NotFound , TimelinesWrapper , ProfileCardView , ListMoreView}  from '../../components/NewCardView'

import * as profileActions from "../../modules/profile";
import * as myPageActionCreators from "../../modules/mypage";
import {makebackPreventStateInit,makebackPreventStateAnotherContainer} from "../../modules/app";

import T from "i18n-react";

/**
 * 마이페이지 메인 페이지 입니다
 */
class ProfilePage extends Component {
    state = {
        user:null,
        timelines:[],
        timelinesCount:0
    }

    componentWillMount() {
        console.log('[ProfilePage]componentWillMount ',window.history.length)
        if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
            this.props.makebackPreventStateAnotherContainer()
            window.history.back()
        }
    }

    componentDidMount(){
        if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
            this.props.makebackPreventStateInit()
            this.fetchInitialData()
        }
        window.scrollTo(0, 0);
    }
    /**
     * 초기 데이터를 세팅합니다. 
     */
    fetchInitialData = async () =>{
        const { match, userProfile, ProfileActions } = this.props;
        await ProfileActions.getUsers({id: (match.params.id && match.params.id !== userProfile.userKey) ? match.params.id : userProfile.userKey});
        const { userResult } = this.props;

       // await ProfileActions.getUsersTimelines({id: (match.params.id && match.params.id !== userProfile.userKey) ? match.params.id : userProfile.userKey}); 

        if(match.params.id && match.params.id !== userProfile.userKey)
           await ProfileActions.getUsersTimelines({id: (match.params.id && match.params.id !== userProfile.userKey) ? match.params.id : userProfile.userKey}); 
        else
           await this.props.ProfileActions.getMyTimelines({id: (match.params.id && match.params.id !== userProfile.userKey) ? match.params.id : userProfile.userKey});  

        await this.props.ProfileActions.getUsersIdIndices({id: userResult.user.userKey});
    }
    /**
     * 더 보기 누를 경우 추가 데이터를 가져옵니다.
     */
    fetchData = async (query,attachMode) =>{
        const { match, userProfile, userResult } = this.props;

        // await this.props.ProfileActions.getMyTimelines(query,true);

        if(match.params.id && match.params.id !== userProfile.userKey)
           await this.props.ProfileActions.getUsersTimelines(query,true); 
        else
           await this.props.ProfileActions.getMyTimelines(query,true);  
    }
    /**
     * 우측 상단 액션에 대한 현재 리스트 선택 여부를 변경합니다.
     */
    updateCallback =(selectedItems) =>{
        const { ProfileActions } = this.props;
        ProfileActions.updateSelectedItems(selectedItems)
    }
    render() {
      if(this.props.userTimelinesResult===undefined || this.props.userTimelinesResult===null){
        return null
      }
      const { ProfileActions ,userResult, match, userProfile} = this.props;

      // 우측 상단 추가 메뉴 객체 입니다.
      const buttonData ={
        default:{
          openButton:T.translate('common.select'),
          closeButton:T.translate('common.cancel')
        },
        pageButton:[
          {
            name:T.translate('common.share'),
            className:'btn',
            fucOnClick:(e,selectedIdx)=>{ //공유하기 콜백 함수입니다.
              this.props.openPopup('CHOOSEUSERS',{
                timelineIdxs:selectedIdx.idx,
                returnType:'share'
              })
            }
          },
          {
            name:T.translate('common.add-to-folder'),
            className:'btn add-to-folder',
            fucOnClick:(e,selectedIdx)=>{ //폴더 콜백 함수입니다.
              this.props.openPopup('FOLDER',{
                timelineIdxList:selectedIdx.idx,
                folderIdx : null,
                message : null,
                type:'add'
              })
            }
          },
          {
            name:T.translate('common.delete'),
            className:'btn',
            fucOnClick:async(e,selectedIdx)=>{ //삭제 콜백 함수입니다.
              try {
                this.props.MyPageActionCreators.deleteTimeline(selectedIdx.idx,null,true)
              } catch (error) {
                  console.log(error)
              }
            }
          }
        ]
      }
      return (
        <div className="right">
            <div className="tab-content-two">
                <div className="tab-content-inner paddingLeft30 paddingRight30">
                    <div className="title-inner-top clearfix">
                        <h3 className="title">{T.translate('common.knowledge')}({this.props.userTimelinesResult.timelinesCount})</h3>
                        {(!match.params.id || (match.params.id && match.params.id == userProfile.userKey) ) ? 
                                  <TimelineButtonForm buttonData={buttonData} selectedItems={this.props.selectedItems} updateCallback={this.updateCallback} disabled={this.props.userTimelinesResult.timelines.length===0?true:false}></TimelineButtonForm>
                                  : ''
                        }                        
                    </div>
                    {this.props.userTimelinesResult.timelines.length===0 ?
                        <div style={{height:'80vh',position:'relative'}}>
                            <NotFound></NotFound>
                        </div>
                        :
                        <Fragment>
                            <div className="wrap-one-three">
                                <TimelinesWrapper data={this.props.userTimelinesResult.timelines} CardView={ProfileCardView} selectedItems={this.props.selectedItems} selectedCallback={this.updateCallback}></TimelinesWrapper>
                            </div>
                            <ListMoreView req={this.props.req} isHideMoreLayer={this.props.isHideMoreLayer} callBack={this.fetchData}></ListMoreView>
                        </Fragment>
                    }
                </div>
            </div>
        </div>
      );
    }
  }

  export default connect(
    (state) => ({
        ...state,
        userProfile: state.user.userProfile,
        selectedItems : state.profile.get("selectedItems"),
        userResult : state.profile.get('userResult'),
        userTimelinesResult : state.profile.get('userTimelinesResult'),
        usersIndicesResult : state.profile.get('usersIndicesResult'),
        veaverStackRankingsResult:state.profile.get('veaverStackRankingsResult'),
        veaverRankingsResult:state.profile.get('veaverRankingsResult'),
        notificationsResult : state.noti.get('notificationsResult'),
        req: state.profile.get("req"),
        attachMode: state.profile.get("attachMode"),
        isHideMoreLayer : state.profile.get("isHideMoreLayer"),
        makebackPreventState:state.app.makebackPreventState,
    }),
    (dispatch) => ({
        ProfileActions : bindActionCreators(profileActions,dispatch),
        MyPageActionCreators : bindActionCreators(myPageActionCreators,dispatch),
        openPopup: bindActionCreators(openPopup, dispatch),
        makebackPreventStateAnotherContainer: bindActionCreators(makebackPreventStateAnotherContainer, dispatch),
        makebackPreventStateInit: bindActionCreators(makebackPreventStateInit, dispatch)
    })
  )(withRouter(ProfilePage));
