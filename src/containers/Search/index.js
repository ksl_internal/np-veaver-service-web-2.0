import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import T from 'i18n-react';
import  {NotFound , TimelinesWrapper , SearchTimelinesCardView ,SectionCardView,UserCardView,ListMoreView,StackCardView}  from '../../components/NewCardView'
import queryString from 'stringquery'
import * as searchActions from "../../modules/searchM";
import {makebackPreventStateInit,makebackPreventStateAnotherContainer} from "../../modules/app";

/**
 * 검색 메인 페이지 입니다
 */
class SearchTimelinesContainer extends Component {
    state = {
    }

    componentWillMount() {
        if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
            this.props.makebackPreventStateAnotherContainer()
            window.history.back()
        }
    }

    componentDidMount(){
        if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
            this.props.makebackPreventStateInit()
            const {location } = this.props
            //url에서 직접 접근을 위해 검색시 url에서 검색 키워드를 가져옵니다.
            const values = queryString(location.search)
            this.fetchData({
                keyword: values.keyword,
                orderType: "R",
                pageNum : 0,
                first:true
            },false);

        }
    }
    /**
     * 검색 페이지 타임라인 데이터를 가져옵니다.
     */
    fetchData = async (query,attachMode) =>{
        try {
            const { items } = this.props
            //최초 검색시 타임라인을 검색합니다. 그 외에 LNB 클릭시 해당 데이터를 가져옵니다. items 에는 type 별로 다른 데이터들이 들어있습니다.
            if(query.first){
                delete query.first
                await this.props.SearchActions.getSearchTimelines(query,attachMode);
            }else{
                if(items.type==='users'){
                    await this.props.SearchActions.getSearchUsers(query,attachMode);
                }else if(items.type==='section'){
                    await this.props.SearchActions.getSearchTimelinesSection(query,attachMode);
                }else if(items.type==='stack'){
                    await this.props.SearchActions.getSearchTimelinesStack(query,attachMode);
                }else if(items.type==='tag'){
                    await this.props.SearchActions.getSearchTimelinesTag(query,attachMode);
                }else if(items.type==='timeline'){
                    await this.props.SearchActions.getSearchTimelines(query,attachMode);
                }
            }
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        const { items } = this.props
        if(items === null && items.search.length > 0) return null

        /**
         * 리스트 별로 카드 뷰를 선택합니다.
         * @param {*} items 서버에서 넘겨주는 타임라인 밎 리스트 데이터 입니다.
         */
        function getListView(items){
            let cardView;
            if(items.type==='timeline'){
                cardView = SearchTimelinesCardView
            }else if(items.type==='section'){
                cardView = SectionCardView
            }else if(items.type==='stack'){
                cardView = StackCardView
            }else if(items.type==='tag'){
                cardView = SectionCardView
            }else if(items.type==='users'){
                cardView = UserCardView
            }
            return (
                <div className="wrap-one-three">
                    <TimelinesWrapper data={items.search} CardView={cardView}></TimelinesWrapper>
                </div>
            )
        }
      return (
        <div className="right">
            <div className="tab-content-two">
                <div className="tab-content-inner paddingLeft30 paddingRight30">
                    <div className="title-inner-top clearfix">
                        <h3 className="title">{T.translate('search_result.search-result')}({items.count})</h3>
                    </div>
                    {items.search.length=== 0 ?
                        <div style={{height:'80vh',position:'relative'}}>
                            <NotFound></NotFound>
                        </div>
                        :
                        getListView(items)

                    }
                    {/* 리스트 더 가져오기 뷰입니다. */}
                    <ListMoreView req={this.props.req} isHideMoreLayer={this.props.isHideMoreLayer} callBack={this.fetchData}></ListMoreView>
                </div>
            </div>
        </div>
      );
    }
  }

  export default connect(
    (state) => ({
        ...state,
        items: state.searchM.get("items"),
        meta: state.searchM.get("meta"),
        req: state.searchM.get("req"),
        attachMode: state.searchM.get("attachMode"),
        isHideMoreLayer : state.searchM.get("isHideMoreLayer"),
        makebackPreventState:state.app.makebackPreventState,
    }),
    (dispatch) => ({
        SearchActions : bindActionCreators(searchActions,dispatch),
        makebackPreventStateAnotherContainer: bindActionCreators(makebackPreventStateAnotherContainer, dispatch),
        makebackPreventStateInit:bindActionCreators(makebackPreventStateInit,dispatch)
    })
  )(withRouter(SearchTimelinesContainer));
