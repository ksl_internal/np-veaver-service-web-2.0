import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Player from '../../components/Player';
import PlayInfoTab from '../../components/TabUI/PlayInfoTab';
import Loading from '../../components/LoadingForm';
import * as playerActions from '../../modules/player';
import { withRouter } from "react-router-dom";
import {makebackPreventStateInit,makebackPreventStateAnotherContainer} from "../../modules/app";
import querySearch from 'stringquery';

class Play extends Component {

    state = {
        isSettingLayerCloseFlag : false,
        setPossibleClick : false,
        begin: 0,
    }
    componentWillMount() {
        if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
            this.props.makebackPreventStateAnotherContainer()
            window.history.back()
        }
    }

  componentDidMount() {

      if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
          this.props.makebackPreventStateInit()
        const { match } = this.props;
        const { timelineIdx } = match.params;
        const obj = querySearch(this.props.location.search);  
        console.log(obj.begin  + ' -in Play ' + Number.isInteger(parseInt(obj.begin)));        
        if( Number.isInteger(parseInt(obj.begin)) ){
          this.setState({begin: obj.begin})
        }
        this.props.getDetails(timelineIdx);

    }
  }

  componentWillUnmount() {
    this.props.clearDetails();
  }

  _changeActiveInfoTab = ({ target }) => {
    this.props.changeActiveInfoTab(target.value);
  }

  get isNotEmpty() {
    const { videoDetail, timelineDetail } = this.props;
    const hasProperties = (obj) => Object.keys(obj).length !== 0;
    return [
      hasProperties(videoDetail),
      hasProperties(timelineDetail),
    ].every(condition => condition);
  }

  // this.props.onAddVoteExample(false); 이건뭐지???
  get playerRender() {
    const { videoDetail, timelineDetail, categories, activeTab } = this.props;
    return this.isNotEmpty ? (<div className="container">
        <div className="play-wrapper" onClick = {() => { this.state.setPossibleClick ? this._changeCloseSettingLayerFlag(true) : ''}}>
        <Loading isLoadingStoped={true}/>
        <Player videoData={videoDetail}
                timelineData={timelineDetail}
                categories={categories}
                isSettingLayerCloseFlag = {this.state.isSettingLayerCloseFlag}
                setPossibleClick = {this.state.setPossibleClick}
                closeSet= {this._changeCloseSettingLayerFlag}
                setPossibleClickFunc={this._setPossibleClickFunc}
                beginFrom = {this.state.begin != 0 ? this.state.begin : null}/>
        <PlayInfoTab timelineData={timelineDetail} activeTab={activeTab} changeActiveInfoTab={this._changeActiveInfoTab}   />
    </div></div>) : '';
  }


    _changeCloseSettingLayerFlag = (value) => {
        this.setState((state) => {
            return {
                ...state,
                isSettingLayerCloseFlag: value,
            }
        })

    }

    _setPossibleClickFunc = (value) => {
        this.setState((state) => {
            return {
                ...state,
                setPossibleClick: value,
            }
        })

    }

  render() {
    return this.playerRender
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  addVoteEvent: state.player.addVoteEvent,
  videoDetail: state.player.videoDetail,
  timelineDetail: state.player.timelineDetail,
  categories: state.player.viewVideoCategoryNames,
  activeTab: state.player.activeTab,
  makebackPreventState:state.app.makebackPreventState,
  closeSettingLayer : state.closeSettingLayer,
});

const mapDispatchToProps = dispatch => bindActionCreators({
 ...playerActions,
    makebackPreventStateAnotherContainer,
    makebackPreventStateInit,
}, dispatch);

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Play));