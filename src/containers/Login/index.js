import 'core-js/es6/map';
import 'core-js/es6/set';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoginForm from '../../components/LoginForm';
import {
  login,
} from '../../modules/auth';
import { openPopup } from "../../modules/popup";
import {getNotices} from "../../modules/noti";

class Login extends Component {
  _getNotices=async(req)=>{
    const {NotiActions} = this.props;
    return await this.props.getNotices(req);
  }
  showNotice=async ()=>{
    var response = await this.props.getNotices({pageSize:10,pageNum:1,getNotices:this._getNotices});
    if(response.data.noticesCount === 0){
        window.location.reload(true);
    }
    this.props.openPopup('NOTICE',null,(value)=>{
      if(value){
        window.location.reload(true)
      }
    });
  }
	render() {
    const {isAuthenticated} =this.props
    if(isAuthenticated){
      this.showNotice()
      //window.location.reload(true)
    }
		return (<LoginForm onSubmit={this.props.login}/>);
	}
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  login,
  openPopup,
  getNotices
}, dispatch);

export default connect(
  mapStateToProps,
	mapDispatchToProps,
)(Login);

