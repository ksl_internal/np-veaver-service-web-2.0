import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Image, Quiz, Vote, Report, LinkCopy, Map, Answer, Link, Url, ServicePolicy, PrivacyPolicy, Basic ,Like, InsertPopup, ChooseUsers , Folder,
    LevelsPoints,LevelsAccordingPoints , Ranking , UserBookMark, Cert,Notice
} from '../../components/PopupForm';
import {
  openPopup,
	closePopup,
} from '../../modules/popup';

class Popup extends Component {

	constructor(props) {
		super(props);
		this._close = this._close.bind(this);
	}

	_close(arg) {
        if (this.props.onClose) {
            this.props.onClose(arg)
        }
		this.props.closePopup();
	}

	renderPopup() {
		// console.log('popup:: ', this.props);
		switch (this.props.popupType) {
            case 'IMAGE':
                return <Image data={this.props.popupData} onClose={this._close} />;
            case 'QUIZ':
                return <Quiz data={this.props.popupData} onClose={this._close} />;
            case 'VOTE':
                return <Vote data={this.props.popupData} onClose={this._close} />;
            case 'REPORT':
                return <Report data={this.props.popupData} onClose={this._close} />;
            case 'LINK_COPY':
                return <LinkCopy data={this.props.popupData} onClose={this._close} />;
            case 'LINK':
                return <Link data={this.props.popupData} onClose={this._close} {...this.props.children} />;
            case 'ANSWER':
                return <Answer data={this.props.popupData} onClose={this._close} />;
            case 'BASIC':
                return <Basic data={this.props.popupData} onClose={this._close} />;
            case 'MAP':
                return <Map onClose={this._close} />;
            case 'URL':
                return <Url onClose={url => this._close(url)} popupTitle="URL 입력" />;
            case 'SERVICEPOLICY':
                return <ServicePolicy onClose={this._close} />;
            case 'PRIVACYPOLICY':
                return <PrivacyPolicy onClose={this._close} />;
            case 'CONFIRM':
                return <Basic data={this.props.popupData} onClose={this._close} />;
            case 'LIKE':
                return <Like data={this.props.popupData} onClose={this._close}/>;
            case 'INSERTPOPUP':
                return <InsertPopup data={this.props.popupData} onClose={this._close} />;    
            case 'CHOOSEUSERS':
                return <ChooseUsers data={this.props.popupData} onClose={this._close} {...this.props.children}/>;  
            case 'FOLDER':
                return <Folder data={this.props.popupData} onClose={this._close} {...this.props.children}/>;  
            case 'LEVELSPOINTS':
                return <LevelsPoints data={this.props.popupData} onClose={this._close} {...this.props.children}/>;    
            case 'LEVELSACCORDINGPOINTS':
                return <LevelsAccordingPoints data={this.props.popupData} onClose={this._close} {...this.props.children}/>; 
            case 'RANKING':
                return <Ranking data={this.props.popupData} onClose={this._close} {...this.props.children}/>;   
            case 'USERBOOKMARK':
                return <UserBookMark data={this.props.popupData} onClose={this._close} {...this.props.children}/>;     
            case 'CERT':
                return <Cert data={this.props.popupData} onClose={this._close} {...this.props.children}/>;      
            case 'NOTICE':
                return <Notice data={this.props.popupData} onClose={this._close}/>;      
                 
			default: return null;
		}
	}

	render() {
		return (
			<div>
				{this.renderPopup()}
			</div>
		);
	}
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
	popupType: state.popup.type,
	popupStatus: state.popup.status,
	popupData: state.popup.data,
  onClose: state.popup.onClose,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  openPopup,
	closePopup,
}, dispatch);

export default connect(
  mapStateToProps,
	mapDispatchToProps,
)(Popup);
