import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from 'redux';
import pt from "prop-types";
import cx from "classnames";
import MediaRegister from '../../components/MakeForm/MediaRegister';
import MetadataRegister from '../../components/MakeForm/MetadataRegister';
import TimelineManager from '../../components/MakeForm/TimelineManager';
import TimelineRegister from '../../components/MakeForm/TimelineRegister';
import StepController from '../../components/MakeForm/StepController';
import Loading from '../../components/LoadingForm';
import {makebackPreventStateInit,
    makebackPreventStateMakeContainer,
    makebackPreventStateShowpopup,
    makebackPreventStateMakestep2Container,
    makebackPreventStateAnotherContainer} from "../../modules/app";
    import {openPopup} from "../../modules/popup";
import {initVideoInfo} from "../../modules/make";
import {navbarClose} from "../../modules/navbar";
import T from "i18n-react";

class Make extends React.PureComponent {
  static propTypes = {
    step: pt.number.isRequired,
  }

  static defaultProps = {
    step: 1,
  }
  
  constructor(props) {
      super(props)
  }


 componentWillMount() {
     if (this.props.makebackPreventState === 'modify') {
         this.props.makebackPreventStateAnotherContainer()
         window.history.back()
     }
  }

  componentDidMount() {
      const { step } = this.props;

      if(this.props.makebackPreventState === 'makepush') {
          this.props.makebackPreventStateShowpopup()
      } else {
          if (this.props.makebackPreventState === 'initial') {
              this.props.makebackPreventStateMakeContainer()
          } else if (this.props.makebackPreventState === 'make') {
          } else if (this.props.makebackPreventState === 'makestep2') {
              window.history.pushState(null, null, window.location.href);
              this.props.makebackPreventStateShowpopup()
          } else if (this.props.makebackPreventState === 'showpopup') {
              window.history.pushState(null, null, window.location.href);
          } else if (this.props.makebackPreventState === 'another') {
              this.props.makebackPreventStateShowpopup()
          }
      }
      window.scrollTo(0, 0);
  }
  
  get currentStep() {
    const { step } = this.props;
    if(step !==1){
      this.props.navbarClose()
    }
    switch (step) {
      case 1: return <MediaRegister />;
      case 2: return <MetadataRegister />;
      case 2.1: case 2.2: case 2.3: return <TimelineManager />;
      case 3: return <TimelineRegister />;
      default: return <Loading isLoadingStoped={true} />;
    }
  }

    showUploadPopup() {
        const data = {
            title: T.translate("make.skip-make-title"),
            description: T.translate("make.skip-make-description")
        };
        this.props.openPopup('CONFIRM', data, (value) => {
            if (value) {
                this.props.initVideoInfo();
                this.props.makebackPreventStateInit()

                window.history.forward()
                // this.props.makebackPreventState === 'makepush' ? window.history.back() : window.history.forward()
            } else {
                this.props.makebackPreventStateMakestep2Container()
            }
        })
    }

  render() {
    const { step } = this.props;
    if(step !== 1){

    }
    return (
      <div className="make-wrapper">
        <ul className="steps">
          <li className={cx({ active: step === 1 })}>
            <p className="step">STEP 1</p>
            <p className="make-category">{T.translate('make.select-video')}</p>
          </li>
          <li className={cx({ active: [2, 2.1, 2.2, 2.3].some(condition => condition === step) })}>
            <p className="step">STEP 2</p>
            <p className="make-category">{T.translate('make.input-info')}</p>
          </li>
          <li className={cx({ active: step === 3 })}>
            <p className="step">STEP 3</p>
            <p className="make-category">{T.translate('make.save-knowledge')}</p>
          </li>
        </ul>
        {this.currentStep}
        <StepController />
      {(this.props.makebackPreventState === 'showpopup' || this.props.makebackPreventState === 'makepush')
          ? this.showUploadPopup() : ''
      }

  </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    step: state.make.step,
    makebackPreventState:state.app.makebackPreventState,
  };
};
const mapDispatchToProps = dispatch => bindActionCreators({
  navbarClose,
  makebackPreventStateInit,
              makebackPreventStateMakeContainer,
            makebackPreventStateShowpopup,
            makebackPreventStateMakestep2Container,
            makebackPreventStateAnotherContainer,
            openPopup,
            initVideoInfo
}, dispatch);

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Make));
