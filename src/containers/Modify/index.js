import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {getModifyInfo, initVideoInfo} from "../../modules/make";
import pt from "prop-types";
import cx from "classnames";
import MetadataRegister from '../../components/MakeForm/MetadataRegister';
import TimelineManager from '../../components/MakeForm/TimelineManager';
import TimelineRegister from '../../components/MakeForm/TimelineRegister';
import StepController from '../../components/MakeForm/StepController';
import Loading from '../../components/LoadingForm';
import T from "i18n-react";
import {makebackPreventStateInit,
    makebackPreventStateMakeContainer,
    makebackPreventStateShowpopup,
    makebackPreventStateMakestep2Container,
    makebackPreventStateAnotherContainer,
    makebackPreventStateModifyContainer} from "../../modules/app";
import {openPopup} from "../../modules/popup";

class Modify extends React.PureComponent {
  static propTypes = {
    step: pt.number.isRequired,
  }

  static defaultProps = {
    step: 2,
  }

  state = {
    isLocker: false,
      backState : 'makestep2'
  }

  componentDidMount() {
    const { timelineIdx } = this.props.match.params;
    const { pathname } = this.props.history.location;
    const isLocker = pathname.includes('locker');
    this.setState(({ isLocker }));
    this.props.getModifyInfo(timelineIdx, isLocker);

    console.log('modify ',this.props.makebackPreventState , pathname.includes('/locker/modify'))

      if (this.props.makebackPreventState === 'initial') {
          window.history.pushState(null, null, window.location.href);
          this.props.makebackPreventStateModifyContainer();
      }  else if (this.props.makebackPreventState === 'modify') {
          this.setState({
              backState : this.props.makebackPreventState
          })

          window.history.pushState(null, null, window.location.href);
          this.props.makebackPreventStateShowpopup()

      } else if (this.props.makebackPreventState === 'showpopup') {
          window.history.pushState(null, null, window.location.href);
      } else if (this.props.makebackPreventState === 'another') {
          this.setState({
              backState : this.props.makebackPreventState
          })

          this.props.makebackPreventStateShowpopup()
      } else {
            if(pathname.includes('/locker/modify')) {
                window.history.pushState(null, null, window.location.href);
                this.props.makebackPreventStateModifyContainer();
            }
      }

  }

  get currentStep() {
    const { step } = this.props;
    switch (step) {
      case 2: return <MetadataRegister modifyMode={true} />;
      case 2.1: case 2.2: case 2.3: return <TimelineManager />;
      case 3: return <TimelineRegister />;
      default: return <Loading isLoadingStoped={true} />;
    }
  }

    showUploadPopup() {
        const data = {
            title: T.translate("make.skip-make-title"),
            description: T.translate("make.skip-make-description")
        };
        this.props.openPopup('CONFIRM', data, (value) => {
            if (value) {
                this.props.initVideoInfo();

                console.log('popup ',this.props.makebackPreventState)
                this.state.backState === 'another' ? window.history.forward() : window.history.go(-2)

                this.props.makebackPreventStateInit()

                // window.history.forward()

                // this.props.makebackPreventState === 'makepush' ? window.history.back() : window.history.forward()
            } else {
                this.props.makebackPreventStateMakestep2Container()
            }
        })
    }


  render() {
    const { step } = this.props;
    const activateModify = true;
    return (
      <div className="make-wrapper">
        <ul className="steps">
          <li className={cx({ active: [2, 2.1, 2.2, 2.3].some(condition => condition === step) })}>
            <p className="step">STEP 2</p>
            <p className="make-category">{T.translate('make.input-info')}</p>
          </li>
          <li className={cx({ active: step === 3 })}>
            <p className="step">STEP 3</p>
            <p className="make-category">{T.translate('make.save-knowledge')}</p>
          </li>
        </ul>
        {this.currentStep}
        <StepController modify={activateModify}/>

      {(this.props.makebackPreventState === 'showpopup' || this.props.makebackPreventState === 'makepush')
          ? this.showUploadPopup() : ''
      }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    step: state.make.step,
    backState: state.backState,
    makebackPreventState:state.app.makebackPreventState,
  };
};

const mapStateToDispatch = dispatch =>
bindActionCreators(
    { getModifyInfo,

        makebackPreventStateInit,
        makebackPreventStateMakeContainer,
        makebackPreventStateShowpopup,
        makebackPreventStateMakestep2Container,
        makebackPreventStateAnotherContainer,
        makebackPreventStateModifyContainer,
        openPopup,
        initVideoInfo
    }, dispatch);

export default withRouter(connect(mapStateToProps, mapStateToDispatch)(Modify));
