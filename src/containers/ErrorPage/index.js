import React, { Component } from 'react';
import { connect } from 'react-redux';
import ErrorPageForm from '../../components/ErrorPageForm';

class Login extends Component {

  render() {
    return (
      <div>
        <ErrorPageForm />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(
  mapStateToProps,
)(Login);
