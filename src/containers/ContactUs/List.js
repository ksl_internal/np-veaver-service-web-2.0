import React, { Component ,Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import {List,Write,Detail} from '../../components/ContactUs';
import "./index.css";

import * as actions from '../../modules/contact';
import Pagination from "react-js-pagination";
import T from "i18n-react";
import  {NotFound}  from '../../components/NewCardView'
import { withRouter } from "react-router-dom";
import {makebackPreventStateInit,makebackPreventStateAnotherContainer} from "../../modules/app";
/**
 * 문의 하기 리스트 페이지 입니다.
 */
class ContactUsListContainer extends Component {
  state = {
    page : (this.props.page == undefined ? 1 : this.props.page)
  }
  getList = async () => {
    const { Actions} = this.props;
    const req = {pageNum: this.state.page  }
    //this.setState({page : page})
    try {
      const p = Actions.getContactUs(req);
      this.cancelRequest = p.cancel;
      const response = await p;
    } catch (e) {
      console.log(e);
    }
  }
  nextPage = () =>{
    let p = this.state.page;
    this.setState({page:p+1})
  }

    componentWillMount() {
        if (this.props.makebackPreventState === 'makestep2' || this.props.makebackPreventState === 'modify') {
            this.props.makebackPreventStateAnotherContainer()
            window.history.back()
        }
    }

  componentDidMount() {
      if (this.props.makebackPreventState !== 'makestep2' && this.props.makebackPreventState !== 'modify') {
          this.props.makebackPreventStateInit()
          this.getList();
      }
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevState.page !== this.state.page) {
      this.getList();
      document.documentElement.scrollTop = 0;
    }
  }
  handlePageChange = (pageNumber) =>{
    this.setState({page: pageNumber});
  }
  render() {
    const { location , match ,loading, data, page , items ,history , language} = this.props;
    if(loading ==undefined || loading == true || items ==undefined) return null;
    return (
      <div>
        <div className="inquiry-board-wapper">
          <div className="board-content">
            <div className="board-title">{T.translate('service.service-inquiry')}</div>
            <Fragment>
                {
                  items.totalCount === 0 
                  ? (
                    <div style={{height:'50vh',position:'relative'}}>
                            <NotFound></NotFound>
                        </div>
                  )
                  : (<List items={items} page={this.state.page} history={this.props.history} language={language}/>)
                }
                <div className="board-submit-div text-center">
                <div style={{marginBottom:'36px'}}>
                  <Pagination
                    activePage={this.state.page}
                    itemsCountPerPage={10}
                    totalItemsCount={items.totalCount}
                    pageRangeDisplayed={10}
                    onChange={this.handlePageChange}
                    innerClass="pagination justify-content-center"
                    activeClassName="active"
                    activeLinkClassName="active"
                    />
                </div>
                <Link to="/contact/write" className="board-submit-btn" >{T.translate('service.inquiry')}</Link>
                </div>
            </Fragment>
          </div>
        </div>
      </div>
    );
  }
}


export default withRouter(connect(
  (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    items: state.contact.get('items'),
    loading: state.pender.pending['contact/GET_CONTACTUS'],
    error: state.pender.failure['contact/GET_CONTACTUS'],
    language : state.language.language,
    makebackPreventState:state.app.makebackPreventState,
  }),
  (dispatch) => ({
    Actions: bindActionCreators(actions, dispatch),
    makebackPreventStateAnotherContainer: bindActionCreators(makebackPreventStateAnotherContainer, dispatch),
      makebackPreventStateInit:bindActionCreators(makebackPreventStateInit,dispatch)
  })
)(ContactUsListContainer));
