import React, { Component ,Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import {List,Write,Detail} from '../../components/ContactUs';
import "./index.css";
import { withRouter } from 'react-router-dom';
import * as actions from '../../modules/contact';
import T from "i18n-react";
/**
 * 문의 하기 상세 페이지 입니다.
 */
class ContactUsDetailContainer extends Component {
  getDetail = async () => {
    const { Actions , match ,page=1} = this.props;
    if(this.isNumeric(match.params.id)){
        try {
          const p = Actions.getDetail(match.params.id);
          this.cancelRequest = p.cancel;
          const response = await p;
        } catch (e) {
          console.log(e);
        }
    }
  }
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    this.getDetail();
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.match.params.id !== this.props.match.params.id){
        this.getDetail();
        document.documentElement.scrollTop = 0;
    }
  }
  isNumeric = (num, opt)=>{
    // 좌우 trim(공백제거)을 해준다.
    num = String(num).replace(/^\s+|\s+$/g, "");

    if(typeof opt == "undefined" || opt == "1"){
      // 모든 10진수 (부호 선택, 자릿수구분기호 선택, 소수점 선택)
      var regex = /^[+\-]?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+){1}(\.[0-9]+)?$/g;
    }else if(opt == "2"){
      // 부호 미사용, 자릿수구분기호 선택, 소수점 선택
      var regex = /^(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+){1}(\.[0-9]+)?$/g;
    }else if(opt == "3"){
      // 부호 미사용, 자릿수구분기호 미사용, 소수점 선택
      var regex = /^[0-9]+(\.[0-9]+)?$/g;
    }else{
      // only 숫자만(부호 미사용, 자릿수구분기호 미사용, 소수점 미사용)
      var regex = /^[0-9]$/g;
    }

    if( regex.test(num) ){
      num = num.replace(/,/g, "");
      return isNaN(num) ? false : true;
    }else{ return false;  }
  }
  removeContact = async() =>{
    const { Actions , item , history } = this.props;
    if(item.statusFlag=='C'){ // 처리된 답변일 경우 삭제 할수 없음.
      return ;
    }
    try {
      await Actions.deleteContact(item.idx);
      if(this.props.result.resultCode==0 && this.props.result.resultMessage == 'SUCCESS'){
        history.push(`/contact`);
      }else{
        console.log('삭제 실패.')
      }
    } catch (error) {

    }
  }
  /**
   * 서버 리턴 값이 기본 영어로 넘어와서 한글로 변경.
   */
  convertText =(idx)=>{
    let afterText=''
    switch(idx){
      case 1:
        afterText="장애(앱종료/로그인/업로드)"   
         break;
      case 2:
        afterText="버그(서비스 이용 중 오류)"
         break;
      case 3:
        afterText="이용 방법 문의(공유/폴더에 담기 등)"
         break;
      case 4:
        afterText="서비스 개선 의견"
         break; 
      default :
        afterText="기타"   
    }
    return afterText
  }
  render() {
    const { location , match ,loading, data, page , item ,language} = this.props;
    if(loading ==undefined || loading == true || item ==undefined) return null;
    
    if(language === 'ko'){
      item.contactUsTypeInfo = this.convertText(item.contactUsTypeIdx)
    }
    return (
      <div>
        <div className="inquiry-board-wapper">
          <div className="board-content">
            <div className="board-title">{T.translate('service.service-inquiry')}</div>
            <Fragment>
                <Detail item={item} page={page} />
                <div className="board-submit-div text-right">
                <button className="board-submit-btn" onClick={this.removeContact} disabled={item.statusFlag==='C'}>{T.translate('service.cancel')}</button>
                <button onClick={this.props.history.goBack} className="board-submit-btn">{T.translate('service.list')}</button>
                </div>
            </Fragment>
          </div>
        </div>
      </div>
    );
  }
}


export default withRouter(connect(
  (state) => ({
    userProfile: state.user.userProfile,
    result: state.contact.get('result'),
    item: state.contact.get('item'),
    loading: state.pender.pending['contact/GET_CONTACTUS_DETAIL'],
    error: state.pender.failure['contact/GET_CONTACTUS_DETAIL'],
    language : state.language.language
  }),
  (dispatch) => ({
    Actions: bindActionCreators(actions, dispatch),
  })
)(ContactUsDetailContainer));
