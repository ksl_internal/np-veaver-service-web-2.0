import React, { Component ,Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import {List,Write,Detail} from '../../components/ContactUs';
import "./index.css";

import * as actions from '../../modules/contact';
import {openPopup} from '../../modules/popup';
import T from "i18n-react";
import { detect } from 'detect-browser';
/**
 * 문의 하기 글쓰는 페이지 입니다.
 */
class ContactUsWriteContainer extends Component {
  state = {
    check : false,
    categoryIdx:'1', //1 : content / 2 : profile / 3 : etc / 4 : veaver
    subject:'',
    content:'',
    importantFlag:'N' ,
  }
  getCategories = async () => {
    const { Actions} = this.props;
    try {
      const p = Actions.getCategories();
      this.cancelRequest = p.cancel;
      const response = await p;
      console.log(response)
    } catch (e) {
      console.log(e);
    }

  }

  componentDidMount() {
    const { match ,post } = this.props;
    this.getCategories()
    document.documentElement.scrollTop = 0;
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevState.page !== this.state.page) {
      document.documentElement.scrollTop = 0;
    }
  }
  handelChange = (e) =>{
    this.setState({
        [e.target.name]:e.target.value
    })
  }
  handleSubmit = async () => {
    const { Actions, history, location } = this.props;
    const {categoryIdx,subject,content,importantFlag} = this.state;
    const browser = detect();
    const post = {
        categoryIdx, //1 : content / 2 : profile / 3 : etc / 4 : veaver
        subject,
        content,
        importantFlag ,
        appVersion : browser.version,
        deviceOs : browser.os,
        deviceModel : browser.name,
    };
    if(this.state.subject==='' || this.state.content===''){
      const data = {
          title: '서비스 문의하기',
          description: (this.state.subject===''? '제목' :'내용') + '을 입력해주세요.',
        };
      this.props.openPopup('BASIC', data, (value) => {
        //history.push(`/contact`);
      });
      return false;
    }
    for(let i in post){
        if(post[i] == null || post[i] == undefined || post[i] ==''){
            return false;
        }
    }
    try {
      const result = await Actions.postContactUs(post);
      if(result.data.header.resultCode===0){
        const data = {
            title: '서비스 문의하기',
            description: '문의 내용이 접수되었습니다. 확인 후 연락 드리겠습니다.',
          };
        this.props.openPopup('BASIC', data, (value) => {
          history.push(`/contact`);
        });
      }
    } catch (e) {
      console.log(e);
    }
  }
  render() {
    const { location , match ,loading, data,categories,language} = this.props;
    const { handelChange ,state} = this;
    return (
      <div>
        <div className="inquiry-board-wapper">
          <div className="board-content">
            <div className="board-title">{T.translate('service.service-inquiry')}</div>
            <Fragment>
                <Write categories={categories} item = {this.state.postItem} onChange={handelChange} postItem={state} language={language}/>
                <div className="board-submit-div text-center">
                <button className="board-submit-btn" onClick={this.handleSubmit} style={{opacity:(this.state.subject==='' || this.state.content==='')?0.5:1}}>{T.translate('service.apply')}</button>
                </div>
            </Fragment>
          </div>
        </div>
      </div>
    );
  }
}


export default connect(
  (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    categories: state.contact.get('categories'),
    loading: state.pender.pending['contact/POST_CONTACTUS'],
    error: state.pender.failure['contact/POST_CONTACTUS'],
    language:state.language.language
  }),
  (dispatch) => ({
    Actions: bindActionCreators(actions, dispatch),
    openPopup: bindActionCreators(openPopup, dispatch),
  })
)(ContactUsWriteContainer);
