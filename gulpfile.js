const gulp = require('gulp');
const less = require('gulp-less');
const path = require('path');

gulp.task('build', function () {
  return gulp.src('./less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', ['build'], function () {
  gulp.watch('./less/**/*.less', ['build']);
});
